<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<div class="wrapper-page">

            <!-- Incorrect password or email address Div. Show it via AJAX, originally display none -->
          <?=$this->session->flashdata("msg"); ?>
            <!-- Incorrect password div ends -->

            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body"><img src="<?=base_url()?>themes/images/immh-logo2.png">
<title>Setup to iManageMyHotel Frontdesk</title>

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="page-title" style="text-align: center;text-transform: uppercase;font-size: 24px;font-weight: bold;margin-top: 24px;margin-bottom:0px;">Setup</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<form class="form-horizontal m-t-20" id="setup_form" action="<?=base_url()?>Fetch_hotel" method="post">

                    
                     <div class="form-group">
                        <div class="col-xs-12">
                         
                        <input class="form-control input-lg" type="text" id="account_id" name="account_id" placeholder="Account Id" required aria-required="true">
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control input-lg" type="text"  id="email" name="email" placeholder="Email address" required aria-required="true">
                            
                        </div>
                    </div>                  

                    <div class="form-group">
                        <div class="col-xs-12">
                         
                        <input class="form-control input-lg" type="text"  name="frontdesk_code" id="frontdesk_code" placeholder="Frontdesk Code" required aria-required="true">
                      
                        </div>
                    </div>

                    
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" id="setup_submit" type="button" >Get Hotels</button>
                        </div>
                    </div>

                   
</form> 
<!-- Page content ends -->  
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script>

$('#setup_submit').click(function(){

   var account_id = $("#account_id").val();
   var email = $("#email").val();
   var frontdesk_code = $("#frontdesk_code").val();
   
    $.ajax({
        url:"<?=base_url()?>setup/check_member",
        type: 'POST',
        dataType: "json",
        data: {email : email,account_id : account_id,frontdesk_code : frontdesk_code},
        success:function(data)
        {
            if(data=='No'){
                swal("No Record Found! Please Cotact IMMH Support")
          $('#value_show').text(data);  
           return false;
          }  else{ 
            $("#setup_form").submit();
         }
        }      
    });
});

</script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>

