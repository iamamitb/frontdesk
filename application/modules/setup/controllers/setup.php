<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('Setup_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
    }

	public function index()
	{
			$this->template->title('Setup','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('login_template','front');
			$this->template->build('setup_view');
	}
	function logout()

	{
		$this->session->sess_destroy();
		redirect("login");

	}

	function check_member()
	{
    $email=$_POST['email'];
    $id=$_POST['account_id'];
    $frontdesk_code=$_POST['frontdesk_code'];
    $email_array=array('email'=>$email,'id'=>$id,'frontdesk_code'=>$frontdesk_code); 
    $fields_string=json_encode($email_array);
     $ch = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/');
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute post
    $data = curl_exec($ch);    
    //close connection
      curl_close($ch); 
      //print_r($fields_string);    
      $data1 = json_decode($data, true);
      echo $data;

    }  
    
     


}
