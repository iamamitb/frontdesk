<link href="<?=base_url()?>themes/css/views/dashboard.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Supplier Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_supplier" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Suppliers</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_supplier" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Supplier</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 

            <div class="tab-pane active" id="all_supplier"> 
                <!-- Row for Table Starts -->
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <h2 style="font-weight:bold;margin-bottom:30px;">All Suppliers</h2>
                                                <table id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Supplier Name</th>
                                                            <th>Supplier Phone Number</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>

                                             
                                                    <tbody>
                                                        <tr>
                                                            <td>Prakash Jha</td>
                                                            <td>9999696895</td>
                                                            <td>
                                                                <div class="btn-group">
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    
                                                                    <li><a href="#">Edit</a></li>
                                                                    <li><a href="#" id="sa-warning">Delete</a></li>
                                                                </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        
                                                      
                                                        
                                                        
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                       <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_supplier"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Supplier</h2>

                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Supplier Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone Number</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mobile Number</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">City</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Contact Person Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" value="">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Supplier</button>
                    </div>



                </div>

                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  

<!-- See User Priviledges Modal Starts -->

<div id="user-priviledges" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h2 class="modal-title">Priviledges for {username}</h2> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        
                                                        <div class="row"> 
                                                            <ul>
                                                                <li>Priviledge 1</li>
                                                                <li>Priviledge 2</li>
                                                                <li>Priviledge 3</li>
                                                            </ul>
                                                        </div> 
                                                        
                                                        
                                                         
                                                    </div> 
                                                    
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->
<!-- See User Priviledges Modal Ends -->


<!-- Sweet Alert used for deleting confirmation -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script>

!function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {

    //Warning Message
    $('#sa-warning').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "This Supplier will be deleted and cannot be recovered",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "The Supplier was deleted", "success"); 
        });
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.SweetAlert.init()
}(window.jQuery);

</script>
<!-- Sweet Alert Ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>