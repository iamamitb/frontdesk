<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Load_hotel extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('Load_hotel_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
    }

	public function index()
	{
			$this->template->title('Setup','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('login_template','front');
			$this->template->build('load_hotel_view');
			   $hotel_id=$_POST['hotel_id'];
    $frontdesk_code=$_POST['frontdesk_code'];
    //close connection
      curl_close($ch);     
    $hotel_id_array=array('frontdesk_code'=>$frontdesk_code); 
    $hotel_id_string=json_encode($hotel_id_array);  
     $ch = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/fetch_data.php');
    curl_setopt($ch,CURLOPT_POSTFIELDS, $hotel_id_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute post
    $data4 = curl_exec($ch);    
    //close connection
    curl_close($ch);     
      $data5 = json_decode($data4, true);


     						$data['account_id'] = $data5['account_id'];
      						$data['hotel_id'] = $data5['hotel_id'];
							$data['hotel_name'] = $data5['hotel_name'];
							$data['state'] = $data5['state'];
							$data['city'] = $data5['city'];
							$data['hotel_address'] = $data5['hotel_address'];
							$data['hotel_contact_person'] = $data5['hotel_contact_person'];
							$data['phone1'] = $data5['phone1'];
							$data['phone2'] = $data5['phone2'];
							$data['contact_email'] = $data5['contact_email'];
							$data['fax'] = $data5['fax'];
							$data['website'] = $data5['website'];
							$data['booking_office_address'] = $data5['booking_office_address'];
							$data['booking_office_phone_number'] = $data5['booking_office_phone_number'];
							$data['service_tax'] = $data5['service_tax'];
							$data['service_charge'] = $data5['service_charge'];
							$data['standard_tac'] = $data5['standard_tac'];
							$data['swachh_bharat_cess'] = $data5['swachh_bharat_cess'];
							$data['checkin_time'] =date("g:i a", strtotime($data5['checkin_time']));
							$data['checkout_time'] = date("g:i a", strtotime($data5['checkout_time']));
							$data['hotel_prefix'] = $data5['hotel_prefix'];
							$data['hotel_logo'] = $data5['hotel_logo'];
							$data['hotel_terms_and_conditions'] = $data5['hotel_terms_and_conditions'];
							$data['booking_engine_code_url'] = $data5['booking_engine_code_url'];
							$data['cleartrip_free_website_address'] = $data5['cleartrip_free_website_address'];							

								$this->Load_hotel_model->addHotel($data);
     
	}
	function logout()

	{
		$this->session->sess_destroy();
		redirect("login");

	}

}
