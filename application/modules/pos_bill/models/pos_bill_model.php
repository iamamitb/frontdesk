<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pos_bill_model extends CI_Model {
		
public function allpos_order($hotel_id){
		
       $query = $this->db->select('pos_master.*,menu_item.menu_name,menu_item.add_vat,menu_name,menu_item.alcholoc_beverage,pos_order.*');
	   $query = $this->db->from('pos_order');
	   $query = $this->db->join('pos_master','pos_master.pos_id=pos_order.pos_id', 'inner');
	   $query = $this->db->join('menu_item','menu_item.menu_item_id=pos_order.pos_order_item_id','inner');
	   $query = $this->db->where('pos_master.hotel_id',$hotel_id);
	   $query = $this->db->order_by('pos_order.pos_order_id','desc');
	   //$query = $this->db->group_by('invoices_send.booking_id');
       $query = $this->db->get();
       return $query->result();   
	}

	public function allpos_master_order($hotel_id){
		
       $query = $this->db->select('pos_master.*,table_master.pos_table_number,steward_master.steward_name,sum(pos_transaction.pos_transaction_amount) as pos_grand_total_transaction_amount,sum(pos_master.pos_total) as pos_grand_total,sum(pos_master.pos_total_vat) as pos_grand_total_vat,sum(pos_master.pos_total_service_tax) as pos_grand_total_service_tax,sum(pos_master.pos_total_service_charge) as pos_grand_total_service_charge');
	   $query = $this->db->from('pos_master');
	   $query = $this->db->join('table_master', 'table_master.pos_table_id = pos_master.pos_table_id','left'); 
	   $query = $this->db->join('steward_master', 'steward_master.steward_id = pos_master.pos_steward_id','left');
	   $query = $this->db->join('pos_transaction', 'pos_transaction.pos_bill = pos_master.pos_bill','left'); 
	   $query = $this->db->where('pos_master.hotel_id',$hotel_id);
	   $query = $this->db->order_by('pos_master.pos_bill','desc');
	   $query = $this->db->group_by('pos_master.pos_bill');
       $query = $this->db->get();
       return $query->result();   
	}
		public function getTodayOccupancy($hotel_id) {

         $this->db->select("b.booking_id, b.booking_reference,b.booking_date,br.room_number as rooms,b.total_adults, b.total_child, gd.name as guest_name, gd.phone as guest_phone");
         //GROUP_CONCAT(br.room_number) as rooms
         $this->db->from('bookings b');
         $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id','left'); 
         $this->db->join('booked_room_detail brd', 'b.booking_id = brd.booking_id','left');
         $this->db->join('booked_room br', 'br.booked_room_id = brd.booked_room_id','left');
         $this->db->where('b.checkin_date <=', Date('Y-m-d'));
         $this->db->where('b.checkout_date >=', Date('Y-m-d'));
         $this->db->where('b.booking_status', 3);
         $this->db->where('b.hotel_id', $hotel_id);   
         //$this->db->group_by("b.booking_id");
         $query = $this->db->get();
         return $query->result();
    }	
}
?>