<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title"><b>Bill</b></h1>
</div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">


    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#dine_in" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">Dine In</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#take_away" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Take Away</span> 
                </a> 
            </li> 

            <li class=""> 
                <a href="#room_service" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Room Service</span> 
                </a> 
            </li>

            <li class="" style="float:left;"> 
                <a href="#pending_bills" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Pending Bills</span> 
                </a> 
            </li> 

            <li class="" style="float:left;"> 
                <a href="#settled_bills" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Settled Bills</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 

            <div class="tab-pane active" id="dine_in"> 

                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                         
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Table Number</label>
                                <select class="form-control" id="table_no_id">
                                <option value="">select table</option>
                                <?php if(isset($alltable)  && !empty($alltable)) {foreach($alltable as $table):?>
                                <option value="<?=$table->pos_table_id?>"><?=$table->pos_table_number?></option>
                                <?php  endforeach; } ?> 
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Guest Name</label>
                                <input type="text" class="form-control" id="dine_in_guest_name" placeholder="" readonly> 
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile Number</label>
                                <input type="text" class="form-control" id="dine_in_mobile_no" placeholder="" readonly> 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p style="margin-right:20px;font-size:20px;font-weight:bold;text-align:center;">Bill Number<span id="dine_in_bill_no" class="badge badge-danger" style="font-size:20px;padding:10px;margin-left:10px;"></span></p>
                            <table class="table table-striped table-bordered" id="table_dine_in">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>KOT</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                     <?php //$sl=1; if(isset($allpos_order)  && !empty($allpos_order)) {foreach($allpos_order as $order): if($order->order_type==3){?>
                                     <tr>
                                        <td><?=$sl?></td>
                                        <td><?=$order->pos_id?></td>
                                        <td><?=$order->menu_name?></td>
                                        <td><?=$order->pos_order_rate?></td>
                                        <td><?=$order->pos_order_quantity?></td>
                                        <td><?=$order->pos_total_vat?></td>
                                        <td><?=$order->pos_total_vat?></td>
                                        <td><?=$order->pos_total?></td>
                                    </tr>                
                                    <?php // } $sl++; endforeach; } ?> 
                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                    
                    <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-1">Service Tax %<br><span id='dine_in_service_tax'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-1">Service Chg %<br><span id='dine_in_service_charge'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-4" style="text-align:right;">Total Payable <br><span id='dine_in_total_payable' style="font-weight:bold;font-size:30px;float:right;"></span></div>
                        </div>
                    </div>
            

                    <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>

            <div class="tab-pane" id="room_service"> 

                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Room Number</label>
                                        <select class="form-control" id="room_service_room_number_id"><option value="" selected>Select Room Number</option>
                    <?php if(isset($today_occupancy)  && !empty($today_occupancy)) {foreach($today_occupancy as $today):?>
                    <option value="<?=$today->rooms?>"><?=$today->rooms?></option>
                    <?php  endforeach; } ?>
                                       </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Guest Name</label>
                                <input type="text" class="form-control" id="room_service_guest_name" placeholder="" readonly> 
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile Number</label> 
                                <input type="text" class="form-control" id="room_service_mobile_no" placeholder="" readonly> 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p style="margin-right:20px;font-size:20px;font-weight:bold;text-align:center;">Bill Number<span id="room_service_bill_no" class="badge badge-danger" style="font-size:20px;padding:10px;margin-left:10px;"></span></p>
                            <table class="table table-striped table-bordered" id="table_room_service" >
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>KOT</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                    
 
                                     <?php //$sl=1; if(isset($allpos_order)  && !empty($allpos_order)) {foreach($allpos_order as $order): if($order->order_type==2){?>
                                     <tr>
                                        <td><?=$sl?></td>
                                        <td><?=$order->pos_id?></td>
                                        <td><?=$order->menu_name?></td>
                                        <td><?=$order->pos_order_rate?></td>
                                        <td><?=$order->pos_order_quantity?></td>
                                        <td><?=$order->pos_total_vat?></td>
                                        <td><?=$order->pos_total_vat?></td>
                                        <td><?=$order->pos_total?></td>
                                    </tr>                
                                    <?php // } $sl++; endforeach; } ?> 
                                   
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                   <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                            <div class="col-md-1">Service Tax %<br><span id='room_service_service_tax'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-1">Service Chg %<br><span id='room_service_service_charge'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-4" style="text-align:right;">Total Payable <br><span id='room_service_total_payable' style="font-weight:bold;font-size:30px;float:right;"></span></div>
                        </div>
                    </div>
            

                    <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>

            <div class="tab-pane" id="take_away"> 
                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                        
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Token Number</label>
                                <input type="text" class="form-control" id="get_bill_by_token_number" placeholder=""> 
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><b>Recent Tokens</b></label><br>
                                <?php if(isset($alltoken_number)  && !empty($alltoken_number)) {foreach($alltoken_number as $token):?>
                        <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;" id="get_bill_by_token_number_span"><?=$token->token_number?></span>        
                                <?php  endforeach; } ?>                                
                            </div>
                        </div>
                        
                        
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <p style="margin-right:20px;font-size:20px;font-weight:bold;text-align:center;">Bill Number<span id="take_away_bill_no" class="badge badge-danger" style="font-size:20px;padding:10px;margin-left:10px;"></span></p>
                            <table class="table table-striped table-bordered" id="table_take_away">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>KOT</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                     
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                    
                <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-1">Service Tax %<br><span id='take_away_service_tax'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-1">Service Chg %<br><span id='take_away_service_charge'style="font-weight:bold;font-size:30px;float:left;"></span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-4" style="text-align:right;">Total Payable <br><span id='take_away_total_payable' style="font-weight:bold;font-size:30px;float:right;"></span></div>
                        </div>
                </div>
                <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>

            <div class="tab-pane" id="pending_bills"> 
                
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <h3>Pending Bills</h3>
                            <table class="table table-striped table-bordered">
                                <thead>
                                   
                                    
                                        <tr>
                                        <th>Bill.No</th>
                                        <th>Type</th>
                                        <th>Table (For Dine In)</th>
                                        <th>Room (For Room service / Dine In)</th>
                                        <th>Token (For take away)</th>
                                        <th>Total Payable</th>
                                        <th>Status</th>
                                       
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                   <?php if(isset($allpos_master_order)  && !empty($allpos_master_order)) {foreach($allpos_master_order as $master_order): if($master_order->pos_payment_status=='pending'){?>
                                     <tr>
                                        <td><?=$master_order->pos_bill?></td>
                                        <td><?php if($master_order->order_type=='1'){ echo "Take Away";} else if($master_order->order_type=='2'){ echo "Room Service";} else {  echo "Dine In";}?></td>
                                        <td><?php if($master_order->order_type=='3'){ echo  $master_order->pos_table_number;} else {echo "N/A";}?></td>
                                        <td><?php if($master_order->order_type=='2'){ echo  $master_order->pos_room_no;} else {echo "N/A";}?></td>
                                        <td><?php if($master_order->order_type=='1'){ echo  $master_order->token_number;} else {echo "N/A";}?></td>
                                        <td>Rs.<?=$master_order->pos_grand_total?></td>
                                        <td>
                                            <a href="#" data-toggle="modal" class="bill_payment_details"  id="<?=$master_order->order_type."_".$master_order->pos_bill?>">Settle Bill</a>
                                        </td>
                                    </tr>                
                                    <?php } endforeach; } ?> 
                                    
                                      
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
            </div>

            <div class="tab-pane" id="settled_bills"> 
                
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <h3>Settled Bills</h3>

                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Bill.No</th>
                                        <th>Type</th>
                                        <th>Table Number</th>
                                        <th>Room Number</th>
                                        <th>Token (For take away)</th>
                                        <th>Steward</th>
                                        <th>Total Paid</th>
                                        <th>Status</th>
                                       
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                      <?php if(isset($allpos_master_order)  && !empty($allpos_master_order)) {foreach($allpos_master_order as $master_order): if($master_order->pos_payment_status=='settle'){?>
                                     <tr>
                                        <td><?=$master_order->pos_bill?></td>
                                        <td><?php if($master_order->order_type=='1'){ echo "Take Away";} else if($master_order->order_type=='2'){ echo "Room Service";} else {  echo "Dine In";}?></td>
                                        <td><?php if($master_order->order_type=='3'){ echo  $master_order->pos_table_number;} else {echo "N/A";}?></td>
                                        <td><?php if($master_order->order_type=='2'){ echo  $master_order->pos_room_no;} else {echo "N/A";}?></td>
                                        <td><?php if($master_order->order_type=='1'){ echo  $master_order->token_number;} else {echo "N/A";}?></td>
                                        <td><?=$master_order->steward_name?></td>
                                        <td>Rs.<?=$master_order->pos_grand_total?></td>
                                        <td><?=$master_order->pos_payment_status?></td>
                                    </tr>                
                                    <?php } endforeach; } ?> 
                                     
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
            </div>
                
            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  

<div id="payment_details_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog" style="width:70%;"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h3 class="modal-title" id='bill_payment_title'>Bill No  - Enter Payment Details</h3> 
            </div> 
            <div class="modal-body"> 

                <div class="row">
                    <p id="bill_guest_details"><b>Guest name:</b></p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6" id='total_payable'>
                            <b>Total Payable:</b>
                   
                        </div>

                        <div class="col-md-6" id='pending_amount'>
                            <b>Pending Amount:</b>
                        </div>
                    </div>
                    <input type='hidden' id='pending_amount_id' value='0'>
                    <input type='hidden' id='hid_pos_bill' value='0'>
                </div>
              <!--   <div class="row" style="margin-top: 15px;text-align: center;margin-bottom: 30px;background-color: #E1EEF2;border-radius: 5px;padding: 13px;" >Item Order</div> -->
               <!--  <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered" style="background:#f2f2f2;" id="item_add">
                                                    <thead>
                                                        <tr>
                                                            <th>Sl.no</th>
                                                            <th>Item</th>
                                                            <th>Total(Rs.)</th>
                                                            <th>Vat(Rs.)</th>
                                                            <th>Service Tax(Rs.)</th>
                                                            <th>Service Charge(Rs.)</th>
                                                            <th>Sub Total(Rs.)</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        
                                                        <tr>
                                                            <td>Total</td>
                                                            
                                                            <td id="total_item_price"colspan="6"><b>Amount:</b> 0</td>

                                                        </tr>
                                                    </tfoot>

                                                     <tbody>
                                                        
                                                    </tbody>
                    </table>        
                        
                         
                </div>
            </div> -->
                <div class="row" style="margin-top: 15px;text-align: center;margin-bottom: 30px;background-color: #E1EEF2;border-radius: 5px;padding: 13px;display:none" id='spot_settle_show'>
                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="spot_settle" value="option2" name="radioInline" class="spot_settle_post_to_room" checked>
                        <label for="inlineRadio2">Spot Settle</label>
                    </div>

                    <div class="radio radio-success radio-inline">
                        <input type="radio" id="post_to_room"  value="option2" name="radioInline"  class="spot_settle_post_to_room">
                        <label for="inlineRadio2">Post to Room</label>
                    </div>
                </div>
                 
                 <div class="row all_amount" id='main_0'>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Amount</label>
                                <input type="text" class="form-control advance_amount" id="amount_0" placeholder="">
                                <p id="amount_error_0" style="color:red;"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Payment Type</label>
                                <select class="form-control" id='payment_type_0'>
                                    <option>Cash</option>
                                    <option>Debit Card</option>
                                    <option>Credit Card</option>
                                    <option>Netbanking</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4"><br>
                            <button type="button" class="btn btn-success" id='add_payment'>+</button>
                            <!-- <button type="button" class="btn btn-danger" id='minus_payment'>-</button> -->
                        </div>
                    </div>
                 <input type='hidden' id='num_div' value='1'>
                 <span id='add_payment_div'></span>
                 </div>
 
                
                <div class="row" id='room_number_show' style="display:none" >
                    <div class="form-group">
                        <label for="exampleInputEmail1">Room Number</label>
          <select class="form-control" id="room_number_id"><option value="" selected>Select Room Number</option>
                    <?php if(isset($today_occupancy)  && !empty($today_occupancy)) {foreach($today_occupancy as $today):?>
                    <option value="<?=$today->rooms?>"><?=$today->rooms?></option>
                    <?php  endforeach; } ?>
                    </select>
                    </div>
                </div>

                <div class="row" id='guest_details_show' style="display:none">
                    <div class="col-md-12">
                        <h4><b>Guest Details</b></h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <b>Guest Name</b>   
                                <span style="float:right;" id='guest_name_id'></span>
                            </li>
                            <li class="list-group-item">
                                <b>Room Number</b>   
                                <span style="float:right;"  id='room_number'></span>
                            </li>
                            <li class="list-group-item">
                                <b>Booking Date</b>   
                                <span style="float:right;" id='bookingdate'></span>
                            </li>
                        </ul>
                    </div>
                 </div>

                 <div class="row" align="center">
                    <button type="button" class="btn btn-success submit_payment" id='settle_bill_button'>Settle Bill</button> 
                    <button type="button" class="btn btn-success submit_payment" style="display:none" id="post_to_room_button">Post to Room And Settle Bill</button> 
                 </div>
                                    
            </div> 
            
        </div> 
    </div>
</div><!-- /.modal -->
<p id='v'></p>

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url()?>themes/js/views/pos_bill.js"></script>
<script>
allpos_master_order_new= <?=json_encode($allpos_master_order);?>;
today_occupancy=     <?=json_encode($today_occupancy);?>;
allpos_order_new=    <?=json_encode($allpos_order);?>;
alltax_new=    <?=json_encode($alltax);?>;
alltoken_number_new=    <?=json_encode($alltoken_number);?>;
</script>