<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_bill extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_bill_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$hotel_id=$this->session->userdata('hotel_id');
		$data['allpos_order'] = $this->pos_bill_model->allpos_order($hotel_id);
		$data['alltoken_number'] = getAll('pos_master',array('hotel_id'=>$hotel_id,'token_number !='=> NULL,'pos_payment_status'=>'pending'),'token_number','desc','','');
		$data['allpos_master_order'] = $this->pos_bill_model->allpos_master_order($hotel_id);
		$data['alltable']=getAll('table_master',array('hotel_id'=>$hotel_id,'pos_table_status'=>1),'pos_table_id','asc','','');
		$data['alltax']=getAll('pos_bill_design',array('hotel_id'=>$hotel_id),'pos_bill_design_id','asc','','');
		$data['today_occupancy'] = $this->pos_bill_model->getTodayOccupancy($hotel_id);
		$this->template->title('POS Bill','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		//print_r($data['alltoken_number']);
		$this->template->build('pos_bill',$data);

				
	}

	public function addtransaction()
	{
		$hotel_id=$this->session->userdata('hotel_id');
		$total_advance=$this->input->post('total_advance');
		$payment_array=$this->input->post('payment_array');
		$pos_bill=$this->input->post('hid_pos_bill');
		$payment_mode=$this->input->post('payment_mode');

		if($payment_mode =='settle_bill_button')
		{

			foreach($payment_array as $payment)
			{
			     $pos_transaction_array=array(
				'pos_bill'=>$pos_bill,
				'pos_transaction_amount'=>$payment['amount'],
				'pos_transaction_type'=>$payment['payment_type'],
				'bank_name'=>'',
				'pos_transaction_time'=>date('Y-m-d H:i:s'),
				'hotel_id'=>$hotel_id,
				); 
			    $pos_order_id=insertValue('pos_transaction',$pos_transaction_array);
			}
   		$result=updateDataCondition('pos_master', array('pos_payment_status'=>'settle'),array('pos_bill'=>$pos_bill));
   	    }
   		else
   		{
   			echo 1;
   		}
	}

}
