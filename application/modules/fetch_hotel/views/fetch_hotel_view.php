<div class="wrapper-page">

            <!-- Incorrect password or email address Div. Show it via AJAX, originally display none -->
          <?=$this->session->flashdata("msg"); ?>
            <!-- Incorrect password div ends -->

            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body"><img src="<?=base_url()?>themes/images/immh-logo2.png">
<title>Setup to iManageMyHotel Frontdesk</title>

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="page-title" style="text-align: center;text-transform: uppercase;font-size: 24px;font-weight: bold;margin-top: 24px;margin-bottom:0px;">Hotel Details</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<?php
    $email=$_POST['email'];
    $id=$_POST['account_id'];
    $frontdesk_code=$_POST['frontdesk_code'];
    $email_array=array('email'=>$email,'id'=>$id,'frontdesk_code'=>$frontdesk_code); 
    $fields_string=json_encode($email_array);
     $ch = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/');
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //execute post
    $data = curl_exec($ch);    
    //close connection
      curl_close($ch);     
      $data1 = json_decode($data, true);
?>

<form class="form-horizontal m-t-20" action="<?=base_url()?>fetch_hotel/curl_test_fetch" method="post">
                    
                    <div class="form-group">
                      <div class="col-xs-12">
                         <div class="form-group">
                            <label for="sel1">Select Hotel (select one):</label>
                            <input type="hidden" name="frontdesk_code" id="frontdesk_code" value="<?php echo $frontdesk_code ?>"/>
                            <select class="form-control" name="hotel_id" id="hotel_id">
                            
                              <option value="<?php echo  $data1['hotel_id']; ?>" ><?php echo $data1['hotel_name']; ?></option>
                             
                            </select>
                        </div>
                      </div>
                    </div>
                   <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Fetch All Data</button>
                        </div>
                    </div>
</form> 
<!-- Page content ends -->  
