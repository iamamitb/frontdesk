<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fetch_hotel_model extends CI_Model {

		function addHotel($data5) {
	
			$this->db->insert('hotels', $data5);
	}	
	function addRooms($data_room_details) {
	
			$this->db->insert('rooms', $data_room_details);
	}
	function bookings($room_num_data2) {
	
			$this->db->insert('bookings', $room_num_data2);
	}		
}