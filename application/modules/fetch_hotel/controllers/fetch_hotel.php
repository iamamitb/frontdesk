<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fetch_hotel extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('Fetch_hotel_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
    }

	public function index()
	{
			$this->template->title('Setup','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('login_template','front');
			$this->template->build('fetch_hotel_view');
	}

	public function curl_data_fetch()
	{
			/// Fetch Hotel ///

			$frontdesk_code=$_POST['frontdesk_code'];
		    //close connection
		    curl_close($ch);     
		    $hotel_id_array=array('frontdesk_code'=>$frontdesk_code); 
		    $hotel_id_string=json_encode($hotel_id_array);  
		    $ch = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/fetch_data.php');
		    curl_setopt($ch,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    //execute post
		    $data4 = curl_exec($ch);    
		    //close connection
		    curl_close($ch);     
		    $data5 = json_decode($data4, true);
		   	//echo "<pre>";
		    //print_r($data5);
		    $hotel_id=$data5['hotel_id'];				
			$result=count_rows('hotels','hotel_id ='.$hotel_id);

			if ($result!=0) {

				updateDataCondition('hotels',$data5,'hotel_id ='.$hotel_id);

			} else{
				 
				insertValue('hotels',$data5);
			}

				
				
			/// Fetch Rooms ///

			$ch_rooms = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_rooms,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/rooms.php');
		    curl_setopt($ch_rooms,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_rooms, CURLOPT_RETURNTRANSFER, true);
		    //execute post
		    $data_room = curl_exec($ch_rooms);    
		    //close connection
		    curl_close($ch_rooms);     
		    $data_room_details = json_decode($data_room, true);
		    //echo "<pre>";
		    //print_r($data_room_details);
			 

			foreach ($data_room_details as $key => $value) {	

					
					$room_id=$value['room_id'];	
					$result_room=count_rows('rooms','room_id ='.$room_id);

					if ($result_room!=0) {

						$room_data = array(
									'room_id'=>$value['room_id'],							
									'hotel_id'=>$value['hotel_id'],
									'room_name'=>$value['room_name'],
									'total_rooms'=>$value['total_rooms'],							
									'total_bed'=>$value['total_bed'],
									'room_number_str'=>$value['room_number_str'],
									'luxury_tax'=>$value['luxury_tax'],	
									'person_limit'=>$value['person_limit'],
									'room_status'=>$value['room_status'],
									'special_tariff_flag'=>$value['special_tariff_flag']
								);
						updateDataCondition('rooms',$room_data,'room_id ='.$room_id);
						//print_r($room_special_tariff);
						
					} else{
						$room_data = array(
									'room_id'=>$value['room_id'],							
									'hotel_id'=>$value['hotel_id'],
									'room_name'=>$value['room_name'],
									'total_rooms'=>$value['total_rooms'],							
									'total_bed'=>$value['total_bed'],
									'room_number_str'=>$value['room_number_str'],
									'luxury_tax'=>$value['luxury_tax'],	
									'person_limit'=>$value['person_limit'],
									'room_status'=>$value['room_status'],
									'special_tariff_flag'=>$value['special_tariff_flag']
									);
						insertValue('rooms',$room_data);
						//print_r($room_special_tariff);
						}
					}

			/// Fetch room number ///

			$ch_rooms_num = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_rooms_num,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/room_number.php');
		    curl_setopt($ch_rooms_num,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_rooms_num, CURLOPT_RETURNTRANSFER, true);
		    //execute post
		    $data_room_num = curl_exec($ch_rooms_num);    
		    //close connection
		    curl_close($ch_rooms_num);     
		    $data_room_number = json_decode($data_room_num, true);	
		    //echo "<pre>";
		    //print_r($data_room_number);

				foreach ($data_room_number as $key => $value) {	
					$id=$value['id'];
					$room_id=$value['room_id'];	
					$result_rn=count_rows('room_number','id ='.$id);
					if ($result_rn!=0) {
						$room_num_data = array(
									'id'=>$value['id'],							
									'room_id'=>$value['room_id'],
									'room_number'=>$value['room_number']												
								);
						updateDataCondition('room_number',$room_num_data,'id ='.$id);
						//print_r($room_num_data);
					} else {
					$room_num_data2 = array(
									'id'=>$value['id'],							
									'room_id'=>$value['room_id'],
									'room_number'=>$value['room_number']												
								);
						insertValue('room_number',$room_num_data2);
						//print_r($room_num_data2);
						}
					}

					/// Fetch Room Price ///
					
			$ch_rooms_price = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_rooms_price,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/room_price.php');
		    curl_setopt($ch_rooms_price,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_rooms_price, CURLOPT_RETURNTRANSFER, true);
		    //execute post
		    $data_room_price = curl_exec($ch_rooms_price);    
		    //close connection
		    curl_close($ch_rooms_price);     
		    $data_room_price_details = json_decode($data_room_price, true);	
		    //echo "<pre>";
		    //print_r($data_room_price_details);

				foreach ($data_room_price_details as $key => $value) {	

					$id=$value['id'];
					$room_id=$value['room_id'];	
					$result_price=count_rows('room_plan_price','id ='.$id);

					if ($result_price!=0) {

						$room_price_data = array(
									'id'=>$value['id'],							
									'plan_type_id'=>$value['plan_type_id'],
									'room_id'=>$value['room_id'],
									'single_occupancy'=>$value['single_occupancy'],							
									'double_occupancy'=>$value['double_occupancy'],
									'extra_bed_price'=>$value['extra_bed_price'],
									'extra_person_charge'=>$value['extra_person_charge']													
								);
						updateDataCondition('room_plan_price',$room_price_data,'id ='.$id);
						//print_r($room_price_data);
						
					} else{
						$room_price_data = array(
										'id'=>$value['id'],							
										'plan_type_id'=>$value['plan_type_id'],
										'room_id'=>$value['room_id'],
										'single_occupancy'=>$value['single_occupancy'],							
										'double_occupancy'=>$value['double_occupancy'],
										'extra_bed_price'=>$value['extra_bed_price'],
										'extra_person_charge'=>$value['extra_person_charge'],					
									);
						 $tt = insertValue("room_plan_price",$room_price_data);
						//print_r($room_price_data);
						}
					}

					/// Fetch Room special tariff ///
						
			$ch_room_special_tariff = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_room_special_tariff,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/room_special_tariff.php');
		    curl_setopt($ch_room_special_tariff,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_room_special_tariff, CURLOPT_RETURNTRANSFER, true);
		    //execute post
		    $data_room_special_tariff = curl_exec($ch_room_special_tariff);    
		    //close connection
		    curl_close($ch_room_special_tariff);     
		    $data_room_special_tariff_details = json_decode($data_room_special_tariff, true);	
		    //echo "<pre>";
		    //print_r($data_room_special_tariff_details);


				foreach ($data_room_special_tariff_details as $key => $value) {	

					$special_tariff_id=$value['special_tariff_id'];
					$room_id=$value['room_id'];	
					$result_special_tariff=count_rows('room_special_tariff','special_tariff_id ='.$special_tariff_id);

					if ($result_special_tariff!=0) {

						$room_special_tariff = array(
									'special_tariff_id'=>$value['special_tariff_id'],							
									'room_id'=>$value['room_id'],
									'plan_type_id'=>$value['plan_type_id'],
									'from_date'=>$value['from_date'],							
									'to_date'=>$value['to_date'],
									'single_occupancy'=>$value['single_occupancy'],
									'double_occupancy'=>$value['double_occupancy']	
								);
						updateDataCondition('room_special_tariff',$room_special_tariff,'special_tariff_id ='.$special_tariff_id);
						//print_r($room_special_tariff);
						
					} else{
						$room_special_tariff = array(
									'special_tariff_id'=>$value['special_tariff_id'],							
									'room_id'=>$value['room_id'],
									'plan_type_id'=>$value['plan_type_id'],
									'from_date'=>$value['from_date'],							
									'to_date'=>$value['to_date'],
									'single_occupancy'=>$value['single_occupancy'],
									'double_occupancy'=>$value['double_occupancy']													
									);
						$result=insertValue('room_special_tariff',$room_special_tariff);
						//print_r($room_special_tariff);
						}
					}

					redirect(base_url().'login');

								
     
	}

	public function curl_test_fetch() {

			//$room_number=getResult('room_number');			
		    //curl_close($ch);
		    //$hotel_id_string=json_encode($room_number);  

			$frontdesk_code=$_POST['frontdesk_code'];
		    $hotel_id_array=array('frontdesk_code'=>$frontdesk_code); 
		    $hotel_id_string=json_encode($hotel_id_array);  

		     $ch_guest_num = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_guest_num,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/guset_details.php');
		    curl_setopt($ch_guest_num,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_guest_num, CURLOPT_RETURNTRANSFER, true);
		      //execute post
		    $data_guest_num = curl_exec($ch_guest_num);    
		    //close connection
		    curl_close($ch_guest_num);     
		    $data_guset_details = json_decode($data_guest_num, true);	
		    echo "<pre>";
		    print_r($data_guset_details);
		    foreach ($data_guset_details as $key => $value) {	
					$guest_id=$value['guest_id'];	
					$result_rn=count_rows('guest_detail','guest_id ='.$guest_id);
					if ($result_rn!=0) {
						$room_num_data = array(
									'guest_id'=>$value['guest_id'],							
									'name'=>$value['name'],
									'address'=>$value['address'],
									'phone'=>$value['phone'],							
									'email'=>$value['email'],
									'id_card_type'=>$value['id_card_type'],
									'id_card_number'=>$value['id_card_number'],							
									'registration_date'=>$value['registration_date'],
									'organization'=>$value['organization'],
									'designation'=>$value['designation'],							
									'arrived_from'=>$value['arrived_from'],
									'going_to'=>$value['going_to'],
									'police_station'=>$value['police_station'],							
									'date_of_birth'=>$value['date_of_birth'],
									'purpose'=>$value['purpose'],
									'is_foreigner'=>$value['is_foreigner'],							
									'nationality'=>$value['nationality'],
									'passport_no'=>$value['passport_no'],
									'passport_issue_date'=>$value['passport_issue_date'],							
									'passport_expiry_date'=>$value['passport_expiry_date'],
									'visa_no'=>$value['visa_no'],
									'visa_issue_date'=>$value['visa_issue_date'],							
									'visa_expiry_date'=>$value['visa_expiry_date'],
									'arrival_date_india'=>$value['arrival_date_india'],
									'duration_in_india'=>$value['duration_in_india'],							
									'is_employed_in_india'=>$value['is_employed_in_india'],
									'arrival_place_india'=>$value['arrival_place_india'],
									'guest_image'=>$value['guest_image']
								);
						updateDataCondition('guest_detail',$room_num_data,'guest_id ='.$guest_id);
						//print_r($room_num_data);
					} else {
					$room_num_data2 = array(
									'guest_id'=>$value['guest_id'],							
									'name'=>$value['name'],
									'address'=>$value['address'],
									'phone'=>$value['phone'],							
									'email'=>$value['email'],
									'id_card_type'=>$value['id_card_type'],
									'id_card_number'=>$value['id_card_number'],							
									'registration_date'=>$value['registration_date'],
									'organization'=>$value['organization'],
									'designation'=>$value['designation'],							
									'arrived_from'=>$value['arrived_from'],
									'going_to'=>$value['going_to'],
									'police_station'=>$value['police_station'],							
									'date_of_birth'=>$value['date_of_birth'],
									'purpose'=>$value['purpose'],
									'is_foreigner'=>$value['is_foreigner'],							
									'nationality'=>$value['nationality'],
									'passport_no'=>$value['passport_no'],
									'passport_issue_date'=>$value['passport_issue_date'],							
									'passport_expiry_date'=>$value['passport_expiry_date'],
									'visa_no'=>$value['visa_no'],
									'visa_issue_date'=>$value['visa_issue_date'],							
									'visa_expiry_date'=>$value['visa_expiry_date'],
									'arrival_date_india'=>$value['arrival_date_india'],
									'duration_in_india'=>$value['duration_in_india'],							
									'is_employed_in_india'=>$value['is_employed_in_india'],
									'arrival_place_india'=>$value['arrival_place_india'],
									'guest_image'=>$value['guest_image']
								);
						$result=insertValue('guest_detail',$room_num_data2);
						//print_r($room_num_data2);
						}
					}

		    $ch_bookings_num = curl_init();
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch_bookings_num,CURLOPT_URL, 'http://imanagemyhotel.com/frontdesk_webservice/bookings.php');
		    curl_setopt($ch_bookings_num,CURLOPT_POSTFIELDS, $hotel_id_string);
		    curl_setopt($ch_bookings_num, CURLOPT_RETURNTRANSFER, true);
		      //execute post
		    $data_bookings_num = curl_exec($ch_bookings_num);    
		    //close connection
		    curl_close($ch_bookings_num);     
		    $data_bookings = json_decode($data_bookings_num, true);	
		    echo "<pre>";
		    //print_r($data_bookings);
				foreach ($data_bookings as $key => $value) {	
					$booking_id=$value['booking_id'];	
					$result_rn=count_rows('bookings','booking_id ='.$booking_id);
					if ($result_rn!=0) {
						$room_num_data = array(
									'booking_id'=>$value['booking_id'],							
									'hotel_id'=>$value['hotel_id'],
									'plan_type_id'=>$value['plan_type_id'],
									'guest_id'=>$value['guest_id'],	
									'agent_id'=>$value['agent_id'],
									'users_id'=>$value['users_id'],							
									'collection_point_id'=>$value['collection_point_id'],
									'checkin_time'=>$value['checkin_time'],
									'checkout_time'=>$value['checkout_time'],						
									'agent_percentage'=>$value['agent_percentage'],
									'total_adults'=>$value['total_adults'],
									'total_child'=>$value['total_child'],							
									'checkin_date'=>$value['checkin_date'],
									'checkout_date'=>$value['checkout_date'],
									'booking_date'=>$value['booking_date'],							
									'total_room_tariff'=>$value['total_room_tariff'],
									'service_charge'=>$value['service_charge'],
									'service_tax'=>$value['service_tax'],							
									'swachh_bharat_cess'=>$value['swachh_bharat_cess'],
									'total_luxury_tax'=>$value['total_luxury_tax'],
									'misc_charge'=>$value['misc_charge'],							
									'extra_cost'=>$value['extra_cost'],
									'discount_type'=>$value['discount_type'],
									'discount_amount'=>$value['discount_amount'],					
									'remarks'=>$value['remarks'],
									'booking_reference'=>$value['booking_reference'],
									'billing_instruction'=>$value['billing_instruction'],
									'no_show_reason'=>$value['no_show_reason'],
									'total_amount'=>$value['total_amount'],
									'tax_amount'=>$value['tax_amount'],							
									'booking_status'=>$value['booking_status']
								);
						updateDataCondition('bookings',$room_num_data,'booking_id ='.$booking_id);
						//print_r($room_num_data);
					} else {
					$room_num_data2 = array(
									'booking_id'=>$value['booking_id'],														
									'hotel_id'=>$value['hotel_id'],
									'plan_type_id'=>$value['plan_type_id'],
									'guest_id'=>$value['guest_id'],									
									'users_id'=>$value['users_id'],							
									'collection_point_id'=>$value['collection_point_id'],
									'checkin_time'=>$value['checkin_time'],
									'checkout_time'=>$value['checkout_time'],						
									'agent_percentage'=>$value['agent_percentage'],
									'total_adults'=>$value['total_adults'],
									'total_child'=>$value['total_child'],							
									'checkin_date'=>$value['checkin_date'],
									'checkout_date'=>$value['checkout_date'],
									'booking_date'=>$value['booking_date'],							
									'total_room_tariff'=>$value['total_room_tariff'],
									'service_charge'=>$value['service_charge'],
									'service_tax'=>$value['service_tax'],							
									'swachh_bharat_cess'=>$value['swachh_bharat_cess'],
									'total_luxury_tax'=>$value['total_luxury_tax'],
									'misc_charge'=>$value['misc_charge'],							
									'extra_cost'=>$value['extra_cost'],
									'discount_type'=>$value['discount_type'],
									'discount_amount'=>$value['discount_amount'],					
									'remarks'=>$value['remarks'],
									'booking_reference'=>$value['booking_reference'],
									'billing_instruction'=>$value['billing_instruction'],
									'no_show_reason'=>$value['no_show_reason'],
									'total_amount'=>$value['total_amount'],
									'tax_amount'=>$value['tax_amount'],							
									'booking_status'=>$value['booking_status']								
								);
						insertValue('bookings',$room_num_data2);
						//print_r($room_num_data2);
						}
					}
	}  


	function logout()

	{
		$this->session->sess_destroy();
		redirect("login");

	}

}
