<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Daywise Purchase</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">From Date</label>
                    <input type="email" class="form-control" id="datepicker" placeholder="Enter Start Date">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">To Date</label>
                    <input type="email" class="form-control" id="datepicker2" placeholder="Enter End Date">
                    </div>
                </div>

                
                
              

                <div class="col-md-2" style="top:25px;">
                    <div class="form-group">
                    <button class="btn btn-primary m-b-5">Filter Results</button>
                    </div>
                </div>




            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Date</th>
                <th>Item</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Total</th>
            </tr>
        </thead>

                                             
        <tbody>
            <tr>
                <td>23 Feb 2016</td>
                <td>Chinese Main Course</td>
                <td>5</td>
                <td>4</td>
                <td>340</td>
                <td>5600</td>
            </tr>

            
        </tbody>
    </table>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>