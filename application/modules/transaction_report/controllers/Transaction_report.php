<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('transaction_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index()
	{
		
		$search = array();
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		
		if($action=='filter')
		$search=$_POST['filter'];

		$data['all_transaction_report']=$this->transaction_report_model->getTransactionReport($account_id,$search,$csv);

		$this->template->title('Transaction Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('transaction_report',$data);

				
	}
	
	public function print_report(){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
		$search=$_POST['filter'];
		$data['all_transaction_report']=$this->transaction_report_model->getTransactionReport($account_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}

}
