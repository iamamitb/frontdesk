<link href="<?=base_url()?>themes/css/views/transaction_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Transaction Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<!-- Panel Starts -->
<div class="row">
	<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">Transaction Report for your Account</h3>
</div>
<div class="panel-body">

<!-- Row for Custom Filter Starts -->	
<?php //if(isset($all_transaction_report) && count($all_transaction_report)>0){ ?> 
<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
	<div class="col-md-12">
        <form action="" method="post" id="transactionForm" >
        <input name="action" type="hidden" value="filter" />
        <input name="csv" type="hidden" id="csv" value="" />
		<div class="col-md-3">
			<div class="form-group">
            <label for="exampleInputEmail1">From Date</label>
            <input type="text" class="form-control" id="from_date" value="<?php echo $fromDate=(isset($_POST['filter']['tr']['from1']['payment_date']))? $_POST['filter']['tr']['from1']['payment_date'] : "" ;?>" name="filter[tr][from1][payment_date]" placeholder="Enter Start Date" readonly="readonly">
        	</div>
    	</div>

    	<div class="col-md-3">
        	<div class="form-group">
            <label for="exampleInputEmail1">To Date</label>
            <input type="text" class="form-control" id="to_date" value="<?php echo $toDate=(isset($_POST['filter']['tr']['to1']['payment_date']))? $_POST['filter']['tr']['to1']['payment_date'] : "" ;?>" name="filter[tr][to1][payment_date]" placeholder="Enter End Date" readonly="readonly">
        	</div>
    	</div>

    	<div class="col-md-3">
			<div class="form-group">
             <?php  $hi=(isset($_POST['filter']['b']['equal']['hotel_id']))? $_POST['filter']['b']['equal']['hotel_id'] : "" ;?>
            <label for="exampleInputEmail1">Hotel Name</label>
            <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id'),$hi,'hotel_id',"filter[h][equal][hotel_id]"); ?> 
            </div>
        </div>
        <div class="col-md-3" style="top:25px;">
            <div class="form-group">
            <button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
            <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
            </div>
        </div>

        </form>
   	</div>
</div>
</div>
<?php //} ?>
<!-- Row for Custom Filter Ends -->

    	<!-- Row for Table Starts -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if(isset($all_transaction_report) && count($all_transaction_report)>0){ ?> 
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Transaction Purpose</th>
                            <th>Hotel Name</th>
                            <th>Grand Total</th>
                            <th>Amount Paid</th>
                            <th>Due</th>
                            <th>Transaction Date</th>
                            <th>Payment Type</th>
                            <th>Transaction Type</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php                                                                                                                
                    foreach($all_transaction_report as $transaction_report):{
                    ?>    
                        <tr>
                            <td>
                                <?php 
                                if($transaction_report->booking_id>0){
                                    echo "Booking";
                                }elseif($transaction_report->cancel_id>0){
                                    echo "Cancel Booking";
                                }elseif($transaction_report->voucher_id>0){
                                    echo "Voucher";
                                }elseif($transaction_report->checkout_id>0){
                                    echo "Checkout";
                                }
                                ?>
                            </td>
                            <td><?php if($transaction_report->booking_id>0){
                                    echo getValue("hotels","hotel_name","hotel_id = ".getValue("bookings","hotel_id","booking_id = ".$transaction_report->booking_id)."");
                                }elseif($transaction_report->cancel_id>0){
                                    $rs=$this->transaction_report_model->cancelHotelName($transaction_report->cancel_id);
                                    echo $rs->hotel_name;
                                }elseif($transaction_report->voucher_id>0){
                                    $rs=$this->transaction_report_model->voucherHotelName($transaction_report->voucher_id);
                                    echo $rs->hotel_name;
                                }elseif($transaction_report->checkout_id>0){
                                    $rs=$this->transaction_report_model->checkoutHotelName($transaction_report->checkout_id);
                                    echo $rs->hotel_name;
                                }
                                ?>
                            </td>
                            <td><?=$transaction_report->grand_total ?></td>
                            <td><?=$transaction_report->advance ?></td>
                            <td><?=$transaction_report->due ?></td>
                            <td><?=date('d-M-Y', strtotime($transaction_report->payment_date))?></td>
                            <td><?php 
                                if($transaction_report->payment_type=='cash'){
                                    echo "Cash";
                                }elseif($transaction_report->payment_type=='payment_by_company'){
                                    echo "Payment By Company";
                                }elseif($transaction_report->payment_type=='cheque'){
                                    echo "Cheque";
                                }elseif($transaction_report->payment_type=='demand_draft'){
                                    echo "Demand Draft";
                                }elseif($transaction_report->payment_type=='credit_card'){
                                    echo "Credit Card";
                                }elseif($transaction_report->payment_type=='debit_card'){
                                    echo "Debit Card";
                                }elseif($transaction_report->payment_type=='neft'){
                                    echo "NEFT";
                                }elseif($transaction_report->payment_type=='rtgs'){
                                    echo "RTGS";
                                }
                            ?>
                            </td>
                            <td><?php 
                                if($transaction_report->transaction_type=='CR'){
                                    echo "Credit";
                                }elseif($transaction_report->transaction_type=='DR'){
                                    echo "Debit";
                                }
                            ?></td>
                        </tr>
                        <?php } endforeach;  ?>   
                    </tbody>
                </table>

            </div>
        </div>
       <!-- Row for Table ends Starts -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->
<div class="col-md-12">
    <div align="center">
        <button id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No data found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>
<!-- Page content ends -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>  
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$("#from_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate : 0});
$("#to_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate : 0});

 $("#downloadReport").click(function() {
     $( "#csv" ).val(1);
     $( "#transactionForm" ).submit();
     $( "#csv" ).val('');
});

$("#printReport").click(function() {
    $( "#transactionForm" ).attr("target","_blank");
    $( "#transactionForm" ).attr("action","<?=base_url()?>transaction_report/print_report");
    $( "#transactionForm" ).submit();
    $( "#transactionForm" ).removeAttr("target");
    $( "#transactionForm" ).attr("action"," ");
});

//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_date").val("");
    $("#to_date").val("");
    $('#hotel_id option:selected').removeAttr('selected');
    window.location.reload(true);
}); 

$(document).ready(function(){
    var fromdate=$("#from_date").val();
    var todate=$("#to_date").val();
    var hotel_id=$("#hotel_id").val();

    if(fromdate != '' || todate != '' || hotel_id != ''){
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }
}); 


</script>



