<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Transaction Report</h1>
</div>
<?php 
if(!empty($_POST)){   
$hotel_name=(($_POST['filter']['h']['equal']['hotel_id'])!="")? getValue('hotels','hotel_name',"hotel_id = ".$_POST['filter']['h']['equal']['hotel_id'] ) : "" ;
?>  
<div class="row" align="center">
<p><b>Filters: </b><span>Date: <?php echo $dt1=(isset($_POST['filter']['tr']['from1']['payment_date']))? $_POST['filter']['tr']['from1']['payment_date'] : "" ;?> - <?php echo $dt2=(isset($_POST['filter']['tr']['to1']['payment_date']))? $_POST['filter']['tr']['to1']['payment_date'] : "" ;?>, Hotel: <?=$hotel_name?></span></p>
</div>
<?php } ?>

<div class="row">
	<table class="table table-bordered">
         <?php                                                                                                                
        if(isset($all_transaction_report) && count($all_transaction_report)>0){
        ?> 
        <thead>
            <tr>
                <th>Hotel Name</th>
                <th>Grand Total</th>
                <th>Advance</th>
                <th>Due</th>
                <th>Transaction Date</th>
                <th>Payment Type</th>
            </tr>
        </thead>

        <?php                                                                                                                
        foreach($all_transaction_report as $transaction_report):{
        ?>  
        <tbody>
            
            <tr>
                <td><?=$transaction_report->hotel_name ?></td>
                <td><?=$transaction_report->grand_total ?></td>
                <td><?=$transaction_report->advance ?></td>
                <td><?=$transaction_report->due ?></td>
                <td><?=date('d-M-Y', strtotime($transaction_report->payment_date))?></td>
                <td><?=$transaction_report->payment_type ?></td>
            </tr>
            <?php } ?>    
        </tbody>
        <?php endforeach;  } else{  ?> 
        
     <tbody> 
        <tr >
        <th style="text-align:center;">No data found !</th>
        </tr> 
     </tbody> 
     <?php } ?>
    </table>

</div>