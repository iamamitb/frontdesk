<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_report_model extends CI_Model {
		

	public function getTransactionReport($account_id,$search=array(),$csv=0){
			
		$row=false;
			
		$sql="SELECT * FROM `transaction` as tr  LEFT JOIN `bookings` as b ON b.booking_id=tr.booking_id
				LEFT JOIN `hotels` as h ON b.hotel_id=h.hotel_id 
				WHERE h.account_id=$account_id ";

		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}

			//$sql.=" order by tr.transaction_id";

			$query = $this->db->query($sql);
			//echo  $this->db->last_query(); die();
			if($csv!=0){
			download_report($query,"transaction_report.csv");		 
			}
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $val)
				{
					$row[]=	$val;
				}
				return $row;
				
			}

		}



	public  function cancelHotelName($cancelid){ 
     
       $this->db->select("h.hotel_name");
       $this->db->from('hotels h');
       $this->db->join('bookings b', 'b.hotel_id=h.hotel_id');
       $this->db->join('cancel_booking cb', 'cb.booking_id=b.booking_id');
       $this->db->join('transaction tr', 'tr.cancel_id=cb.cancel_id');
       $this->db->where('tr.cancel_id', $cancelid);
       $query = $this->db->get();
       //echo $this->db->last_query();
       return $query->row();
    }

    public  function voucherHotelName($voucherid){ 
     
       $this->db->select("h.hotel_name");
       $this->db->from('hotels h');
       $this->db->join('voucher v', 'v.hotel_id=h.hotel_id');
       $this->db->join('transaction tr', 'tr.voucher_id=v.voucher_id');
       $this->db->where('tr.voucher_id', $voucherid);
       $query = $this->db->get();
       //echo $this->db->last_query();
       return $query->row();
    }

    public  function checkoutHotelName($checkoutid){ 
     
       $this->db->select("h.hotel_name");
       $this->db->from('hotels h');
       $this->db->join('bookings b', 'b.hotel_id=h.hotel_id');
       $this->db->join('checkout_details cd', 'b.booking_id=cd.booking_id');
       $this->db->join('transaction tr', 'tr.checkout_id=cd.checkout_id');
       $this->db->where('tr.checkout_id', $checkoutid);
       $query = $this->db->get();
       //echo $this->db->last_query();
       return $query->row();
    }

}




?>