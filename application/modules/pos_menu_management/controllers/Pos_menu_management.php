<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_menu_management extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_menu_management_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);

		$data['allRecord']=getData("menu_item","menu_status = '1'");
		$data['countRecordMaterial']=count_rows("raw_material_master","material_status = '1'");
        $data['countRecordUnit']=count_rows("unit_master","unit_status = '1'");
        $data['countRecordCategory']=count_rows("category_management","category_status = '1'");
      
		$this->template->title('POS Menu Management','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_menu_management',$data);
				
	}

	public function addMenu(){
		$category_id=$this->input->post('category_id'); 
		$menu_name=$this->input->post('menu_name');
		$selling_price=$this->input->post('selling_price');
		$cost_price=$this->input->post('cost_price');
		$menu_rank=$this->input->post('menu_rank');
		$add_vat=$this->input->post('add_vat');
		$menu_description=$this->input->post('menu_description');
		$alcholoc_beverage=$this->input->post('alcholoc_beverage');
		$hotel_id=$this->session->userdata('hotel_id');

		if($alcholoc_beverage ==""){ $alcholoc_beverage="0"; }

		$data=array('hotel_id'=>$hotel_id,
					'category_id'=>$category_id,
					'menu_name'=>$menu_name,
					'selling_price'=>$selling_price,
					'cost_price'=>$cost_price,
					'menu_rank'=>$menu_rank,
					'add_vat'=>$add_vat,
					'alcholoc_beverage'=>$alcholoc_beverage,
					'menu_description'=>$menu_description,
					'menu_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('menu_item',array('menu_name' => $menu_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('menu_name',$menu_name);
			  $this->db->update('menu_item',$data);

			  $this->session->set_flashdata('chkmenudata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Menu already exists.
            </div>');
			redirect('pos_menu_management#');

		}else{  
			  $insertID=insertValue('menu_item',$data); 

			$menu_item_id=$insertID;
			$material_id=$this->input->post('materials');
			$unit_id=$this->input->post('units');
			$quantity=$this->input->post('qty');
			$record_count=count($material_id);	
			
			//print_r($material_id); die();

			for ($i=0; $i < $record_count; $i++) { 
			
				$ingredient_data = array('hotel_id'=>$hotel_id,
										'menu_item_id'=>$menu_item_id,
										'material_id'=>$material_id[$i],
										'unit_id'=>$unit_id[$i],
										'quantity'=>$quantity[$i],
										'ingredient_status'=>'1'
										);
			

				$result=insertValue('ingredients',$ingredient_data);
					
			}
		} 	
		
		
		$this->session->set_flashdata('addmenudata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Menu.
            </div>');
		
		
		redirect('pos_menu_management#');
		
	}


	public function editMenu($idm) { 
		$menu=getSingle('menu_item','menu_item_id = '.$idm,'','','','');
		$alcholoc_beverage=$menu->alcholoc_beverage;
		if(isset($alcholoc_beverage) && $alcholoc_beverage == 1){ $alcholocBeverage='checked="checked"';} else{ $alcholocBeverage=''; }
		
		$ingredientInfo=getData('ingredients','menu_item_id = '.$idm);
		$html['item']='';
		$html['item'].='<div class="modal-dialog" style="width:75%;">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Menu</h3>
			    </div>
		    <form method="post" id="menu_form_edit" action="'.base_url().'pos_menu_management/updateMenu" >
			<input type="hidden" id="menuID" name="menuID" value="'.$menu->menu_item_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		        <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Name <span class="error">*</span></label>
		              '.get_data_dropdown("category_management","category_id","category_name","category_status='1'","$menu->category_id","category_id","category_id","required","","","").'
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Menu Name <span class="error">*</span></label>
		              <input type="text" id="menu_name" name="menu_name" class="form-control" value="'.$menu->menu_name.'" required>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		        <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Selling Price <span class="error">*</span></label>
		              <input type="text" id="selling_price" name="selling_price" class="form-control" value="'.$menu->selling_price.'" required>
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Cost Price<span class="error">*</span></label>
		              <input type="text" id="cost_price" name="cost_price" class="form-control" value="'.$menu->cost_price.'" required>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Menu Rank <span class="error">*</span></label>
		              <input type="text" id="menu_rank" name="menu_rank" class="form-control" value="'.$menu->menu_rank.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Add VAT <span class="error">*</span></label>
		              <select class="form-control" id="add_vat" name="add_vat" required aria-required="true" >';
		              	if ($menu->add_vat == 'Yes' ){ $yes='selected="selected"'; $no=''; }
		              	elseif ($menu->add_vat == 'No' ){ $no='selected="selected"'; $yes=''; }
        $html['item'].='<option '.$yes.' value="Yes">Yes</option>
                        <option '.$no.' value="No">No</option>
                      </select>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <input name="alcholoc_beverage" type="checkbox" value="1" '.$alcholocBeverage.' >
		              <label for="field-1" class="control-label">Alcholoc Beverage? </label>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Menu Description<span class="error">*</span></label>
		              <input type="text" id="menu_description" name="menu_description" class="form-control" value="'.$menu->menu_description.'" required>
		             </div>
		          </div>
		        </div>
		        
		        
		        <div class="row" style="margin-top:30px;">
                    <h3 style="font-weight:bold;margin-bottom:60px;text-align:center;">Add Ingredients</h3>';

		    if(isset($ingredientInfo) && count($ingredientInfo)>0){
		    	$k=1;
		    $html['item'].='<div class="row">
                        <div class="col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Material <span class="error">*</span></label>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Quantity <span class="error">*</span></label>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Unit <span class="error">*</span></label>
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>';	
            foreach ($ingredientInfo as $key => $valueI) {    
		     $html['item'].='<div class="row" id="'.$k.'">
		     			<input type="hidden" value="1" id="rowEditID" name="rowEditID"/>
		     			<input type="hidden" value="'.$valueI->ingredient_id.'" id="ingredientID" name="ingredientID[]"/>
                        <div class="col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    '.get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","$valueI->material_id","material_$k","materials[]","required","","","").'    
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="form-group">
                                   <input type="text" class="form-control qtyError" id="qty_'.$k.'" name="qty[]" value="'.$valueI->quantity.'"  required>
                                    <span class="qty_error_msg"></span>
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="form-group">
                                    '.get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","$valueI->unit_id","unit_$k","units[]","required","","","").'        
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <!-- <button type="button" class="btn btn-success" onclick="editRows(this.form);" >Add</button> -->
                            </div>
                        </div>
                    </div>'; 
             $k++;          
		     }}else{
		    /*$html['item'].='<div class="row" style="margin-top:30px;">No Data Found!</div>';*/
		     }   
		    $html['item'].='<div id="editedRows"></div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_menu" id="edit_menu" class="btn btn-success waves-effect waves-light">Update Menu</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateMenu() {
		$menuID=$this->input->post('menuID'); 
		$category_id=$this->input->post('category_id'); 
		$menu_name=$this->input->post('menu_name');
		$selling_price=$this->input->post('selling_price');
		$cost_price=$this->input->post('cost_price');
		$menu_rank=$this->input->post('menu_rank');
		$add_vat=$this->input->post('add_vat');
		$menu_description=$this->input->post('menu_description');
		$alcholoc_beverage=$this->input->post('alcholoc_beverage');

		if($alcholoc_beverage ==""){ $alcholoc_beverage="0"; }
		
		$data=array(
					'category_id'=>$category_id,
					'menu_name'=>$menu_name,
					'selling_price'=>$selling_price,
					'cost_price'=>$cost_price,
					'menu_rank'=>$menu_rank,
					'add_vat'=>$add_vat,
					'alcholoc_beverage'=>$alcholoc_beverage,
					'menu_description'=>$menu_description,
					'menu_status'=>'1'
					);

		//print_r($data); die();

  		$menuUpdate=updateDataCondition('menu_item',$data,'menu_item_id = '.$menuID);

			$ingredientID=$this->input->post('ingredientID');
			$material_id=$this->input->post('materials');
			$unit_id=$this->input->post('units');
			$quantity=$this->input->post('qty');
			$record_count=count($material_id);	
			//echo $record_count; die();
			//print_r($material_id); die();
			for ($i=0; $i < $record_count; $i++) { 
			
				$ingredient_data = array(
									'material_id'=>$material_id[$i],
									'unit_id'=>$unit_id[$i],
									'quantity'=>$quantity[$i],
									'ingredient_status'=>'1'
								);
				//print_r($ingredient_data); 
				
				$ingredient_update=updateDataCondition('ingredients',$ingredient_data,'ingredient_id = '.$ingredientID[$i]); 
				//echo $this->db->last_query();
				
			} 
			
	               
  		$this->session->set_flashdata('msgMenuUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the menu.
            </div>');	
                          
  		redirect('pos_menu_management#');

	}


	public function menuDelete() { 
		
		$menu_id=$_POST['menu_id'];		
		$data['menu_status']='0';			
		$menu_delete=updateDataCondition('menu_item',$data,'menu_item_id = '.$menu_id);
		
		$ingredientData['ingredient_status']='0';			
		$ingredient_delete=updateDataCondition('ingredients',$ingredientData,'menu_item_id = '.$menu_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	






	

}
