<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Menu Management</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_menu" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Menu</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_menu" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Menu</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addmenudata"); ?>
            <?=$this->session->flashdata("chkmenudata"); ?> 
            <?=$this->session->flashdata("msgMenuUpdate"); ?> 
            <div class="tab-pane active" id="all_menu"> 
                <!-- Row for Table Starts -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="font-weight:bold;margin-bottom:30px;">All Menu</h2>
                    <?php if(isset($allRecord) && count($allRecord)>0){ ?> 
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Menu ID</th>
                                <th>Menu Name</th>
                                <th>Menu Rank</th>
                                <th>Menu Description</th>
                                <th>Selling Price</th>
                                <th>Cost Price</th>
                                <th>VAT Added</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                 
                        <tbody>
                            <?php                                                                                                                
                            foreach($allRecord as $menu_item):{
                            ?>
                            <tr>
                                <td><?=$menu_item->menu_item_id?></td>
                                <td><?=$menu_item->menu_name?></td>
                                <td><?=$menu_item->menu_rank?></td>
                                <td><?=$menu_item->menu_description?></td>
                                <td><?=$menu_item->selling_price?></td>
                                <td><?=$menu_item->cost_price?></td>
                                <td><?=$menu_item->add_vat?></td>
                                <td>
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="#" data-toggle="modal" onclick="editMenu(<?=$menu_item->menu_item_id?>)" data-target="#edit-menu">Edit</a></li>
                                        <li><a onclick="menuDelete(<?=$menu_item->menu_item_id?>)" href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php } endforeach;  ?> 
                        </tbody>
                    </table>
                    <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any Menu Yet.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>

                </div>
            </div>
           <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_menu"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Menu Item</h2>
                <form name="menu_form" method="post" id="menu_form" action="<?php echo base_url(); ?>pos_menu_management/addMenu">
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                     <?=get_data_dropdown("category_management","category_id","category_name","category_status = '1'","","category_id","category_id","required","","","")?>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Menu Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="menu_name" name="menu_name" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:30px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Selling Price <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="selling_price" name="selling_price" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cost Price <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="cost_price" name="cost_price" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:30px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Menu Rank <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="menu_rank" name="menu_rank" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Add VAT <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="add_vat" name="add_vat" required aria-required="true">
                                            <option value="Yes">Yes</option><option value="No">No</option>
                                        </select>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-top:30px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                           <div class="checkbox checkbox-success">
                               <input id="alcholoc_beverage" name="alcholoc_beverage" type="checkbox" value="1" >
                                <label for="checkbox3">Alcholoc Beverage? </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Menu Description <span class="error">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" id="menu_description" name="menu_description" required aria-required="true" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                

                <!-- New Design Starts -->
                <div class="row" style="margin-top:30px;">
                    <h3 style="font-weight:bold;margin-bottom:60px;text-align:center;">Add Ingredients</h3>
                    <input type="hidden" value="1" id="rowID" name="rowID"/> 
                    <div class="row">
                        <div class="col-md-12">
                           
                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Material <span class="error">*</span></label>
                                 <?=get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","","materials_1","materials[]","required","","","matClss");?> 
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Quantity <span class="error">*</span></label>
                                    <input type="text" class="form-control qtyError" id="qty_1" name="qty[]" required>
                                    <span class="qty_error_msg"></span>
                                </div>
                            </div>

                             <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Unit <span class="error">*</span></label>
                                        <?=get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","","units_1","units[]","required","","","");?> 
                                </div>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <button type="button" class="btn btn-success" onclick="addMoreRows(this.form);" >Add</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div id="addedRows"></div>

                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Menu Item</button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends --> 
<!-- modal --> 
<div id="edit-menu" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 


<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


<?php if($countRecordMaterial==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Material Entry found.To use this section, please go to POS Raw Material Master and add Material information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_raw_material_master';
        });
<?php } ?> 

<?php if($countRecordUnit==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Unit Entry found.To use this section, please go to POS Unit Master and add Unit information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_unit_master';
        });
<?php } ?>   

<?php if($countRecordCategory==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Category Entry found.To use this section, please go to POS Category and add Category information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_category_management';
        });
<?php } ?>  

$(document).ready(function() {
    $("#menu_form").validate({
         rules: {
            
            menu_name: {
                required: true,
                lettersonly: true,                     
                noSpace: true
            },
            category_id: {
                required: true
            },
            selling_price: {
                required: true,
                number: true
            },
            cost_price: {
                required: true,
                number: true
            },
            menu_rank: {
                required: true,
                number: true
            },
            add_vat: {
                required: true
            },
            menu_description: {
                required: true
            }
            
         },
         messages: {
            menu_name: {
                    required: "Please enter menu name",
                    lettersonly: "only letters are allowed"
                },    
            category_id: {
                    required: "Please select a category"
                },
            selling_price: {
                    required: "Please enter selling price",
                    number: "Please specify a number"
                },
            cost_price: {
                    required: "Please enter cost price",
                    number: "Please specify a number"
                },
            menu_rank: {
                    required: "Please enter menu rank",
                    number: "Please specify a number"
                },
            add_vat: {
                    required: "Please select vat"
                },
            menu_description: {
                    required: "Please enter menu description"
                }           
         },
        
    });

});


function editMenu(idm)
{
    var idm = idm;
    //alert(idm);
   
    $.ajax({
        url:"<?php echo site_url('pos_menu_management/editMenu');?>/"+idm,
        type: 'POST',
        dataType: "json",
        data: {'idm': idm},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-menu').html(data.item);
            
        }      
    });
}



$('#edit_menu').click(function(){

   var menuID = $("#menuID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_menu_management/updateMenu",
        type: 'POST',
        dataType: "json",
        data: {menuID : menuID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function menuDelete(menu_id) {
    //alert(menu_id); 
    swal({
        title: "Are you sure?",
        text: "This menu will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_menu_management/menuDelete",
            type: 'POST',
            dataType: "json",
            data: {'menu_id': menu_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}





//================ Add Issue rows ====================

 function addMoreRows(frm) {
    var rowCount = document.getElementById("rowID").value;

    rowCount ++;

     var materialName='<?php echo get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","","materials_'+rowCount+'","materials[]","required","","","matClss") ?>';
     var UnitName='<?php echo get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","","units_'+rowCount+'","units[]","required","","","") ?>';
     

     var recRow = '<div class="row" id="'+rowCount+'" style="margin-top:15px;"><div class="col-md-12"><div class="col-md-3"><div class="form-group">'+ materialName +'</div></div><div class="col-md-3"><div class="form-group"><input type="text" class="form-control" id="qty_'+rowCount+'" name="qty[]" required></div></div><div class="col-md-3"><div class="form-group">'+ UnitName +'</div></div><div class="col-md-3"><button type="button" class="btn btn-success"  style="margin-right:7px;" onclick="addMoreRows(this.form);" >Add</button><button type="button"  class="btn btn-danger" onclick="removeRow('+rowCount+');" >Remove</button></div></div></div>';

    jQuery('#addedRows').append(recRow);

    $('#rowID').val(rowCount);

}


function removeRow(removeNum) {
    //alert(removeNum);
    jQuery("#"+removeNum).remove();

    var rowCount = document.getElementById("rowID").value;

    rowCount --;
    $('#rowID').val(rowCount);
}


//============= Edit Issue rows =============

//============= Edit rows =============

    function editRows(fm) {
     var editRowCount = document.getElementById("rowEditID").value;

     editRowCount ++;
        
     var materialName='<?php echo get_data_dropdown('raw_material_master','material_id','material_name','material_status=1',"","materials_'+editRowCount+'","materials[]","required","","","matClss") ?>';
     var UnitName='<?php echo get_data_dropdown('unit_master','unit_id','unit_name','unit_status=1',"","units_'+editRowCount+'","units[]","required","","","") ?>';

    var recRow = '<div class="row" id="'+editRowCount+'" style="margin-top:15px;"><div class="col-md-12"><div class="col-md-1"></div><div class="col-md-2"><div class="form-group">'+ materialName +'</div></div><div class="col-md-2"><div class="form-group"><input type="text" class="form-control" id="qnty_'+editRowCount+'" name="qty[]" required></div></div><div class="col-md-2"><div class="form-group">'+ UnitName +'</div></div><div class="col-md-2"><br><button type="button" class="btn btn-success"  style="margin-right:7px;" onclick="editRows(this.form);" >Add</button><button type="button"  class="btn btn-danger" onclick="removeEdtRow('+editRowCount+');" >Remove</button></div><div class="col-md-1"></div></div></div><div id="editedRows"></div>';

    jQuery('#editedRows').append(recRow);
    //$('#rowEditID').val(editRowCount);
}


function removeEdtRow(rmNum) {
    jQuery('#'+rmNum).remove();

    //var editRowCount = document.getElementById("rowEditID").value;

    //editRowCount --;
    //$('#rowEditID').val(editRowCount);
}

//============= Add rows =============



</script>

