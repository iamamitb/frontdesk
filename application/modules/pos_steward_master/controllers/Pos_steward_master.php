<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_steward_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_steward_master_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allRecord']=getData('steward_master','steward_status = 1');
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Steward Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_steward_master',$data);

				
	}

	public function addSteward(){
		$steward_name=$this->input->post('steward_name'); 
		$steward_phone=$this->input->post('steward_phone');
		$hotel_id=$this->session->userdata('hotel_id');

		$data=array('hotel_id'=>$hotel_id,
					'steward_name'=>$steward_name,
					'steward_phone_no'=>$steward_phone,
					'steward_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('steward_master',array('steward_name' => $steward_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('steward_name',$steward_name);
			  $this->db->update('steward_master',$data);

			  $this->session->set_flashdata('chkStewardData','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Steward name already exists.
            </div>');
			redirect('pos_steward_master#');

		}else{  
			  $steward_id=insertValue('steward_master',$data); 
		} 	
		
		
		$this->session->set_flashdata('addStewardData','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Steward.
            </div>');
		
		
		redirect('pos_steward_master#');
		
	}


	public function editSteward($ids) { 
		$steward=getSingle('steward_master','steward_id = '.$ids,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Steward</h3>
			    </div>
		    <form method="post" id="steward_form_edit" action="'.base_url().'pos_steward_master/updateSteward" >
			<input type="hidden" id="stewardID" name="stewardID" value="'.$steward->steward_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Steward Name <span class="error">*</span></label>
		              <input type="text" id="steward_name" name="steward_name" class="form-control" value="'.$steward->steward_name.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Steward Phone Number <span class="error">*</span></label>
		              <input type="text" id="steward_phone" name="steward_phone" class="form-control digit_only1" value="'.$steward->steward_phone_no.'" required maxlength="12">
		              <span class="error_msg1" style="color:red"></span>
		            </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_steward" id="edit_steward" class="btn btn-success waves-effect waves-light">Update Steward</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateSteward() {
		$stewardID=$this->input->post('stewardID'); 
		$steward_name=$this->input->post('steward_name'); 
		$steward_phone=$this->input->post('steward_phone');
		
		$data=array(
					'steward_name'=>$steward_name,
					'steward_phone_no'=>$steward_phone,
					'steward_status'=>'1'
					);
		

  		$stewardUpdate=updateDataCondition('steward_master',$data,'steward_id = '.$stewardID);
                          
  		$this->session->set_flashdata('msgStewardUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the steward.
            </div>');	
                          
  		redirect('pos_steward_master#');

	}


	public function stewardDelete() { 
		
		$steward_id=$_POST['steward_id'];		
	
		$data['steward_status']='0';			
		
		$steward_delete=updateDataCondition('steward_master',$data,'steward_id = '.$steward_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	






	

}
