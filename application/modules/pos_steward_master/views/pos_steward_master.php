<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Steward Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_material" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Stewards</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_material" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Steward</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addStewardData"); ?>
            <?=$this->session->flashdata("chkStewardData"); ?> 
            <?=$this->session->flashdata("msgStewardUpdate"); ?> 
            <div class="tab-pane active" id="all_material"> 
                <!-- Row for Table Starts -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="font-weight:bold;margin-bottom:30px;">All Stewards</h2>
                        <?php if(isset($allRecord) && count($allRecord)>0){ ?>
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Steward Name</th>
                                    <th>Steward Phone Number</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                     
                            <tbody>
                                <?php                                                                                                                
                                foreach($allRecord as $steward):{
                                ?>
                                <tr>
                                    <td><?=$steward->steward_name?></td>
                                    <td><?=$steward->steward_phone_no?></td>
                                    <td>
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            
                                            <li><a href="#"  data-toggle="modal" onclick="editSteward(<?=$steward->steward_id?>)" data-target="#edit-steward">Edit</a></li>
                                            <li><a onclick="stewardDelete(<?=$steward->steward_id?>)" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any steward.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>

                    </div>
                </div>
               <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_material"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Steward</h2>
                <form name="steward_form" method="post" id="steward_form" action="<?php echo base_url(); ?>pos_steward_master/addSteward">
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Steward Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="steward_name" name="steward_name" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Steward Phone Number <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="steward_phone" name="steward_phone" class="form-control" required aria-required="true" maxlength="12">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Steward</button>
                    </div>
                </div>
                </form>
                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<!-- modal --> 
<div id="edit-steward" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


$(document).ready(function() {
    $("#steward_form").validate({
         rules: {
            
            steward_name: {
                required: true,
                lettersonly: true,                     
                noSpace: true
            },
            steward_phone: {
                required: true,
                number: true
            }
            
         },
         messages: {
            steward_name: {
                    required: "Please enter steward name"
                },    
            steward_phone: {
                    required: "Please enter steward phone number",
                    number: "Please specify a number"
                }        
         },
        
    });

});


function editSteward(ids)
{
    var ids = ids;
    //alert(ids);
   
    $.ajax({
        url:"<?php echo site_url('pos_steward_master/editSteward');?>/"+ids,
        type: 'POST',
        dataType: "json",
        data: {'ids': ids},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-steward').html(data.item);
            
        }      
    });
}



$('#edit_steward').click(function(){

   var stewardID = $("#stewardID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_steward_master/updateSteward",
        type: 'POST',
        dataType: "json",
        data: {stewardID : stewardID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});



function stewardDelete(steward_id) {
    //alert(steward_id); 
    swal({
        title: "Are you sure?",
        text: "This steward will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_steward_master/stewardDelete",
            type: 'POST',
            dataType: "json",
            data: {'steward_id': steward_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}


</script>

