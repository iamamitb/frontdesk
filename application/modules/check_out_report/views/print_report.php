<!-- Style for page display -->
<style>
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Check Out Report</h1>
</div>
<?php if(!empty($_POST)){ ?>
<div class="row" align="center">
<?php  
$agent_name=(($_POST['filter']['b']['equal']['travel_agent_id'])!="")? getValue('agents','agent_name',"agent_id = ".$_POST['filter']['b']['equal']['agent_id'] ) : "" ;    
$hotel_name=(($_POST['filter']['b']['equal']['hotel_id'])!="")? getValue('hotels','hotel_name',"hotel_id = ".$_POST['filter']['b']['equal']['hotel_id'] ) : "" ;
?>    
</div>
<?php } ?>

<div class="row">
	<table class="table table-bordered">
        <?php                                                                                                                
                    if(isset($all_bookings) && count($all_bookings)>0){
                    ?>    
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Hotel</th>
                        <th>Agent Name</th>
                        <th>Guest Name</th>
                        <th>Mobile Number</th>
                        <th>Booking Date</th>
                        <th>Net Total</th>
                        <th>Advance Deposit</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                    </tr>
                </thead>

                     <?php                                                                                                                
                    foreach($all_bookings as $all_booking):{
                       // $agent_name=getValue('travel_agents','travel_agent_name',"travel_agent_id = ".$all_booking->travel_agent_id); 

                    ?>                           
                    <tbody>
                   
                        <tr>
                            <td><?=$all_booking->booking_reference ?></td>
                            <td><?=$all_booking->hotel_name ?></td>
                            <td><?php if (!empty($agent_name)) {
    echo $agent_name;    
}else{  
    echo "Direct Booking";
}?></td>
                            <td><?=$all_booking->name ?></td>
                            <td><?=$all_booking->phone ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
                            <td><?=$all_booking->grand_total ?></td>
                            <td><?=$all_booking->advance ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->checkin_date)); ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->checkout_date)); ?></td>
                            
                        </tr>
                       
                       <?php } ?>
                    </tbody>
              <?php endforeach;  } else{  ?> 
                    
                 <tbody> 
                    <tr >
                    <td style="margin-top: 80px;background: #E1EEF2;padding: 1%;border-radius: 5px;">No data found !</td>
                    </tr> 
                 </tbody> 
                 <?php } ?>
    </table>

</div>