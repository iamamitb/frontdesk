<link href="<?=base_url()?>themes/css/views/check_out_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Check Out Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  

<!-- Panel Starts -->
<div class="row">
	<div class="panel panel-default">
<div class="panel-heading">
    <h3 class="panel-title">Check Out Report for your Account</h3>
</div>
<div class="panel-body">
	<!-- Row for Custom Filter Starts -->
    <?php //if(isset($all_bookings) && count($all_bookings)>0){ ?> 	
	<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
		<div class="col-md-12">
            <form action="" method="post" id="checkoutForm" >
            <input name="action" type="hidden" value="filter" />
            <input name="csv" type="hidden" id="csv" value="" />
			<div class="col-md-2">
    			<div class="form-group">
                <label for="exampleInputEmail1">From Date</label>
                <input type="text" readonly="readonly" id="from_booking_date" value="<?php echo $dt=(isset($_POST['filter']['b']['from1']['checkout_date']))? $_POST['filter']['b']['from1']['checkout_date'] : "" ;?>" name="filter[b][from1][checkout_date]" class="form-control" placeholder="">
            	</div>
        	</div>

        	<div class="col-md-2">
            	<div class="form-group">
                <label for="exampleInputEmail1">To Date</label>
                <input type="text" readonly="readonly" value="<?php echo $dt1=(isset($_POST['filter']['b']['to1']['checkout_date']))? $_POST['filter']['b']['to1']['checkout_date'] : "" ;?>" id="to_booking_date" name="filter[b][to1][checkout_date]" class="form-control" placeholder="">
            	</div>
        	</div>

        	<div class="col-md-1">
    			<div class="form-group">
                <label for="exampleInputEmail1">Booking ID</label>
                <input type="text" value="<?php echo $bi=(isset($_POST['filter']['b']['equal']['booking_reference']))? $_POST['filter']['b']['equal']['booking_reference'] : "" ;?>" name="filter[b][equal][booking_reference]" class="form-control" id="booking_reference" placeholder="">
            	</div>
        	</div>

        	<div class="col-md-2">
    			<div class="form-group">
                <label for="exampleInputEmail1">Hotel Name</label>
                <?php  $hi=(isset($_POST['filter']['b']['equal']['hotel_id']))? $_POST['filter']['b']['equal']['hotel_id'] : "" ;?>
                <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id'),$hi,'hotel_id',"filter[b][equal][hotel_id]"); ?>
            	</div>
        	</div>

        	<div class="col-md-2">
    			<div class="form-group">
                <label for="exampleInputEmail1">Travel Agent</label>
                <?php  $tai=(isset($_POST['filter']['b']['equal']['travel_agent_id']))? $_POST['filter']['b']['equal']['travel_agent_id'] : "" ;?>
                <?=get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id'),$tai,'agent_id',"filter[b][equal][agent_id]"); ?>
            	</div>
        	</div>

        	<div class="col-md-3" style="top:25px;">
            	<div class="form-group">
                <!-- <label for="exampleInputEmail1">Filter</label> -->
				<button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
            	</div>
        	</div>
        </form>



		</div>
	</div>
    <?php// } ?>
	<!-- Row for Custom Filter Ends -->

    	<!-- Row for Table Starts -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if(isset($all_bookings) && count($all_bookings)>0){ ?> 
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Booking ID</th>
                            <th>Hotel</th>
                            <th>Agent Name</th>
                            <th>Guest Name</th>
                            <th>Mobile Number</th>
                            <th>Booking Date</th>
                            <th>Net Total</th>
                            <th>Advance Deposit</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <?php                                                                                                                
                        foreach($all_bookings as $all_booking):{
                            $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 

                        ?>
                    	<tr>
                            <td><?=$all_booking->booking_reference ?></td>
                            <td><?=$all_booking->hotel_name ?></td>
                            <td><?php if (!empty($agent_name)) {
    echo $agent_name;    
}else{  
    echo "Direct Booking";
}?></td>
                            <td><?=$all_booking->name ?></td>
                            <td><?=$all_booking->phone ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
                            <td><?=$all_booking->grand_total ?></td>
                            <td><?=$all_booking->advance ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->checkin_date)); ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->checkout_date)); ?></td>
                            
                        </tr>
                     <?php } endforeach;  ?>
                    </tbody>
                </table>

            </div>
        </div>
       <!-- Row for Table ends -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->
<div class="col-md-12" style="margin-top:25px;">
    <div align="center">
        <button id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No  Checkout Found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>
<!-- Page content ends --> 

<script src="<?=base_url()?>themes/js/jquery.min.js"></script> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
// check in and check out date picker...
    $("#to_booking_date").datepicker({ dateFormat: 'dd-mm-yy'});
    $("#from_booking_date").datepicker({ dateFormat: 'dd-mm-yy'}).bind("change",function(){
        /*var minValue = $(this).val();
        minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
        minValue.setDate(minValue.getDate()+1);
        $("#to_booking_date").datepicker( "option", "minDate", minValue );*/
    })
     $("#downloadReport").click(function() {
             $( "#csv" ).val(1);
             $( "#checkoutForm" ).submit();
             $( "#csv" ).val('');
       });
        $("#printReport").click(function() {
            $( "#checkoutForm" ).attr("target","_blank");
            $( "#checkoutForm" ).attr("action","<?=base_url()?>check_out_report/print_report");
            $( "#checkoutForm" ).submit();
            $( "#checkoutForm" ).removeAttr("target");
            $( "#checkoutForm" ).attr("action"," ");
       });


//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_booking_date").val("");
    $("#to_booking_date").val("");
    $("#booking_reference").val("");
    $('#hotel_id option:selected').removeAttr('selected');
    $('#agent_id option:selected').removeAttr('selected');
    window.location.reload(true);
}); 

$(document).ready(function(){
    var fromdate=$("#from_booking_date").val();
    var todate=$("#to_booking_date").val();
    var agent_id=$("#agent_id").val();
    var hotel_id=$("#hotel_id").val();
    var booking_reference=$("#booking_reference").val();
    if(fromdate != '' || todate != '' || agent_id != '' || hotel_id != '' || booking_reference != ''){
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }
}); 


</script>

