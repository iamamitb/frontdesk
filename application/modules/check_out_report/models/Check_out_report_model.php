<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check_out_report_model extends CI_Model {
		
	function getreport($account_id,$search=array(),$csv=0,$travelAgent=0){

	 	$row=false;

		$sql="SELECT b.booking_id,agent_id,booking_reference,hotel_name,name,agent_id,phone,booking_date,grand_total,advance,(grand_total-advance) as due,
checkin_date,checkout_date,plan_type_id,GROUP_CONCAT(room_number) as room_number,
GROUP_CONCAT(room_name) as room_name,COUNT(room_number) as no_of_room from hotels as h 
								LEFT JOIN `bookings` as b on h.hotel_id=b.hotel_id
								LEFT JOIN  `booked_room_detail` as brd on b.booking_id=brd.booking_id
								LEFT JOIN  `booked_room` as br on brd.booked_room_id=br.booked_room_id
								LEFT JOIN `transaction` as tran on b.booking_id=tran.booking_id
								LEFT JOIN `rooms` as r on r.room_id=br.room_id
								LEFT JOIN `guest_detail` as gd on b.guest_id=gd.guest_id
								where h.account_id=1 AND b.booking_status='4' "; 		
								
		/*if($booking_id!=0)
		{
			$sql.=" and b.booking_id=$booking_id ";
		}*/

		if($travelAgent!=0)
		{
			$sql.=" and b.agent_id=$travelAgent ";
		}

		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}
		$sql.=" GROUP BY b.booking_id";
		$query = $this->db->query($sql);
		
		if($csv!=0){
			download_report($query,"check_out_report.csv");		 
		}
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}			
			return $row;
		}
		
		
	}	






}
?>