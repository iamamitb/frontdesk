<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_out_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('check_out_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index()
	{
		
		$search = array();
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		$travelAgent=trim($this->input->post('travel_agent_id'));
		if($action=='filter')
		$search=$_POST['filter'];

		$this->template->title('Check Out Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$data['all_bookings']=$this->check_out_report_model->getreport($account_id,$search,$csv,$travelAgent);
		$this->template->build('check_out_report',$data);
		
	}

	public function print_report(){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
			
		$search=$_POST['filter'];
		$data['all_bookings']=$this->check_out_report_model->getreport($account_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}
	

}
