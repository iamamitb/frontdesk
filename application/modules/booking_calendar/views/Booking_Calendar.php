<link href="<?=base_url()?>themes/css/views/booking_calendar.css" rel="stylesheet" type="text/css">
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
--><!-- Page content starts -->  
<div class="row">
	<section class="top-calender-bar">
    <input type="hidden" id="no_of_hotels_hidden" value="<?=count_rows("hotels", "account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'")?>"> 
   <?php if(!empty($hotel_id)) { ?>
     <input type="hidden" id="no_of_room" value="<?=count_rows("rooms", "hotel_id = ".$hotel_id." and room_status ='1'")?>">  
     <?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<div class="calendar-months">
					<span class="glyphicon glyphicon-calendar"></span>
					<span class="month-text"><?=$months?> <?=$year?></span>
                    <form method="post" action=""> 
                    <input type="hidden" name="prevmonth" value="<?=$prevMonth?>">
                    <input type="hidden" name="prevyear" value="<?=$prevYear?>">
                    <input type="hidden" name="nextmonth" value="<?=$nextMonth?>">
                    <input type="hidden" name="nextyear" value="<?=$nextYear?>">
                    <input type="hidden" name="hotel_id" value="<?=$hotel_id?>">

                <div class="previous_next_buttons">
					<input type="submit" name="prev" class="previous fa-arrow-left" value="Prev"> 
					<input type="submit" name="next" class="next fa-arrow-right" value="Next"> 
				</div>

               <!--  <span class="glyphicon glyphicon-triangle-left"> <input type="submit" name="prev" value="Prev"></span>
				<span class="glyphicon glyphicon-triangle-right"> <input type="submit" name="next" value="Next"> </span>   -->         
				</form>
					
				</div>
				<ul class="select-month-hotel">
                 <input type="hidden" name="days_in_month" value="<?=$days_in_month?>">
              	<li>
                     <form method="post" action="" id="my_from"> 
                       <input type="hidden" name="dt" value="<?=$selectdate?>">
                     <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'",$hotel_id,'hotel_id','hotel_id','style="margin-left:50%;margin-top:0px;float:left;width:40%;background: transparent none repeat scroll 0 0;color: white;"', "hotel_name"); ?>
                     </form>						
					</li>
				</ul>
			</div>
			<div class="col-md-4">
				<ul class="indication-list">
               		<li><span class="booked-dot"></span>Booked Room <strong><?=array_sum($booked_room)?></strong></li>
					<li><span class="occupied-dot"></span>Occupied <strong><?=array_sum($occupied)?></strong></li>
					<li><span class="onhold-dot"></span>On Hold <strong><?=array_sum($hold)?></strong></li>
					<li><span class="maintenance-dot"></span>Maintenance <strong><?=array_sum($maintenance)?></strong></li>
					<!--<li><span class="available-dot"></span>Available <strong>399</strong></li>-->
				</ul>
			</div>
			<div class="col-md-3 text-center">
				<button class="book-btn sa-alert" data-toggle="modal">Book Room</button>
			</div>
		</div>
	</div>
</section>


<div class="container calendar-box">
	<div class="row">
		<div class="col-md-12">
		<!-- //calendar-grid start -->
			<div id="redips-drag">
			  <div id="g123">
			    <table id="view">
			      <colgroup>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			      </colgroup>
			    	<thead>
			    		<tr class="gray-row">
			    			<th class="all-collapse thin-head basecell redips-mark expand_all" id="" data-value="0">Expand All Room Types <span class="collapse-arrow"></span></th>
      <?php 
		$day_num = 1;
		$day_num1 = 1;
		while ( $day_num <= $days_in_month ){  
		$today=strtotime($day_num."-".$month."-".$year);
		$day = date('D', $today);
		if($day !='Sat' && $day !='Sun')
		echo '<th class="basecell redips-mark"><span class="day-text">'.$day.'</span>'.sprintf('%02d', $day_num).'</th>';
		else
		echo '<th class="day-sun-sat basecell redips-mark"><span class="day-text">'.$day.'</span>'.sprintf('%02d', $day_num).'</th>';
		$day_num++; 
		}
	?>		    	</tr>
			    	</thead>
			    	<tbody>
			    		<tr class="gray-row" id="total_daily_availibility">
			    			<th class="basecell redips-mark thin-text">Total Daily Availibility</th>
			 <?php	for($day_num2 = 1;$day_num2 <= $days_in_month;$day_num2++ ){
				 	echo '<td class="basecell redips-mark availibility" ></td>';} ?>			    
			    		</tr>
			<?php foreach($room_info as $key=>$val){
						$room='room_number'.$key;
						$room_name = str_replace(' ', '-', $val->room_name); ?>
			    		<tr>
			    		<th class="basecell redips-mark room-status-bar arrow-mover" colspan="32" data-toggle="collapse" data-target=".<?=$room_name?>">
			    				<h4><?=$val->room_name?></h4>
			    				<ul>
                    <li>Booked Room <strong><?php echo $br=(isset($booked_room[$val->room_id]))? $booked_room[$val->room_id] : 0;?></strong></li>
                    <li>Occupied <strong><?php echo $o=(isset($occupied[$val->room_id]))? $occupied[$val->room_id] : 0;?></strong></li>
                    <li>On Hold <strong><?php echo $h=(isset($hold[$val->room_id]))? $hold[$val->room_id] : 0;?></strong></li>
                    <li>Maintenance <strong><?php echo $m =(isset($maintenance[$val->room_id]))? $maintenance[$val->room_id] : 0;?></strong></li>
			    	<!--<li><h5>Rs 8903786</h5></li>-->
			    				</ul>
			    				<span class="collapse-arrow"></span>
			    			</th>
			    		</tr>
			    		<tr class="all-open-close collapse in room_name" id="<?=$room_name?>">
			    			<th class="basecell redips-mark thin-text font12">Room type Availibility (day)</th>
			    	<?php	for($day_num2 = 1;$day_num2 <= $days_in_month;$day_num2++ ) {
							echo '<td class="basecell redips-mark availibility"></td>';} ?>	
			    		</tr>
				  <?php foreach($$room as $val){  ?>
				      <tr class="all-open-close <?=$room_name?> collapse in">
				      	<th class="basecell redips-mark thin-text font18">Room <?=$val->room_number?></th>
                        <?php  while ( $day_num1 <= $days_in_month ){
								//$cell="initcell";
								?>
 
				      	<td class="initcell" day="<?=$day_num1?>" room_n="<?=$val->room_number?>" id="<?=$val->room_number?><?=$day_num1?>"><?=$val->room_number?><?=$day_num1?></td>
                          <?php $day_num1++; } $day_num1=1;  ?>
                         </tr>
                 <?php }  ?>
                  <tr>
			        	<td colspan="32" class="divider-collapse basecell redips-mark"></td>
			        </tr>
                    <?php } ?>
			    	</tbody>
			    </table>
			    <table id="header-fixed" class="table-common">
			    	<colgroup>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			    		<col/>
			      </colgroup>
			    </table>
			  </div>
			</div>
		</div>
	</div>
</div>
</div> <!-- End row -->
<!-- Page content ends --> 
<!-- Customer Information Modal Starts -->
 <div id="customer-info-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none">
     <!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Customer Information Modal Ends -->
<!-- Booking Form Modal Starts -->
<div id="book-room-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;"><iframe width="100%" height="100%"  src="<?php //echo site_url('central_reservation');?>">>   </iframe>
</div><!-- /.modal -->
<!-- Booking Form Modal Ends -->

<!-- Travel Agent Modal --> 
<!--<div id="travel-agent-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        
                                                        <button type="button" class="btn btn-icon btn-danger m-b-5" data-dismiss="modal" aria-hidden="true" style="float:right;">×</button> 
                                                        <h3 class="modal-title">Add New Travel Agent</h3> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Agency Name</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder="John"> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Address</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder="Doe"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Phone Number</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder="John"> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Phone Number</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder="Doe"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Email address</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder="John"> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Website</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder="Doe"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Contact Person</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder="John"> 
                                                                </div> 
                                                            </div> 
                                                            
                                                        </div> 
                                                        
                                                        
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light">Add Travel Agent</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>--><!-- /.modal -->

<!-- End of Travel Agent Modal -->
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>themes/js/views/booking_calendar.js"></script>
<script src="<?=base_url()?>themes/plugins/jquery-ui/jquery-ui.min.js"></script>
<script>
"use strict";
var test_data=<?=$getbookinginfo?>;
console.log(test_data);
//var test_data =bookinginfo;*/
var color_map = {"booked": "booked", "occupied": "occupied", "onhold" : "onhold","maintenance" : "maintenance"};
var current_data = {};  // this will hold the current view of the data
function render_data(data){ // called only once, during init
    for(var obj in data){
        color_cells(data, obj);
    }
}

</script>
<style>
.availibility{
	font-family: "Noto Sans",sans-serif;
	color: #666666 !important;
    font-size: 15px;
	font-weight:700;}
</style>