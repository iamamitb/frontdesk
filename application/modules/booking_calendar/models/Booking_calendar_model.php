<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_calendar_model extends CI_Model {
function getBookingInfo($month,$year,$hotel_id=0)
	{
		
		$row=array();
		$row1=array();
		$r='';
		$sql="SELECT b.booking_status,b.booking_id,GROUP_CONCAT(CONCAT(`room_number`,DAY(stay_date)) SEPARATOR ',') as locator 
				FROM booked_room_stay_date as brsd 
				JOIN booked_room as br on brsd.booked_room=br.id 
				JOIN booked_room_detail as brd on br.booked_room_id=brd.booked_room_id 
				JOIN bookings as b on b.booking_id=brd.booking_id 
				WHERE MONTH(`stay_date`) = $month AND YEAR(`stay_date`) = $year and hotel_id=$hotel_id GROUP BY b.booking_id";
$query = $this->db->query($sql);
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $key=>$val)
					{
						switch ($val->booking_status) { 
							case 1: 
								$type = 'booked';
							break; 							
							case 2: 
								$type = 'onhold';
							break;							
							case 3: 
								$type = 'occupied';
							break; 
							case 4: 
								$type = 'occupied';
							break; 
							}
						//$locator=()
						//$row[]=$val->booking_id;
						$row[$val->booking_id]['locator']=explode(",",$val->locator);
						$row[$val->booking_id]['type']= $type;		
					}
					
				}
		$sql_m="SELECT rm.maintenance_id,GROUP_CONCAT(CONCAT(`room_number`,DAY(date)) SEPARATOR ',') as locator from room_maintenance as rm
				join room_id_maintenance as rim on rm.maintenance_id=rim.maintenance_id
                join room_maintenance_date as rmd on rm.maintenance_id=rmd.maintenance_id
				join room_no_maintenance as rnm on rnm.room_maintenance_id=rim.room_maintenance_id
                WHERE MONTH(`date`) = $month AND YEAR(`date`) = $year and hotel_id=$hotel_id GROUP by hotel_id ";
			$query_m = $this->db->query($sql_m);
				if($query_m->num_rows() > 0)
				{
					foreach($query_m->result() as $key=>$val)
					{
						$row1['m'.$val->maintenance_id]['locator']=explode(",",$val->locator);
						$row1['m'.$val->maintenance_id]['type']= "maintenance";
					}
				}
			$result = $row+$row1;
			return json_encode($result);
		}
		function getNoOfRoom($month,$year,$hotel_id=0,$booking_status=""){
		$row=array();	
		$sql="SELECT count(stay_date) as total,br.room_id FROM booked_room_stay_date as brsd
				JOIN booked_room as br on brsd.booked_room=br.id
				JOIN booked_room_detail as brd on br.booked_room_id=brd.booked_room_id 
				JOIN bookings as b on b.booking_id=brd.booking_id
				WHERE MONTH(`stay_date`) = $month AND YEAR(`stay_date`) = $year and hotel_id=$hotel_id AND b.booking_status=$booking_status
				GROUP by br.room_id";
				$query = $this->db->query($sql);
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $key=>$val)
					{
							$row[$val->room_id]= $val->total;	
	
					}
				}
				return $row;
		}
		
		function getMaintenanceCount($month,$year,$hotel_id=0){
			$row=array();	
			$sql="SELECT  COUNT(date) as total,rim.room_id from room_maintenance as rm
				join room_id_maintenance as rim on rm.maintenance_id=rim.maintenance_id
                join room_maintenance_date as rmd on rm.maintenance_id=rmd.maintenance_id
				join room_no_maintenance as rnm on rnm.room_maintenance_id=rim.room_maintenance_id
                WHERE MONTH(`date`) = $month AND YEAR(`date`) = $year and hotel_id=$hotel_id
				GROUP by rim.room_id";
				$query = $this->db->query($sql);
				if($query->num_rows() > 0)
				{
					foreach($query->result() as $key=>$val)
					{
									$row[$val->room_id]= $val->total;	
	
					}
				}
				return $row;
		}
		
		function getGuestInfo($booking_id){
			$result=array();
			$sql="select * from bookings as b join guest_detail as gd on b.guest_id=gd.guest_id WHERE b.booking_id = $booking_id";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
				{
					foreach($query->result() as $key=>$val)
					{
			$id_card_type=(!empty($val->id_card_type))? $val->id_card_type." - ".$val->id_card_number : " " ;
			$img=(!empty($val->guest_image))? $val->guest_image : "user.png";
			$agent_name=(!empty($val->agent_id))? getValue("agents", "agent_name", "agent_id = ".$val->agent_id) : "Direct Booking ";
			$result['item']='<div class="modal-dialog" style="width:85%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="custom-width-modalLabel">Customer Information</h4>
            </div>
            <div class="modal-body">
            		<div class="row user-profile-box mt40">
		<div class="col-md-5">
			<div class="media paddingl30">
			  <div class="media-left">
			    <a href="javascript:void(0)">
			      <img class="media-object" style="border-radius: 50%;" src="assets/guest_image/'.$img.'" >
			    </a>
			  </div>
			  <div class="media-body">
			    <h5 class="media-heading">Customer Details</h5>
			    <address>
			    	<p>'.$val->name.'</p>
			    	<p><a href="tel:'.$val->phone.'">'.$val->phone.'</a></p>
			    	<p><a href="mailto:'.$val->email.'">'.$val->email.'</a></p>
			    	<p>'.$id_card_type.'</p>
			    	<p>'.$val->address.'</p>
			    </address>
			  </div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="row">
				<div class="col-sm-12">
					<h5 class="media-heading">Booking Information</h5>
				</div>
				<div class="col-sm-6">
			    <p><strong>Checkin</strong></p>
			    <p>'.dateformate($val->checkin_date).'</p>
			    <p><strong>Special Instructions/Requests</strong></p>
			    <p>'.$val->billing_instruction.'</p>
				</div>
				<div class="col-sm-6">
					
			    	<p><strong>Agent Name</strong></p>
			    	<p>'.$agent_name.'</p>
			    	<p><strong>Payment Mode / Status</strong></p>
			    	<p>'.$val->payment_type.'</p>
			    	<p><strong>Payment Amount</strong></p>
			    	<p><i class="fa fa-rupee"></i> '.$val->total_amount.'</p>
			    
				</div>
			</div>
		</div>
	</div>
               
            </div>
            <div class="modal-footer">
            	<div class="row">
					<div class="col-md-12 text-center mtb25">
						<button class="cancle-btn">Cancel Booking</button>
						<button class="edit-btn">Edit Booking</button>
					</div>
				</div>
                
            </div>
        </div>
    </div>';
					
					}
					$result['type']="success";
				}
				else{
					$result['type']="error";
				}
				// $result['item'];
			echo json_encode($result);
		}
}
?>