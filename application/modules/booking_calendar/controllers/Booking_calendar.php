<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_calendar extends MX_Controller {
	
	    public function __construct(){
        parent::__construct();
		$this->load->model('booking_calendar_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in();
		is_privileges();
   		}

	public function index(){
		if(!empty($_POST['prev'])){
			$data['selectdate']="1-".$_POST['prevmonth']."-".$_POST['prevyear'];
		}
		else if(!empty($_POST['next'])){
			$data['selectdate']="1-".$_POST['nextmonth']."-".$_POST['nextyear'];
		}
		else if(!empty($_POST['dt'])){
			$data['selectdate']=$this->input->post('dt');
		}		
		else{
			$data['selectdate']=date ('d-m-Y');
		}
		if(!empty($_POST['hotel_id'])){
			$hotel_id=$this->input->post('hotel_id');
		}
		else{
			$hotel_id=0;
		}
		$today=strtotime(date('d-m-Y'));
		$data['currentmonth']=date('m',$today);  
		$data['currentdate']=date('d',$today); 
		$date =strtotime($data['selectdate']);
		$data['hotel_id']=$hotel_id;
		$data['months'] = date('F', $date) ;
		$month = date('m', $date) ;
		$data['month']=$month;  
		$year = date('Y', $date) ;
		$data['year']=$year;
		$prevDate = strtotime('last month', $date);
		$nextDate = strtotime('next month', $date);
		$data['prevMonth'] = date('m', $prevDate);
		$data['prevYear'] = date('Y', $prevDate);
		$data['nextMonth'] = date('m', $nextDate);
		$data['nextYear'] = date('Y', $nextDate);
		$first_day = mktime(0,0,0,$month, 1, $year) ; 
		$data['days_in_month'] = cal_days_in_month(0, $month, $year);
		$data['booked_room']=$this->booking_calendar_model->getNoOfRoom($month,$year,$hotel_id,1);
		$data['hold']=$this->booking_calendar_model->getNoOfRoom($month,$year,$hotel_id,2);
		$data['occupied']=$this->booking_calendar_model->getNoOfRoom($month,$year,$hotel_id,3);
		$data['maintenance']=$this->booking_calendar_model->getMaintenanceCount($month,$year,$hotel_id);
		
		$data['room_info'] =getAllFields('rooms', "room_name ,room_id","hotel_id = ".$hotel_id ." and room_status = '1'");
			foreach($data['room_info'] as $key => $val ){
				$data['room_number'.$key]=getAllFields('room_number',"room_number","room_id =".$val->room_id);
			}
		$data['getbookinginfo']=$this->booking_calendar_model->getBookingInfo($month,$year,$hotel_id);
		
				
		$this->template->title('Booking Calendar','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('booking_calendar_template','front');
		$this->template->build('booking_calendar',$data);

				
	}
	function getGuestInfo($booking_id=0){		
		$booking_id=$this->input->post('booking_id');
		$this->booking_calendar_model->getGuestInfo($booking_id);
	}
	

}
