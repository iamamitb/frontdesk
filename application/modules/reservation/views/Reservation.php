<?php
	$mobile = array(
		'name'	=> 'mobile',
		'id'	=> 'mobile',
		'value' => set_value('mobile'),
		'maxlength' => "10"	,
		'placeholder' => 'Mobile Number',
		// 'required' => '',
		'class' => 'form-control'
	);		
	$booking_id = array(
		'name'	=> 'booking_id',
		'id'	=> 'booking_id',
		'value' => set_value('booking_id'),
		'maxlength' => "10"	,
		'placeholder' => 'Booking ID',
		// 'required' => '',
		'class' => 'form-control'
	);		
	$guest_name = array(
		'name'	=> 'guest_name',
		'id'	=> 'guest_name',
		'value' => set_value('guest_name'),
		'maxlength' => "50"	,	
		'placeholder' => 'Guest Name',
		'required' => '',
		'class' => 'form-control only_character required'
	);		
	$phone = array(
		'name'	=> 'phone',
		'id'	=> 'phone',
		'value' => set_value('phone'),
		'maxlength' => "10"	,	
		'placeholder' => 'Contact Number',
		'required' => '',
		'class' => 'form-control required'
	);		
	$address = array(
		'name'	=> 'address',
		'id'	=> 'address',
		'value' => set_value('address'),
		'maxlength' => "60"	,	
		'placeholder' => 'Address',
		'required' => '',
		'class' => 'form-control required'
	);		
	$email = array(
		'type' => 'email',
		'name'	=> 'email',
		'id'	=> 'email',
		'value' => set_value('email'),
		'maxlength' => "60"	,	
		'placeholder' => 'Email',
		'required' => '',
		'class' => 'form-control'
	);		
	$total_adults = array(
		'name'	=> 'total_adults',
		'id'	=> 'total_adults',
		'value' => set_value('total_adults'),
		'maxlength' => "3"	,	
		'placeholder' => 'Adult',
		'required' => '',
		'class' => 'form-control only_digit required'
	);		
	$total_child = array(
		'name'	=> 'total_child',
		'id'	=> 'total_child',
		'value' => set_value('total_child'),
		'maxlength' => "3"	,	
		'placeholder' => 'Child',
		'class' => 'form-control only_digit'
	);		
	$checkin_date = array(
		'name'	=> 'checkin_date',
		'id'	=> 'checkin_date',
		'value' => set_value('checkin_date'),		
		'readonly' => 'readonly',	
		'placeholder' => 'Checkin Date',
		'required' => '',
		'class' => 'form-control checkin_date required'
	);		
	$checkout_date = array(
		'name'	=> 'checkout_date',
		'id'	=> 'checkout_date',
		'value' => set_value('checkout_date'),		
		'readonly' => 'readonly',	
		'placeholder' => 'Checkout Date',
		'class' => 'form-control checkout_date required'
	);
	
	$stay_date_from = array(
		'name'	=> 'stay_date_from[]',	
		'id'	=> 'stay_date_from_1',	
		'value' => set_value('stay_date_from'),		
		'readonly' => 'readonly',	
		'placeholder' => 'Stay Date',
		'required' => '',
		'class' => 'form-control stay_date_from required'
	);	

	$stay_date_to = array(
		'name'	=> 'stay_date_to[]',
		'id'	=> 'stay_date_to_1',		
		'value' => set_value('stay_date_to'),		
		'readonly' => 'readonly',	
		'placeholder' => 'Stay Date',
		'required' => '',
		'class' => 'form-control stay_date_to required'
	);

	$submit = array(
		'id'	=> 'submit',
		'class' => 'btn btn-primary btn-lg center-block',
		'value'	=> 'Submit Booking',
	);

// php code for travel agent add...

	$travel_agent_name = array(
		'name'	=> 'travel_agent_name',
		'id'	=> 'travel_agent_name',
		'value' => set_value('travel_agent_name'),
		'maxlength' => "50"	,			
		'required' => '',
		'class' => 'form-control only_character required'
	);		
	
	$travel_agent_address = array(
		'name'	=> 'travel_agent_address',
		'id'	=> 'travel_agent_address',
		'value' => set_value('travel_agent_address'),
		'maxlength' => "60"	,			
		'required' => '',
		'class' => 'form-control only_character required'
	);		

	$travel_agent_phonenumber1 = array(
		'name'	=> 'travel_agent_phonenumber1',
		'id'	=> 'travel_agent_phonenumber1',
		'value' => set_value('travel_agent_phonenumber1'),
		'maxlength' => "10"	,			
		'required' => '',
		'class' => 'form-control required'
	);	

	$travel_agent_phonenumber2 = array(
		'name'	=> 'travel_agent_phonenumber2',
		'id'	=> 'travel_agent_phonenumber2',
		'value' => set_value('travel_agent_phonenumber2'),
		'maxlength' => "10"	,					
		'class' => 'form-control'
	);	

	$travel_agent_email_address = array(
		'type' => 'email',
		'name'	=> 'travel_agent_email_address',
		'id'	=> 'travel_agent_email_address',
		'value' => set_value('travel_agent_email_address'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);		

	$travel_agent_website = array(
		'type' => 'url',
		'name'	=> 'travel_agent_website',
		'id'	=> 'travel_agent_website',
		'value' => set_value('travel_agent_website'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);		

	$travel_agent_contact_person = array(
		'name'	=> 'travel_agent_contact_person',
		'id'	=> 'travel_agent_contact_person',
		'value' => set_value('travel_agent_contact_person'),
		'maxlength' => "60"	,
		'required' => '',			
		'class' => 'form-control only_character required'
	);		

	$travel_agent_email_address = array(
		'type' => 'email',
		'name'	=> 'travel_agent_email_address',
		'id'	=> 'travel_agent_email_address',
		'value' => set_value('travel_agent_email_address'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);
		
?>

<link href="<?php echo base_url(); ?>themes/css/views/central_reservation.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/jquery-ui-timepicker-addon.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Reservation</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

<!-- Alert shown after booking is made -->
<?php if($this->session->flashdata('booking_id')) { ?>
<div class="col-md-12" id="message" align="center" style="margin-bottom:30px;">
	<div class="alert alert-warning alert-dismissable" style="margin:0 auto;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<div class="col-md-5"><span>Well done, Your Booking is Saved.<br>Booking Reference: <?php echo $this->session->flashdata('booking_reference'); ?></span></div>
			<div class="col-md-2"><a href="#" data-toggle="modal" data-target="#send-email-modal"><i class="fa fa-envelope"></i><span>Send Email</span></a></div>
			<div class="col-md-2"><a href="#" data-toggle="modal" data-target="#send-sms-modal"><i class="fa fa-mobile"></i><span>Send SMS</span></a></div>
			<div class="col-md-2"><a href="#"><i class="fa fa-print"></i><span>Print Booking</span></a></div>
		
	</div>
</div>
<?php } ?>
<!-- Alert ends -->
<!-- Panel starts --> 
<div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <h3 class="panel-title" style="color:#000;">Make New Reservation</h3> </div> 
                                    <div class="panel-body"> 
                                        <form class="form-horizontal" role="form" method="post" >
                                        	<input type="hidden" name="guest_id" id="guest_id" value="" />
                                        			<!-- Existing customer row--> 
													
													<div class="col-md-12" align="center" style="margin-bottom:25px;"> 
	                                        			<div class="checkbox col-md-6">
                                            				<input id="ext_customer" type="checkbox" data-toggle="modal" data-target="#existing-customer-modal">
                                            				<label for="ext_customer">Existing Guest ? </label>
                                            			</div>
														<div class="col-md-6"> 
			                                                <span style="position:absolute;top:6px;">Choose Agent</span>
			                                                <div class="radio radio-success radio-inline" style="margin-left:95px;"><input type="radio" id="inlineRadio1" value="travel" name="agent" ><label for="inlineRadio1">Travel</label></div>
					                                        <div class="radio radio-success radio-inline"><input type="radio" id="inlineRadio2" value="corporate" name="agent"><label for="inlineRadio2">Corporate</label></div>
		                                        		</div>
	                                        		</div>


                                        			<!-- Existing customer row ends -->
                                        		
	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Guest Name <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($guest_name); ?></div>
                                                            <span class="error"><?php echo form_error($guest_name['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Contact number <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($phone); ?></div>
                                                            <span class="error"><?php echo form_error($phone['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Address <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($address); ?></div>
                                                            <span class="error"><?php echo form_error($address['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Email Address <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($email); ?></div>
                                                            <span class="error"><?php echo form_error($email['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Adult <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($total_adults); ?></div>
                                                            <span class="error"><?php echo form_error($total_adults['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Child</label>
		                                                	<div class="col-sm-9"><?php echo form_input($total_child); ?></div>
                                                            <span class="error"><?php echo form_error($total_child['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Hotel <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status = '1'",'','hotel_id','hotel_id','required'); ?></div>
		                                                	<span class="error"><?php echo form_error('hotel_id'); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6" id="meal_plan_type"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Meal Plan Type <span class="error">*</span> </label>
		                                                	<div class="col-sm-9" id="meal_plan"></div>
		                                                	<span class="error"><?php echo form_error('plan_type_id'); ?></span>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6 travel" id="travel_agent"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Travel Agent </label>
		                                                	<div class="col-sm-8" id="travel_agent_list"><?php echo get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id')." and agents_type = 1 and agent_status = '1'",'','travel_agent_id','travel_agent_id'); ?></div>
		                                                	<div class="col-sm-1"><button type="button" data-toggle="modal" data-target="#travel-agent-modal" class="btn btn-success">+</button></div>
		                                                	<span class="error"><?php echo form_error('travel_agent_id'); ?></span>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6 travel" id="tac_percentage"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">TAC Percentage</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="travel_agent_percentage" name="travel_agent_percentage" maxlength="2" placeholder=""></div>
	                                            		</div>
	                                        		</div>
	                                        		<div class="col-md-6 corporate" > 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Corporate </label>
		                                                	<div class="col-sm-9" ><?php echo get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id')." and agents_type = 2 ",'','corporate_agent_id','corporate_agent_id'); ?></div>
		                                                	
	                                            		</div>
	                                        		</div>
	                                        		<div class="col-md-6 corporate" > 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Corporate Percentage</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="corporate_percentage" name="corporate_percentage" placeholder="" maxlength="2"></div>
	                                            		</div> 
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Check-in Date Time <span class="error">*</span> </label>
		                                                	<div class="col-sm-9">
		                                                		<div class="input-group">
                                                                    <div class="col-sm-6"><?php echo form_input($checkin_date); ?></div>
                                                                    <div class="col-sm-6"><input type="text" name="check_in_time" id="check_in_time" value="" readonly class="form-control checkin_date" style="cursor: pointer;"></div>

	                                    							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar checkin_date"></i></span>
	                                    							<span class="error"><?php echo form_error($checkin_date['name']); ?></span> 

	                                            				</div>
	                                        				</div> 
                                						</div>
                                					</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Check-out Date Time <span class="error">*</span> </label>
		                                                	<div class="col-sm-9">
		                                                		<div class="input-group">
                                                                    <div class="col-sm-6"><?php echo form_input($checkout_date); ?></div>
                                                                    <div class="col-sm-6"><input type="text" name="check_out_time" id="check_out_time" value="" readonly class="form-control checkout_date" style="cursor: pointer;"></div>
                                                                    
	                                    							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar checkin_date"></i></span>
	                                    							<span class="error"><?php echo form_error($checkout_date['name']); ?></span> 

	                                            				</div>
	                                        				</div> 
                                						</div>
                                					</div>

                                					<!-- Start Checkin details panel -->  
                                        			
                                        			<div class="col-md-12" > 
						                                <div class="panel panel-border panel-primary">
						                                    <div class="panel-heading"> 
						                                        <h3 class="panel-title" style="text-align: center;font-size: 20px;">Select Room for Checkin</h3> 
						                                    </div> 
						                                    <div class="panel-body"> 
						                                        
						                                        <!-- Row for Checkin room starts. Repeat this row with JS if client selects more than one room -->

					                                        	<input type="hidden" value="1" id="no_of_room" name="no_of_room" >
					                                        	
						                                        <div class="row select_room" id="1"  style="background: #f4f4f4;padding-top:20px;border-radius:5px;margin-top:5px;">
							                                        <input type="hidden" value="" id="room_no_1" name="room_no[]" >							                                        
							                                        <div class="row">
						                                        		<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Type <span class="error">*</span> </label>
								                                                	<div class="col-sm-6" id="room_type"></div>

							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Number of Rooms <span class="error">*</span> </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control" id="no_of_rooms_1" name="no_of_rooms[]" readonly required="" placeholder=""></div>
								                                                	<span class="error"><?php echo form_error('no_of_rooms[]'); ?></span> 
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Rate <span class="error">*</span> </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control" id="room_rate_1" name="room_rate[]" readonly required="" placeholder=""></div>
								                                                	<span class="error"><?php echo form_error('room_rate[]'); ?></span>
							                                            		</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                                <span style="position:absolute;top:6px;">Occupancy <span class="error">*</span> </span>
								                                                <div class="radio radio-success radio-inline" style="margin-left:80px;"><input type="radio" id="inlineRadio3" value="single" name="occupancy[1][]" checked="checked" onclick="occupancy(1);"><label for="inlineRadio3">Single</label></div>
										                                        <div class="radio radio-success radio-inline"><input type="radio" id="inlineRadio4" value="double" name="occupancy[1][]" onclick="occupancy(1);"><label for="inlineRadio4">Double</label></div>
										                                        <span class="error"><?php echo form_error('occupancy[]'); ?></span>
							                                        		</div>
						                                        		</div>
						                                        	</div>
							                                        	
							                                        <div class="row">
							                                        	<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Net Rate <span class="error">*</span> </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="net_rate_1" name="net_rate[]" placeholder="" maxlength="5" ></div>
								                                                	<span class="error"><?php echo form_error('net_rate[]'); ?></span> 
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_1" name="extra_bed[]" placeholder="" maxlength="5"></div>
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed Price</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_price_1" name="extra_bed_price[]" placeholder="" maxlength="5"></div>
							                                            		</div>
							                                        		</div>


							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_1" name="extra_person[]" placeholder="" maxlength="5"></div>
							                                            		</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>

						                                        	<div class="row">
						                                        		<div class="col-md-12">
											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person Charge</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_charge_1" name="extra_person_charge[]" placeholder="" maxlength="5"></div>
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Luxury Tax (%)</label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="luxury_tax_1" name="luxury_tax[]" placeholder="" maxlength="2"></div>
								                                            	</div>
							                                        		</div>

							                                        		<div class="col-md-6">
							                                        			<div align="center"><button type="button" class="btn btn-success m-b-5 add_more_room" style="">Add one more Room</button></div>
							                                        		</div>

						                                        		</div>
						                                        	</div>                                        	
						                                        	<div class="row">
						                                        		<div class="col-md-12">
											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date From <span class="error">*</span></label>
								                                                	<div class="col-sm-6"><?=form_input($stay_date_from); ?></div>
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date To <span class="error">*</span> </label>
									                                                	<div class="col-sm-6"><?=form_input($stay_date_to); ?></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Selected Room Number</label>
									                                                	<div class="col-sm-6" id="display_room_no_1"></div>
								                                            	</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>                                        	
							                                        
						                                    	</div>
						                                        <!-- Row for One room checkin ends -->				                                        

						                                        <div id="add_room"></div>
						                                        <!-- Row for One room checkin ends -->


						                                    </div> 
						                                </div>

                                        			</div>

                                					<!-- End Checkin details panel -->

                                        			<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Booking Status <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><select class="form-control" name="booking_status" id="booking_status" required ><option value="">Select</option><option value="1">Book</option><option value="2">Hold</option></select></div>
		                                                	<span class="error"><?php echo form_error('booking_status'); ?></span>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Total Room Tariff <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="total_room_tariff" name="total_room_tariff" readonly required="" maxlength="10" placeholder=""></div>
		                                                	<span class="error"><?php echo form_error('total_room_tariff'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-3"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Discount Type</label>
		                                                	<?php 
		                                                	$percentage = "";
		                                                	$cash = "";
		                                                	
		                                                	if($booking_detail->discount_type == "percentage")
		                                                		$percentage = "checked='checked'";
		                                                	if($booking_detail->discount_type == "cash")
		                                                		$cash = "checked='checked'";
		                                                	?>
			                                                <div class="radio radio-success radio-inline" ><input type="radio" id="inlineRadio5" value="percentage" name="discount_type" <?=$percentage?> ><label for="inlineRadio5">Percentage(%)</label></div>
					                                        <div class="radio radio-success radio-inline"><input type="radio" id="inlineRadio6" value="cash" name="discount_type" <?=$cash?> ><label for="inlineRadio6">Cash</label></div>		                                                	
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-3" id="discount"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Discount Amount</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="discount_amount" name="discount_amount" placeholder="" maxlength="5" value="<?=$booking_detail->discount_amount?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6">  
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Luxury Tax Amount (Rs.)</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="luxury_tax_amount" name="luxury_tax_amount" readonly placeholder=""></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Service Tax %</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="service_tax" name="service_tax" maxlength="2" placeholder="" ></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Service Charge %</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="service_charge" name="service_charge"  placeholder="" maxlength="2"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Tax Amount</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="tax_amount" name="tax_amount" maxlength="10" readonly placeholder=""></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Total Amount <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="total_amount" name="total_amount" maxlength="10" required="" readonly placeholder=""></div>
		                                                	<span class="error"><?php echo form_error('total_amount'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Misc Charge</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="misc_charge" name="misc_charge" maxlength="10" placeholder=""></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Grand Total <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="grand_total" name="grand_total" required="" maxlength="10" readonly placeholder=""></div>
		                                                	<span class="error"><?php echo form_error('grand_total'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Advance</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="advance" name="advance" maxlength="10" placeholder=""></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Due</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="due" name="due" readonly maxlength="10" placeholder=""></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Remarks</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control" id="remarks" placeholder="" name="remarks"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Collection Point <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo get_data_dropdown("collection_point","collection_point_id","collection_point_name","account_id = ".$this->session->userdata('account_id'),'','collection_point_id','collection_point_id','required'); ?></div>
		                                                	<span class="error"><?php echo form_error('collection_point_id'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6 advance_payment"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Payment Type <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?=get_payment_type_list('','') ?></div>
		                                                	<span class="error"><?php echo form_error('payment_type'); ?></span> 
	                                            		</div>
	                                        		</div>
                                                    <div class="col-md-6 advance_payment"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="col-sm-3 control-label">Payment Date</label> 
                                                            <div class="col-sm-9"><input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="Date" readonly=""> </div>
                                                        </div> 
                                                    </div> 

	                                        		<div class="col-md-6" > 
	                                        			<div id="cheque_details_central_reservation" >
		                                        				<div class="col-md-3" id="chq_details_central_reservation"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Cheque Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" id="cheque_no" name="cheque_no" placeholder="Cheque Number" maxlength="10" > 

	                                                                </div> 
	                                                            </div> 
		                                        				<div class="col-md-3" id="dd_details_central_reservation"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">DD Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" id="dd_no" name="dd_no" placeholder="DD Number" maxlength="10"> 
	                                                                </div> 
	                                                            </div>                
                                                        </div>	       
                                                        <div class="col-md-3 bank_name_status"> 
                                                            <div class="form-group"> 
                                                                <label for="field-2" class="control-label">Bank Name <span class="error">*</span> </label> 
                                                                <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name"> 
                                                            </div> 
                                                        </div>                                                                      			
	                                        			<div id="cc_details_central_reservation" >
		                                        				<div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Card Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" name="card_no" id="card_no" placeholder="Card Number" maxlength="10"> 
	                                                                </div> 
	                                                            </div> 
	                                                            <div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-2" class="control-label">Expiry Date <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control" name ="expiry_date" id="expiry_date" placeholder="Expiry Date mm/yy"> 
	                                                                </div> 
	                                                            </div> 	                                                            
                                                        </div>
	                                        			<div id="neft_details_central_reservation" >
		                                        				<div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Account Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" name="account_no" id="account_no" placeholder="Account Number" maxlength="10"> 
	                                                                </div> 
	                                                            </div> 
	                                                            <div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-2" class="control-label">IFSC Code</label> 
	                                                                    <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="IFSC Code"> 
	                                                                </div> 
	                                                            </div> 	                                                            
                                                        </div>
	                                        		</div>
													<div class="col-md-12">
	                                        			<div class="form-group">
			                                                <label class="col-md-2 control-label">Billing Instructions</label>
			                                                <div class="col-md-10">
			                                                    <textarea class="form-control" rows="5" name="billing_instruction" id="billing_instruction"></textarea>
			                                                </div>
			                                            </div>

	                                        		</div>	                                        		
                                            
                               				<div align="center"><div class="col-md-12" style="margin-top:45px;"> <span class="error">*</span> Represents mandatory fields. <input type="submit" name="submit" id="submit" value="Submit Booking" class="btn btn-success btn-lg m-b-5" /> </div></div>
                                        </form>
                                    
                                    </div>
                                </div> <!-- End of panel panel-default-->
</div>

<!-- Panel Ends -->



</div> <!-- End row -->
<!-- Page content ends -->  


<!-- Room Number Selection modal --> 
 <div id="room-number-selection-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Choose A Room</h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                        	<div align="center" id="room_number"></div>
                                                        </div> 
                                                        
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light" id="select_room">Select Room</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

<!-- End of Room Number selection Modal -->


<!-- Existing customer modal --> 
 <div id="existing-customer-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close existing_customer" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Enter Guest's Mobile Number </h3> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Mobile Number</label> 
																	<?php echo form_input($mobile); ?>
						                                            <span class="error"><?php echo form_error($mobile['name']); ?></span> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                        
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect existing_customer" data-dismiss="modal">Cancel</button> 
                                                        <input type="submit" name="fill_guest_details" id="fill_guest_details" value="Fill Details From System" class="btn btn-success" /> 
                                                        <!-- <button type="button" class="btn btn-success waves-effect waves-light" id="fill_guest_details">Fill Details From System</button> --> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->                                      

<!-- End of Existing customer Modal -->

<!-- Travel Agent Modal --> 
 <div id="travel-agent-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Add New Travel Agent</h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Agency Name <span class="error">*</span></label>
                                                                    <?php echo form_input($travel_agent_name); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_name['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Address <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_address); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_address['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Phone Number 1 <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_phonenumber1); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_phonenumber1['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Phone Number 2</label> 
                                                                    <?php echo form_input($travel_agent_phonenumber2); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_phonenumber2['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Email address</label> 
                                                                    <?php echo form_input($travel_agent_email_address); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_email_address['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Website</label> 
                                                                    <?php echo form_input($travel_agent_website); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_website['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Contact Person <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_contact_person); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_contact_person['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            
                                                        </div> 
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light" id="add_travel_agent">Add Travel Agent</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->



<!-- End of Travel Agent Modal -->

<!-- Send SMS Modal --> 
 <div id="send-sms-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Send an SMS to Customer's Mobile Number </h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-12"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label"><b>Mobile Number</b></label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-12">
	                                                            <div class="form-group no-margin"> 
	                                                                    <label for="field-7" class="control-label"><b>SMS Content</b></label> 
	                                                                    <textarea class="form-control autogrow" id="field-7" placeholder="" style="height:80px;">Dear Amit Baneri, Your booking on 0000-00-00 in Anutri Beach Resort is confirmed. Contact us on 7439027269 / 9903995948.</textarea> 
	                                                                </div>
	                                                        	</div> 
	                                                        	
                                                        	</div>
                                                        
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light">Send SMS</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

<!-- End of Send SMS Modal -->

<!-- Send Email Modal -->

<div id="send-email-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog modal-full">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h2 class="modal-title" id="custom-width-modalLabel">Send Email To Customer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!-- Start Compose Email Panel -->
                                                        <div class="panel panel-default m-t-20">
						                                    <div class="panel-body p-t-30">
						                                        
						                                        <form role="form">
						                                            <div class="form-group">
						                                                <input type="email" class="form-control" placeholder="To">
						                                            </div>
						                                            <div class="form-group">
						                                                <div class="row">
						                                                    <div class="col-md-6">
						                                                        <input type="email" class="form-control" placeholder="Cc">
						                                                    </div>
						                                                    <div class="col-md-6">
						                                                        <input type="email" class="form-control" placeholder="Bcc">
						                                                    </div>
						                                                </div>
						                                            </div>
						                                            <div class="form-group">
						                                                <input type="text" class="form-control" placeholder="Subject">
						                                            </div>
						                                            <div class="form-group">
						                                                <ul class="wysihtml5-toolbar"><li class="dropdown"><a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-font"></i>&nbsp;<span class="current-font">Normal text</span>&nbsp;<i class="fa fa-angle-down"></i></a><ul class="dropdown-menu"><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="div" tabindex="-1" href="javascript:;" unselectable="on">Normal text</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" tabindex="-1" href="javascript:;" unselectable="on">Heading 1</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" tabindex="-1" href="javascript:;" unselectable="on">Heading 2</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h3" tabindex="-1" href="javascript:;" unselectable="on">Heading 3</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h4" href="javascript:;" unselectable="on">Heading 4</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h5" href="javascript:;" unselectable="on">Heading 5</a></li><li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h6" href="javascript:;" unselectable="on">Heading 6</a></li></ul></li><li><div class="btn-group"><a class="btn btn-default" data-wysihtml5-command="bold" title="CTRL+B" tabindex="-1" href="javascript:;" unselectable="on">Bold</a><a class="btn btn-default" data-wysihtml5-command="italic" title="CTRL+I" tabindex="-1" href="javascript:;" unselectable="on">Italic</a><a class="btn btn-default" data-wysihtml5-command="underline" title="CTRL+U" tabindex="-1" href="javascript:;" unselectable="on">Underline</a></div></li><li><div class="btn-group"><a class="btn btn-default" data-wysihtml5-command="insertUnorderedList" title="Unordered list" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-list-ul"></i></a><a class="btn btn-default" data-wysihtml5-command="insertOrderedList" title="Ordered list" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-list-ol"></i></a><a class="btn btn-default" data-wysihtml5-command="Outdent" title="Outdent" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-indent"></i></a><a class="btn btn-default" data-wysihtml5-command="Indent" title="Indent" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-dedent"></i></a></div></li><li><div class="bootstrap-wysihtml5-insert-link-modal modal fade"> <div class="modal-dialog"> <div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal"></a><h4 class="modal-title">Insert link</h4></div><div class="modal-body"><input type="text" value="http://" class="bootstrap-wysihtml5-insert-link-url1 form-control large"><label style="margin-top:5px;"> <input type="checkbox" class="bootstrap-wysihtml5-insert-link-target" checked="">Open link in new window</label></div><div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert link</a></div></div></div></div><a class="btn btn-default" data-wysihtml5-command="createLink" title="Insert link" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-share-square-o"></i></a></li><li><div class="bootstrap-wysihtml5-insert-image-modal modal fade"> <div class="modal-dialog"> <div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal"></a><h4 class="modal-title">Insert image</h4></div><div class="modal-body"><input type="text" value="http://" class="bootstrap-wysihtml5-insert-image-url form-control large"></div><div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a><a href="#" class="btn btn-primary" data-dismiss="modal">Insert image</a></div></div></div></div><a class="btn btn-default" data-wysihtml5-command="insertImage" title="Insert image" tabindex="-1" href="javascript:;" unselectable="on"><i class="fa fa-picture-o"></i></a></li></ul><textarea class="wysihtml5 form-control" style="height: 200px; display: none;" placeholder="Message body"></textarea><input type="hidden" name="_wysihtml5_mode" value="1"><iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true" frameborder="0" width="0" height="0" marginwidth="0" marginheight="0" style="border-collapse: separate; border: 1px solid rgb(238, 238, 238); clear: none; display: block; float: none; margin: 0px; outline: rgba(0, 0, 0, 0.6) none 0px; outline-offset: 0px; padding: 6px 12px; position: static; z-index: auto; vertical-align: baseline; text-align: start; box-shadow: none; border-radius: 2px; width: 769.5px; height: 200px; top: auto; left: auto; right: auto; bottom: auto; background-color: rgb(250, 250, 250);"></iframe>
						                                            </div>
						                                        </form>
						                                    </div> <!-- panel -body -->
						                                </div>

                                                        <!-- End Compose Email Panel -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary waves-effect waves-light">Send Email</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- End of Send Email Modal -->

<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery-ui-timepicker-addon.js" ></script>
<!-- <script type="text/javascript" src="http://trentrichardson.com/examples/timepicker/i18n/jquery-ui-timepicker-addon-i18n.min.js"></script> -->
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
</script>
<?php
	// we need to write a condition where we will ceck whetehr any hotel, room, meal plan and other required entity(which will be required at the time of booking) is added ot not... if its not added then we will redirect it to concenr page to add that entity first....		
	if(isset($hotel_error) && $hotel_error == 1){
?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
		        swal({
		            title: "Please add a hotel.",
		            text: "You will be redirected to a page from where you can add a hotel.",
		            type: "warning",
		            showCancelButton: false,
		            confirmButtonColor: "#DD6B55",
		            confirmButtonText: "Yes, move to add hotel page",
		            closeOnConfirm: false,
		            allowEscapeKey: false
		        }, function (isConfirm) {
		            if (!isConfirm) return;
		            $(location).attr("href", base_url+"hotel/");
		        });	            
			});
		</script>
<?php } if(isset($meal_plan_type_error) && $meal_plan_type_error == 1){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
	            swal({
	                title: "Please add a plan type for hotel.",
	                text: "You will be redirected to a page from where you can add plan type for all hotel.",
	                type: "warning",
	                showCancelButton: false,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, move to add plan type for hotel",
		            closeOnConfirm: false,
		            allowEscapeKey: false
	            }, function (isConfirm) {
	                if (!isConfirm) return;
	                $(location).attr("href", base_url+"hotel/");
	            });
			});
		</script>

<?php } if(isset($meal_plan_error) && $meal_plan_error == 1){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
	            swal({
	                title: "Please add a meal plan for hotel.",
	                text: "You will be redirected to a page from where you can add meal plan for a particular hotel.",
	                type: "warning",
	                showCancelButton: false,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, move to add meal plan for hotel",
		            closeOnConfirm: false,
		            allowEscapeKey: false
	            }, function (isConfirm) {
	                if (!isConfirm) return;
	                $(location).attr("href", base_url+"hotel/");
	            });
			});
		</script>

<?php } if(isset($collection_point_error) && $collection_point_error == 1){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
	            swal({
	                title: "Please add a collection point for hotel.",
	                text: "You will be redirected to a page from where you can add collection point for a particular hotel.",
	                type: "warning",
	                showCancelButton: false,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, move to add collection point for hotel",
		            closeOnConfirm: false,
		            allowEscapeKey: false
	            }, function (isConfirm) {
	                if (!isConfirm) return;
	                $(location).attr("href", base_url+"collection_point/");
	            });
			});
		</script>

<?php } if(isset($room_plan_price_error) && $room_plan_price_error == 1){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
	            swal({
	                title: "Please add a room plan price.",
	                text: "You will be redirected to a page from where you can add room plan price for a particular hotel.",
	                type: "warning",
	                showCancelButton: false,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, move to add room plan price",
		            closeOnConfirm: false,
		            allowEscapeKey: false
	            }, function (isConfirm) {
	                if (!isConfirm) return;
	                $(location).attr("href", base_url+"room/");
	            });
			});
		</script>

<?php } if(isset($room_error) && $room_error == 1){ ?>
		<script type="text/javascript">
			$(document).ready(function(){
				var base_url = $.cookie("base_url");
	            swal({
	                title: "Please add rooms in hotel.",
	                text: "You will be redirected to a page from where you can add rooms in a particular hotel.",
	                type: "warning",
	                showCancelButton: false,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Yes, move to add hotel room",
		            closeOnConfirm: false,
		            allowEscapeKey: false
	            }, function (isConfirm) {
	                if (!isConfirm) return;
	                $(location).attr("href", base_url+"room/");
	            });
			});
		</script>

<?php } ?>

<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/views/reservation.js" ></script>
