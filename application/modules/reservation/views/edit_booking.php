<?php
	$booking_id = array(
		'name'	=> 'booking_id',
		'id'	=> 'booking_id',
		'value' => $booking_detail->booking_id,
		'maxlength' => "10"	,
		'placeholder' => 'Booking ID',
		// 'required' => '',
		'class' => 'form-control'
	);		
	$guest_name = array(
		'name'	=> 'guest_name',
		'id'	=> 'guest_name',
		'value' => $booking_detail->name,
		'maxlength' => "50"	,	
		'placeholder' => 'Guest Name',
		'required' => '',
		'class' => 'form-control only_character required'
	);		
	$phone = array(
		'name'	=> 'phone',
		'id'	=> 'phone',
		'value' => $booking_detail->phone,
		'maxlength' => "10"	,	
		'placeholder' => 'Contact Number',
		'required' => '',
		'class' => 'form-control required'
	);		
	$address = array(
		'name'	=> 'address',
		'id'	=> 'address',
		'value' => $booking_detail->address,
		'maxlength' => "60"	,	
		'placeholder' => 'Address',
		'required' => '',
		'class' => 'form-control required'
	);		
	$email = array(
		'type' => 'email',
		'name'	=> 'email',
		'id'	=> 'email',
		'value' => $booking_detail->email,
		'maxlength' => "60"	,	
		'placeholder' => 'Email',
		'required' => '',
		'class' => 'form-control'
	);		
	$total_adults = array(
		'name'	=> 'total_adults',
		'id'	=> 'total_adults',
		'value' => $booking_detail->total_adults,
		'maxlength' => "3"	,	
		'placeholder' => 'Adult',
		'required' => '',
		'class' => 'form-control only_digit required'
	);		
	$total_child = array(
		'name'	=> 'total_child',
		'id'	=> 'total_child',
		'value' => $booking_detail->total_child,
		'maxlength' => "3"	,	
		'placeholder' => 'Child',
		'class' => 'form-control only_digit'
	);		
	$checkin_date = array(
		'name'	=> 'checkin_date',
		'id'	=> 'checkin_date',
		'value' => dateformate($booking_detail->checkin_date,"d-m-Y","-"),		
		'readonly' => 'readonly',	
		'placeholder' => 'Checkin Date',
		'required' => '',
		'class' => 'form-control required'
	);		
	$checkout_date = array(
		'name'	=> 'checkout_date',
		'id'	=> 'checkout_date',
		'value' => dateformate($booking_detail->checkout_date,"d-m-Y","-"),		
		'readonly' => 'readonly',	
		'placeholder' => 'Checkout Date',
		'class' => 'form-control checkout_date'
	);
	
	$submit = array(
		'id'	=> 'submit',
		'class' => 'btn btn-primary btn-lg center-block',
		'value'	=> 'Edit Booking',
	);

// php code for travel agent add...

	$travel_agent_name = array(
		'name'	=> 'travel_agent_name',
		'id'	=> 'travel_agent_name',
		'value' => set_value('travel_agent_name'),
		'maxlength' => "50"	,			
		'required' => '',
		'class' => 'form-control only_character required'
	);		
	
	$travel_agent_address = array(
		'name'	=> 'travel_agent_address',
		'id'	=> 'travel_agent_address',
		'value' => set_value('travel_agent_address'),
		'maxlength' => "60"	,			
		'required' => '',
		'class' => 'form-control only_character required'
	);		

	$travel_agent_phonenumber1 = array(
		'name'	=> 'travel_agent_phonenumber1',
		'id'	=> 'travel_agent_phonenumber1',
		'value' => set_value('travel_agent_phonenumber1'),
		'maxlength' => "10"	,			
		'required' => '',
		'class' => 'form-control'
	);	

	$travel_agent_phonenumber2 = array(
		'name'	=> 'travel_agent_phonenumber2',
		'id'	=> 'travel_agent_phonenumber2',
		'value' => set_value('travel_agent_phonenumber2'),
		'maxlength' => "10"	,					
		'class' => 'form-control'
	);	

	$travel_agent_email_address = array(
		'type' => 'email',
		'name'	=> 'travel_agent_email_address',
		'id'	=> 'travel_agent_email_address',
		'value' => set_value('travel_agent_email_address'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);		

	$travel_agent_website = array(
		'type' => 'url',
		'name'	=> 'travel_agent_website',
		'id'	=> 'travel_agent_website',
		'value' => set_value('travel_agent_website'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);		

	$travel_agent_contact_person = array(
		'name'	=> 'travel_agent_contact_person',
		'id'	=> 'travel_agent_contact_person',
		'value' => set_value('travel_agent_contact_person'),
		'maxlength' => "60"	,
		'required' => '',			
		'class' => 'form-control only_character required'
	);		

	$travel_agent_email_address = array(
		'type' => 'email',
		'name'	=> 'travel_agent_email_address',
		'id'	=> 'travel_agent_email_address',
		'value' => set_value('travel_agent_email_address'),
		'maxlength' => "60"	,			
		'class' => 'form-control'
	);		

	$travel_agent = "";
	$corporate_agent = "";
	// we need to write a condition where we will check whther any agent is selected or not ..if any agent is selected then which agent was selected at that time we neeed to display that agent as selected agent....
	if($booking_detail->agent_id > 0 && $booking_detail->agent_id != "") {
		// it means agent was selected..so we need to get its type..it may be travel or corporate...
		if (getValue("agents", "agents_type", "agent_id = ".$booking_detail->agent_id) == "1")
			$travel_agent = "checked='checked'";
		else if (getValue("agents", "agents_type", "agent_id = ".$booking_detail->agent_id) == "2")
			$corporate_agent = "checked='checked'"; 		// it means corporate agent is selected..		
	}
		
?>


<link href="<?php echo base_url(); ?>themes/css/views/central_reservation.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>themes/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/jquery-ui-timepicker-addon.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Central Reservation</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">
<!-- Panel starts --> 
<div class="col-md-12"> 
                                <div class="panel panel-default">
                                    <div class="panel-heading"> <h3 class="panel-title" style="color:#000;">Edit Booking</h3> </div> 
                                    <div class="panel-body"> 
                                        <form class="form-horizontal" role="form" method="post" >
                                        	<input type="hidden" name="guest_id" id="guest_id" value="<?=$booking_detail->guest_id ?>" />
                                        	<input type="hidden" name="hotel_id" id="hotel_id" value="<?=$booking_detail->hotel_id ?>" />
                                        	<input type="hidden" name="booking_id" id="booking_id" value="<?=$booking_detail->booking_id ?>" />
													
													<div class="col-md-12" align="right" style="margin-bottom:25px;">
		                                                <span style="position:absolute;top:6px;">Choose Agent</span>
		                                                <div class="radio radio-success radio-inline" style="margin-left:95px;"><input type="radio" id="inlineRadio1" value="travel" name="agent" <?= $travel_agent; ?> ><label for="inlineRadio1">Travel</label></div>
				                                        <div class="radio radio-success radio-inline"><input type="radio" id="inlineRadio2" value="corporate" name="agent" <?=$corporate_agent ?>><label for="inlineRadio2">Corporate</label></div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Guest Name <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($guest_name); ?></div>
                                                            <span class="error"><?php echo form_error($guest_name['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Contact number <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($phone); ?></div>
                                                            <span class="error"><?php echo form_error($phone['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Address <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($address); ?></div>
                                                            <span class="error"><?php echo form_error($address['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Email Address <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($email); ?></div>
                                                            <span class="error"><?php echo form_error($email['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Adult <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo form_input($total_adults); ?></div>
                                                            <span class="error"><?php echo form_error($total_adults['name']); ?></span> 
	                                            		</div>
	                                        		</div>


	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Child</label>
		                                                	<div class="col-sm-9"><?php echo form_input($total_child); ?></div>
                                                            <span class="error"><?php echo form_error($total_child['name']); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Hotel <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?=$booking_detail->hotel_name ?></div>
		                                                	<span class="error"><?php echo form_error('hotel_id'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6" > 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Meal Plan Type <span class="error">*</span> </label>
		                                                	<div class="col-sm-9" ><?= get_data_dropdown("plan_type","plan_type_id","name","",$booking_detail->plan_type_id,'plan_type_id','plan_type_id','required');?></div>
		                                                	<span class="error"><?php echo form_error('plan_type_id'); ?></span>
	                                            		</div>
	                                        		</div>
 
	                                        		<div class="col-md-6 travel" id="travel_agent"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Travel Agent </label>
		                                                	<div class="col-sm-8" id="travel_agent_list"><?php echo get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id'),$booking_detail->agent_id,'travel_agent_id','travel_agent_id'); ?></div>
		                                                	<div class="col-sm-1"><button type="button" data-toggle="modal" data-target="#travel-agent-modal" class="btn btn-success">+</button></div>
		                                                	<span class="error"><?php echo form_error('travel_agent_id'); ?></span>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6 travel" id="tac_percentage"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">TAC Percentage</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="travel_agent_percentage" name="travel_agent_percentage" value="<?=$booking_detail->agent_percentage ?>" maxlength="2" placeholder=""></div>
	                                            		</div>
	                                        		</div>
                                         		
	                                        		<div class="col-md-6 corporate" > 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Corporate </label>
		                                                	<div class="col-sm-9" ><?php echo get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id'),$booking_detail->agent_id,'corporate_agent_id','corporate_agent_id'); ?></div>
		                                                	
	                                            		</div>
	                                        		</div>
	                                        		<div class="col-md-6 corporate" > 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Corporate Percentage</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="corporate_percentage" name="corporate_percentage" value="<?=$booking_detail->agent_percentage ?>" placeholder="" maxlength="2"></div>
	                                            		</div> 
	                                        		</div>
	
	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Check-in Date Time <span class="error">*</span> </label>
		                                                	<div class="col-sm-9">
		                                                		<div class="input-group">
                                                                    <div class="col-sm-6"><?php echo form_input($checkin_date); ?></div>
                                                                    <div class="col-sm-6"><input type="text" name="check_in_time" id="check_in_time" value="<?=$booking_detail->checkin_time ?>" readonly class="form-control"></div>
	                                    							
	                                    							<span class="error"><?php echo form_error($checkin_date['name']); ?></span> 

	                                            				</div>
	                                        				</div> 
                                						</div>
                                					</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Check-out Date Time <span class="error">*</span> </label>
		                                                	<div class="col-sm-9">
		                                                		<div class="input-group">
                                                                    <div class="col-sm-6"><?php echo form_input($checkout_date); ?></div>
                                                                    <div class="col-sm-6"><input type="text" name="check_out_time" id="check_out_time" value="<?=$booking_detail->checkout_time ?>" readonly class="form-control checkout_date" style="cursor: pointer;"></div>
                                                                    
	                                    							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar checkin_date"></i></span>
	                                    							<span class="error"><?php echo form_error($checkout_date['name']); ?></span> 

	                                            				</div>
	                                        				</div> 
                                						</div>
                                					</div>

                                					<!-- Start Checkin details panel -->  
                                        			
                                        			<div class="col-md-12" > 
						                                <div class="panel panel-border panel-primary">
						                                    <div class="panel-heading"> 
						                                        <h3 class="panel-title" style="text-align: center;font-size: 20px;">Date wise room booking status</h3> 
						                                    </div> 
						                                    <div class="panel-body" id="default"> 
						                                        
						                                        <!-- Row for Checkin room starts. Repeat this row with JS if client selects more than one room -->
<?php					                                        	
		$checkin_date = dateformate($booking_detail->checkin_date,"d-m-Y","-");	
		$checkout_date = dateformate($booking_detail->checkout_date,"d-m-Y","-");			

//		echo "<pre>";print_r($booked_room_info);
		$html = '';
		$checkin_date_no = 1;
		$booking_record_no = 1;
		$no_of_checkbox = 1; // this variable will be used to count how many room number check boxes are there...so that when we check and uncheck that check box we can track on which check box we have checked and unchecked..
		$no_of_div = 0; // this variable will be used to count how many divs are added ..it will be a combination of each room type on different dates...this will be used as div id..so we can keep track of all variables id diff to get its values..this variable will be used how many times we need to iterate a for loop..
        // calculate total no of booking day....
        $no_of_booking_day = DaysDiff($checkin_date,$checkout_date);
        // we need to count how many room types a hotel is having...if a hotel is having 2 room types and at the booking if both room types were used then we wont display add one more room button....
        // but if both room types were not used at the booking then we will display an option where it can select only nt added room type during edit a booking section...
        $no_of_hotel_room_types = count_rows("rooms", "hotel_id = ".$booking_detail->hotel_id) ;
        
//        for($k = 0; $k < $no_of_booking_day; $k++ ){
//        	$temp = 0;
			for($i = 0; $i < count($booked_room_info); $i++ ){ 
//				$temp++; 				
				$no_of_div++;
//				echo $booked_room_info[$i]['id'];
				$min_max_stay_date = $this->CR->getMinMaxStayDate($booked_room_info[$i]['id']);
//echo "<pre>";print_r($min_max_stay_date); 
//echo $min_max_stay_date->stay_date_from.$min_max_stay_date->stay_date_to;
?>					                                        	

						                                        <div class="row select_room" id="<?=$no_of_div?>"  style="background: #f4f4f4;padding-top:20px;border-radius:5px;margin-top:5px;">
							                                        <input type="hidden" value="" id="room_no_<?=$no_of_div?>" name="room_no[<?=$no_of_div?>]" >						
<input type="hidden" value="<?=$booked_room_info[$i]['room_id'] ?>" id="room_id_<?=$no_of_div?>" name="room_id[]" >
							                                        <div class="row">
						                                        		<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Type <span class="error">*</span> </label>
								                                                	<div class="col-sm-6" id="room_type"><?=$booked_room_info[$i]['room_name'] ?></div>

							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Number <span class="error">*</span> </label>
								                                                	<div class="col-sm-6">
<?php 
$room_number = explode("/",$booked_room_info[$i]['room_number']) ;
$no_of_rooms = 0; // this variable will be used to store how many rooms have been selected for a paerticular room type under a specific date.....

if(count($room_number) > 0 ) {
	for($j = 0; $j < count($room_number) ; $j++){
?>
								                                                	<div class="checkbox checkbox-inline"><input type="checkbox" value="<?=$room_number[$j] ?>" checked="checked" name="room_number[<?=$no_of_div?>][]" id="room_number_<?=$no_of_checkbox?>" placeholder="" onClick="room_number(<?=$no_of_div?>,<?=$no_of_checkbox?>);"><label ><?=$room_number[$j] ?></label></div>
<?php $no_of_rooms++; $no_of_checkbox++; } } ?>    <button type="button" class="btn btn-success m-b-5 add_more_room_number" id="add_more_room_number_<?=$no_of_div?>" style="">Add More Room</button>                                        	
<input type="hidden" value="<?=$no_of_rooms?>" id="no_of_rooms_<?=$no_of_div?>" name="no_of_rooms[]" >

								                                                	</div>
								                                                	
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Rate <span class="error">*</span> </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control" id="room_rate_<?=$no_of_div?>" name="room_rate[]" readonly required="" placeholder="" value="<?=$booked_room_info[$i]['base_price'] ?>"></div>
								                                                	
							                                            		</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                                <span style="position:absolute;top:6px;">Occupancy <span class="error">*</span> </span>
								                                                <div class="radio radio-success radio-inline" style="margin-left:80px;"><input type="radio" value="single" name="occupancy[<?=$no_of_div?>][]" <?php if($booked_room_info[$i]['occupancy'] == 'single') echo 'checked';  ?> onclick="occupancy(<?=$no_of_div?>);"><label for="inlineRadio3">Single</label></div>
										                                        <div class="radio radio-success radio-inline"><input type="radio" value="double" name="occupancy[<?=$no_of_div?>][]" <?php if($booked_room_info[$i]['occupancy'] == 'double') echo 'checked';  ?> onclick="occupancy(<?=$no_of_div?>);"><label for="inlineRadio4">Double</label></div>
										                                        
							                                        		</div>
						                                        		</div>
						                                        	</div>
							                                        	
							                                        <div class="row">
							                                        	<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Net Rate <span class="error">*</span> </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="net_rate_<?=$no_of_div?>" name="net_rate[]" placeholder="" maxlength="5" value="<?=$booked_room_info[$i]['sell_price'] ?>"></div>
								                                                	<span class="error"><?php echo form_error('net_rate[]'); ?></span> 
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_<?=$no_of_div?>" name="extra_bed[]" placeholder="" maxlength="5" value="<?=$booked_room_info[$i]['extra_bed'] ?>"></div>
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed Price</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_price_<?=$no_of_div?>" name="extra_bed_price[]" placeholder="" maxlength="5" value="<?=$booked_room_info[$i]['extra_bed_charge'] ?>"></div>
							                                            		</div>
							                                        		</div>


							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_<?=$no_of_div?>" name="extra_person[]" placeholder="" maxlength="5" value="<?=$booked_room_info[$i]['extra_person'] ?>"></div>
							                                            		</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>

						                                        	<div class="row">
						                                        		<div class="col-md-12">
											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person Charge</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_charge_<?=$no_of_div?>" name="extra_person_charge[]" placeholder="" maxlength="5" value="<?=$booked_room_info[$i]['extra_person_charge'] ?>"></div>
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Luxury Tax (%)</label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="luxury_tax_<?=$no_of_div?>" name="luxury_tax[]" placeholder="" maxlength="2" value="<?=$booked_room_info[$i]['luxury_tax'] ?>"></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date From <span class="error">*</span> </label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control stay_date_from" id="stay_date_from_<?=$no_of_div?>" name="stay_date_from[]" readonly placeholder=""  value="<?= dateformate($min_max_stay_date->stay_date_from,'d-m-Y','-')?>"></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date To <span class="error">*</span></label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control stay_date_to" id="stay_date_to_<?=$no_of_div?>" name="stay_date_to[]" readonly placeholder=""  value="<?= date("d-m-Y", strtotime("+1 day", strtotime(dateformate($min_max_stay_date->stay_date_to,"Y-m-d","-")))) ?>"></div>
								                                            	</div>
							                                        		</div>
							                                        		<!-- <div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Selected Room Number</label>
									                                                	<div class="col-sm-6" id="display_room_no_1"></div>
								                                            	</div>
							                                        		</div> -->

						                                        		</div>
						                                        	</div>
						                                        	<div class="row">
						                                        		<div class="col-md-12">
						                                        			<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                <label for="inputEmail3" class="col-sm-6 control-label">Selected Room Number</label>
									                                                <div class="col-sm-6" id="display_room_no_<?=$no_of_div?>"></div>
								                                            	</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>

						                                    	</div>
<?php //} ?>							                                        
						                                        <!-- Row for One room checkin ends -->				                                      
<?php } if($no_of_room_types_used->max_room_type_used < $no_of_hotel_room_types){ //  we need to display add more room button..?>
															<div class="col-md-3">
			                                        			<div align="center"><button type="button" class="btn btn-success m-b-5 add_one_more_room" style="">Add one more Room</button></div>
			                                        		</div>
<?php } ?> 
<input type="hidden" value="<?=$no_of_div?>" id="no_of_div" name="no_of_div" >

						                                        <div id="add_room"></div>
						                                        <!-- Row for One room checkin ends -->
						                                    </div> 
						                                </div>
                                        			</div>

                                					<!-- End Checkin details panel -->

                                        			<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Booking Status <span class="error">*</span> </label>
		                                                	<?php 
		                                                	$book = "";
		                                                	$hold = "";
		                                                	if($booking_detail->booking_status == 1 || $booking_detail->booking_status == 3)
		                                                		$book = "selected='selected'";
		                                                	if($booking_detail->booking_status == 2)
		                                                		$hold = "selected='selected'";
		                                                	?>
		                                                	<div class="col-sm-9"><select class="form-control" name="booking_status" id="booking_status" required ><option value="">Select</option><option value="1" <?=$book?>>Book</option><option value="2" <?=$hold?>>Hold</option></select></div>
		                                                	<span class="error"><?php echo form_error('booking_status'); ?></span>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Total Room Tariff <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="total_room_tariff" name="total_room_tariff" readonly required="" maxlength="10" placeholder="" value="<?=$booking_detail->total_room_tariff?>"></div>
		                                                	<span class="error"><?php echo form_error('total_room_tariff'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-3"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Discount Type</label>
		                                                	<?php 
		                                                	$percentage = "";
		                                                	$cash = "";
		                                                	
		                                                	if($booking_detail->discount_type == "percentage")
		                                                		$percentage = "checked='checked'";
		                                                	if($booking_detail->discount_type == "cash")
		                                                		$cash = "checked='checked'";
		                                                	?>
			                                                <div class="radio radio-success radio-inline" ><input type="radio" id="inlineRadio5" value="percentage" name="discount_type" <?=$percentage?> ><label for="inlineRadio5">Percentage(%)</label></div>
					                                        <div class="radio radio-success radio-inline"><input type="radio" id="inlineRadio6" value="cash" name="discount_type" <?=$cash?> ><label for="inlineRadio6">Cash</label></div>		                                                	
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-3" id="discount"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Discount Amount</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="discount_amount" name="discount_amount" placeholder="" <?php if($booking_detail->discount_type == "percentage") echo 'maxlength="2"'; else if($booking_detail->discount_type == "cash") echo 'maxlength="5"'; ?>  value="<?=$booking_detail->discount_amount?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6">  
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Luxury Tax Amount (Rs.)</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="luxury_tax_amount" name="luxury_tax_amount" readonly placeholder="" value="<?=$booking_detail->total_luxury_tax?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Service Tax %</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="service_tax" name="service_tax" maxlength="2" placeholder="" value="<?=$booking_detail->service_tax?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Service Charge %</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="service_charge" name="service_charge"  placeholder="" maxlength="2" value="<?=$booking_detail->service_charge?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Tax Amount</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="tax_amount" name="tax_amount" maxlength="10" readonly placeholder="" value="<?=$booking_detail->tax_amount?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Total Amount <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="total_amount" name="total_amount" maxlength="10" required="" readonly placeholder="" value="<?=$booking_detail->total_amount?>"></div>
		                                                	<span class="error"><?php echo form_error('total_amount'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Misc Charge</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="misc_charge" name="misc_charge" maxlength="10" placeholder="" value="<?=$booking_detail->misc_charge?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Grand Total <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit" id="grand_total" name="grand_total" required="" maxlength="10" readonly placeholder="" value="<?=$booking_detail->grand_total?>"></div>
		                                                	<span class="error"><?php echo form_error('grand_total'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Advance</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="advance" name="advance" maxlength="10" placeholder="" value="<?=$booking_detail->advance?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Due</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control only_digit calculate_total_amount" id="due" name="due" readonly maxlength="10" placeholder="" value="<?=$booking_detail->due?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Remarks</label>
		                                                	<div class="col-sm-9"><input type="text" class="form-control" id="remarks" placeholder="" name="remarks" value="<?=$booking_detail->remarks?>"></div>
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Collection Point <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?php echo get_data_dropdown("collection_point","collection_point_id","collection_point_name","account_id = ".$this->session->userdata('account_id'),$booking_detail->collection_point_id,'collection_point_id','collection_point_id','required'); ?></div>
		                                                	<span class="error"><?php echo form_error('collection_point_id'); ?></span> 
	                                            		</div>
	                                        		</div>

	                                        		<div class="col-md-6 advance_payment"> 
	                                        			<div class="form-group">
		                                                	<label for="inputEmail3" class="col-sm-3 control-label">Payment Type <span class="error">*</span> </label>
		                                                	<div class="col-sm-9"><?=get_payment_type_list($booking_detail->payment_type) ?></div>
		                                                	<span class="error"><?php echo form_error('payment_type'); ?></span> 
	                                            		</div>
	                                        		</div>
                                                    <div class="col-md-6 advance_payment"> 
                                                        <div class="form-group"> 
                                                            <label for="field-2" class="col-sm-3 control-label">Payment Date</label> 
                                                            <div class="col-sm-9"><input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="Date" value="<?=dateformate($booking_detail->payment_date,"d-m-Y","-") ?>"> </div>
                                                        </div> 
                                                    </div> 

	                                        		<div class="col-md-6" > 
	                                        			<div id="cheque_details_central_reservation" >
		                                        				<div class="col-md-3" id="chq_details_central_reservation"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Cheque Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" id="cheque_no" name="cheque_no" placeholder="Cheque Number" maxlength="10" value="<?=$booking_detail->cheque_no ?>" > 

	                                                                </div> 
	                                                            </div> 
		                                        				<div class="col-md-3" id="dd_details_central_reservation"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">DD Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" id="dd_no" name="dd_no" placeholder="DD Number" maxlength="10" value="<?=$booking_detail->dd_no ?>"> 
	                                                                </div> 
	                                                            </div>               
                                                        </div>	                                        			
                                                        <div class="col-md-3 bank_name_status"> 
                                                            <div class="form-group"> 
                                                                <label for="field-2" class="control-label">Bank Name <span class="error">*</span> </label> 
                                                                <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name" value="<?=$booking_detail->bank_name ?>"> 
                                                            </div> 
                                                        </div>
                                                        <div id="cc_details_central_reservation" >
		                                        				<div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Card Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" name="card_no" id="card_no" placeholder="Card Number" maxlength="10" value="<?=$booking_detail->card_no ?>"> 
	                                                                </div> 
	                                                            </div> 
	                                                            <div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-2" class="control-label">Expiry Date <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control" name ="expiry_date" id="expiry_date" placeholder="Expiry Date mm/yy" value="<?=$booking_detail->expiry_date ?>"> 
	                                                                </div> 
	                                                            </div> 	                                                            
                                                        </div>
	                                        			<div id="neft_details_central_reservation" >
		                                        				<div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-1" class="control-label">Account Number <span class="error">*</span> </label> 
	                                                                    <input type="text" class="form-control only_digit" name="account_no" id="account_no" placeholder="Account Number" maxlength="10" value="<?=$booking_detail->account_no ?>"> 
	                                                                </div> 
	                                                            </div> 
	                                                            <div class="col-md-3"> 
	                                                                <div class="form-group"> 
	                                                                    <label for="field-2" class="control-label">IFSC Code</label> 
	                                                                    <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="IFSC Code" value="<?=$booking_detail->ifsc_code ?>"> 
	                                                                </div> 
	                                                            </div> 	                                                                              
                                                        </div>
	                                        		</div>
													<div class="col-md-12">
	                                        			<div class="form-group">
			                                                <label class="col-md-2 control-label">Billing Instructions</label>
			                                                <div class="col-md-10">
			                                                    <textarea class="form-control" rows="5" name="billing_instruction" id="billing_instruction"><?=$booking_detail->billing_instruction ?></textarea>
			                                                </div>
			                                            </div>

	                                        		</div>	                                        		
                                            
                               				<div align="center"><div class="col-md-12" style="margin-top:45px;"> <span class="error">*</span> Represents mandatory fields. <input type="submit" name="submit" id="submit" value="Submit Booking" class="btn btn-success btn-lg m-b-5" /> </div></div>
                                        </form>
                                    
                                    </div>
                                </div> <!-- End of panel panel-default-->
</div>

<!-- Panel Ends -->



</div> <!-- End row -->
<!-- Page content ends -->  

<!-- Room Number Selection modal --> 
 <div id="room-number-selection-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Choose A Room</h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                        	<div align="center" id="room_number"></div>
                                                        </div> 
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light" id="select_room">Select Room</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

<!-- End of Room Number selection Modal -->

<!-- Travel Agent Modal --> 
 <div id="travel-agent-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title">Add New Travel Agent</h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Agency Name <span class="error">*</span></label>
                                                                    <?php echo form_input($travel_agent_name); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_name['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Address <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_address); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_address['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Phone Number 1 <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_phonenumber1); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_phonenumber1['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Phone Number 2</label> 
                                                                    <?php echo form_input($travel_agent_phonenumber2); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_phonenumber2['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Email address</label> 
                                                                    <?php echo form_input($travel_agent_email_address); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_email_address['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Website</label> 
                                                                    <?php echo form_input($travel_agent_website); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_website['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Contact Person <span class="error">*</span></label> 
                                                                    <?php echo form_input($travel_agent_contact_person); ?>
                                                            		<span class="error"><?php echo form_error($travel_agent_contact_person['name']); ?></span>                                                                      
                                                                </div> 
                                                            </div> 
                                                            
                                                        </div> 
                                                        
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cancel</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light" id="add_travel_agent">Add Travel Agent</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

<!-- End of Travel Agent Modal -->

<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery-ui-timepicker-addon.js" ></script>
<!-- <script type="text/javascript" src="http://trentrichardson.com/examples/timepicker/i18n/jquery-ui-timepicker-addon-i18n.min.js"></script> -->

<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/views/edit_booking.js" ></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$(".travel").hide();
		$(".corporate").hide();
		var account_id = $.cookie("account_id"); // global decalration so dat we can use it any where..
		var base_url = $.cookie("base_url");

		var booking_id = $("#booking_id").val();
		var hotel_id = $("#hotel_id").val();
		var base_url = $.cookie("base_url");

		var date = new Date("<?= $booking_detail->checkout_date ?>");
	    var currentMonth = date.getMonth();
	    var currentDate = date.getDate();
	    var currentYear = date.getFullYear();
	    $("#payment_date").datepicker( "option", "maxDate", new Date(currentYear, currentMonth, currentDate));       		

//		$("#payment_date").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0, maxDate: <?= $booking_detail->checkout_date ?> });

		<?php if($travel_agent != "") { // it means travel agent was selected..... ?>
			$(".travel").show();
			$(".corporate").hide();
		<?php } if($corporate_agent != "") { // it means corporate agent was selected..... ?>
			$(".travel").hide();
			$(".corporate").show();
		<?php } ?>				
		<?php if($booking_detail->discount_type == "cash" || $booking_detail->discount_type == "percentage") { // it means discount type was selected..... ?>
			$("#discount").show();
		<?php } else { // it means discount type is not selected..... ?>				
			$("#discount").hide();
		<?php } if($booking_detail->advance != "" && $booking_detail->advance > 0) {  ?>
			$(".advance_payment").show();	
		<?php } else { ?>				
			$(".advance_payment").hide();		
		<?php } if($booking_detail->payment_type == "cheque") {  ?>
			$("#cheque_details_central_reservation").show();
			$("#chq_details_central_reservation").show();
			$(".bank_name_status").show();
			$("#dd_details_central_reservation").hide();
			$("#cc_details_central_reservation").hide();
			$("#neft_details_central_reservation").hide();		
		<?php } if($booking_detail->payment_type == "demand_draft") { ?>
			$("#cheque_details_central_reservation").show();
			$("#dd_details_central_reservation").show();
			$(".bank_name_status").show();
			$("#chq_details_central_reservation").hide();
			$("#cc_details_central_reservation").hide();
			$("#neft_details_central_reservation").hide();		
		<?php } if($booking_detail->payment_type == "credit_card" || $booking_detail->payment_type == "debit_card") { ?>
			$("#cheque_details_central_reservation").hide();
			$("#dd_details_central_reservation").hide();
			$("#chq_details_central_reservation").hide();
			$("#cc_details_central_reservation").show();
			$("#neft_details_central_reservation").hide();
			$(".bank_name_status").hide();		
		<?php } if($booking_detail->payment_type == "neft" || $booking_detail->payment_type == "rtgs") { ?>
			$("#cheque_details_central_reservation").show();
			$("#dd_details_central_reservation").hide();
			$("#chq_details_central_reservation").hide();
			$("#cc_details_central_reservation").hide();
			$("#neft_details_central_reservation").show();	
			$(".bank_name_status").show();	
		<?php } ?>

	});

</script>