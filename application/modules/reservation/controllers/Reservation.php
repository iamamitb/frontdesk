<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends MX_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->load->model('reservation/Reservation_model','CR');
		$this->load->model('dashboard/dashboard_model','DM');
		$this->load->library('form_validation');
		is_logged_in(); 
		is_privileges(); 
	}

	public function index(){

		$response = "";
		$room_id_error = 1;
		$no_of_rooms_error = 1;
		$room_rate_error = 1;
		$occupancy_error = 1;
		$net_rate_error = 1;
		$stay_date_from_error = 1;
		$stay_date_to_error = 1;

        // validation code...
        $this->form_validation->set_rules('guest_name', 'Guest Name', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('total_adults', 'Adults', 'trim|required');
        $this->form_validation->set_rules('hotel_id', 'Hotel', 'trim|required');
        $this->form_validation->set_rules('plan_type_id', 'Meal Plan Type', 'trim|required');
        $this->form_validation->set_rules('checkin_date', 'Check In Date', 'trim|required');
        $this->form_validation->set_rules('checkout_date', 'Check Out Date', 'trim|required');
		if (!empty($_POST['room_id']) ) {// it means all required fileds are filled up...
		    $this->form_validation->set_rules('room_id[]',"Room Type", "required");
		} else $room_id_error = 0;
		if (!empty($_POST['no_of_rooms']) ) {// it means all required fileds are filled up...
		    $this->form_validation->set_rules('no_of_rooms[]',"Number of Rooms", "required");
		}  else $no_of_rooms_error = 0;
		if (!empty($_POST['room_rate'])) {// it means all required fileds are filled up...
		    $this->form_validation->set_rules('room_rate[]',"Room Rate", "required");
		} else $room_rate_error = 0;
		if (!empty($_POST['occupancy'])) {// it means all required fileds are filled up...
		    $this->form_validation->set_rules('occupancy[]',"Occupancy", "required");
		} else $occupancy_error = 0;
		if (!empty($_POST['net_rate'])) {// it means all required fileds are filled up...
			$this->form_validation->set_rules('net_rate[]',"Net Rate", "required");
		} else $net_rate_error = 0;
		if (!empty($_POST['stay_date_from'])) {// it means all required fileds are filled up...
			$this->form_validation->set_rules('stay_date_from[]',"Stay Date From", "required");
		} else $stay_date_from_error = 0;
		if (!empty($_POST['stay_date_to'])) {// it means all required fileds are filled up...
			$this->form_validation->set_rules('stay_date_to[]',"Stay Date To", "required");
		} else $stay_date_to_error = 0;
        
        $this->form_validation->set_rules('booking_status', 'Booking Status', 'trim|required');
        $this->form_validation->set_rules('total_room_tariff', 'Total Room Tariff', 'trim|required');
        $this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|required');
        $this->form_validation->set_rules('grand_total', 'Grand Total', 'trim|required');
        $this->form_validation->set_rules('collection_point_id', 'Collection Point', 'trim|required');
//        $this->form_validation->set_rules('payment_type', 'Payment Type', 'trim|required');
		if ($this->form_validation->run() && $room_id_error == 1 && $no_of_rooms_error == 1 && $room_rate_error == 1 && $occupancy_error == 1 && $net_rate_error == 1 && $stay_date_from_error == 1 && $stay_date_to_error == 1){
			
			// it dmeans we dont have any validation error..so we will insert all the data according to the requirement..
			// we need to insert guest info in guest table and get guest id from there....
			// before inserting record of guest info into table we need to check whether its a new guest info or any exisitng guest info
			// if we have a guest id it means we have an existing guest so we wont insert that record in table...
			$guest = array();
			$booking = array();
			$transaction = array();
			$booked_room = array();
			if($this->input->post('guest_id')){
				$guest_id = $this->input->post('guest_id');
			} else {
				$guest['name'] = $this->input->post('guest_name');
				$guest['phone'] = $this->input->post('phone');
				$guest['address'] = $this->input->post('address');
				$guest['email'] = $this->input->post('email');
				$guest['registration_date'] = date('Y-m-d') ;

				$guest_id = insertValue('guest_detail', $guest);					
			}
			
			// now we need to insert booking table details...
			$booking['hotel_id'] = $this->input->post('hotel_id');
			$booking['plan_type_id'] = $this->input->post('plan_type_id');
			$booking['guest_id'] = $guest_id;
			$booking['users_id'] = $this->session->userdata('id');
			$booking['collection_point_id'] = $this->input->post('collection_point_id');
			$booking['total_adults'] = $this->input->post('total_adults');
			$booking['total_child'] = $this->input->post('total_child');
			$booking['checkin_date'] = dateformate($this->input->post('checkin_date'),"Y-m-d","-");
			$booking['checkout_date'] = dateformate($this->input->post('checkout_date'),"Y-m-d","-");
			$booking['checkin_time'] = $this->input->post('check_in_time');
			$booking['checkout_time'] = $this->input->post('check_out_time');
			$booking['booking_status'] = $this->input->post('booking_status');
			$booking['total_room_tariff'] = $this->input->post('total_room_tariff');
			if($this->input->post('travel_agent_id')){
				$booking['agent_id'] = $this->input->post('travel_agent_id');
				$booking['agent_percentage'] = $this->input->post('travel_agent_percentage');
			}
			if($this->input->post('corporate_agent_id')){
				$booking['agent_id'] = $this->input->post('corporate_agent_id');
				$booking['agent_percentage'] = $this->input->post('corporate_percentage');
			}
			$booking['booking_date'] = date('Y-m-d');
			$booking['discount_type'] = $this->input->post('discount_type');
			$booking['discount_amount'] = $this->input->post('discount_amount');
			$booking['total_luxury_tax'] = $this->input->post('luxury_tax_amount');
			$booking['service_tax'] = $this->input->post('service_tax');
			$booking['service_charge'] = $this->input->post('service_charge');
			$booking['tax_amount'] = $this->input->post('tax_amount');
			$booking['misc_charge'] = $this->input->post('misc_charge');
			$booking['total_amount'] = $this->input->post('total_amount');
			$booking['remarks'] = $this->input->post('remarks');
			$booking['billing_instruction'] = $this->input->post('billing_instruction');

			$booking_id = insertValue('bookings', $booking);
			
			$transaction['booking_id'] = $booking_id;
			// we need to check whether a guest is paying any advance amount or not...if guest is not paying any amount then in transaction table everything will be emptry except booking id, grand total and due...
			if($this->input->post('advance') > 0 && $this->input->post('advance') != ""){
				// it means guest is paying some money....
				$transaction['advance'] = $this->input->post('advance');
				$transaction['due'] = $this->input->post('due');
				$transaction['grand_total'] = $this->input->post('grand_total');
				$transaction['payment_type'] = $this->input->post('payment_type') ;
				if($this->input->post('payment_date') != "")
					$transaction['payment_date'] = dateformate($this->input->post('payment_date'),"Y-m-d","-");
				else
					$transaction['payment_date'] = date('Y-m-d');
				$transaction['cheque_no'] = $this->input->post('cheque_no');
				$transaction['dd_no'] = $this->input->post('dd_no');
				$transaction['card_no'] = $this->input->post('card_no');
				$transaction['account_no'] = $this->input->post('account_no');
				$transaction['ifsc_code'] = $this->input->post('ifsc_code');
				$transaction['expiry_date'] = $this->input->post('expiry_date');
				$transaction['bank_name'] = $this->input->post('bank_name');
			} else {				
				$transaction['advance'] = "";
				$transaction['due'] = $this->input->post('due');
				$transaction['grand_total'] = $this->input->post('grand_total');
				$transaction['payment_type'] = "" ;
				$transaction['payment_date'] = "";
				$transaction['cheque_no'] = "N/A";
				$transaction['dd_no'] = "N/A";
				$transaction['card_no'] = "N/A";
				$transaction['account_no'] = "N/A";
				$transaction['ifsc_code'] = "N/A";
				$transaction['expiry_date'] = "N/A";
				$transaction['bank_name'] = "N/A";
			}

			insertValue('transaction', $transaction);
			
			// now we need to update booking reference for this current booking id...
			// it will be done on the basis of account id and by using last boooking ref..
			// we need to read last booking ref and increase its count by 1 and then concat it with account id..
			// we need to just read how many bookings are there by using that hotel id...
			// if by using that hotel id if we dont have any booking then we will start with account_id.0....
			// if we have 5 bookings  by using that hotel id we will go ahead with account_id.6....

			$hotel_id = $this->input->post('hotel_id');

			$no_of_booking = count_rows('bookings', 'hotel_id = '.$hotel_id);
			if($no_of_booking > 0) // it means by using this hotel id bookings are already there so we will just increase its value by 1 nad concatenate it 
				$no_of_booking++;
			$account_id = $this->session->userdata('account_id');
			$data['booking_reference'] = $account_id."00".$no_of_booking;
			updateDataCondition('bookings', $data, 'booking_id = '.$booking_id);				

			// now we need to read the room details like base price, sell price etc from form and insert..
			// these data will be in form of an array..and it may be in an odd format like [1],[3],[4],[5] etc..
			// we need to make it in a serial way like [1],[2],[3] ...etc..
			$no_of_room = $this->input->post('no_of_room');	// it will be used to see how many more rooms are added...
			$room_id = $this->input->post('room_id');
			$base_price = $this->input->post('room_rate');
			$occupancy = $this->input->post('occupancy');
			$sell_price = $this->input->post('net_rate');
			$extra_bed = $this->input->post('extra_bed');
			$extra_bed_charge = $this->input->post('extra_bed_price');
			$extra_person = $this->input->post('extra_person');
			$extra_person_charge = $this->input->post('extra_person_charge');
			$luxury_tax = $this->input->post('luxury_tax');
			$room_no = $this->input->post('room_no');
			$stay_date_from = $this->input->post('stay_date_from');
			$stay_date_to = $this->input->post('stay_date_to');
			$booking_id = $booking_id;

			$ocu = 1;
			$occupancy = array_map('current', $occupancy);
			for ($i = 0; $i < $no_of_room; $i++) { 
				$booked_room_detail['base_price'] = $base_price[$i];
				$booked_room_detail['occupancy'] = $occupancy[$ocu];
				$booked_room_detail['sell_price'] = $sell_price[$i];
				$booked_room_detail['extra_bed'] = $extra_bed[$i];
				$booked_room_detail['extra_bed_charge'] = $extra_bed_charge[$i];
				$booked_room_detail['extra_person'] = $extra_person[$i];
				$booked_room_detail['extra_person_charge'] = $extra_person_charge[$i];
				$booked_room_detail['luxury_tax'] = $luxury_tax[$i];
				$booked_room_detail['booking_id'] = $booking_id;
				$ocu++;

				$booked_room_id = insertValue('booked_room_detail', $booked_room_detail);

				// now we need to read the room details like room number date etc from form..				

				// get the room number and explode it from , and insert it into table by using for loop..
				$total_room_no = explode(',',$room_no[$i]);

				for($j = 0; $j < count($total_room_no); $j++){
					$booked_room['booked_room_id'] = $booked_room_id;
					$booked_room['room_number'] = $total_room_no[$j];
					$booked_room['room_id'] = $room_id[$i];

					$id = insertValue('booked_room', $booked_room);
					// now we need to insert stay date for this room number....
					// supppose we are booking from 25-2 to 28-2 for room no 102 then for 102 will have 4 entries in booked room stay date table like 102-25-2,102-26-2,102-27-2,102-28-2
					// calculate total no of booking day....
					$no_of_booking_day = DaysDiff($stay_date_from[$i],$stay_date_to[$i]);
					for($k = 0; $k < $no_of_booking_day; $k++ ){
						$booked_room_stay_date['booked_room'] = $id;
						$booked_room_stay_date['stay_date'] =  date("Y-m-d", strtotime("+".$k." day", strtotime(dateformate($stay_date_from[$i],"Y-m-d","-")))) ;
//						echo "<pre>"; print_r($booked_room_stay_date);
						insertValue('booked_room_stay_date', $booked_room_stay_date);

					}

				}

			}
//			die;
			$this->session->set_flashdata('booking_id', $booking_id);
			$this->session->set_flashdata('booking_reference', getValue('bookings', 'booking_reference', 'booking_id = '.$booking_id));
            redirect(current_url());				
		} else {
			// it means there is a validation error...
			$data = array();			
			
			// we need to write a condition where we will ceck whetehr any hotel, room, meal plan and other required entity(which will be required at the time of booking) is added ot not... if its not added then we will redirect it to concenr page to add that entity first....		
			if(count_rows("hotels", "account_id = ".$this->session->userdata('account_id')." and hotels_status = '1' ")){
				// it means hotel is added....
				if(count_rows("plan_type", "")){
					// it means plan type is added....
					if(count_rows("hotel_plan_type", "")){
						// it means plan type for any hotel is added....
						if(count_rows("collection_point", "account_id = ".$this->session->userdata('account_id')." and collection_point_status = '1'")){
							// it means collection point is added....
							if(count_rows("room_plan_price", "")){
								// it means room plan prie is not added....
								if(!count_rows("rooms", "")){
									// it means rooms are not added for a hotel.....
									$data['room_error'] = 1;
								}			
							}else {
								// it means collection points are not added .....
								$data['room_plan_price_error'] = 1;
							}			
						}else {
							// it means collection points are not added .....
							$data['collection_point_error'] = 1;
						}			
					}else {
						// it means meal plans are not added for a hotel.....
						$data['meal_plan_error'] = 1;
					}			
				}else {
					// it means plan types are not added for a hotel.....
					$data['meal_plan_type_error'] = 1;
				}				
			}else {
				// it means hotels are not added .....
				$data['hotel_error'] = 1;
			}				
			$this->template->title('Central Reservation','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('main_template','front');
			$this->template->build('reservation',$data);

		}
	}

//	Define a function to edit a booking....

	public function edit_booking($booking_id){

		// we need to store its previous url...and if booking id is wrong or malfunctioned then we will redirect  it to the dashboard or reservation or previous url page...
		//$previous_page_url = $this->agent->referrer();
		
		// we need to validate whether the current booking id is correct or not...if booking id is modified manually or malfunctioned we will redirect it to previous page url...
		if(count_rows("bookings", "booking_id = ".$booking_id." and (booking_status = 1 OR booking_status = 3) ")){
			// it means booking id is valid and booked only....

			$response = "";
			$room_id_error = 1;
			$no_of_rooms_error = 1;
			$room_rate_error = 1;
			$occupancy_error = 1;
			$net_rate_error = 1;

	        // validation code...
	        $this->form_validation->set_rules('guest_name', 'Guest Name', 'trim|required|xss_clean');
	        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
	        $this->form_validation->set_rules('address', 'Address', 'trim|required');
	        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
	        $this->form_validation->set_rules('total_adults', 'Adults', 'trim|required');	        
	        $this->form_validation->set_rules('plan_type_id', 'Meal Plan Type', 'trim|required');
	        $this->form_validation->set_rules('checkin_date', 'Check In Date', 'trim|required');
	        $this->form_validation->set_rules('checkout_date', 'Check Out Date', 'trim|required');
			// if (!empty($_POST['room_id']) ) {// it means all required fileds are filled up...
			//     $this->form_validation->set_rules('room_id[]',"Room Type", "xss_clean|required");
			// } else $room_id_error = 0;
			// if (!empty($_POST['no_of_rooms']) ) {// it means all required fileds are filled up...
			//     $this->form_validation->set_rules('no_of_rooms[]',"Number of Rooms", "xss_clean|required");
			// }  else $no_of_rooms_error = 0;
			// if (!empty($_POST['room_rate'])) {// it means all required fileds are filled up...
			//     $this->form_validation->set_rules('room_rate[]',"Room Rate", "xss_clean|required");
			// } else $room_rate_error = 0;
			// if (!empty($_POST['occupancy'])) {// it means all required fileds are filled up...
			//     $this->form_validation->set_rules('occupancy[]',"Occupancy", "xss_clean|required");
			// } else $occupancy_error = 0;
			// if (!empty($_POST['net_rate'])) {// it means all required fileds are filled up...
			// 	$this->form_validation->set_rules('net_rate[]',"Net Rate", "xss_clean|required");
			// } else $net_rate_error = 0;
	        
	        $this->form_validation->set_rules('booking_status', 'Booking Status', 'trim|requireroom_rate');
	        $this->form_validation->set_rules('total_room_tariff', 'Total Room Tariff', 'trim|required');
	        $this->form_validation->set_rules('total_amount', 'Total Amount', 'trim|required');
	        $this->form_validation->set_rules('grand_total', 'Grand Total', 'trim|required');
	        $this->form_validation->set_rules('collection_point_id', 'Collection Point', 'trim|required');
//	        $this->form_validation->set_rules('payment_type', 'Payment Type', 'trim|required');
			if ($this->form_validation->run() ){
				// it dmeans we dont have any validation error..so we will insert all the data according to the requirement..
				// we need to insert guest info in guest table and get guest id from there....
				// before inserting record of guest info into table we need to check whether its a new guest info or any exisitng guest info
				// if we have a guest id it means we have an existing guest so we wont insert that record in table...
				$guest = array();
				$booking = array();
				$booked_room = array();

				$guest_id = getValue("bookings", "guest_id", "booking_id = ".$booking_id);
				$hotel_id = getValue("bookings", "hotel_id", "booking_id = ".$booking_id);				

				$guest['name'] = $this->input->post('guest_name');
				$guest['phone'] = $this->input->post('phone');
				$guest['address'] = $this->input->post('address');
				$guest['email'] = $this->input->post('email');

				updateDataCondition('guest_detail', $guest,'guest_id = '.$guest_id);					
				
				// now we need to insert booking table details...
				
				$booking['plan_type_id'] = $this->input->post('plan_type_id');
				$booking['users_id'] = $this->session->userdata('id');
				$booking['collection_point_id'] = $this->input->post('collection_point_id');
				$booking['total_adults'] = $this->input->post('total_adults');
				$booking['total_child'] = $this->input->post('total_child');
				$booking['checkin_date'] = dateformate($this->input->post('checkin_date'),"Y-m-d","-");
				$booking['checkout_date'] = dateformate($this->input->post('checkout_date'),"Y-m-d","-");
				$booking['checkin_time'] = $this->input->post('check_in_time');
				$booking['checkout_time'] = $this->input->post('check_out_time');
				$booking['booking_status'] = $this->input->post('booking_status');
				if($this->input->post('travel_agent_id')){
					$booking['agent_id'] = $this->input->post('travel_agent_id');
					$booking['agent_percentage'] = $this->input->post('travel_agent_percentage');
				}
				if($this->input->post('corporate_agent_id')){
					$booking['agent_id'] = $this->input->post('corporate_agent_id');
					$booking['agent_percentage'] = $this->input->post('corporate_percentage');
				}
				$booking['total_room_tariff'] = $this->input->post('total_room_tariff');
				$booking['discount_type'] = $this->input->post('discount_type');
				$booking['discount_amount'] = $this->input->post('discount_amount');
				$booking['total_luxury_tax'] = $this->input->post('luxury_tax_amount');
				$booking['service_tax'] = $this->input->post('service_tax');
				$booking['service_charge'] = $this->input->post('service_charge');
				$booking['tax_amount'] = $this->input->post('tax_amount');				
				$booking['misc_charge'] = $this->input->post('misc_charge');
				$booking['total_amount'] = $this->input->post('total_amount');
				$booking['billing_instruction'] = $this->input->post('billing_instruction');
				$booking['remarks'] = $this->input->post('remarks');

				updateDataCondition('bookings', $booking,'booking_id = '.$booking_id);

				$transaction['booking_id'] = $booking_id;
			// we need to check whether a guest is paying any advance amount or not...if guest is not paying any amount then in transaction table everything will be emptry except booking id, grand total and due...
			if($this->input->post('advance') > 0 && $this->input->post('advance') != ""){
				// it means guest is paying some money....
				$transaction['advance'] = $this->input->post('advance');
				$transaction['due'] = $this->input->post('due');
				$transaction['grand_total'] = $this->input->post('grand_total');
				$transaction['payment_type'] = $this->input->post('payment_type') ;
				$transaction['payment_date'] = dateformate($this->input->post('payment_date'),"Y-m-d","-");
				$transaction['cheque_no'] = $this->input->post('cheque_no');
				$transaction['dd_no'] = $this->input->post('dd_no');
				$transaction['card_no'] = $this->input->post('card_no');
				$transaction['account_no'] = $this->input->post('account_no');
				$transaction['ifsc_code'] = $this->input->post('ifsc_code');
				$transaction['expiry_date'] = $this->input->post('expiry_date');
				$transaction['bank_name'] = $this->input->post('bank_name');
			} else {				
				$transaction['advance'] = "";
				$transaction['due'] = $this->input->post('due');
				$transaction['grand_total'] = $this->input->post('grand_total');
				$transaction['payment_type'] = "" ;
				$transaction['payment_date'] = "";
				$transaction['cheque_no'] = "N/A";
				$transaction['dd_no'] = "N/A";
				$transaction['card_no'] = "N/A";
				$transaction['account_no'] = "N/A";
				$transaction['ifsc_code'] = "N/A";
				$transaction['expiry_date'] = "N/A";
				$transaction['bank_name'] = "N/A";
			}

				// first we need to delete all records from db table by considering booking id....
				Delete_data("transaction","booking_id = ".$booking_id);

				insertValue('transaction', $transaction);
				
				// now we need to read the room details like base price, sell price etc from form and insert..
				// these data will be in form of an array..and it may be in an odd format like [1],[3],[4],[5] etc..
				// we need to make it in a serial way like [1],[2],[3] ...etc..
				$no_of_room = $this->input->post('no_of_div');	// it will be used to see how many more rooms are added...
				$room_id = $this->input->post('room_id');
				$base_price = $this->input->post('room_rate');
				$occupancy = $this->input->post('occupancy');
				$sell_price = $this->input->post('net_rate');
				$extra_bed = $this->input->post('extra_bed');
				$extra_bed_charge = $this->input->post('extra_bed_price');
				$extra_person = $this->input->post('extra_person');
				$extra_person_charge = $this->input->post('extra_person_charge');
				$luxury_tax = $this->input->post('luxury_tax');
				$new_room_no = $this->input->post('room_no');
				$room_no = $this->input->post('room_number');
				$stay_date_from = $this->input->post('stay_date_from');
				$stay_date_to = $this->input->post('stay_date_to');
				$no_of_rooms = $this->input->post('no_of_rooms');

				// first we need to delete all records from db table by considering booking id....
				Delete_data("booked_room_detail","booking_id = ".$booking_id);

				$ocu = 1;
				$occupancy = array_map('current', $occupancy);
				for ($i = 0; $i < $no_of_room; $i++) { 
					// before proceedings we need to check that whether we have any room no selected or not..if no room selected then we will not proceed...room no selection will be done by 2 way..either by selecting check box or by clicking on add new room and selectin new rooms...if in both way no room is selected then we need to igore that condition and move ahead to next loop...
					if($no_of_rooms[$i] > 0 || $new_room_no[$ocu] != "" ){
						// it means in both way rooms were selected..so we need proceed with that condition only.....
						$booked_room_detail['base_price'] = $base_price[$i];
						$booked_room_detail['occupancy'] = $occupancy[$ocu];
						$booked_room_detail['sell_price'] = $sell_price[$i];
						$booked_room_detail['extra_bed'] = $extra_bed[$i];
						$booked_room_detail['extra_bed_charge'] = $extra_bed_charge[$i];
						$booked_room_detail['extra_person'] = $extra_person[$i];
						$booked_room_detail['extra_person_charge'] = $extra_person_charge[$i];
						$booked_room_detail['luxury_tax'] = $luxury_tax[$i];
						$booked_room_detail['booking_id'] = $booking_id;
												
						$booked_room_id = insertValue('booked_room_detail', $booked_room_detail);
						// now we need to read the room details like room number date etc from form..				

						// this for loop will work for check box room selection....
						for($j = 0; $j < count($room_no[$ocu]); $j++){
							$booked_room['booked_room_id'] = $booked_room_id;
							$booked_room['room_number'] = $room_no[$ocu][$j];
							$booked_room['room_id'] = $room_id[$i];
							$id = insertValue('booked_room', $booked_room);
							// now we need to insert stay date for this room number....
							// supppose we are booking from 25-2 to 28-2 for room no 102 then for 102 will have 4 entries in booked room stay date table like 102-25-2,102-26-2,102-27-2,102-28-2
							// calculate total no of booking day....
							$no_of_booking_day = DaysDiff($stay_date_from[$i],$stay_date_to[$i]);
							for($k = 0; $k < $no_of_booking_day; $k++ ){
								$booked_room_stay_date['booked_room'] = $id;
								$booked_room_stay_date['stay_date'] =  date("Y-m-d", strtotime("+".$k." day", strtotime(dateformate($stay_date_from[$i],"Y-m-d","-")))) ;
								insertValue('booked_room_stay_date', $booked_room_stay_date);
							}
						}	
						// this for loop willl work for add more room selection i.e. new room selectio....			// get the room number and explode it from , and insert it into table by using for loop..
						if($new_room_no[$ocu] != ""){
							$total_room_no = explode(',',$new_room_no[$ocu]);
							for($a = 0; $a < count($total_room_no); $a++){
								$booked_room['booked_room_id'] = $booked_room_id;
								$booked_room['room_number'] = $total_room_no[$a];
								$booked_room['room_id'] = $room_id[$i];
								$id = insertValue('booked_room', $booked_room);
								// now we need to insert stay date for this room number....
								// supppose we are booking from 25-2 to 28-2 for room no 102 then for 102 will have 4 entries in booked room stay date table like 102-25-2,102-26-2,102-27-2,102-28-2
								// calculate total no of booking day....
								$no_of_booking_day = DaysDiff($stay_date_from[$i],$stay_date_to[$i]);
								for($b = 0; $b < $no_of_booking_day; $b++ ){
									$booked_room_stay_date['booked_room'] = $id;
									$booked_room_stay_date['stay_date'] =  date("Y-m-d", strtotime("+".$b." day", strtotime(dateformate($stay_date_from[$i],"Y-m-d","-")))) ;
									insertValue('booked_room_stay_date', $booked_room_stay_date);
								}
							}						
						}						
					}
					$ocu++;
				}
				$this->session->set_flashdata('booking_id', $booking_id);
				$this->session->set_flashdata('booking_reference', getValue('bookings', 'booking_reference', 'booking_id = '.$booking_id));
	            redirect(base_url().'dashboard');				
			} else {
				// it means there is a validation error...
				$data['booking_detail'] = $this->CR->getBookingDetails($booking_id);
				$data['booked_room_info'] = $this->DM->getBookedRoomDetailDatewise($booking_id);				
				$data['no_of_room_types_used'] = $this->CR->getNoOfUsedRoomTypes($booking_id);
				$this->template->title('Central Reservation','Imanagemyhotel');
				$this->template->set('metaDesc','Imanagemyhotel');
				$this->template->set('metaKeyword','Imanagemyhotel');
				$this->template->set_layout('main_template','front');
				$this->template->build('edit_booking',$data);
			}

		} else {
			// it means booking id is not valid...so we will redirect it to previous page url..
			redirect(base_url().'dashboard');
		}
	}

	// define a function to get meal plan....

	public function get_meal_plan(){
		$hotel_id = $this->uri->segment(3);

		$meal_plan = $this->CR->getMealPlanByHotelId($hotel_id);

		$select = "";
		if(isset($meal_plan) && !empty($meal_plan)){
			$select .= '<select class="form-control" name="plan_type_id" id="plan_type_id" required>';
			foreach ($meal_plan as $meal) {
				$select .= '<option value="'.$meal->plan_type_id.'">'.$meal->name.'</option>';
			}	

			$select .= '</select>';
		}
		echo $select;
				
	}

	// define a function to get all rooms type of a hotel ....

	public function get_hotel_room_type(){
		$hotel_id = $this->uri->segment(3);
		
		echo get_data_dropdown("rooms", "room_id", "room_name", "hotel_id = ".$hotel_id." and room_status = '1'", "", "room_id_1", "room_id[]", "required", "", "", "room_id"); 				
	}

	// define a function to get all rooms type of a hotel ....

	public function get_hotel_info(){
		$result = [];
		$hotel_id = $this->uri->segment(3);
		$hotel_info = getSingle("hotels","hotel_id = ".$hotel_id,"","","","");
//		print_r($hotel_info);
		$result[0] = $hotel_info->service_tax;
		$result[1] = $hotel_info->service_charge;
		echo json_encode($result);				
	}

	// define a function to check whether a travel agent is already in DB table or not ...

	public function is_agent_name(){

		$travel_agent_name = $this->uri->segment(3);
		$account_id = $this->uri->segment(4);
		$agents_type = $this->uri->segment(5);

		echo count_rows("agents", "agent_name = '".$travel_agent_name."' and account_id = ".$account_id."' and agents_type = ".$agents_type." and agent_status = '1'");					
	}

	// define a function which will insert travel agent by using ajax call

	public function add_travel_agent(){

		$data['account_id'] = $this->input->post('account_id');	
		$data['agent_name'] = $this->input->post('travel_agent_name');	
		$data['agent_address'] = $this->input->post('travel_agent_address');
		$data['agent_phonenumber1'] = $this->input->post('travel_agent_phonenumber1');
		$data['agent_phonenumber2'] = $this->input->post('travel_agent_phonenumber2');
		$data['agent_email_address'] = $this->input->post('travel_agent_email_address');
		$data['agent_website'] = $this->input->post('travel_agent_website');
		$data['agent_contact_person'] = $this->input->post('travel_agent_contact_person');
		$data['agents_type'] = 1;
		
		echo insertValue('agents',$data); // it will print last insert id values..

	}

	// define a function which will select all travel agent list with latest added and selected travel agent name  ...

	public function get_all_travel_agent_list(){
		$travel_agent_id = $this->input->post('travel_agent_id');	
		echo get_data_dropdown('agents', 'agent_id','agent_name', 'account_id = '.$this->session->userdata('account_id')." and agents_type = '1' and agent_status = '1'", $travel_agent_id, "travel_agent_id", "travel_agent_id", "required", "","") ;
							
	}

	// define a function to get all empty rooms from a hotel on the basis of date, hotel name, plan etc ....

	public function get_hotel_room(){
		$room_id = $this->input->post('room_id');	
		$room_no = explode(',',$this->input->post('room_no'));
		$no = $this->input->post('no');	
//		$selected_room_id = $this->input->post('selected_room_id');	
		$checkin_date = dateformate($this->input->post('checkin_date'),"Y-m-d","-");	
		$checkout_date = dateformate($this->input->post('checkout_date'),"Y-m-d","-");
		
		// first we need to get the list of all booked room during dis date range...and then laetr we will select only dose rooms which is not in array list...
		$rm_no = $this->CR->getBookedRoom($checkin_date,$checkout_date);
		// it may possible that during this date range there is no any booked room so it will return as empty room...
		// if its an empty then we just need to fetch a room on the basis of room id only...
		if(!empty($rm_no)){
			// convert two dimensional array to one...
			// we need to write a logic where we will find out total selected room numbers for individual room id..
			// if a room number is already selected then we need to hide that room number for that selected room id...
			$room_number = array_map('current', $rm_no);
			$room_number = $this->CR->getVacantRoom($room_id,$room_number);
		} else {
			$room_number = getData("room_number","room_id = ".$room_id);			
		}
		$html = ""; 
		$temp = 0; // this will be a temp variable....
		if(!empty($room_number)){
			$html .= "<input type='hidden' name='no' id='no' value='".$no."' />"; 
			foreach ($room_number as $rn) {
				if(!empty($room_no)){ // it means array is not empty..it will be empty for 1st time..
					if (!in_array($rn->room_number, $room_no)) {
						$html .= ' <button id="'.$rn->room_number.'" type="button" class="btn btn-default m-b-5 room_number_bg" >'.$rn->room_number.'</button> ';
						$temp = 1;	
					}
				}else {
					$html .= ' <button id="'.$rn->room_number.'" type="button" class="btn btn-default m-b-5 room_number_bg" >'.$rn->room_number.'</button> ';
					$temp = 1;					
				}	
			}
		}
		// now we need to check the temp variable value...if its 0 it means we need to return and empty html...if its 1 we need to return the whole html...
		if($temp == 0)
			echo "";
		else if($temp == 1)
			echo $html;
	}

	// define a function to get all empty rooms from a hotel on the basis of date, hotel name, plan etc ....

	public function get_room_plan_price(){
		$result = [];
		$room_id = $this->input->post('room_id');	
		$plan_type_id = $this->input->post('plan_type_id');	
		$no = $this->input->post('no');	
		$occupancy = trim($this->input->post('occupancy'));
		$travel_agent_id = $this->input->post('travel_agent_id');			
		$corporate_agent_id = $this->input->post('corporate_agent_id');	
		$checkin_date = dateformate($this->input->post('checkin_date'),"Y-m-d","-");
		$checkout_date = dateformate($this->input->post('checkout_date'),"Y-m-d","-");

//		before calculating the room plan price we need to check whether we have selected any agent or not ...
//		a travel or corporate agent may be selected or not..so accordingly we need to calculate the price..
//		we also need to check whether the seected date rannge comes under a special room tariff or not..
//		if selected date range is between special tariff then we need to get room price from sprecial room tariff plan table only..

		// check whether selected date range is between special tariff or not..

//		echo "res = ".$this->CR->isDateSpecialTariff($checkin_date,$checkout_date,$room_id,$plan_type_id); die;
		if($this->CR->isDateSpecialTariff($checkin_date,$checkout_date,$room_id,$plan_type_id)){
			// it means selected date range is from special tariff..so we need to get the price from diff table...
			if($occupancy == "single") {
				if(isset($travel_agent_id) && $travel_agent_id != "") // it means travel agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$travel_agent_id );
				else if(isset($corporate_agent_id) && $corporate_agent_id != "") // it means corporate agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$corporate_agent_id );
				else 
					$room_price = getValue('room_special_tariff', 'single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
			}else if($occupancy == "double") {
				if(isset($travel_agent_id) && $travel_agent_id != "") // it means travel agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$travel_agent_id );
				else if(isset($corporate_agent_id) && $corporate_agent_id != "") // it means corporate agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$corporate_agent_id );
				else 			
					$room_price = getValue('room_special_tariff', 'double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
			}

		}else{
			// it means selected date range is not from special tariff..so we need to get the price from diff table...
			if($occupancy == "single") {
				if(isset($travel_agent_id) && $travel_agent_id != "") // it means travel agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$travel_agent_id );
				else if(isset($corporate_agent_id) && $corporate_agent_id != "") // it means corporate agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$corporate_agent_id );
				else 
					$room_price = getValue('room_plan_price', 'single_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
			}else if($occupancy == "double") {
				if(isset($travel_agent_id) && $travel_agent_id != "") // it means travel agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$travel_agent_id );
				else if(isset($corporate_agent_id) && $corporate_agent_id != "") // it means corporate agent is selected..
					$room_price = getValue('agents_rate', 'rack_rate_double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id.' and agent_id = '.$corporate_agent_id );
				else 			
					$room_price = getValue('room_plan_price', 'double_occupancy', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
			}
		}

//		echo $room_price;
		$extra_bed_price = getValue('room_plan_price', 'extra_bed_price', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
		$extra_person_charge = getValue('room_plan_price', 'extra_person_charge', "room_id = ".$room_id.' and plan_type_id = '.$plan_type_id );
		$luxury_tax = getValue('rooms', 'luxury_tax', "room_id = ".$room_id );

		$result[0] = $room_price;
		$result[1] = $extra_bed_price;
		$result[2] = $extra_person_charge;
		$result[3] = $luxury_tax;
		echo json_encode($result);
	}

	// define a function which will get a date differences from start and end date..it will return no of day  ....

	public function get_date_diff(){
		$checkin_date = $this->input->post('checkin_date');	
		$checkout_date = $this->input->post('checkout_date');	
		echo DaysDiff($checkin_date,$checkout_date);
	}

	// define a function which will get a guest data on the basis of mobile numebr or booking id or reference  ....
	public function get_guest_detail_mobile(){
		$mobile_no = $this->input->post('mobile_no');
		if(count_rows("guest_detail","phone = '".$mobile_no."'") > 0){
			$result[0] = getValue("guest_detail", "name", "phone = '".$mobile_no."'");		
			$result[1] = $mobile_no;
			$result[2] = getValue("guest_detail", "address", "phone = '".$mobile_no."'");
			$result[3] = getValue("guest_detail", "email", "phone = '".$mobile_no."'");
			$result[4] = getValue("guest_detail", "guest_id", "phone = '".$mobile_no."'");
			echo json_encode($result);			
		}else
			echo "0";
	}

	// write a code which will be used in a case if a hotel is having 5 room types..but at the time of reservation we have used only 3 room types......sso when at the time to edit a booking we will get an option to add one more room....once you will click on that button we need to fetch data from controller...by considering only remaining room types and the other thing will be same like remove and add room......

	public function get_remaining_room_type(){

		$booking_id = $this->input->post('booking_id');
		$hotel_id = $this->input->post('hotel_id');
		$no_of_div = $this->input->post('no_of_div');

		$room_id = $this->CR->getUsedRoomTypes($booking_id);
		$room_id = array_map('current', $room_id);
		$room_name = $this->CR->getUnusedRoomTypes($room_id,$hotel_id);
		//echo "<pre>"; print_r($room_name);
		$no_of_div++;
		
		$html = "";
		$html .= '<div class="row select_room" id="'.$no_of_div.'"  style="background: #f4f4f4;padding-top:20px;border-radius:5px;margin-top:5px;">
							                                        <input type="hidden" value="" id="room_no_'.$no_of_div.'" name="room_no['.$no_of_div.']" >						
							                                        <div class="row">
						                                        		<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Type * </label>
								                                                	<div class="col-sm-6" id="room_type"><select class="form-control room_id" id="room_id_'.$no_of_div.'" name="room_id[]" required=""><option value="">Select</option>';
		foreach ($room_name as $rn) {
			$html .= '<option value="'.$rn->room_id.'">'.$rn->room_name.'</option>';
		}								                                                	
		$html .='</select></div>
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Number of Rooms * </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control" id="no_of_rooms_'.$no_of_div.'" name="no_of_rooms[]" readonly required="" placeholder="" value="" /></div>
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Room Rate * </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control" id="room_rate_'.$no_of_div.'" name="room_rate[]" readonly required="" placeholder="" ></div>
								                                                	
							                                            		</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                                <span style="position:absolute;top:6px;">Occupancy * </span>
								                                                <div class="radio radio-success radio-inline" style="margin-left:80px;"><input type="radio" value="single" checked="checked" name="occupancy['.$no_of_div.'][]" onclick="occupancy('.$no_of_div.');"><label for="">Single</label></div>
										                                        <div class="radio radio-success radio-inline"><input type="radio" value="double" name="occupancy['.$no_of_div.'][]" onclick="occupancy('.$no_of_div.');"><label for="">Double</label></div>
										                                        
							                                        		</div>
						                                        		</div>
						                                        	</div>
							                                        	
							                                        <div class="row">
							                                        	<div class="col-md-12">
																			<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Net Rate * </label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="net_rate_'.$no_of_div.'" name="net_rate[]" placeholder="" maxlength="5" ></div>
								                                                	
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_'.$no_of_div.'" name="extra_bed[]" placeholder="" maxlength="5" ></div>
							                                            		</div>
							                                        		</div>

											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Bed Price</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_price_'.$no_of_div.'" name="extra_bed_price[]" placeholder="" maxlength="5" ></div>
							                                            		</div>
							                                        		</div>


							                                        		<div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_'.$no_of_div.'" name="extra_person[]" placeholder="" maxlength="5" ></div>
							                                            		</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>

						                                        	<div class="row">
						                                        		<div class="col-md-12">
											                                <div class="col-md-3"> 
							                                        			<div class="form-group">
								                                                	<label for="inputEmail3" class="col-sm-6 control-label">Extra Person Charge</label>
								                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_charge_'.$no_of_div.'" name="extra_person_charge[]" placeholder="" maxlength="5" ></div>
							                                            		</div>
							                                        		</div>

							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Luxury Tax (%)</label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="luxury_tax_'.$no_of_div.'" name="luxury_tax[]" placeholder="" maxlength="2" ></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date From * </label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control stay_date_from" id="stay_date_from_'.$no_of_div.'" name="stay_date_from[]" readonly placeholder=""  ></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                	<label for="inputEmail3" class="col-sm-6 control-label">Stay Date To *</label>
									                                                	<div class="col-sm-6"><input type="text" class="form-control stay_date_to" id="stay_date_to_'.$no_of_div.'" name="stay_date_to[]" readonly placeholder=""  ></div>
								                                            	</div>
							                                        		</div>
						                                        		</div>
						                                        	</div>
						                                        	<div class="row">
						                                        		<div class="col-md-12">
						                                        			<div class="col-md-3"> 
								                                        		<div class="form-group">
									                                                <label for="inputEmail3" class="col-sm-6 control-label">Selected Room Number</label>
									                                                <div class="col-sm-6" id="display_room_no_'.$no_of_div.'"></div>
								                                            	</div>
							                                        		</div>
							                                        		<div class="col-md-6"><div align="center"><button type="button" class="btn btn-success m-b-5 add_one_more_room" style="">Add one more Room</button><button type="button" class="btn btn-danger m-b-5 remove_room" id="'.$no_of_div.'" style="margin-left:10px;">Remove Room</button></div></div>
						                                        		</div>
						                                        	</div>
						                                    	</div>';
		echo $html;						                                    	
	}

}
