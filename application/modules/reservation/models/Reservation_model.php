<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_model extends CI_Model {

    public function getMealPlanByHotelId($hotel_id) {

        $this->db->select("pt.*");
        $this->db->from('plan_type pt');
        $this->db->join('hotel_plan_type hpt', 'pt.plan_type_id=hpt.plan_type_id');       
        $this->db->where('hpt.hotel_id', $hotel_id);       
        $query = $this->db->get();
        return $query->result();
    }

// define a function which will get a booked room on the basis of room id and checkin date and check out date....

    public function getBookedRoom($checkin_date, $checkout_date) {

        // here first we need to get an array of room on the basis of check in and check out date...
        // after getting an array of all rooms we will pass it to the below function code so dat it will retrun an empty room numbers on that date range..

        $this->db->select("br.room_number");
        $this->db->from('booked_room br');
        $this->db->join('booked_room_stay_date brsd', 'br.id = brsd.booked_room');
        $this->db->where('brsd.stay_date >=', $checkin_date);
        $this->db->where('brsd.stay_date <=', $checkout_date); 
        $this->db->group_by("br.room_number");         
        $query = $this->db->get();
        return $query->result_array();
    }

// define a function which will check whether the date range is from special room tariff or not ...

    public function isDateSpecialTariff($checkin_date,$checkout_date,$room_id,$plan_type_id) {

        $this->db->select("special_tariff_id");
        $this->db->from('room_special_tariff');
        $this->db->where('from_date <=', $checkin_date);
        $this->db->where('to_date >=', $checkout_date);
        $this->db->where('room_id', $room_id);
        $this->db->where('plan_type_id', $plan_type_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
// define a function which will get a vacant room on the basis of room id and checkin date and check out date....

    public function getVacantRoom($room_id,$room_no_array) {

        // after getting an array of all rooms we will get an empty room numbers on that date range..

        $this->db->select("rn.room_number");
        $this->db->from('room_number rn');
        $this->db->where('rn.room_id', $room_id);
        $this->db->where_not_in('rn.room_number', $room_no_array);
        $query = $this->db->get();
//      echo $this->db->last_query();
        return $query->result();
    }

// define a function which will return all details related to booking id... it will get a record from 4 diif tables and return it......

    public function getBookingDetails($booking_id) {

        // after getting an array of all rooms we will get an empty room numbers on that date range..

        $this->db->select("b.*, gd.name, gd.address, gd.phone, gd.email, h.hotel_name, t.payment_date, t.grand_total, t.advance, t.due, t.payment_type, t.bank_name, t.cheque_no, t.dd_no, t.card_no, t.expiry_date, t.account_no, t.ifsc_code");
        $this->db->from('bookings b');
        $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id');
        $this->db->join('hotels h', 'b.hotel_id = h.hotel_id');
        $this->db->join('transaction t', 'b.booking_id = t.booking_id');
        $this->db->where('b.booking_id', $booking_id);
        $this->db->order_by("t.transaction_id", "desc");
        $this->db->limit(1); 
        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->row();
    }

// define a function which will get a vacant room on the basis of room id and current date ....

    public function getVacantRoomDateWise($room_id,$date) {

        $query = $this->db->query('SELECT rn.room_number FROM room_number rn WHERE rn.room_id = '.$room_id.' AND rn.room_number NOT IN (SELECT DISTINCT(br.room_number) from booked_room br JOIN booked_room_stay_date brsd ON br.id = brsd.booked_room WHERE br.room_id = '.$room_id.' AND brsd.stay_date = "'.$date.'" GROUP BY br.room_id ) order BY rn.room_number asc');        
//      echo $this->db->last_query();
        return $query->result();
    }

// define a function which will return a min and max date...it will be used stay date from and satay date to for a room no ....

    public function getMinMaxStayDate($booked_room) {

        // $this->db->select_min('stay_date as stay_date_from');
        // $this->db->select_max('stay_date as stay_date_to');
        // $this->db->where('booked_room', $booked_room);
        // $query = $this->db->get();
        $query = $this->db->query('SELECT MIN(stay_date) as stay_date_from, max(stay_date) as stay_date_to FROM booked_room_stay_date WHERE booked_room = '.$booked_room);
//        echo $this->db->last_query();
        return $query->row();
    }

// define a function which will return how many room types are used during booking..suppose a hotel is having 5 room types and during booking we have used 3 room types then at the time to edit a booking we will provide a button and once you will click on that button you will get a list of all remaiinnign rom type as a drop down...it will be used on edit a booking page.......

    public function getNoOfUsedRoomTypes($booking_id) {
       $query = $this->db->query('SELECT count(DISTINCT r.room_id) as max_room_type_used FROM (booked_room_detail brd) LEFT JOIN booked_room br ON brd.booked_room_id = br.booked_room_id LEFT JOIN bookings b ON b.booking_id = brd.booking_id LEFT JOIN rooms r ON r.room_id = br.room_id WHERE b.booking_id = '.$booking_id.' GROUP BY  b.booking_id');
//        echo $this->db->last_query();
        return $query->row();
    }

// define a function which will return used room types during booking..suppose a hotel is having 5 room types and during booking we have used 3 room types then at the time to edit a booking we will provide a button and once you will click on that button you will get a list of all remaiinnign room type as a drop down...it will be used on edit a booking page.......

    public function getUsedRoomTypes($booking_id) {
       $query = $this->db->query('SELECT DISTINCT r.room_id FROM booked_room_detail brd LEFT JOIN booked_room br ON brd.booked_room_id = br.booked_room_id LEFT JOIN bookings b ON b.booking_id = brd.booking_id LEFT JOIN rooms r ON r.room_id = br.room_id WHERE b.booking_id = '.$booking_id.' GROUP BY  b.booking_id');
//        echo $this->db->last_query();
        return $query->result();
    }

// define a function which will return unused room types during booking..suppose a hotel is having 5 room types and during booking we have used 3 room types then at the time to edit a booking we will provide a button and once you will click on that button you will get a list of all unused room type as a drop down...it will be used on edit a booking page.......

    public function getUnusedRoomTypes($room_id,$hotel_id) {
        $this->db->select("r.room_name,r.room_id");
        $this->db->from('rooms r');
        $this->db->where('r.hotel_id', $hotel_id);
        $this->db->where_not_in('r.room_id', $room_id);
        $query = $this->db->get();
//      echo $this->db->last_query();
        return $query->result();      
    }

}

?>