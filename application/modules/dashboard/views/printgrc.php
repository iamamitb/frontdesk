<link href="<?=base_url()?>themes/css/views/print_grc.css" rel="stylesheet" type="text/css">

<style>
@media print {
   .col-md-3{width:25%;float:left;;line-height:3px;}
   h1{font-size:20px;line-height:0px;}
   h3{font-size:14px;}
   span, p{font-size:11px;}
   #guest_details, #foreginer_details, #hotel_use{}   
   
}
</style>

<title>Print Guest Registration Form</title>

<div class="row" align="center" style="">
<h1><?=$hotel_info->hotel_name ?></h1><br class="grc">Guest Registration Card</p>
</div>

<!-- Bill body -->
<div class="row" id="bill">
	<!-- Guest details -->
	<div class="row" id="guest_details">
		<h3>Guest Details</h3>

		<div class="row" style="margin-left:5%;margin-right:5%;padding:10px;">
			<div class="col-md-12">
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Guest Name</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->name ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><b>Mobile Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->phone ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Organization</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->organization ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Designation</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->designation ?></span>
				</div>
			</div>
		</div>

		<div class="row" style="margin-left:5%;margin-right:5%;padding:10px;">
			<div class="col-md-12" style="margin-left:10px;">
				<span><b>Address</b></span>
				
				<p><?=$guest_info->address ?></p>
			</div>
		</div>

		<div class="row" style="margin-left:5%;margin-right:5%;padding:10px;">
			<div class="col-md-12">
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Email address</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->email ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><b>Date of Birth</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->date_of_birth,"d-m-Y","-") ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Arrived From</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->arrived_from ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Going to</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->going_to ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>ID Card Type</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?php 
					if($guest_info->id_card_type == "driving_licence") echo "Driving Licence"; 
					if($guest_info->id_card_type == "pan_card") echo "PAN Card"; 
					if($guest_info->id_card_type == "passport") echo "Passport"; 
					if($guest_info->id_card_type == "aadhar_card") echo "Aadhar Card"; 
					if($guest_info->id_card_type == "voter_card") echo "Voter Card"; 
					if($guest_info->id_card_type == "others") echo "Others"; 
					?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>ID Card Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->id_card_number ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Arrival date</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($booking_info->checkin_date,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Arrival Time</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->checkin_time ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Departure date</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($booking_info->checkout_date,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Departure Time</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->checkout_time ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Mode Of Payment</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->payment_type ?></span>
				</div>
				<?php if($booking_info->payment_type == "cheque" || $booking_info->payment_type == "demand_draft" || $booking_info->payment_type == "neft" || $booking_info->payment_type == "rtgs" ) { ?>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Bank Name</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->bank_name ?></span>
				</div>
				<?php if($booking_info->payment_type == "neft" || $booking_info->payment_type == "rtgs" ) { ?>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Account Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->account_no ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>IFSC Code</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->ifsc_code ?></span>
				</div>
				<?php } if($booking_info->payment_type == "cheque" ) { ?>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Cheque Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->cheque_no ?></span>
				</div>
				<?php } if($booking_info->payment_type == "demand_draft" ) { ?>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>DD Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->dd_no ?></span>
				</div>

				<?php } } if($booking_info->payment_type == "credit_card" || $booking_info->payment_type == "debit_card" ) { ?>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Card Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->card_no ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Expiry Date</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info->expiry_date ?></span>
				</div>
				<?php } ?>
			</div>
		</div>


	</div>
	<!-- Guest details ends -->
	<?php if($guest_info->is_foreigner == 1) { // it means current guest is a foreigner...  ?>
	<!-- Foreginer details -->
	<div class="row" id="foreginer_details">
		<h3>Foreigner Details</h3>

		<div class="row" style="margin-left:5%;margin-right:5%;padding:10px;">
			<div class="col-md-12">
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Nationality</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=get_country_name($guest_info->nationality) ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><b>Passport Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->passport_no ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Date of Issue</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->passport_issue_date,"d-m-Y","-") ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Date of Expiry</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->passport_expiry_date,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Arrival in India</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->arrival_date_india,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Visa Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->visa_no ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Date of Issue</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->visa_issue_date,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Date of Expiry</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=dateformate($guest_info->visa_expiry_date,"d-m-Y","-") ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Employed in India</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?php if($guest_info->is_employed_in_india == 0) echo "Yes"; else if($guest_info->is_employed_in_india == 1) echo "No"; ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Duration Of Stay</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$guest_info->duration_in_india ?></span>
				</div>
			</div>
		</div>

	</div>
	<!-- Foreginer details ends -->
	<?php } ?>
	
	<!-- Hotel use details -->
	<div class="row" id="hot el_use">
		<h3>For Hotel Use</h3>

		<div class="row" style="margin-left:5%;margin-right:5%;padding:10px;">
			<div class="col-md-12">
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Room Number</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['rooms'] ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><b>Room Type</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['room_type'] ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Room Rate</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['total_room_tariff'] ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Adult</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['total_adults'] ?></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Child</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['total_child'] ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Extra Bed</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['extra_bed'] ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Meal Plan</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['plan_name'] ?></span>
				</div>

				<div class="col-md-3" style="margin-bottom:10px;"><span><b>Booked By</b></span>
				</div>
				<div class="col-md-3" style="margin-bottom:10px;">
					<span><?=$booking_info[0]['full_name'] ?></span>
				</div>
			</div>
		</div>
	</div>
	<!-- Hotel use details ends -->

</div>
<!-- Bill body ends -->

<!-- Terms and conditions -->
<br>
<div class="row" id="terms">
	<p><b>Terms and Conditions</b></p>

	<div class="row">
		<p><?=$hotel_info->hotel_terms_and_conditions ?></p>
	</div>
</div>

<!-- Signature -->

<div class="row" style="margin-left:5%;margin-right:5%;">
	<div style="float:left;"><b>Front Office</b></div>
	<div style="float:right;"><b>Guest Signature</b></div>
</div>