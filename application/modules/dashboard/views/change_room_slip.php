<title>Print Room Change Slip</title>
<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
.panel-title{text-align:center;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Hotel Name</h1>
    <p align="center"><b>Room Change Acknowledgement Slip</b></p>
</div>

<div class="row">
    <div class="row">
                    <div class="panel panel-border panel-inverse">
                        
                        <div class="panel-heading"> 
                            <h3 class="panel-title">21st February 2016</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Current Room Type</th>
                                        <th>Current Room Number</th>
                                        <th>New Room Type</th>
                                        <th>New Room Number</th>
                                        <th>Sell Price</th>
                                        <th>Extra Bed</th>
                                        <th>Extra Bed Price</th>
                                        <th>Extra Person</th>
                                        <th>Extra Person Charge</th>
                                        <th>Luxury Tax</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div> 

                        <div class="panel-heading"> 
                            <h3 class="panel-title">21st February 2016</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Current Room Type</th>
                                        <th>Current Room Number</th>
                                        <th>New Room Type</th>
                                        <th>New Room Number</th>
                                        <th>Sell Price</th>
                                        <th>Extra Bed</th>
                                        <th>Extra Bed Price</th>
                                        <th>Extra Person</th>
                                        <th>Extra Person Charge</th>
                                        <th>Luxury Tax</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
    </div>

    <div class="row">
        <div class="panel panel-border panel-inverse">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">Price Difference</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Original Room Tariff</th>
                                        <th>New Room Tariff</th>
                                        <th>Difference</th>
                                        <th>Discount</th>
                                        <th>Net Difference</th>
                                        <th>Service Tax</th>
                                        <th>Payable Difference</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    
                                   <tr>
                                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                   </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
    </div>
</div>

<div class="row" style="margin-left:10%;margin-right:10%;">
    <p>I hereby acknowledge the change of my Room and agree to pay the difference amount incurred(if any) to Hotel Name for changing my room</p>
	
    <p><b>Date</b></p><br>
    <p><b>Guest Signature</b></p><br>
    
</div>