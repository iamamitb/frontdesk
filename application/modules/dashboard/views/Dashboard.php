<link href="<?=base_url()?>themes/css/views/dashboard.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>themes/css/jquery-ui-timepicker-addon.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Dashboard</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

<!--Checkin panel starts-->
<div class="col-md-12">
	<div class="panel panel-color panel-success">
        <div class="panel-heading"> 
            <h3 class="panel-title">Today's Arrival</h3> 
        </div> 
        <div class="panel-body"> 
            <table class="table table-striped table-bordered table-hover">
            <?php if(isset($today_arrival) && count($today_arrival) > 0) { // it means there is an arrival today ?>
    			<thead>
    				<tr>
    					<th>Booking ID</th>
    					<th>Name</th>
    					<th>Phone</th>
    					<th>Rooms</th>
    					<th>Pax</th>
    					<th>EB</th>
    					<th>Plan</th>
    					<th>Agent</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Actions</th>                        
    				</tr>
    			</thead>
                <tbody id="get_search" class="get_search">
                <?php foreach($today_arrival as $arrival) { // it means there is an arrival today ?>
					<tr>
						<td><?= $arrival->booking_reference; ?></td>
						<td><?= $arrival->guest_name; ?></td>
						<td><span><?= $arrival->guest_phone; ?></span></td>
                        <td><?= $arrival->rooms; ?></td>
						<td><?php echo $arrival->total_adults; if($arrival->total_child != "" && $arrival->total_child > 0) echo " + ".$arrival->total_child; ?></td> 
						<td><?= $arrival->extra_bed; ?></td>
                        <td><?= $arrival->plan_name; ?></td>
						<td><?php if($arrival->agent_name != "" ) echo $arrival->agent_name; else echo "NA" ?></td>
                        <td><?=dateformate($arrival->checkin_date,"d-M-Y","-"); ?></td>
                        <td><?=dateformate($arrival->checkout_date,"d-M-Y","-"); ?></td>
						<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" id="<?=$arrival->booking_id ?>" class="booking_id"  data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-briefcase"></i><span class="link_text">Checkin Guest</span></a></li>                                  
                                    <li><a href="#" id="<?=$arrival->booking_id ?>" class="sa-warning"  data-toggle="modal"><i class="md md-location-off"></i><span class="link_text">No Show</span></a></li>                                    
                                </ul>
                            </div>
                            
                        </td>
                    </tr>
                <?php } ?>
				</tbody>
            <?php } else { ?>
                <tbody class="get_search">
                    <tr>
                        <th colspan="9">Sorry there is no arrival today.</th>
                    </tr>
                </tbody>            
            <?php } ?>
			</table>
        </div> 
    </div>
</div>
<!--Checkin panel ends-->

<!--Occupancy panel starts-->

<div class="col-md-12">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">Today's Occupancy</h3> 
        </div> 
        <div class="panel-body"> 
            <table class="table table-striped table-bordered table-hover">
            <?php if(isset($today_occupancy) && count($today_occupancy) > 0) { // it means there is an arrival today ?>
                <thead>
                    <tr>
                        <th>Room Number</th>
                        <th>Guest Name</th>
                        <th>PAX</th>
                        <th>Phone</th>
                        <th>Meal Plan</th>
                        <th>Coming From</th>
                        <th>Reason For Stay</th>
                        <th>Nationality</th>
                        <th>ID Type/ No.</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Action</th>                        
                    </tr>
                </thead>
                <tbody id="get_search" class="get_search">
                <?php foreach($today_occupancy as $occupancy) { // it means there is an arrival today ?>
                    <tr>
                        <td><span class="label label-inverse"><?=$occupancy->rooms; ?></span></td>
                        <td><?=$occupancy->guest_name; ?></td>
                        <td><?php echo $occupancy->total_adults; if($occupancy->total_child != "" && $occupancy->total_child > 0) echo " + ".$occupancy->total_child; ?></td>
                        <td><span><?=$occupancy->guest_phone; ?></span></td>
                        <td><?=$occupancy->plan_name; ?></td> 
                        <td><?=$occupancy->arrived_from; ?></td>
                        <td><?=$occupancy->purpose; ?></td>
                        <td><?=get_country_name($occupancy->nationality); ?></td>
                        <td><?=$occupancy->id_card_type; ?> / <?=$occupancy->id_card_number; ?></td>
                        <td><?=dateformate($occupancy->checkin_date,"d-M-Y","-"); ?></td>
                        <td><?=dateformate($occupancy->checkout_date,"d-M-Y","-"); ?></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?=base_url()?>reservation/edit_booking/<?=$occupancy->booking_id?>" id="briefcase_id"  data-toggle="modal" ><i class="fa fa-exchange"></i><span class="link_text">Change Room</span></a></li>
                                    <li><a href="<?=base_url()?>checkout/checkout_guest/<?=$occupancy->booking_id; ?>"><i class="ion-person"></i><span class="link_text">Checkout</span></a></li>
                                    <li><a href="<?=base_url()?>dashboard/show_bill/<?=$occupancy->booking_id; ?>" id=""  data-toggle="modal"><i class="md md-print"></i><span class="link_text">Print Bill</span></a></li>
                                    <li><a href="<?=base_url()?>reservation/edit_booking/<?=$occupancy->booking_id?>" id="sa-warning"  data-toggle="modal" ><i class="fa fa-plus"></i><span class="link_text">Extend Booking</span></a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
                <?php } else { ?>
                    <tbody class="get_search">
                        <tr>
                            <th colspan="9">Sorry there is no occupancy today.</th>
                        </tr>
                    </tbody>            
                <?php } ?>
            </table>
        </div> 
    </div>
</div>
<!--Occupancy panel ends-->   

<!--Checkout panel starts-->

<div class="col-md-12">
	<div class="panel panel-color panel-danger">
        <div class="panel-heading"> 
            <h3 class="panel-title">Today's Checkout</h3> 
        </div> 
        <div class="panel-body"> 
            <table class="table table-striped table-bordered table-hover">
            <?php if(isset($today_checkout) && count($today_checkout) > 0) { // it means there is an arrival today ?>
				<thead>
					<tr>
						<th>Booking ID</th>
						<th>Guest Name</th>
                        <th>Hotel Name</th>
						<th>Phone</th>
						<th>Rooms</th>
						<th>Pax</th>
						<th>EB</th>
						<th>Plan</th>
						<th>Agent</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Action</th>                        
					</tr>
				</thead>
				<tbody id="get_search" class="get_search">
                <?php foreach($today_checkout as $checkout) { // it means there is an arrival today ?>
					<tr>
						<td><?=$checkout->booking_reference; ?></td>
						<td><?=$checkout->guest_name; ?></td>
                        <td><?=$checkout->hotel_name; ?></td>
						<td><span><?=$checkout->guest_phone; ?></span></td>
						<td><span class="label label-inverse"><?=$checkout->rooms; ?></span></td> 
						<td><?php echo $checkout->total_adults; if($checkout->total_child != "" && $checkout->total_child > 0) echo " + ".$checkout->total_child; ?></td>
						<td><?=$checkout->extra_bed; ?></td>
						<td><?=$checkout->plan_name; ?></td>
						<td><?php if($checkout->agent_name != "" ) echo $checkout->agent_name; else echo "NA" ?></td>
                        <td><?=dateformate($checkout->checkin_date,"d-M-Y","-"); ?></td>
                        <td><?=dateformate($checkout->checkout_date,"d-M-Y","-"); ?></td>
						<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?=base_url()?>checkout/checkout_guest/<?=$checkout->booking_id; ?>"><i class="ion-person"></i><span class="link_text">Checkout</span></a></li>
                                    <li><a href="<?=base_url()?>reservation/edit_booking/<?=$checkout->booking_id?>" id="sa-warning"  data-toggle="modal" ><i class="fa fa-plus"></i><span class="link_text">Extend Booking</span></a></li>
                                </ul>
                            </div>
                        </td>
					</tr>
                <?php } ?>
                </tbody>
                <?php } else { ?>
                    <tbody class="get_search">
                        <tr>
                            <th colspan="9">Sorry there is no checkout today.</th>
                        </tr>
                    </tbody>            
                <?php } ?>
			</table>
        </div> 
    </div>
</div>
<!--Checkout panel ends-->                       

</div> <!-- End row -->
<!-- Page content ends -->  

<!-- Check in Modal --> 
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="close_modal_id"  class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Guest Registration Card</h2>
            </div>
            <div class="modal-body">
              <!-- Checkin form starts -->
                <form name="grc_form" id="grc_form" action="<?= base_url().'dashboard/update_checkin_status' ?>" method="post" class="" enctype="multipart/form-data">
                    <input type="hidden" name="booking_id" id="booking_id" value="" />
                    <input type="hidden" name="users_id" id="users_id" value="" />
                    <input type="hidden" name="guest_id" id="guest_id" value="" />
                    <input type="hidden" name="hotel_id" id="hotel_id" value="" />
                <div class="row">
              		<div class="row">
              			<div class-"col-md-12">
              				<div class="col-md-8">
              					<div class="checkbox checkbox-success">
                                    <input id="is_foreigner" type="checkbox" name="is_foreigner" value="1">
                                    <label for="is_foreigner"><b>Foreigner?</b></label>
                                </div>
              				</div>
              				<div class="col-md-4">
              					Booking ID: <span id="booking_reference"><b>BKI - 890345</b></span>
              				</div>              			
              			</div>
              		</div><hr>
              		<div class="row">
              			<div class="panel panel-border panel-primary">
                            <div class="panel-heading"> 
                                <h3 class="panel-title">Guest Details</h3> 
                            </div> 
                            <div class="panel-body"> 
                            	<!-- First row -->
                            	<div class="row">
                            		<div class="col-md-12">
	                            		<div class="col-md-3">
	                            			<div class="form-group">
                                                <label for="">Guest Name <span class="error">*</span> </label>
                                                <input type="text" class="form-control only_character" id="guest_name" required aria-required="true" name="guest_name" placeholder="">
                                                <span class="error"><?php echo form_error('guest_name'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Address <span class="error">*</span> </label>
                                                <input type="text" class="form-control" name="address" id="address" maxlength="60" required aria-required="true" placeholder="">
                                                <span class="error"><?php echo form_error('address'); ?></span>
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Arrived From <span class="error">*</span> </label>
                                                <input type="text" class="form-control only_character" id="arrived_from" name="arrived_from" placeholder="" required aria-required="true">
                                                <span class="error"><?php echo form_error('arrived_from'); ?></span>
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Going To <span class="error">*</span></label>
                                                <input type="text" class="form-control only_character" id="going_to" name="going_to" placeholder="" required aria-required="true">
                                                <span class="error"><?php echo form_error('going_to'); ?></span>
                                            </div>

	                            		</div>

	                            		<div class="col-md-3">
	                            			<div class="form-group">
                                                <label for="">Mobile Number <span class="error">*</span> </label>
                                                <input type="text" class="form-control only_digit" name="phone" value="" id="phone" maxlength="10" placeholder="" required aria-required="true">
                                                <span class="error"><?php echo form_error('phone'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Organization </label>
                                                <input type="text" class="form-control only_character" id="organization" name="organization" placeholder="">
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Police Station <span class="error">*</span> </label>
                                                <input type="text" class="form-control only_character" id="police_station" name="police_station" placeholder="" required aria-required="true">
                                                <span class="error"><?php echo form_error('police_station'); ?></span>
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Date of Birth <span class="error">*</span></label>
                                                <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="DD-MM-YYYY" required aria-required="true">
                                                <span class="error"><?php echo form_error('date_of_birth'); ?></span>
                                            </div>
                                            
	                            		</div>

	                            		<div class="col-md-3">
	                            			<div class="form-group">
                                                <label for="">Email address <span class="error">*</span></label>
                                                <input type="email" class="form-control" name="email" value="" id="email" maxlength="60" placeholder="Email" required aria-required="true">
                                                <span class="error"><?php echo form_error('email'); ?></span>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Designation</label>
                                                <input type="text" class="form-control only_character" id="designation" name="designation" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Mode Of Payment <span class="error">*</span> </label>
                                                <?=get_payment_type_list() ?>
                                                <span class="error"><?php echo form_error('payment_type'); ?></span>
                                            </div>                                            
	                            		</div>

	                            		<div class="col-md-3" align="center">
	                            			<div class="form-group">
                                                <label for=""><b>Guest Photo</b></label><br>
                                                <!-- For webcam -->
                                                <div id="my_web_camera"></div>
                                                 <!-- webcam end -->
                                                <div id="my_web_camera_picture"></div>
                                                <input id="guest_image" type="hidden" name="guest_image" value=""/>
                                               <!-- <img src="<?=base_url()?>/themes/images/webcam.png" width="180px" height="180px"> -->
                                            </div>
	                            		</div>
                            		</div>
                            	</div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Arrival Date <span class="error">*</span></label>
                                                <input type="text" name="checkin_date" value="" id="checkin_date" readonly="readonly" placeholder="Arrival Date" required aria-required="true" class="form-control" readonly="">
                                                <span class="error"><?php echo form_error('checkin_date'); ?></span>
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Time <span class="error">*</span> </label>
                                                <input type="text" name="check_in_time" id="check_in_time" value="" readonly="readonly" class="form-control" required aria-required="true" placeholder="Arrival Time" readonly="">
                                                <span class="error"><?php echo form_error('check_in_time'); ?></span>
                                            </div>

                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Departure Date <span class="error">*</span></label>
                                                <input type="text" name="checkout_date" id="checkout_date" readonly="readonly" placeholder="Departure Date" class="form-control checkin_checkout_date" style="cursor: pointer;" required aria-required="true">
                                                <span class="error"><?php echo form_error('checkout_date'); ?></span>
                                            </div>

                                            <div class="form-group" style="width:46%;float:left;margin-right:5px;">
                                                <label for="">Time <span class="error">*</span></label>
                                                <input type="text" name="check_out_time" id="check_out_time" value="" readonly="readonly" placeholder="Departure Time" class="form-control checkin_checkout_date" style="cursor: pointer;" required aria-required="true">
                                                <span class="error"><?php echo form_error('check_out_time'); ?></span>
                                            </div>
                                            
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">ID Card Type </label>
                                                <?=get_id_card_type_list() ?>
                                                <span class="error"><?php echo form_error('id_card_type'); ?></span>
                                            </div>                                            
                                        </div>

                                        <div class="col-md-3" align="center">
                                            <div class="form-group">
                                                <label for=""></label>
                                                <div class="col-md-6">
                                                <!-- <button type="button" class="btn btn-primary">Upload</button> -->
                                                <button type="button" class="btn btn-default" id="shoot_another_picture">Shoot Another </button>
                                                </div>
                                                <div class="col-md-6">
                                                <button type="button" class="btn btn-default" id="shoot_picture">Shoot Picture</button>
                                                <button type="button" class="btn btn-default" id="save_picture">Save Picture</button>
                                                <button type="button" class="btn btn-default" id="shoot_again">Shoot Again</button>
                                                </div>
                                                <!-- For webcam -->
                                                
                                                 <!-- webcam end -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            	<!-- Second row-->
                                <div class="row">
                                    <div class="col-md-3 form-group"> 
                                        <label for="field-1" class="control-label">ID Card Number </label> 
                                        <input type="text" class="form-control" id="id_card_number" name="id_card_number" placeholder="ID Card Number" maxlength="15" > 
                                        <span class="error"><?php echo form_error('card_number'); ?></span>
                                    </div> 
                                    <div class="col-md-9 form-group"> 
                                        <label for="field-1" class="control-label">Purpose of Visit <span class="error">*</span> </label> 
                                        <input type="text" class="form-control" id="purpose" name="purpose" placeholder="Purpose of Visit" required aria-required="true" > 
                                        <span class="error"><?php echo form_error('purpose'); ?></span>
                                    </div> 
                                </div>
                            	<!-- Third row (load on AJAX) -->
                            	<div class="row">
                                    <div id="cheque_details_central_reservation" >
                                        <div class="col-md-3" id="chq_details_central_reservation"> 
                                            <div class="form-group"> 
                                                <label for="field-1" class="control-label">Cheque Number <span class="error">*</span> </label> 
                                                <input type="text" class="form-control only_digit" id="cheque_no" name="cheque_no" placeholder="Cheque Number" maxlength="10" > 
                                                <span class="error"><?php echo form_error('cheque_no'); ?></span>
                                            </div> 
                                        </div> 
                                        <div class="col-md-3" id="dd_details_central_reservation"> 
                                            <div class="form-group"> 
                                                <label for="field-1" class="control-label">DD Number <span class="error">*</span> </label> 
                                                <input type="text" class="form-control only_digit" id="dd_no" name="dd_no" placeholder="DD Number" maxlength="10"> 
                                                <span class="error"><?php echo form_error('dd_no'); ?></span>
                                            </div> 
                                        </div> 

                                    </div>                                                      
                                    <div class="col-md-3 bank_name_status"> 
                                        <div class="form-group"> 
                                            <label for="field-2" class="control-label">Bank Name <span class="error">*</span> </label> 
                                            <input type="text" class="form-control only_character bank_name"  name="bank_name" placeholder="Bank" id="bank_name"> 
                                            <span class="error"><?php echo form_error('bank_name'); ?></span>
                                        </div> 
                                    </div>                                                              
                                    <div id="cc_details_central_reservation" >
                                        <div class="col-md-3"> 
                                            <div class="form-group"> 
                                                <label for="field-1" class="control-label">Card Number <span class="error">*</span> </label> 
                                                <input type="text" class="form-control only_digit" name="card_no" id="card_no" placeholder="Card Number" maxlength="16"> 
                                                <span class="error"><?php echo form_error('card_no'); ?></span>
                                            </div> 
                                        </div> 
                                        <div class="col-md-3"> 
                                            <div class="form-group"> 
                                                <label for="field-2" class="control-label">Expiry Date <span class="error">*</span> </label> 
                                                <input type="text" class="form-control" name ="expiry_date" id="expiry_date" placeholder="Expiry Date mm/yy"> 
                                                <span class="error"><?php echo form_error('expiry_date'); ?></span>
                                            </div> 
                                        </div>                                                              
                                    </div>
                                    <div id="neft_details_central_reservation" >
                                        <div class="col-md-3"> 
                                            <div class="form-group"> 
                                                <label for="field-1" class="control-label">Account Number <span class="error">*</span> </label> 
                                                <input type="text" class="form-control only_digit" name="account_no" id="account_no" placeholder="Account Number" maxlength="10"> 
                                                <span class="error"><?php echo form_error('account_no'); ?></span>
                                            </div> 
                                        </div> 
                                        <div class="col-md-3"> 
                                            <div class="form-group"> 
                                                <label for="field-2" class="control-label">IFSC Code</label> 
                                                <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="IFSC Code"> 
                                            </div> 
                                        </div>                                                              
                                    </div>
                            	</div>
                            </div> 
                        </div>

              		</div>
              		<div class="row" id="foreigner">
              			<div class="panel panel-border panel-danger">
                            <div class="panel-heading"> 
                                <h3 class="panel-title">Foreigner's Only</h3> 
                            </div> 
                            <div class="panel-body"> 
                            	<!-- First row -->
                            	<div class="row">
                            		<div class="col-md-12">
                            			<div class="col-md-2">
                            				<div class="form-group">
                                                <label for="">Nationality <span class="error">*</span> </label>
                                                <?//=country_dropdown()?>
                                            </div>
                            			</div>

                            			<div class="col-md-2">
      										<div class="form-group">
                                                <label for="">Passport Number <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="passport_no" name="passport_no" placeholder="">
                                            </div>
    									</div>                            				
                            			<div class="col-md-2">
      										<div class="form-group">
                                                <label for="">Date Of Issue <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="passport_issue_date" placeholder="DD-MM-YYYY" name="passport_issue_date">
                                            </div>
                            			</div>

                            			<div class="col-md-3">
      										<div class="form-group">
                                                <label for="">Date of Expiry <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="passport_expiry_date" name="passport_expiry_date" placeholder="DD-MM-YYYY">
                                            </div>
                            			</div>

                            			<div class="col-md-3">
                            				<div class="form-group">
                                                <label for="">Date of Arrival in India <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="arrival_date_india" name="arrival_date_india" placeholder="DD-MM-YYYY">
                                            </div>

                            			</div>
                            		</div>
                            	</div>
                            	<!-- Second row -->
                            	<div class="row">
                            		<div class="col-md-12">
                            			<div class="col-md-2">
                            				<div class="form-group">
                                                <label for="">Visa Number <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="visa_no" name="visa_no" placeholder="">
	                                        </div>
                            			</div>

                            			<div class="col-md-2">
	      										<div class="form-group">
	                                                <label for="">Date Of Issue <span class="error">*</span> </label>
	                                                <input type="text" class="form-control" id="visa_issue_date" name="visa_issue_date" placeholder="DD-MM-YYYY">
	                                            </div>
    									</div>
                            				

                            			<div class="col-md-2">
	      										<div class="form-group">
	                                                <label for="">Date Of Expiry <span class="error">*</span> </label>
	                                                <input type="text" class="form-control" id="visa_expiry_date" name="visa_expiry_date" placeholder="DD-MM-YYYY">
	                                            </div>
                            			</div>

                            			<div class="col-md-3">
      										<div class="form-group">
                                                <label for="">Whether Employed in India</label>
                                                <select class="form-control">
                                                    <option>Select</option>
                                                    <option value="0">Yes</option>
                                                    <option value="1">No</option>
                                                </select>
                                            </div>
                            			</div>

                            			<div class="col-md-3">
                            				<div class="form-group">
                                                <label for="">Proposed Duration of Stay in India</label>
                                                <input type="text" class="form-control" id="duration_in_india" name="duration_in_india" placeholder="">
                                            </div>

                            			</div>
                            		</div>
                            	</div>
                                <!-- Third row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Place of Arrival in India <span class="error">*</span> </label>
                                                <input type="text" class="form-control" id="arrival_place_india" name="arrival_place_india" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            	
                            </div> 
                        </div>
              		</div>
              		<div class="row">
              			<div class="panel panel-border panel-primary">
                            <div class="panel-heading"> 
                                <h3 class="panel-title">For Hotel Use</h3> 
                            </div> 
                            <div class="panel-body"> 
                            	<div class="row">
                            		<div class="col-md-12">
                            			<div class="col-md-2">
                            				<div class="form-group">
	                                            <label for="">Room Number</label>
	                                            <input type="text" class="form-control" id="room_no" name="room_no" readonly="">
	                                        </div>
                            			</div>

                            			<div class="col-md-2">
                            				<div class="form-group">
	                                            <label for="">Room Type</label>
	                                            <input type="text" class="form-control" id="room_type" name="room_type" placeholder="" readonly="">
	                                        </div>
                            			</div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="">Edit Booking</label>
                                                <button type="button" class="btn btn-primary waves-effect waves-light w-xs m-b-5" data-toggle="modal" id="change_room" >Edit Booking</button>
                                            </div>
                                        </div>
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Room Rate</label>
	                                            <input type="text" class="form-control" id="room_rate" name="room_rate" placeholder="" readonly="">
	                                        </div>
                            			</div>
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Adults</label>
	                                            <input type="text" class="form-control only_digit" id="total_adults" name="total_adults" placeholder="" readonly="">
	                                        </div>
                            			</div>
                            		</div>
                            	</div>
                            	<div class="row">
                            		<div class="col-md-12">
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Children</label>
	                                            <input type="text" class="form-control only_digit" id="total_child" name="total_child" placeholder="" readonly="">
	                                        </div>
                            			</div>
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Extra Bed</label>
	                                            <input type="text" class="form-control only_digit" id="extra_bed" name="extra_bed" placeholder="" readonly="">
	                                        </div>
                            			</div>
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Meal Plan <span class="error">*</span> </label>
                                                <?php echo get_data_dropdown("plan_type","plan_type_id","name",'','','plan_type_id','plan_type_id','required'); ?>
                                                <span class="error"><?php echo form_error('plan_type_id'); ?></span>
                                            </div>
                            			</div>
                            			<div class="col-md-3">
                            				<div class="form-group">
	                                            <label for="">Booked By <span class="error">*</span> </label>
                                                <?php echo get_data_dropdown("users","id","full_name","account_id = ".$this->session->userdata('account_id'),'','user_id','user_id','required'); ?>
                                                <span class="error"><?php echo form_error('user_id'); ?></span>
	                                        </div>
                            			</div>
                            		</div>
                            	</div>
                            	<div class="row">
                            		<div class="form-group">
                                        <label for="">Billing Instructions</label>
                                        <textarea class="form-control autogrow" id="billing_instruction" name="billing_instruction" placeholder="" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px"></textarea>
                                    </div>
                            	</div>

                            	<div class="row" align="center">
                            		<!-- <a target="_blank" href="dashboard/print_guest_reg_card"><button type="button" class="btn btn-success btn-lg">Checkin Guest and Print registration card</button> </a> 
                                    <input type="submit" name="checkin_guest_print_grc" id="checkin_guest_print_grc" value="Checkin Guest and Print registration card" class="btn btn-success btn-lg" />-->
                                    <button type="button" class="btn btn-success btn-lg" name="checkin_guest_print_grc" id="checkin_guest_print_grc">Checkin Guest and Print registration card</button>
                                    <input type="submit" name="checkin_guest" id="checkin_guest" value="Checkin Guest" class="btn btn-primary btn-lg" />
			    					<!-- <button type="button" class="btn btn-primary btn-lg">Checkin Guest</button> --> 
                            	</div>


                            </div> 
                        </div>
              		</div>

                </div>
                </form>
              <!-- Checkin Form ends-->
            </div>            			    
			
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- End of Modal -->

<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>themes/js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>themes/js/jquery-ui-timepicker-addon.js" ></script>

<!-- End of Modal -->
<!-- For webcam Js -->
<script src="<?=base_url()?>themes/js/webcam.js"></script>
<script src="<?=base_url()?>themes/js/views/dashboard.js"></script>

<!-- End webcam Js -->