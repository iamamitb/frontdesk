<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
		
    public function getTodayArrivalList() {

        $this->db->select("b.booking_id, b.booking_reference, b.total_adults, b.total_child, b.checkin_date, b.checkout_date, gd.name as guest_name, gd.phone as guest_phone,GROUP_CONCAT(br.room_number) as rooms, SUM(brd.extra_bed ) as extra_bed, gd.guest_image, pt.name as plan_name, ta.agent_name ");
        $this->db->from('bookings b');
        $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id','left');       
        $this->db->join('agents ta', 'b.agent_id = ta.agent_id','left');       
        $this->db->join('plan_type pt', 'b.plan_type_id = pt.plan_type_id','left');       
        $this->db->join('booked_room_detail brd', 'b.booking_id = brd.booking_id','left');       
        $this->db->join('booked_room br', 'br.booked_room_id = brd.booked_room_id','left');       
        $this->db->where('b.checkin_date', Date('Y-m-d'));
        $this->db->where('b.booking_status', 1);
        $this->db->group_by("b.booking_id");      
        $query = $this->db->get();
        //echo $this->db->last_query();
        //die();
        return $query->result();
    }
		
    public function getTodayOccupancy() {

        $this->db->select("b.booking_id, b.booking_reference, b.total_adults, b.total_child, b.checkin_date, b.checkout_date, gd.name as guest_name, gd.phone as guest_phone,GROUP_CONCAT(br.room_number) as rooms, SUM(brd.extra_bed ) as extra_bed, gd.arrived_from, gd.purpose, gd.nationality, gd.id_card_type, gd.id_card_number, pt.name as plan_name, ta.agent_name ");
        $this->db->from('bookings b');
        $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id','left');       
        $this->db->join('agents ta', 'b.agent_id = ta.agent_id','left');       
        $this->db->join('plan_type pt', 'b.plan_type_id = pt.plan_type_id','left');       
        $this->db->join('booked_room_detail brd', 'b.booking_id = brd.booking_id','left');       
        $this->db->join('booked_room br', 'br.booked_room_id = brd.booked_room_id','left');       
        $this->db->where('b.checkin_date <=', Date('Y-m-d'));
        $this->db->where('b.checkout_date >', Date('Y-m-d'));
        $this->db->where('b.booking_status', 3);
        $this->db->group_by("b.booking_id");      
        $query = $this->db->get();
        return $query->result();
    }

    public function getTodayCheckoutList() { 

        $today= Date('Y-m-d');
        // $this->db->select(' b.booking_id, b.booking_reference, GROUP_CONCAT(CONCAT(r.room_name,"-",br.room_number ) SEPARATOR " / " ) as rooms, SUM(brd.extra_bed ) as extra_bed, b.total_adults, b.total_child, b.checkin_date, b.checkout_date, gd.name as guest_name, gd.phone as guest_phone, pt.name as plan_name, ta.agent_name, h.hotel_name ' );
        // $this->db->from('bookings b');
        // $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id','left');       
        // $this->db->join('agents ta', 'b.agent_id = ta.agent_id','left');       
        // $this->db->join('plan_type pt', 'b.plan_type_id = pt.plan_type_id','left');       
        // $this->db->join('booked_room_detail brd', 'b.booking_id = brd.booking_id','left');       
        // $this->db->join('booked_room br', 'br.booked_room_id = brd.booked_room_id','left');       
        // $this->db->join('hotels h', 'h.hotel_id = b.hotel_id','left');       
        // $this->db->join('rooms r', 'r.room_id = br.room_id','right');       
        // $this->db->where('b.checkout_date', Date('Y-m-d'));
        // $this->db->group_by("b.booking_id");      
        // $query = $this->db->get();

        $query = $this->db->query("SELECT b.booking_id, b.booking_reference,  b.total_adults, b.total_child, b.checkin_date, b.checkout_date,GROUP_CONCAT(br.room_number) as rooms, SUM(brd.extra_bed ) as extra_bed, gd.name as guest_name, gd.phone as guest_phone, pt.name as plan_name, ta.agent_name, h.hotel_name 
            FROM `bookings`  as `b` 
            LEFT JOIN `guest_detail`as `gd` ON b.guest_id = gd.guest_id 
            LEFT JOIN `agents`  as`ta` ON b.agent_id = ta.agent_id 
            LEFT JOIN `plan_type` as `pt` ON b.plan_type_id = pt.plan_type_id 
            LEFT JOIN `booked_room_detail` as `brd` ON b.booking_id = brd.booking_id 
            LEFT JOIN `booked_room` as `br` ON br.booked_room_id = brd.booked_room_id 
            LEFT JOIN `hotels` as `h` ON h.hotel_id = b.hotel_id 
            LEFT JOIN `rooms` as `r` ON r.room_id = br.room_id 
             WHERE b.checkout_date = '".$today."' AND b.booking_status = 3
            GROUP BY b.booking_id");
        return $query->result();
    }

    public function getBookingDetails($booking_id) {

        $this->db->select("b.booking_id, b.booking_reference, b.total_adults, b.total_child, b.billing_instruction , b.total_room_tariff, b.users_id, b.hotel_id, b.checkin_time, b.checkout_time, b.checkin_date, b.checkout_date, t.payment_type,GROUP_CONCAT(br.room_number) as rooms,GROUP_CONCAT(r.room_name) as room_type, t.bank_name, t.cheque_no, t.dd_no, t.card_no, t.account_no, t.ifsc_code, t.expiry_date, SUM(brd.extra_bed ) as extra_bed,  pt.plan_type_id as plan_type_id, pt.name as plan_name, u.full_name as full_name, gd.* ");
        $this->db->from('bookings b');
        $this->db->join('transaction t', 'b.booking_id = t.booking_id','left');       
        $this->db->join('guest_detail gd', 'b.guest_id = gd.guest_id','left');       
        $this->db->join('booked_room_detail brd', 'b.booking_id = brd.booking_id','left');       
        $this->db->join('plan_type pt', 'b.plan_type_id = pt.plan_type_id','left');       
        $this->db->join('booked_room br', 'br.booked_room_id = brd.booked_room_id','left');       
        $this->db->join('rooms r', 'br.room_id = r.room_id','left');       
        $this->db->join('users u', 'b.users_id = u.id','left');       
        $this->db->where('b.booking_id', $booking_id);
        $this->db->group_by("b.booking_id");
        $this->db->order_by("t.transaction_id", "desc");        
        $query = $this->db->get();
        return $query->result_array();
    }

//  we need to define a function where we will get all booked room stay date id...
//  we need to delete all this id from table so that these rooms will be marked as vacant..once some one is marking it as no show...if a user is marking any booking as no show all rooms under that booking must be available for booking..

    public function getBookedRoomStayDateId($booking_id) {

        $this->db->select("brsd.id");
        $this->db->from('booked_room_stay_date brsd');
        $this->db->join('booked_room br', 'br.id = brsd.booked_room','left');
        $this->db->join('booked_room_detail brd', 'brd.booked_room_id = br.booked_room_id','left'); 
        $this->db->join('bookings b', 'b.booking_id = brd.booking_id','left');                     
        $this->db->where('b.booking_id', $booking_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

//  we need to define a function where we will get all booked room details on the basis of booking id...
//  it will return room details on the basis of date wise.....

    public function getBookedRoomDetailDatewise($booking_id) {

        $this->db->select("brd.extra_bed, brd.extra_bed_charge, brd.extra_person, brd.extra_person_charge, brd.base_price, brd.sell_price, brd.luxury_tax, brd.occupancy, r.room_id, br.id ");
        $this->db->from('booked_room_detail brd');
        $this->db->join('booked_room br', 'brd.booked_room_id = br.booked_room_id','left'); 
        $this->db->join('bookings b', 'b.booking_id = brd.booking_id','left');                     
        $this->db->join('rooms r', 'r.room_id = br.room_id','left');
        $this->db->where('b.booking_id', $booking_id);
        $this->db->group_by("brd.booked_room_id,r.room_id");

        $query = $this->db->get();
        return $query->result_array();
    }

}
?>