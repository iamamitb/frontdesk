<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->model('dashboard/dashboard_model','DM');
		$this->load->library('form_validation');
		is_logged_in();		
   	}

	public function index(){

		// write a code to get TODAY'S ARRIVAL occupancy and checkout...

		$data['today_arrival'] = $this->DM->getTodayArrivalList();
		$data['today_occupancy'] = $this->DM->getTodayOccupancy();
		$data['today_checkout'] = $this->DM->getTodayCheckoutList();
		$this->template->title('Dashboard','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('dashboard',$data);
				
	}

	public function update_checkin_status(){

        // validation code...

        $this->form_validation->set_rules('booking_id', 'Booking ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('users_id', 'User Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('guest_id', 'Guest ID', 'trim|required|xss_clean');
        $this->form_validation->set_rules('guest_name', 'Guest Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('arrived_from', 'Arrived From', 'trim|required');
        $this->form_validation->set_rules('going_to', 'Going To', 'trim|required');
        $this->form_validation->set_rules('police_station', 'Police Station', 'trim|required');
        $this->form_validation->set_rules('date_of_birth', 'Date Of Birth', 'trim|required');
        $this->form_validation->set_rules('payment_type', 'Mode of Payment', 'trim|required');
        $this->form_validation->set_rules('checkin_date', 'Arrival Date', 'trim|required');
        $this->form_validation->set_rules('id_card_type', 'ID Card Type', 'trim|required');
        $this->form_validation->set_rules('purpose', 'Purpose', 'trim|required');
		if (!empty($_POST['is_foreigner']) ) {// it means guest is foreigner...
	        $this->form_validation->set_rules('country', 'Nationality', 'trim|required');
	        $this->form_validation->set_rules('passport_no', 'Passport Number', 'trim|required');
	        $this->form_validation->set_rules('passport_issue_date', 'Paaport Issue Date', 'trim|required');
	        $this->form_validation->set_rules('passport_expiry_date', 'Passport Expiry Date', 'trim|required');
	        $this->form_validation->set_rules('arrival_date_india', 'Arrival Date In India', 'trim|required');
	        $this->form_validation->set_rules('visa_no', 'Visa Number', 'trim|required');
	        $this->form_validation->set_rules('visa_issue_date', 'Visa Issue Date', 'trim|required');
	        $this->form_validation->set_rules('visa_expiry_date', 'Visa Expiry Date', 'trim|required');
	        $this->form_validation->set_rules('arrival_place_india', 'Arrival Place In India', 'trim|required');
		} 
		if ($this->form_validation->run() ){
			// it dmeans we dont have any validation error..so we will insert all the data according to the requirement..

			// we need to update guest info in guest table ....
			$guest = array();
			$booking = array();

			$guest_id = $this->input->post('guest_id');
			$booking_id = $this->input->post('booking_id');
			$hotel_id = $this->input->post('hotel_id');

			$guest['name'] = $this->input->post('guest_name');
			$guest['phone'] = $this->input->post('phone');
			$guest['email'] = $this->input->post('email');
			$guest['address'] = $this->input->post('address');
			$guest['organization'] = $this->input->post('organization');
			$guest['designation'] = $this->input->post('designation');
			$guest['arrived_from'] = $this->input->post('arrived_from');
			$guest['going_to'] = $this->input->post('going_to');
			$guest['police_station'] = $this->input->post('police_station');
			$guest['date_of_birth'] = dateformate($this->input->post('date_of_birth'),"Y-m-d","-");
			$guest['id_card_type'] = $this->input->post('id_card_type');
			$guest['id_card_number'] = $this->input->post('id_card_number');
			$guest['purpose'] = $this->input->post('purpose');

			// we need to check whether we have saved any guest images or not...
			if ($this->input->post('guest_image') ) {
				$encoded_data = $this->input->post('guest_image');
			    $binary_data = base64_decode( $encoded_data );
				$guest['guest_image'] = $binary_data;
			}

			if($this->input->post('is_foreigner')){
				// it means guest is a foreigner...so we need to update some data as well...
				$guest['is_foreigner'] = $this->input->post('is_foreigner');
				$guest['nationality'] = $this->input->post('country');
				$guest['passport_no'] = $this->input->post('passport_no');
				$guest['passport_issue_date'] = dateformate($this->input->post('passport_issue_date'),"Y-m-d","-");
				$guest['passport_expiry_date'] = dateformate($this->input->post('passport_expiry_date'),"Y-m-d","-");
				$guest['visa_no'] = $this->input->post('visa_no');
				$guest['visa_issue_date'] = dateformate($this->input->post('visa_issue_date'),"Y-m-d","-");
				$guest['visa_expiry_date'] = dateformate($this->input->post('visa_expiry_date'),"Y-m-d","-");
				$guest['arrival_date_india'] = dateformate($this->input->post('arrival_date_india'),"Y-m-d","-");
				$guest['is_employed_in_india'] = $this->input->post('is_employed_in_india');
				$guest['arrival_place_india'] = $this->input->post('arrival_place_india');
				$guest['duration_in_india'] = $this->input->post('duration_in_india');
			}
			//echo "<pre>"; print_r($guest);
			updateDataCondition('guest_detail', $guest,'guest_id = '.$guest_id);					
			
			// now we need to update booking table details...

			$transaction['payment_type'] = $this->input->post('payment_type') ;
			$transaction['cheque_no'] = $this->input->post('cheque_no');
			$transaction['dd_no'] = $this->input->post('dd_no');
			$transaction['card_no'] = $this->input->post('card_no');
			$transaction['account_no'] = $this->input->post('account_no');
			$transaction['ifsc_code'] = $this->input->post('ifsc_code');
			$transaction['expiry_date'] = $this->input->post('expiry_date');
			$transaction['bank_name'] = $this->input->post('bank_name');
			$booking['checkin_date'] = dateformate($this->input->post('checkin_date'),"Y-m-d","-");
			$booking['checkout_date'] = dateformate($this->input->post('checkout_date'),"Y-m-d","-");
			$booking['checkin_time'] = $this->input->post('check_in_time');
			$booking['checkout_time'] = $this->input->post('check_out_time');
			$booking['plan_type_id'] = $this->input->post('plan_type_id');
			$booking['booking_status'] = 3; //1 = booked, 2 = hold, 3 = check in, 4 = check out, 5 = no show, 6 = cancel..
			updateDataCondition('bookings', $booking, 'booking_id = '.$booking_id);
			updateDataCondition('transaction', $transaction, 'booking_id = '.$booking_id);
		//	we need to check a condition where we will check on which button a user has clicked on....
		//  it may be like on check in guest button he has clicked or on another button has clicked...
		//	according to the clicked button we need to redirect that user on that page....

			if($this->input->post('checkin_guest'))
				redirect(base_url()."dashboard");			
			else {

				$data['hotel_info'] = getSingle("hotels","hotel_id = ".$hotel_id,"","","","");
				$data['guest_info'] = getSingle("guest_detail","guest_id = ".$guest_id,"","","","");
				$data['booking_info'] = $this->DM->getBookingDetails($booking_id);

				$this->template->set_layout('print_template','front');
				$this->template->build('printgrc',$data);
			}			
		} else {
			// it means there is a validation error...
			echo validation_errors(); die("validation error...");
			redirect(base_url()."dashboard");			
		}
				
	}

//	we need to define a function where we will update a check in status as nooo show..
//  on the basis of booking id we will mark a booking as no show and delete unnecessary data from diff table which is used to store the booking data..

	public function update_checkin_status_noshow(){

		$booking_id = $this->input->post('booking_id');	

		$booking['booking_status'] = 5; //1 = booked, 2 = hold, 3 = check in, 4 = check out, 5 = no show, 6 = cancel..
		$booking['no_show_reason'] = $this->input->post('no_show_reason');	
 
		updateDataCondition("bookings", $booking, "booking_id = ".$booking_id);
		// as per the guideline given by Sourav on 26-3-16 regarding no show functionalities...we will mark booking status as 5 and add a no show reason in the booking table..
		// we will keep the data in booking table..it needs guest name, checkin and out date, source of booking like travel agent or direct booking or corporate booking, booking date...
		//	we need to write a condition where we will delete all data on the basis of booking id from all 3 tables i.e booked_room_stay_date and booked_room_detail and booked room tables....
		Delete_data("booked_room_detail","booking_id = ".$booking_id);

		echo json_encode("success");
	}

	public function change_room_slip(){
		$this->template->set_layout('print_template','front');
		$this->template->build('change_room_slip');
	}

	// define a function which will show a print bill page...

	public function show_bill($booking_id){
		// get hotel id and guest id from booking id....
		$hotel_id = getValue("bookings", "hotel_id", "booking_id =".$booking_id);
		$guest_id = getValue("bookings", "guest_id", "booking_id =".$booking_id);
		$data['hotel_info'] = getSingle("hotels","hotel_id = ".$hotel_id,"","","","");
		$data['guest_info'] = getSingle("guest_detail","guest_id = ".$guest_id,"","","","");
		$data['booking_info'] = $this->DM->getBookingDetails($booking_id);
		$this->template->set_layout('print_template','front');
		$this->template->build('printgrc',$data);
	}

	// define a function to get the details of a booking id  ....

	public function get_booking_detail(){
		$result = [];
		$booking_id = $this->input->post('booking_id');	
		$booking_info = $this->DM->getBookingDetails($booking_id);
		//echo "<pre>";print_r($booking_info);
		
		$result[0] = $booking_info[0]['booking_id'];
		$result[1] = $booking_info[0]['booking_reference'];
		$result[2] = $booking_info[0]['name'];
		$result[3] = $booking_info[0]['phone'];
		$result[4] = $booking_info[0]['address'];
		$result[5] = $booking_info[0]['email'];
		$result[6] = $booking_info[0]['plan_name'];
		$result[7] = $booking_info[0]['full_name'];
		$result[8] = $booking_info[0]['total_adults'];
		$result[9] = $booking_info[0]['total_child'];
		$result[10] = $booking_info[0]['billing_instruction'];
		$result[11] = $booking_info[0]['extra_bed'];
		$result[12] = $booking_info[0]['users_id'];
		$result[13] = $booking_info[0]['rooms'];
		$result[14] = $booking_info[0]['total_room_tariff'];
		$result[15] = $booking_info[0]['room_type'];
		$result[16] = $booking_info[0]['payment_type'];
		$result[17] = $booking_info[0]['plan_type_id'];
		$result[18] = $booking_info[0]['checkin_time'];
		$result[19] = $booking_info[0]['checkout_time'];
		$result[20] = dateformate($booking_info[0]['checkin_date'],"d-m-Y","-");
		$result[21] = dateformate($booking_info[0]['checkout_date'],"d-m-Y","-");
		$result[22] = $booking_info[0]['bank_name'];
		$result[23] = $booking_info[0]['cheque_no'];
		$result[24] = $booking_info[0]['dd_no'];
		$result[25] = $booking_info[0]['card_no'];
		$result[26] = $booking_info[0]['account_no'];
		$result[27] = $booking_info[0]['ifsc_code'];
		$result[28] = $booking_info[0]['expiry_date'];
		$result[29] = $booking_info[0]['guest_id'];
		$result[30] = $booking_info[0]['hotel_id'];
		$result[31] = $booking_info[0]['organization'];
		$result[32] = $booking_info[0]['designation'];
		$result[33] = $booking_info[0]['arrived_from'];
		$result[34] = $booking_info[0]['going_to'];
		$result[35] = $booking_info[0]['police_station'];
		$result[36] = $booking_info[0]['date_of_birth'];
		$result[37] = $booking_info[0]['purpose'];
		$result[38] = $booking_info[0]['is_foreigner'];
		$result[39] = $booking_info[0]['nationality'];
		$result[40] = $booking_info[0]['passport_no'];
		$result[41] = $booking_info[0]['passport_issue_date'];
		$result[42] = $booking_info[0]['passport_expiry_date'];
		$result[43] = $booking_info[0]['visa_no'];
		$result[44] = $booking_info[0]['visa_issue_date'];
		$result[45] = $booking_info[0]['visa_expiry_date'];
		$result[46] = $booking_info[0]['arrival_date_india'];
		$result[47] = $booking_info[0]['duration_in_india'];
		$result[48] = $booking_info[0]['is_employed_in_india'];
		$result[49] = $booking_info[0]['arrival_place_india'];
		$result[50] = $booking_info[0]['guest_image'];
		$result[51] = $booking_info[0]['id_card_type'];
		$result[52] = $booking_info[0]['id_card_number'];
		$result[53] = $booking_info[0]['guest_image'];
		
		echo json_encode($result);				
	}

	public function change_room(){
		$booking_id = $this->input->post('booking_id');	
		$hotel_id = $this->input->post('hotel_id');	
		$checkin_date = $this->input->post('checkin_date');	
		$checkout_date = $this->input->post('checkout_date');	
		$booked_room_info = $this->DM->getBookedRoomDetailDatewise($booking_id);

		//echo "<pre>";print_r($booked_room_info);
		$html = '';
		$checkin_date_no = 1;
		$booking_record_no = 1;
        // calculate total no of booking day....
        $no_of_booking_day = DaysDiff($checkin_date,$checkout_date);
        for($k = 0; $k < $no_of_booking_day; $k++ ){

			$html .= '<div class="row">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">'.date("dS F Y", strtotime("+".$k." day", strtotime(dateformate($checkin_date,"d-m-Y","-")))).'</h3> 
                        </div>
                        <input type="hidden" name="checkin_date_'.$checkin_date_no.'" id="checkin_date_'.$checkin_date_no.'" value="'.date("Y-m-d", strtotime("+".$k." day", strtotime(dateformate($checkin_date,"d-m-Y","-")))).'" /> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Current Room Type</th>
                                        <th>Current Room Number</th>
                                        <th>New Room Type</th>
                                        <th>New Room Number</th>
                                        <th>Sell Price</th>
                                        <th>Extra Bed</th>
                                        <th>Extra Bed Price</th>
                                        <th>Extra Person</th>
                                        <th>Extra Person Charge</th>
                                        <th>Luxury Tax</th>
                                    </tr>
                                </thead>
                                <tbody>';
					for($i = 0; $i < count($booked_room_info); $i++ ){ 
						$html .= '                               
                                    <tr>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly" value="'.$booked_room_info[$i]['room_name'].'"></td>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly" value="'.$booked_room_info[$i]['room_number'].'"></td>
                                        <td>'.get_data_dropdown("rooms", "room_id", "room_name", "hotel_id = ".$hotel_id, "", "room_id_".$booking_record_no."_".$checkin_date_no, "room_id[]", "required", "", "", "room_id").'
                                        </td>
                                        <td id="room_number"></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td> 
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>  
                           	         </tr>';
                           	         $booking_record_no++;
                    }
                    $html .= '
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>';
                $checkin_date_no++;
        }
        echo json_encode($html);
	}	
	
}