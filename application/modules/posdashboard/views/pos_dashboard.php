<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title"><b>POS Dashboard</b></h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts --> 


<div class="col-md-12">
    <div class="panel panel-color panel-primary">
        <!-- <div class="panel-heading"> 
        </div>  -->
        <div class="panel-body"> 
            <table class="table table-striped table-bordered table-hover">
            <tbody class="get_search">
                    <tr>
                        <th colspan="9">You have not made any orders through the POS yet. Please make some orders to see statistics on this page.</th>
                    </tr>
                </tbody>            
            </table>

            <div class="row" align="center">
                <a href="<?=base_url()?>pos_pos"><button type="button" class="btn btn-success btn-lg" >Go to POS</button></a>
            </div>

        </div> 
    </div>
</div>

<!-- <div class="row">
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-info"><i class="ion-social-usd"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">15,852</span>
                                        Total Sale
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-purple"><i class="ion-ios7-cart"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">56</span>
                                        Orders
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-success"><i class="ion-android-contacts"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">52</span>
                                        Customers
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-lg-3">
                                <div class="mini-stat clearfix bx-shadow bg-white">
                                    <span class="mini-stat-icon bg-primary"><i class="ion-eye"></i></span>
                                    <div class="mini-stat-info text-right text-dark">
                                        <span class="counter text-dark">20</span>
                                        Orders in Queue
                                    </div>
                                    
                                </div>
                            </div>
                        </div> 
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Pending Orders/KOT</h3> 
                </div> 
                <div class="panel-body"> 
                	<table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>KOT #</th>
                                                                <th>Room No.</th>
                                                                <th>Steward</th>
                                                                <th>View KOT</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-kot">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-kot">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-kot">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-kot">View</a></td>
                                                               
                                                            </tr>
                                                            
                                                        </tbody>
                    </table>
                    
                </div> 
            </div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-border panel-success">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Recent Bills</h3> 
                </div> 
                <div class="panel-body"> 
                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Bill #</th>
                                                                <th>Table #</th>
                                                                <th>Steward</th>
                                                                <th>Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>103</td>
                                                                <td>Pandu</td>
                                                                <td><a href="#" data-toggle="modal" data-target="#show-bill">View</a></td>
                                                               
                                                            </tr>
                                                        </tbody>
                    </table>
                </div> 
            </div>
		</div>


		
	</div>
                            
</div> --> <!-- End row -->

<!-- <div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="panel panel-border panel-danger">
                
                <div class="panel-heading"> 
                    <h2 class="panel-title" style="">Table Status</h2> 
                </div> 
                
                <div class="panel-body"> 
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Table #</th>
                                <th>Steward</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>1</td><td>Vacant</td></tr>
                            <tr><td>2</td><td>Saikat</td></tr>
                            <tr><td>3</td><td>Pandu</td></tr>
                            <tr><td>4</td><td>Sunirmal</td></tr>
                            <tr><td>5</td><td>Tanmay</td></tr>
                            <tr><td>6</td><td>Sunirmal</td></tr>
                            <tr><td>7</td><td>Vacant</td></tr>
                            <tr><td>8</td><td>Tanmay</td></tr>
                            <tr><td>9</td><td>Vacant</td></tr>
                            <tr><td>10</td><td>Saikat</td></tr>
                            
                            
                            
                        </tbody>
                    </table>
                    
                </div> 
            </div>
        </div>
        <div class="col-md-6">
			<div class="panel panel-border panel-inverse">
                
                <div class="panel-heading" style=""> 
                    <h2 class="panel-title" style="">Recent Takeaways</h2> 
                </div> 
                
                <div class="panel-body"> 
                	<table class="table">
                        <thead>
                            <tr>
                                <th>Token #</th>
                                <th>Item</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>41</td><td>Chicken Kasha</td></tr>
                            <tr><td></td><td>Chana Masala</td></tr>
                            <tr><td>33</td><td>Biriyani</td></tr>
                            <tr><td></td><td>Dal</td></tr>
                            <tr><td></td><td>Bhat</td></tr>
                            <tr><td>17</td><td>Pepsi</td></tr>
                            <tr><td></td><td>Beer</td></tr>
                            <tr><td></td><td>CHingri Mach Bhaja</td></tr>
                            <tr><td>9</td><td>Egg Roll</td></tr>
                            <tr><td>10</td><td>Chicken Roll</td></tr>
                            
                            
                            
                        </tbody>
                    </table>
                    
                </div> 
            </div>
        </div>
	</div>
</div>

<div class="row" style="background-color:#fafafa;padding:10px;border-radius:5px;margin-left:5%;margin-right:5%;text-align:center;">
	<div class="col-md-12"><h2 class="panel-title" style="text-align:center;margin-bottom:30px;">Most Ordered Items For Today</h2>
		<div class="col-md-2">
			<span class="order_number">35</span><br>
			<span class="dish_name">Biriyani</span>
		</div>
		<div class="col-md-2">
			<span class="order_number">15</span><br>
			<span class="dish_name">Chicken Chowmein</span>
		</div>
		<div class="col-md-2">
			<span class="order_number">20</span><br>
			<span class="dish_name">Mutton Korma</span>
		</div>
		<div class="col-md-2">
			<span class="order_number">180</span><br>
			<span class="dish_name">Tandoori Roti</span>
		</div>
		<div class="col-md-2">
			<span class="order_number">69</span><br>
			<span class="dish_name">Lachha Parota</span>
		</div>
		<div class="col-md-2">
			<span class="order_number">41</span><br>
			<span class="dish_name">Ice Cream</span>
		</div>
	</div>
</div> -->
<!-- Page content ends -->  



<!-- Bill Modal -->

<div id="show-kot" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    		
                                                    <div class="modal-body"> 
                                                        
                                                        <div class="row">

				<div class="panel panel-border panel-inverse" style="margin-top:20px;margin-left:2%;margin-right:2%;">
                    <div class="panel-heading"> <h3 class="panel-title" style="text-align:center;font-size:25px;">KOT</h3></div> 
                    <div class="panel-body"> 
                    	<p>
                    		<table class="table table-bordered" id="kot_table">
                                
                                                    <tbody>
                                                        <tr>
                                                            <th>Kot Number</th>
                                                            <td>143</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Date</th>
                                                            <td>31/12/2019</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Time</th>
                                                            <td>03:17 PM</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Room Number</th>
                                                            <td>143</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Steward Name</th>
                                                            <td>Pandu</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                        	<th colspan="2" style="background-color:#f2f2f2;font-weight:bold;">Order</th>

                                                        </tr>

                                                        <tr>
                                                            <th>Item</th>
                                                            <td>Qty</td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Sobji Bhat</th>
                                                            <td>2</td>
                                                        </tr>

                                                        <tr>
                                                            <th>Tandoori Chicken with Jhol</th>
                                                            <td>8</td>
                                                        </tr>

                                                        <tr>
                                                        	<th colspan="2" style="background-color:#f2f2f2;font-weight:bold;">Special Instruction</th>
                                                        </tr>

                                                        <tr>
                                                        	<th colspan="2">Please treat this guest nicely, give him free cold drinks and show him the path to wash room</th>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                    	</p>
                        
                    </div> 
                </div>


			</div>
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button> 
                                                        <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Print KOT</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->
<!-- KOT Modal ends -->

<!-- KOT Modal -->

<div id="show-bill" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    		
                                                    <div class="modal-body"> 
                                                        
                                                        <div class="row">

				<div class="panel panel-border panel-inverse" style="margin-top:20px;margin-left:2%;margin-right:2%;">
                    <div class="panel-heading"> <h3 class="panel-title" style="text-align:center;font-size:25px;">BILL</h3></div> 
                    <div class="panel-body"> 
                    	<div class="restaurant_bill">
                		<div class="row">
                			<div class="restaurant_name">Mon Chasa Punjabi Dhaba</div>
                		</div>
                		<div class="row">
                			<div class="restaurant_phone_number"><i class="ion-ios7-telephone"></i>9999696895, 8902268432</div>
                		</div>
                		<div class="row">
                			<div class="restaurant_address">34/c Bablatola more, Chinar Park Kolkata 700091</div>
                		</div>
                		<div class="row">
                			<div class="restaurant_header">This is a header message. We're closed on Thursdays. Please keep this bill with you for reference at times of checkout.</div>
                		</div>
                		
                		<div class="row" style="border-top:1px solid #f2f2f2;border-bottom:1px solid #f2f2f2;margin-top:15px;font-size:10px;">
                			<div class="col-md-1"></div>
                			<div class="col-md-2">Date<br>21/02/2016</div>
                			<div class="col-md-2">Time<br>10:49 A.M</div>
                			<div class="col-md-2">Table No.<br>18</div>
                			<div class="col-md-2">Bill No.<br>37</div>
                			<div class="col-md-2">Room No.<br>A-281</div>
                			<div class="col-md-1"></div>
                		</div>

                		<div class="row">
                			<div class="col-md-3"></div>
                			<div class="col-md-6" style="margin-top:10px;margin-bottom:10px;"><b style="font-size:20px;margin-top:15px;">**** FOOD BILL ****</b></div>
                			<div class="col-md-3"></div>
                		</div>

                		<div class="row">
                			<table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Item</th>
                                                                <th>Qty</th>
                                                                <th>Rate</th>
                                                                <th>Amount</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Chicken Kosha</td>
                                                                <td>3</td>
                                                                <td>100</td>
                                                                <td>300</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>Hyderabadi Biriyani</td>
                                                                <td>3</td>
                                                                <td>180</td>
                                                                <td>540</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>Mexican Salad</td>
                                                                <td>2</td>
                                                                <td>80</td>
                                                                <td>160</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>4</td>
                                                                <td>Mineral Water</td>
                                                                <td>3</td>
                                                                <td>50</td>
                                                                <td>150</td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td>5</td>
                                                                <td>Chicken Kosha</td>
                                                                <td>3</td>
                                                                <td>100</td>
                                                                <td>300</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>6</td>
                                                                <td>Hyderabadi Biriyani</td>
                                                                <td>3</td>
                                                                <td>180</td>
                                                                <td>540</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>7</td>
                                                                <td>Mexican Salad</td>
                                                                <td>2</td>
                                                                <td>80</td>
                                                                <td>160</td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>8</td>
                                                                <td>Mineral Water</td>
                                                                <td>3</td>
                                                                <td>50</td>
                                                                <td>150</td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2">Total</td>
                                                                <td>10</td>
                                                                
                                                                <td></td>
                                                                <td><b style="font-size:20px;font-weight:bold;">1500</b></td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>VAT %</td>
                                                                
                                                                <td>8</td>
                                                                <td><b style="font-size:20px;font-weight:bold;">90</b></td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>Service Charge %</td>
                                                                
                                                                <td>5</td>
                                                                <td><b style="font-size:20px;font-weight:bold;">50</b></td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>Room Service Charge %</td>
                                                                
                                                                <td>4</td>
                                                                <td><b style="font-size:20px;font-weight:bold;">40</b></td>
                                                                
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2"></td>
                                                                <td>Net Payable</td>
                                                                
                                                                <td></td>
                                                                <td><b style="font-size:30px;font-weight:bold;color:#000;">1690</b></td>
                                                                
                                                            </tr>
                                                            
                                                        </tbody>
                            </table>
                		</div>

                		<div class="row" style="border-top:1px solid #f2f2f2;border-bottom:1px solid #f2f2f2;text-align:center;margin-bottom:10px;">
                			<p>VAT No. - 89037438003839
                				<br>TIN No. - 7YUJIK340389343439
                				<br>KOT No. - 45
                			</p>
                		</div>
                		<div class="row" style="text-align:center;">This is Footer message.</div>
                	</div>
                    </div> 
                </div>


			</div>
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button> 
                                                        <button type="button" class="btn btn-success btn-lg" data-dismiss="modal">Print Bill</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->
<!-- Bill Modal ends -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script>
<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 

</script>