<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posdashboard extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('posdashboard_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('POS Dashboard','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_dashboard',$data);

				
	}
	

}
