<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Raw Material Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_material" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Material</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_material" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Material</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addmaterialdata"); ?>
            <?=$this->session->flashdata("chkmaterialdata"); ?> 
            <?=$this->session->flashdata("msgMaterialUpdate"); ?> 
            <div class="tab-pane active" id="all_material"> 
                <!-- Row for Table Starts -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="font-weight:bold;margin-bottom:30px;">All Material</h2>
                        <table id="datatable" class="table table-striped table-bordered">
                            <?php 
                            if(isset($allRecord) && count($allRecord)>0){
                            ?>
                            <thead>
                                <tr>
                                    <th>Material Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                     
                            <tbody>
                            <?php 
                                foreach($allRecord as $material):{
                              ?>    
                            <tr>
                                <td><?=$material->material_name?></td>
                                <td>
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="#" data-toggle="modal" onclick="editMaterial(<?=$material->material_id?>)" data-target="#edit-material">Edit</a></li>
                                        <li><a onclick="materialDelete(<?=$material->material_id?>)" href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                             <?php } ?> 
                             </tbody>
                             <?php endforeach;  } else{ ?>
                            <tbody> 
                                <tr >
                                <th style="text-align:center;">You have not added any Material Yet.</th>
                                </tr> 
                             </tbody> 
                             <?php } ?>
                        </table>

                    </div>
                </div>
               <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_material"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Material</h2>
                <form name="material_form" method="post" id="material_form" action="<?php echo base_url(); ?>pos_raw_material_master/addMaterial"> 
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Material Name<span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="material_name" name="material_name" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                        <label class="col-md-3 control-label">Purchasing Unit<span class="error">*</span></label>
                            <div class="col-md-9">
                            <?=get_data_dropdown("unit_master","unit_id","unit_name","unit_status = '1'","","unit_id","unit_id","","","","")?>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Material</button>
                    </div>
                </div>
                </form> 

                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends --> 

<!-- modal --> 
<div id="edit-material" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 

$(document).ready(function() {
    $("#material_form").validate({
         rules: {
            
            material_name: {
                required: true,                     
                lettersonly: true,
                noSpace: true
            },
            unit_id: {
                required: true
            }
            
         },
         messages: {
            material_name: {
                    required: "Please enter material name"
                    
                },    
            unit_id: {
                    required: "Please select an unit"
                }        
         },
        
    });




$("#material_form_edit").validate({
         rules: {
            
           material_name_edit: {
                required: true,                     
                lettersonly: true,
                noSpace: true
            },
            unit_id_edit: {
                required: true
            }
            
         },
         messages: {
            material_name_edit: {
                    required: "Please enter material name"
                    
                },    
            unit_id_edit: {
                    required: "Please select an unit"
                }          
         },
        
    });








});


function editMaterial(idm)
{
    var idm = idm;
    //alert(idm);
   
    $.ajax({
        url:"<?php echo site_url('pos_raw_material_master/editMaterial');?>/"+idm,
        type: 'POST',
        dataType: "json",
        data: {'idm': idm},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-material').html(data.item);
            
        }      
    });
}



$('#edit_material').click(function(){

   var materialID = $("#materialID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_raw_material_master/updateMaterial",
        type: 'POST',
        dataType: "json",
        data: {materialID : materialID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function materialDelete(material_id) {
    //alert(material_id); 
    swal({
        title: "Are you sure?",
        text: "This material will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_raw_material_master/materialDelete",
            type: 'POST',
            dataType: "json",
            data: {'material_id': material_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}

</script>
