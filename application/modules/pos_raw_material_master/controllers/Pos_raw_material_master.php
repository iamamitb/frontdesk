<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_raw_material_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_raw_material_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allRecord']=getData("raw_material_master","material_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Raw Material Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_raw_material_master',$data);

				
	}

	public function addMaterial(){
		$material_name=$this->input->post('material_name'); 
		$unit_id=$this->input->post('unit_id');
		$hotel_id=$this->session->userdata('hotel_id');
		
		$data=array('hotel_id'=>$hotel_id,
					'material_name'=>$material_name,
					'unit_id'=>$unit_id,
					'material_insert_date'=>date("Y-m-d"),
					'material_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('raw_material_master',array('material_name' => $material_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('material_name',$material_name);
			  $this->db->update('raw_material_master',$data);

			  $this->session->set_flashdata('chkmaterialdata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Material already exists.
            </div>');
			redirect('pos_raw_material_master#');

		}else{ 
			  $material_id=insertValue('raw_material_master',$data);
		} 	
		
		
		$this->session->set_flashdata('addmaterialdata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the material.
            </div>');
		
		
		redirect('pos_raw_material_master#');
		
	}

	public function editMaterial($idm) { 
		$material=getSingle('raw_material_master','material_id = '.$idm,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Material</h3>
			    </div>
		    <form method="post" id="material_form_edit" action="'.base_url().'pos_raw_material_master/updateMaterial" >
			<input type="hidden" id="materialID" name="materialID" value="'.$material->material_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Material Name<span class="error">*</span></label>
		              <input type="text" id="material_name_edit" name="material_name" class="form-control" value="'.$material->material_name.'" required  aria-required="true">
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Purchasing Unit<span class="error">*</span></label>
		              '.get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","$material->unit_id","unit_id_edit","unit_id","required","","","").'
		            </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_material" id="edit_material" class="btn btn-success waves-effect waves-light">Update Material</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateMaterial() {
		$materialID=$this->input->post('materialID'); 
		$material_name=$this->input->post('material_name'); 
		$unit_id=$this->input->post('unit_id');
		
		$data=array(
					'material_name'=>$material_name,
					'unit_id'=>$unit_id,
					'material_status'=>'1'
					);

  		$materialUpdate=updateDataCondition('raw_material_master',$data,'material_id = '.$materialID);
                          
  		$this->session->set_flashdata('msgMaterialUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the Material.
            </div>');	
                          
  		redirect('pos_raw_material_master#');

	}


	public function materialDelete() { 
		
		$material_id=$_POST['material_id'];		
	
		$data['material_status']='0';			
		
		$material_delete=updateDataCondition('raw_material_master',$data,'material_id = '.$material_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	












	

}
