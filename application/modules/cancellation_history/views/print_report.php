<!-- Style for page display -->
<style>
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Cancellation History Report</h1>
</div>
<?php if(!empty($_POST)){ ?>
<div class="row" align="center">
<?php  
$agent_name=(($_POST['filter']['b']['equal']['agent_id'])!="")? getValue('agents','agent_name',"agent_id = ".$_POST['filter']['b']['equal']['agent_id'] ) : "" ;    
$hotel_name=(($_POST['filter']['b']['equal']['hotel_id'])!="")? getValue('hotels','hotel_name',"hotel_id = ".$_POST['filter']['b']['equal']['hotel_id'] ) : "" ;
?>    
<!-- <p><b>Filters: </b><span>Date: <?php echo $dt1=(isset($_POST['filter']['b']['from1']['checkin_date']))? $_POST['filter']['b']['from1']['checkin_date'] : "" ;?> - <?php echo $dt2=(isset($_POST['filter']['b']['to1']['checkin_date']))? $_POST['filter']['b']['to1']['checkin_date'] : "" ;?>, Hotel: <?=$hotel_name?>, Travel Agent: <?=$agent_name?></span></p> -->
</div>
<?php } ?>

<div class="row">
	<table class="table table-bordered">
        <?php                                                                                                                
                    if(isset($all_bookings) && count($all_bookings)>0){
                    ?>    
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Customer Name</th>
                        <th>Hotel Name</th>
                        <th>Cancel Date</th>
                        <th>Refund</th>
                        <th>Cancelled By</th>
                        
                    </tr>
                </thead>

                     <?php                                                                                                                
                    foreach($all_bookings as $all_booking):{
                        $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 

                    ?>                           
                    <tbody>
                   
                        <tr>
                            <td><?=$all_booking->booking_id ?></td>
                            <td><?=$all_booking->name ?></td>
                            <td><?=$all_booking->hotel_name ?></td>
                            <td><?=date('d-M-Y', strtotime($all_booking->cancel_date))?></td>
                            <td><?=$all_booking->advance ?></td>
                            <td><?=$all_booking->full_name ?></td>
                        </tr>
                       
                       <?php } ?>
                    </tbody>
              <?php endforeach;  } else{  ?> 
                    
                 <tbody> 
                    <tr >
                    <td style="margin-top: 80px;background: #E1EEF2;padding: 1%;border-radius: 5px;">No data found !</td>
                    </tr> 
                 </tbody> 
                 <?php } ?>
    </table>

</div>