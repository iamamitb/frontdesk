<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class checkout_model extends CI_Model {
		function all_bookings($account_id,$hotel_id){		

		$sql="SELECT bo.booking_id,booking_reference,hotel_name,name,agent_id,phone,booking_date,checkin_date,checkout_date,grand_total,advance,(grand_total-advance) as due,plan_type_id,GROUP_CONCAT(room_number SEPARATOR ', ') as room_number,GROUP_CONCAT(room_name SEPARATOR ', ') as room_name,group_concat(concat(r.room_name,'-',br.room_number) separator ' ||  ') as room_number1,COUNT(room_number) as no_of_room from hotels as h 
								RIGHT JOIN bookings as bo on h.hotel_id=bo.hotel_id
								RIGHT JOIN  booked_room_detail as brd on bo.booking_id=brd.booking_id
                                RIGHT JOIN  transaction as trns on bo.booking_id=trns.booking_id
								RIGHT JOIN  booked_room as br on brd.booked_room_id=br.booked_room_id
								RIGHT JOIN rooms as r on r.room_id=br.room_id
								RIGHT JOIN guest_detail as gd on bo.guest_id=gd.guest_id
								where h.account_id=$account_id AND bo.booking_status='3' AND h.hotel_id=$hotel_id
								GROUP BY brd.booking_id"; 
         
        $query = $this->db->query($sql);       
        //echo  $this->db->last_query();
        //die();   
		return $query->result();
	}

/**************************************************************************/		

	function all_not_checkin($account_id,$hotel_id){		

		$sql="SELECT bo.booking_id,booking_reference,hotel_name,name,agent_id,phone,booking_date,checkin_date,checkout_date,grand_total,advance,(grand_total-advance) as due,plan_type_id,GROUP_CONCAT(room_number SEPARATOR ', ') as room_number,GROUP_CONCAT(room_name SEPARATOR ', ') as room_name,group_concat(concat(r.room_name,'-',br.room_number) separator ' ||  ') as room_number1,COUNT(room_number) as no_of_room from hotels as h 
								RIGHT JOIN bookings as bo on h.hotel_id=bo.hotel_id
								RIGHT JOIN  booked_room_detail as brd on bo.booking_id=brd.booking_id
                                RIGHT JOIN  transaction as trns on bo.booking_id=trns.booking_id
								RIGHT JOIN  booked_room as br on brd.booked_room_id=br.booked_room_id
								RIGHT JOIN rooms as r on r.room_id=br.room_id
								RIGHT JOIN guest_detail as gd on bo.guest_id=gd.guest_id
								where h.account_id=$account_id AND bo.booking_status='1' AND h.hotel_id=$hotel_id
								GROUP BY brd.booking_id"; 
         
        $query = $this->db->query($sql);       
        //echo  $this->db->last_query();
        //die();   
		return $query->result();
	}

/**************************************************************************/	
	function booking_checkout($account_id,$booking_id){		

		$sql="SELECT sum(trns.advance) as total_advance,bo.booking_id,bo.service_charge,bo.service_tax,brd.luxury_tax,advance,grand_total,checkin_date,checkout_date,total_room_tariff,hotel_name,name,agent_id,phone,booking_date,grand_total,advance,(grand_total-advance) as due,checkin_date,checkout_date,plan_type_id,GROUP_CONCAT(room_number SEPARATOR '- ' ) as room_number1,GROUP_CONCAT(room_name SEPARATOR '- ') as room_name,group_concat(concat(`room_name`,'-',`room_number`) separator ',') as room_number,COUNT(room_number) as no_of_room from hotels as h 
								RIGHT JOIN bookings as bo on h.hotel_id=bo.hotel_id
								RIGHT JOIN  booked_room_detail as brd on bo.booking_id=brd.booking_id
								RIGHT JOIN  transaction as trns on bo.booking_id=trns.booking_id
								RIGHT JOIN  booked_room as br on brd.booked_room_id=br.booked_room_id
								RIGHT JOIN rooms as r on r.room_id=br.room_id
								RIGHT JOIN guest_detail as gd on bo.guest_id=gd.guest_id							
								where h.account_id=$account_id AND bo.booking_status='3'  AND bo.booking_id=$booking_id
								GROUP BY brd.booking_id "; 
         
        $query = $this->db->query($sql); 
		return $query->result();
	}

/**************************************************************************/
	function booking_checkout_details($account_id,$booking_id){		

		$sql="SELECT sum(trns.advance) as total_advance,bo.booking_id,bo.service_charge,bo.service_tax,brd.luxury_tax,advance,grand_total,checkin_date,checkout_date,total_room_tariff,hotel_name,name,agent_id,phone,booking_date,grand_total,advance,(grand_total-advance) as due,checkin_date,checkout_date,plan_type_id,GROUP_CONCAT(room_number SEPARATOR '- ' ) as room_number1,GROUP_CONCAT(room_name SEPARATOR '- ') as room_name,group_concat(concat(`room_name`,'-',`room_number`) separator ',') as room_number,COUNT(room_number) as no_of_room from hotels as h 
								RIGHT JOIN bookings as bo on h.hotel_id=bo.hotel_id
								RIGHT JOIN  booked_room_detail as brd on bo.booking_id=brd.booking_id
								RIGHT JOIN  transaction as trns on bo.booking_id=trns.booking_id
								RIGHT JOIN  booked_room as br on brd.booked_room_id=br.booked_room_id
								RIGHT JOIN rooms as r on r.room_id=br.room_id
								RIGHT JOIN guest_detail as gd on bo.guest_id=gd.guest_id							
								where h.account_id=$account_id  AND bo.booking_id=$booking_id
								GROUP BY brd.booking_id "; 
         
        $query = $this->db->query($sql); 
		return $query->result();
	}

/**************************************************************************/				
	function booking_expenses($account_id,$booking_id){		

		$sql="SELECT oi.item_name,oib.room_number,oib.date,oib.no_of_item,oib.price,oib.total_amount,bo.booking_reference 
			  					FROM other_item_booked AS oib 
								RIGHT JOIN bookings as bo ON bo.booking_id=oib.booking_id
								RIGHT JOIN other_item as oi ON oi.item_id=oib.item_id
								RIGHT JOIN hotels as h ON h.hotel_id=oib.hotel_id
								WHERE oib.booking_id=$booking_id AND h.account_id=$account_id "; 
         
        $query = $this->db->query($sql); 
		return $query->result();
	}


/**************************************************************************/
		
	function booking_transaction($account_id,$booking_id){		

		$sql="SELECT t.payment_date,t.grand_total,t.advance,t.due,t.remarks,t.payment_type,h.hotel_name,b.booking_reference FROM `transaction` AS t
					LEFT JOIN bookings AS b ON b.booking_id=t.booking_id
					LEFT JOIN hotels AS h ON h.hotel_id=b.hotel_id
					WHERE b.booking_id=$booking_id AND h.account_id=$account_id "; 
         
        $query = $this->db->query($sql); 
		return $query->result();
	}


/**************************************************************************/	
	function delete_staydate($booking_id){
		$date_now = date("Y-m-d");
	    $sql="DELETE brsd FROM booked_room_stay_date as brsd 
	    		INNER JOIN booked_room AS br ON brsd.booked_room=br.id 
	    		INNER JOIN booked_room_detail as brd ON br.booked_room_id=brd.booked_room_id 
	    		Where brd.booking_id=$booking_id AND brsd.stay_date>='$date_now'"; 
         
        $query = $this->db->query($sql); 
		//return $query->result();
	}


/**************************************************************************/
	function get_payment_type_list(){ 
		$row =array("cash"=>"Cash",
					"cheque"=>"Cheque",
					"demand_draft"=>"Demand Draft",
					"credit_card"=>"Credit Card",
					"debit_card"=>"Debit Card",
					"neft"=>"NEFT",
					"rtgs"=>"RTGS",
					"payment_by_company"=>"Payment by compnay"
					);
		$html ='<select name="payment_type[]" id="payment_type"  class="form-control payment_type_class"  required aria-required="true">';
		$html .='<option value="">Please select payment type</option>';
		foreach($row as $key=>$val){
		$html .='<option value="'.$key.'" >'.$val.'</option>';   
		}
		$html .='</select>';
		return $html;
	}
/**************************************************************************/	
	
}
?>