<link href="<?=base_url()?>themes/css/views/checkout.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<style type="text/css">
	.add_btn{
		padding: 5px;
		font-size: 15px;
		font-weight: 
		bold;margin-top: 4px;
	}
	.remove_btn{
		padding: 5px;
		font-size: 15px;
		font-weight: bold;
		margin-top: 4px;
	}
</style>


<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Guest Check Out</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
			<?php
                     if(isset($booking_checkout)):
                     foreach($booking_checkout as $v):
                     	$booking_id=$v->booking_id;
                 		$name=$v->name;
                 		$checkin_date=$v->checkin_date;
                 		$checkout_date=$v->checkout_date;
                 		$room_name=$v->room_name;
                 		$room_number=$v->room_number;
                 		$total_room_tariff=$v->total_room_tariff;
                 		$service_charge=$v->service_charge;
                 		$service_tax=$v->service_tax;
                 		$luxury_tax=$v->luxury_tax; 
                 		$advance=$v->total_advance;
                 		$grand_total=$v->grand_total;
                     endforeach;
                     endif;
            ?>
 <form id="checkout_form" action="" method="post" >
<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="panel panel-border panel-primary">
	            <div class="panel-heading"> 
	                <h3 class="panel-title">Guest Details</h3> 
	            </div> 
	            <div class="panel-body"> 
	                <ul class="list-group">
                        <li class="list-group-item">
                            Guest Name:<span><?=$name ?></span>
                        </li>
                        <li class="list-group-item">
                            Check In:<span><?= $checkin_date = date("d-M-Y", strtotime($checkin_date )); ?></span>
                        </li>
                        <li class="list-group-item">
                            Check Out:<span><?php echo $date_now = date("d-M-Y");//= $checkout_date = date("d-M-Y", strtotime($checkout_date ));  ?></span>
                        </li>
                        <li class="list-group-item">
                            Room:<span class="label label-inverse" style="color:#EEE;padding:5px;cursor:none;"><?=$room_number ?></span>
                        </li>
                        <li class="list-group-item">
                            Room Tariff:<span><?=$total_room_tariff ?></span>
                        </li>
                        <li class="list-group-item">
                            Service Tax:<span><?=$service_charge ?>%</span>
                        </li>
                        <li class="list-group-item">
                            Service Charge:<span><?=$service_tax ?>%</span>
                        </li>
                        <li class="list-group-item">
                            Luxury Tax:<span><?=$luxury_tax ?>%</span>
                        </li>
                         <li class="list-group-item">
                            Total Payable:<span><?=$grand_total ?></span>
                        </li>
                         <li class="list-group-item">
                            Total Advance(With Pay Due)<span><?=$advance ?></span>
                        </li>
                        <li class="list-group-item">
                            Due Amount:<span class="due_amount" id="grand_total_booking"><?=$grand_total-$advance ?></span>
                        </li>
                    </ul>
	            </div> 
	        </div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-border panel-primary">
		        <div class="panel-heading"> 
		            <h3 class="panel-title">Checkout Details</h3> 
		        </div> 
		        <div class="panel-body"> 
		            <div class="row">
		            	
		            
		        <div class="col-md-12" id="main_checkout">
		          <div class="col-md-12" id="sub_checkout">
		            <div class="row">
		            	<div class="col-md-12">
		            		<div class="col-md-4">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Amount <span class="error">*</span></label>                                
                                    <input type="text" class="form-control only_digit total_bill main_amount" required="" id="amount" name="amount[]" placeholder="" onblur="total_bill()">
                                    <input type="hidden" value="<?=$booking_id?>" id="booking_id" name="booking_id" >
                                </div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Payment Type <span class="error">*</span></label>
                                    <span id="payment_1"><?=$this->checkout_model->get_payment_type_list(); ?></span>
                                   <?//=get_payment_type_list($payment_type_class);?>
                                </div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Collection Point <span class="error">*</span></label>
                                    <?php echo get_data_dropdown("collection_point","collection_point_id","collection_point_name","account_id = ".$this->session->userdata('account_id'),'','collection_point_id','collection_point_id[]','required'); ?>
                                </div>
		            		</div>
		            		<div class="col-md-2"><br>
		            			<button class="btn btn-icon btn-success add_btn" id="add_checkout"><i class="md-add"></i></button>
		            			<button class="btn btn-icon btn-danger remove_btn" id="remove_checkout"><i class="md-clear"></i> </button>
		            		</div>
		            	</div>
		            </div>
			             	<div class="row">
				            	<div class="col-md-12" > 
		                			<div class="cheque_details_central_reservation hide" id="cheque_details_central_reservation_1">
	                    				<div class="col-md-4 chq_details_central_reservation" > 
	                                        <div class="form-group"> 
	                                            <label for="field-1" class="control-label">Cheque Number  </label> 
	                                            <input type="text" class="form-control only_digit" id="cheque_no_1" name="cheque_no[]" placeholder="Cheque Number" maxlength="10" > 

	                                        </div> 
	                                    </div>
	                                    <div class="col-md-4"> 
	                                        <div class="form-group"> 
	                                            <label for="field-2" class="control-label">Bank Name </label> 
	                                            <input type="text" class="form-control" id="bank_name_1" name="bank_name[]" placeholder="Bank Name"> 
	                                        </div> 
	                                    </div>
	                                </div> 
	                                <div class="dd_details_central_reservation hide" id="dd_details_central_reservation_1" >    
	                    				<div class="col-md-4 "> 
	                                        <div class="form-group"> 
	                                            <label for="field-1" class="control-label">DD Number  </label> 
	                                            <input type="text" class="form-control only_digit" id="dd_no_1" name="dd_no[]" placeholder="DD Number" maxlength="10"> 
	                                        </div> 
	                                    </div> 

	                                    <div class="col-md-4"> 
	                                        <div class="form-group"> 
	                                            <label for="field-2" class="control-label">Bank Namee  </label> 
	                                            <input type="text" class="form-control" id="bank_name_1" name="bank_name[]" placeholder="Bank Name"> 
	                                        </div> 
	                                    </div> 	                                                            
		                            </div>	                                        			
		                			<div class="cc_details_central_reservation hide" id="cc_details_central_reservation_1" >
		                    				<div class="col-md-3"> 
		                                        <div class="form-group"> 
		                                            <label for="field-1" class="control-label">Card Number  </label> 
		                                            <input type="text" class="form-control only_digit" name="card_no[]" id="card_no_1" placeholder="Card Number" maxlength="10"> 
		                                        </div> 
		                                    </div> 
		                                    <div class="col-md-3"> 
		                                        <div class="form-group"> 
		                                            <label for="field-2" class="control-label">Expiry Date  </label> 
		                                            <input type="text" class="form-control" name ="expiry_date[]" id="expiry_date_1" placeholder="Expiry Date mm/yy"> 
		                                        </div> 
		                                    </div> 	                                                            
		                            </div>
		                			<div class="neft_details_central_reservation hide"  id="neft_details_central_reservation_1">
		                				<div class="col-md-4"> 
		                                    <div class="form-group"> 
		                                        <label for="field-1" class="control-label">Account Number  </label> 
		                                        <input type="text" class="form-control only_digit" name="account_no[]" id="account_no_1" placeholder="Account Number" maxlength="10"> 
		                                    </div> 
		                                </div> 
		                                <div class="col-md-4"> 
		                                    <div class="form-group"> 
		                                        <label for="field-2" class="control-label">IFSC Code</label> 
		                                        <input type="text" class="form-control" name ="ifsc_code[]" id="ifsc_code_1" placeholder="IFSC Code"> 
		                                    </div> 
		                                </div> 	                                                            
		                                <div class="col-md-4"> 
		                                    <div class="form-group"> 
		                                        <label for="field-2" class="control-label">Bank Name  </label> 
		                                        <input type="text" class="form-control" name ="bank_name[]"  placeholder="Bank Name"> 
		                                    </div> 
		                                </div> 	                                                            
		                            </div>
		                		</div>
				            </div>
			          </div>	
		           </div> 
		           <div class="row">
		            	<div class="col-md-12">
		            	<div class="form-group no-margin"> 
                            <label for="field-7" class="control-label">Remarks</label> 
                            <textarea class="form-control autogrow" id="checkout_remarks" name="checkout_remarks" id="field-7" placeholder="" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px"></textarea> 
                        </div>
                    </div>
		            </div>
		        </div> 
		    </div>
		</div>
		<?php if(count($booking_transaction)>0) { ?>
			<div class="panel panel-border panel-primary">
	            <div class="panel-heading"> 
	                <h3 class="panel-title">Pay Due Amount</h3> 
	            </div> 
	            <div class="panel-body"> 
	                <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>Booking ID</th>
                                <th>Hotel Name</th>
                                <th>Grand Total</th>
                                <th>Advance</th>
                                <th>Due</th>
                                <th>Payment Type</th>
                                <th>Payment Date</th>
                            </tr>
                        </thead>
                         <tbody>
                        <?php
                        $total=0; 
                        foreach($booking_transaction as $transaction): 
                            {
                            	$cnt++;
                            	$total=$total+$transaction->advance;
                        ?>
                    	<tr>
                            <td><?=$cnt ?></td>
                            <td><?=$transaction->booking_reference ?></td>
                            <td><?=$transaction->hotel_name ?></td>
                            <td><?=$transaction->grand_total?></td>
                            <td><?=$transaction->advance ?></td>
                            <td><?=$transaction->due ?></td>
                            <td><?=$transaction->payment_type?></td>
                            <td><?= $payment_date = date("d-M-Y", strtotime($transaction->payment_date)); ?></td>                            
                        </tr>
                        <?php } endforeach;  ?>  
                    </tbody>
                    </table>
                    <p class="other_income_grand">Total Paid Amount: <span class="other_income_figure" id="transaction_1"><?=$total ?></span></p>
	            </div> 
	        </div>
	        <?php } else {?>
	        <div class="panel panel-border panel-primary">
	            <div class="panel-heading"> 
	                <h3 class="panel-title">Other Expenses</h3> 
	            </div> 
	            <div class="panel-body"> 
	            	<table class="table table-bordered" style="margin-top:25px;">
		                <tbody> 
		                    <tr>
		                    	<th style="text-align:center;">No Paydue Amount Found.</th>
		                    </tr> 
		                </tbody> 
             		</table>
	            </div>
	        </div>
	        <?php } ?>
	</div>
</div> <!-- End row -->

<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
		<?php if(count($booking_expenses)>0) { ?>
			<div class="panel panel-border panel-primary">
	            <div class="panel-heading"> 
	                <h3 class="panel-title">Other Expenses</h3> 
	            </div> 
	            <div class="panel-body"> 
	                <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>Booking ID</th>
                                <th>Room Number</th>
                                <th>Item</th>
                                <th>Date</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                         <tbody>
                        <?php
                        $total=0; 
                        foreach($booking_expenses as $expenses): 
                            {
                            	$cnt++;
                            	$total=$total+$expenses->total_amount;
                        ?>
                    	<tr>
                            <td><?=$cnt ?></td>
                            <td><?=$expenses->booking_reference ?></td>
                            <td><?=$expenses->room_number ?></td>
                            <td><?=$expenses->item_name?></td>
                            <td><?=$expenses_date = date("d-M-Y", strtotime($expenses->date));?></td>                            
                            <td><?=$expenses->no_of_item ?></td>
                            <td><?=$expenses->price?></td>
                            <td><?=$expenses->total_amount?></td>
                        </tr>
                        <?php } endforeach;  ?>  
                    </tbody>
                    </table>
                    <p class="other_income_grand">Other income Grand total: <span class="other_income_figure" id="expenses_1"><?=$total ?></span></p>
	            </div> 
	        </div>
	        <?php } else {?>
	        <div class="panel panel-border panel-primary">
	            <div class="panel-heading"> 
	                <h3 class="panel-title">Other Expenses</h3> 
	            </div> 
	            <div class="panel-body"> 
	            	<table class="table table-bordered" style="margin-top:25px;">
		                <tbody> 
		                    <tr>
		                    	<th style="text-align:center;">No Other Expenses found.</th>
		                    </tr> 
		                </tbody> 
             		</table>
	            </div>
	        </div>
	        <?php } ?>
		</div>


		<div class="col-md-6">
			<div class="panel panel-border panel-primary">
		        <div class="panel-heading"> 
		            <h3 class="panel-title">Checkout Expenses</h3> 
		        </div> 
		        <div class="panel-body"> 
		            <div class="row" id="main_expenses"  style="min-height:135px;">
		            	<div id="sub_expenses" class="col-md-12">
		            		<div class="col-md-3">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Item Name</label>
                                    <input type="text" class="form-control" id="expenses_item_1" name="expenses_item[]" placeholder="">
                                </div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Item Price</label>
                                    <input type="text" class="form-control input_price_cls only_digit"  id="expenses_price_1" name="expenses_price[]" placeholder="" onkeyup="total_expenses()">
                                </div>
		            		</div>
		            		<div class="col-md-3">
		            			<div class="form-group">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="text" class="form-control date_checkout" id="expenses_date_1" name="expenses_date[]"  placeholder="" onclick="dtpickr(this.id)">
                                </div>
		            		</div>
		            		<div class="col-md-2"><br>
		            			<button class="btn btn-icon btn-success add_btn" id="add_button"><i class="md-add"></i></button>
		            			<button class="btn btn-icon btn-danger remove_btn" id="remove_button"><i class="md-clear"></i> </button>
		            		</div>
		            	</div>
		            </div>

		            <div class="row">
		            	<p class="other_income_grand">Check Out Expenses Grand total: <span class="other_income_figure" id="income_sum" name="income_sum" ></span></p>
		            </div>
		        </div> 
		    </div>
		</div>
	</div>
</div> <!-- End row -->

<div class="row" id="net_payable">
	<div class="panel panel-border panel-danger">
		<div class="panel-heading"> 
	                <h3 class="panel-title">Net Payable</h3> 
	            </div>
		<div class="panel-body">
			
			<div class="col-md-12">
				<div class="col-md-3">
					<label>Room rent</label><br>
					<span class="net_payable"><?=$grand_total-$advance ?></span>
				</div>
				<div class="col-md-3">
					<label>Other Expenses</label><br>
					<span class="net_payable"><?=$total ?></span>
				</div>
				<div class="col-md-2">
					<label>Check Out Expenses</label><br>
					<span class="net_payable" id="income_sum1" name="income_sum1"></span>
				</div>
				<div class="col-md-2">
					<label><b>Grand Total</b></label><br>
					<span class="net_payable" id="net_payable_final"  style="color:#ef5350 !important;"></span>					
				</div>
				<div class="col-md-2">
					<label><b>Payable Amount</b></label><br>
					<span class="net_payable " id="net_payable1"   style="color:#ef5350 !important;"></span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row" align="center">
	<button class="btn btn-default btn-lg m-b-5" id="print_bill_only">Print Final Bill</button>
	<button type="button" class="btn btn-success btn-lg m-b-5" id="only_checkout">Checkout Guest</button>
	<button class="btn btn-primary btn-lg m-b-5" id="all_print_checkout">Checkout Guest and Print Final Bill</button>
</div>
<!-- Page content ends --> 
</form> 


<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script type="text/javascript">

	//add dynamic field script expenses..

		total_expenses();
		total_bill();
        var scntDiv = $('#main_expenses');
        var i = $('#main_expenses').size() + 1;
	 	$(document).on("click", "#add_button", function(){
  	 	//do something
  		//alert(scntDiv+i);
  		$('<div id="sub_expenses" class="col-md-12"><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Item Name</label><input type="text" class="form-control" name="expenses_item[]"  id="expenses_item_'+i+'" placeholder=""></div></div><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Item Price</label><input type="text" class="form-control input_price_cls only_digit" name="expenses_price[]"  id="expenses_price_'+i+'" placeholder="" onkeyup="total_expenses()"></div></div><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Date</label><input type="text" class="form-control"  id="expenses_date_'+i+'" name="expenses_date[]"  onclick="dtpickr(this.id)" placeholder=""></div></div><div class="col-md-2"><br><button class="btn btn-icon btn-success add_btn" id="add_button"><i class="md-add"></i></button><button class="btn btn-icon btn-danger remove_btn" style="margin-left:5px;" id="remove_button"><i class="md-clear"></i></button></div></div>').appendTo(scntDiv);
                i++;
                return false;
		});

	  	$(document).on("click", "#remove_button", function(){
                if( i > 2 ) {
                        $(this).parents('#sub_expenses').remove();
                        i--;
                }
                total_expenses();
                return false;
        });
		//end dynamic field script expenses..

		//add dynamic field script Payment Type..
		var scntDiv1 = $('#main_checkout');
	    var c = $('#sub_checkout').size() + 1;
	   	// alert(c);

		$(document).on("click", "#add_checkout", function(){
	  	 	//do something
	  		//alert(scntDiv+i);
	  		$('<div class="col-md-12" id="sub_checkout"><div class="row"><div class="col-md-12"><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Amount <span class="error">*</span></label><input type="text" class="form-control only_digit total_bill main_amount" required="" id="amount" name="amount[]" placeholder="" onblur="total_bill()"><input type="hidden" value="<?=$booking_id?>" id="booking_id" name="booking_id" ></div></div><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Payment Type <span class="error">*</span></label><span id="payment_'+c+'"><?=$this->checkout_model->get_payment_type_list(); ?></span></div></div><div class="col-md-3"><div class="form-group"><label for="exampleInputEmail1">Collection Point <span class="error">*</span></label><?php echo get_data_dropdown("collection_point","collection_point_id","collection_point_name","account_id = ".$this->session->userdata('account_id'),'','collection_point_id','collection_point_id[]','required'); ?></div></div><div class="col-md-2"><br><button class="btn btn-icon btn-success add_btn" id="add_checkout"><i class="md-add"></i></button><button class="btn btn-icon btn-danger remove_btn" style="margin-left:5px" id="remove_checkout"><i class="md-clear"></i> </button></div></div></div><div class="row"><div class="col-md-12" ><div class="cheque_details_central_reservation hide" id="cheque_details_central_reservation_'+c+'"><div class="col-md-4 chq_details_central_reservation" > <div class="form-group"> <label for="field-1" class="control-label">Cheque Number </label><input type="text" class="form-control only_digit" id="cheque_no_1" name="cheque_no[]" placeholder="Cheque Number" maxlength="10" ></div></div><div class="col-md-4"><div class="form-group"><label for="field-2" class="control-label">Bank Name </label><input type="text" class="form-control" id="bank_name_1" name="bank_name[]" placeholder="Bank Name"></div></div></div><div class="dd_details_central_reservation hide" id="dd_details_central_reservation_'+c+'" ><div class="col-md-4 "><div class="form-group"><label for="field-1" class="control-label">DD Number </label><input type="text" class="form-control only_digit" id="dd_no_1" name="dd_no[]" placeholder="DD Number" maxlength="10"></div></div><div class="col-md-4"><div class="form-group"><label for="field-2" class="control-label">Bank Namee </label><input type="text" class="form-control" id="bank_name_1" name="bank_name[]" placeholder="Bank Name"></div></div></div><div class="cc_details_central_reservation hide" id="cc_details_central_reservation_'+c+'"><div class="col-md-3"><div class="form-group"><label for="field-1" class="control-label">Card Number </label><input type="text" class="form-control only_digit" name="card_no[]" id="card_no_1" placeholder="Card Number" maxlength="10"></div></div><div class="col-md-3"><div class="form-group"><label for="field-2" class="control-label">Expiry Date </label><input type="text" class="form-control" name ="expiry_date[]" id="expiry_date_1" placeholder="Expiry Date mm/yy"></div></div></div><div class="neft_details_central_reservation hide"  id="neft_details_central_reservation_'+c+'"><div class="col-md-4"><div class="form-group"><label for="field-1" class="control-label">Account Number </label><input type="text" class="form-control only_digit" name="account_no[]" id="account_no_1" placeholder="Account Number" maxlength="10"></div></div><div class="col-md-4"><div class="form-group"><label for="field-2" class="control-label">IFSC Code</label><input type="text" class="form-control" name ="ifsc_code[]" id="ifsc_code_1" placeholder="IFSC Code"></div></div><div class="col-md-4"><div class="form-group"><label for="field-2" class="control-label">Bank Name </label><input type="text" class="form-control" name ="bank_name[]"  placeholder="Bank Name"></div></div></div></div></div></div>').appendTo(scntDiv1);
	                c++;
	                return false;
		});
		$(document).on("click", "#remove_checkout", function(){
	                if( c > 2 ) {
	                        $(this).parents('#sub_checkout').remove();
	                        c--;
	                }
	                total_bill();
	                return false;
        });

        //end dynamic field script Payment Type..

        //include total expenses 

		function total_expenses() {
			total_expenses_ = 0;
				$('#sub_expenses .input_price_cls').each(function(){
					var input_text_val = $(this).val();
					if (input_text_val.length>0)
					{ 
			     total_expenses_ += parseInt(input_text_val);
			        }
				}
			); // end of each
			$("#income_sum").text(total_expenses_);
			$("#income_sum1").text(total_expenses_);
			total_ammount()
		}

		 //include total checkout payment 

		function total_bill() {
			total_bill_ = 0;
			$('#sub_checkout .total_bill').each(function(){
					var input_text_val1 = $(this).val();
					if (input_text_val1.length>0)
					{ 
			     total_bill_ += parseInt(input_text_val1);
			        }
				}
			); // end of each

			$("#net_payable1").text(total_bill_);
			var net_payable1 = $( "#net_payable1" ).text();
			total_ammount()
			var net_payable_final = $( "#net_payable_final" ).text();
			//alert (net_payable_final);
			if (parseInt(net_payable_final)<parseInt(net_payable1)) {

				//alert(net_payable1+net_payable_final);
				swal("Please Check Amount", "you enter maximum amount, Your total enter "+net_payable1+" . and total amount you have to collect "+net_payable_final, "error");
			}
		}

		// showing golbal total 
		function total_ammount() {
			var grand_total_booking = $( "#grand_total_booking" ).text();
			if (grand_total_booking=='') {

				net_payable1='0';
			}
			var income_sum = $( "#income_sum" ).text();
			if (net_payable1=='') {

				income_sum='0';
			}
			var expenses_1 = $( "#expenses_1" ).text();
			if (expenses_1=='') {

				expenses_1='0';
			}

			var net_payable_final= parseInt(grand_total_booking)+parseInt(income_sum)+parseInt(expenses_1);
			$( "#net_payable_final" ).html( net_payable_final );

		}	

		 $("#checkout_date").datepicker({ dateFormat: 'dd-mm-yy'});
		 $("#expenses_date").datepicker({ dateFormat: 'dd-mm-yy'});
		 function dtpickr(id)
		{
		    //alert(id);
		    $("#"+id).datepicker({
		      //maxDate: "02/26/2016"
		    });
		}
		$(document).ready(function() {

				var base_url = $.cookie("base_url");	
				var booking_id = $("#booking_id").val();
				//alert(booking_id);
		
		 		$("#print_bill_only").click(function() {
			    	
					$( "#checkout_form" ).attr("target","_blank");
					$( "#checkout_form" ).attr("action",base_url+"print_preview/"+booking_id);
					$( "#checkout_form" ).submit();
					$( "#checkout_form" ).removeAttr("target");
					$( "#checkout_form" ).attr("action"," ");
	           });

	           $("#only_checkout").click(function() {

			    	
					var net_payable1 = $( "#net_payable1" ).text();		
					var net_payable_final = $( "#net_payable_final" ).text();
					//alert (net_payable_final);
					if (parseInt(net_payable_final)<parseInt(net_payable1)) {

						//alert(net_payable1+net_payable_final);
						swal("Please Check Amount", "you enter maximum amount, Your total enter "+net_payable1+" . and total amount you have to collect "+net_payable_final, "error");					
						return false;
					} else if (parseInt(net_payable_final)>parseInt(net_payable1)) {

					var flag = true;
 					if (flag==true){ 
						$(".main_amount").each(function(){
	    	                dynamic1 = $(this).val();
					 	// alert(dynamic);
	    		             if(dynamic1==""){
		                             flag=false;
		                             swal({   title: "Please enter amount",   text: "It will close in 2 seconds.",   timer: 1000,   showConfirmButton: false });
		                              return false;
			                      }  
							});
						}

					if (flag==true){ 
						$("#sub_checkout select").each(function(){
							 	dynamic = $(this).val();
							 	// alert(dynamic);
			    		             if(dynamic==""){
			     		               flag = false;
			     		               swal({   title: "Collection point or Payment type not selected ",   text: "It will close in 2 seconds.",   timer: 1000,   showConfirmButton: false });	
			     		               return false;
			     	              }   

							 }) ; // end of each
					}
					// swal({   title: "arijit",   text: "It will close in 2 seconds.",   timer: 5000,   showConfirmButton: false });	
				    //alert(flag);
				    if (flag==true){
				    	swal({   title: "Are you sure?",  
				    	 text: "You enter minimum amount is it ok...!", 
				    	   type: "warning",   showCancelButton: true,   
				    	   confirmButtonColor: "#DD6B55", 
				    	     confirmButtonText: "Yes, submit it!",   
				    	     cancelButtonText: "No, cancel it!",   closeOnConfirm: false,   
				    	     closeOnCancel: false 
				    	 }, function(isConfirm)
					    	 {   if (isConfirm) { 
					    	 	$( "#checkout_form" ).attr("action",base_url+"checkout/insert_checkout_details");
								$( "#checkout_form" ).submit();	
								$( "#checkout_form" ).attr("action"," ");  
					    	   swal("Checkout!", "Your imaginary file has been deleted.", "success");  
					    	    } else {     swal("Cancelled", "please check the amount:)", "error");   } });
				    }
				    return flag;
				    
				   	//$( "#checkout_form" ).attr("action",base_url+"checkout/insert_checkout_details");
					//$( "#checkout_form" ).submit();					
					//$( "#checkout_form" ).attr("action"," ");
				} else if (parseInt(net_payable_final)==parseInt(net_payable1)) {					
					
					var flag = true;
 					if (flag==true){ 
						$(".main_amount").each(function(){
	    	                dynamic1 = $(this).val();
					 	// alert(dynamic);
	    		             if(dynamic1==""){
		                             flag=false;
		                             swal({   title: "Please enter amount",   text: "It will close in 2 seconds.",   timer: 1000,   showConfirmButton: false });
		                              return false;
			                      }  
							});
						}

					if (flag==true){ 
						$("#sub_checkout select").each(function(){
							 	dynamic = $(this).val();
							 	// alert(dynamic);
			    		             if(dynamic==""){
			     		               flag = false;
			     		               swal({   title: "Collection point or Payment type not selected ",   text: "It will close in 2 seconds.",   timer: 1000,   showConfirmButton: false });	
			     		               return false;
			     	              }   

							 }) ; // end of each
					}

					if (flag==true){
				    	swal({   title: "Are you sure?",  
				    	 text: "You enter "+net_payable1+"  amount is it ok...!", 
				    	   type: "warning",   showCancelButton: true,   
				    	   confirmButtonColor: "#DD6B55", 
				    	     confirmButtonText: "Yes, submit it!",   
				    	     cancelButtonText: "No, cancel it!",   closeOnConfirm: false,   
				    	     closeOnCancel: false 
				    	 }, function(isConfirm)
					    	 {   if (isConfirm) { 
					    	 	$( "#checkout_form" ).attr("action",base_url+"checkout/insert_checkout_details");
								$( "#checkout_form" ).submit();	
								$( "#checkout_form" ).attr("action"," ");  
					    	   swal("Checkout!", "Your imaginary file has been deleted.", "success");  
					    	    } else {     swal("Cancelled", "please check the amount:)", "error");   } });
				    }


					
				    return flag;
				 //   	$( "#checkout_form" ).attr("action",base_url+"checkout/insert_checkout_details");
				 //   	$( "#only_checkout" ).attr("type","submit");
					// $( "#checkout_form" ).submit();					
					// $( "#checkout_form" ).attr("action"," ");
					// $( "#only_checkout" ).attr("type","button");
					}

	           });

	           $("#all_print_checkout").click(function() {
			    
					$( "#checkout_form" ).attr("target","_blank");
					$( "#checkout_form" ).attr("action",base_url+"print_preview/"+booking_id);
					$( "#checkout_form" ).submit();
					$( "#checkout_form" ).removeAttr("target");
					$( "#checkout_form" ).attr("action"," ");
	           });

		 }); 

			   
			  	

	// write a validation which will accept only numbers in fileds values. But 0 shouldn't be allowed at 1st place.

		$(document).on('keypress', '.only_digit', function(e) {

		  	if (this.value.length == 0 && e.which == '48' ) {

		 				return false;

		 			}else if (e.which < 48 || e.which > 57 ) {

						return false;  
		  		}
	    });

	// for payment type we need by default hide all bank related html..
		// we will shoow and hide accordingly on the basis of payment type selected from drop down..

$(document).ready(function() {


		$(document).on('change', '.payment_type_class', function(){
			var result = ($(this).parent().attr('id')).replace('payment_', '');
			//alert(result );
			//alert("change on select..")
			if(result != ""){
				//alert('change payment type have value');
				/// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
				var payment_type = $(this).val();
				//alert(payment_type);
				
					if(payment_type == "cash" || payment_type == "payment_by_company" || payment_type == "" ){
						//alert(payment_type);
						$("#cheque_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#dd_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#cc_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#neft_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );	
					}
					if(payment_type == "cheque" ){

						//alert(payment_type);
						$("#cheque_details_central_reservation_"+result).removeClass( "hide", 1000, "easeInBack" );
						$("#dd_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#cc_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#neft_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );	
					
					}
					if(payment_type == "demand_draft" ){

						$("#cheque_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#dd_details_central_reservation_"+result).removeClass( "hide", 1000, "easeInBack" );
						$("#cc_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#neft_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );	
					}
					if(payment_type == "credit_card" || payment_type == "debit_card" ){

						$("#cheque_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#dd_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#cc_details_central_reservation_"+result).removeClass( "hide", 1000, "easeInBack" );
						$("#neft_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );		
					}
					if(payment_type == "neft" || payment_type == "rtgs" ){

						$("#cheque_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#dd_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#cc_details_central_reservation_"+result).addClass( "hide", 1000, "easeInBack" );
						$("#neft_details_central_reservation_"+result).removeClass( "hide", 1000, "easeInBack" );		
					}
				}
		});			
		
	});

</script>





