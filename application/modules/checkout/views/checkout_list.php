<link href="<?=base_url()?>themes/css/views/checkout.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Current Occupancy List</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">
<div class="panel panel-default">
    <?=$this->session->flashdata("insert_success"); ?> 
            <div class="panel-heading"> 
                <h3 class="panel-title">Select Hotel</h3> 
            </div> 
        
            <div class="panel-body"> 
                
                <div class="row">
                    <div class="form-group" style="margin-left:40%;width:20%;margin-bottom:25px;"> 
                     <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'",'0','hotel_id','hotel_id',"required aria-required='true'", "hotel_name"); ?>       
                    </div>
                </div> <!-- End Row -->

                <div id="travel_agent_list">
                <!-- Here the checkout table will show after selecting hotel-->
                </div>
                <!-- Table ends -->
            </div>
 </div>
<div class="panel panel-default">
    <div id="checkout_list"> 
         
    </div>   
</div>
			
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    
    $("#hotel_id").change(function() {
    var hotel_id=$("#hotel_id").val();
    //alert(hotel_id);
   if (hotel_id!='') {
    $.ajax({
        url: "<?=base_url()?>Checkout/fetch_checkout",
        type: 'POST',
        dataType: "json",
        data: {'hotel_id': hotel_id},  
        
        success: function(result) {
            //alert(result);
        $('#checkout_list').html(result.item);
        }
        
        });
    }else if(hotel_id==''){

        $.ajax({
        url: "<?=base_url()?>Checkout/no_select_checkout",
        type: 'POST',
        dataType: "json",
        data: {'hotel_id': hotel_id},  
        
        success: function(result) {
            //alert(result);
        $('#checkout_list').html(result.item);
        }
        
        });

    } else{
        $.ajax({
        url: "<?=base_url()?>Checkout/no_checkout",
        type: 'POST',
        dataType: "json",
        data: {'hotel_id': hotel_id},  
        
        success: function(result) {
            //alert(result);
        $('#checkout_list').html(result.item);
        }
        
        });
    }

    
     });
</script>
<?php
    // we need to write a condition where we will ceck whetehr any hotel, room, meal plan and other required entity(which will be required at the time of booking) is added ot not... if its not added then we will redirect it to concenr page to add that entity first....        
    if(isset($hotel_error) && $hotel_error == 1){
?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "Please add a hotel.",
                    text: "You will be redirected to a page from where you can add a hotel.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add hotel page",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"hotel/");
                });             
            });
        </script>
<?php } if(isset($meal_plan_type_error) && $meal_plan_type_error == 1){ ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "Please add a plan type for hotel.",
                    text: "You will be redirected to a page from where you can add plan type for all hotel.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add plan type for hotel",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"hotel/");
                });
            });
        </script>

<?php } if(isset($meal_plan_error) && $meal_plan_error == 1){ ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "Please add a meal plan for hotel.",
                    text: "You will be redirected to a page from where you can add meal plan for a particular hotel.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add meal plan for hotel",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"hotel/");
                });
            });
        </script>

<?php } if(isset($collection_point_error) && $collection_point_error == 1){ ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "Please add a collection point for hotel.",
                    text: "You will be redirected to a page from where you can add collection point for a particular hotel.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add collection point for hotel",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"collection_point/");
                });
            });
        </script>

<?php } if(isset($room_error) && $room_error == 1){ ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "Please add rooms in hotel.",
                    text: "You will be redirected to a page from where you can add rooms in a particular hotel.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add hotel room",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"room/");
                });
            });
        </script>
<?php } if(isset($checkout_error) && $checkout_error == 1){ ?>
        <script type="text/javascript">
            $(document).ready(function(){
                var base_url = $.cookie("base_url");
                swal({
                    title: "This booking allready checkout.",
                    text: "You will be redirected to a page from where other checkout enytry.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, move to add hotel room",
                    closeOnConfirm: false,
                    allowEscapeKey: false
                }, function (isConfirm) {
                    if (!isConfirm) return;
                    $(location).attr("href", base_url+"checkout/");
                });
            });
        </script>        

<?php } ?>


