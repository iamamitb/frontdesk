<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('checkout_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }
	public function index() {

		$account_id=$this->session->userdata('account_id');
		$this->template->title('Guest List For Checkout','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('checkout_list');
		// it means there is a validation error...
			// we need to write a condition where we will ceck whetehr any hotel, room, meal plan and other required entity(which will be required at the time of booking) is added ot not... if its not added then we will redirect it to concenr page to add that entity first....		
			if(count_rows("hotels", "account_id = ".$this->session->userdata('account_id')." and hotels_status = '1' ")){
				// it means hotel is added....
				if(count_rows("plan_type", "")){
					// it means plan type is added....
					if(count_rows("hotel_plan_type", "")){
						// it means plan type for any hotel is added....
						if(count_rows("collection_point", "account_id = ".$this->session->userdata('account_id')." and collection_point_status = '1'")){
							// it means collection point is added....
							if(!count_rows("rooms", "")){
								// it means rooms are not added for a hotel.....
								$data['room_error'] = 1;
							}			
						}else {
							// it means collection points are not added .....
							$data['collection_point_error'] = 1;
						}			
					}else {
						// it means meal plans are not added for a hotel.....
						$data['meal_plan_error'] = 1;
					}			
				}else {
					// it means plan types are not added for a hotel.....
					$data['meal_plan_type_error'] = 1;
				}				
			}else {
				// it means hotels are not added .....
				$data['hotel_error'] = 1;
			}	
		//$data['all_bookings']=$this->checkout_model->all_bookings($account_id);
 		$this->template->build('checkout_list',$data);	

				
	}
	public function fetch_checkout() {

		$account_id=$this->session->userdata('account_id');
		$hotel_id= $this->input->post('hotel_id');
		$all_bookings=$this->checkout_model->all_bookings($account_id,$hotel_id);
		$all_not_checkin=$this->checkout_model->all_not_checkin($account_id,$hotel_id);
		$cnt = 0;
		$html['item']='';
		
				if(count($all_bookings)>0) {
				$html['item'].='<div class="row">
		                <div class="col-md-12 col-sm-12 col-xs-12">
		                    <table id="datatable" class="table table-striped table-bordered">
		                        <thead>
		                            <tr>
		                                <th>Sl.no</th>
		                                <th>Booking ID</th>
		                                <th>Guest Name</th>
		                                <th>Hotel Name</th>
		                                <th>Phone Number</th>
		                                <th>Checkin Date</th>
		                                <th>Checkout Date</th>
		                                <th>Room Number</th>
		                                <th>Action</th>
		                            </tr>
		                        </thead>
		                        <tbody>';
		            foreach ($all_bookings as $key => $all_booking) { 
		            	$cnt++;
		            	$checkin_date = date("d-M-Y", strtotime($all_booking->checkin_date));
		            	$checkout_date = date("d-M-Y", strtotime($all_booking->checkout_date ));
					$html['item'].='<tr>
		                                <td>'.$cnt.'</td>
		                                <td>'.$all_booking->booking_reference.'</td>
		                                <td>'.$all_booking->hotel_name.'</td>
		                                <td>'.$all_booking->name.'</td>
		                                <td>'.$all_booking->phone.'</td>
		                                <td>'.$checkin_date .'</td>
		                                <td>'.$checkout_date.'</td>
		                                <td align="center"><span class="label label-inverse">'.$all_booking->room_number1.'</span></td>
		                                <td align="center"><a target="_blank" href="'.base_url().'checkout/checkout_guest/'.$all_booking->booking_id.'"><span class="label label-success">Checkout Now</span></a></td>
		                            </tr>';

		            }    
		            $html['item'].='</tbody>
		                    </table>

		                </div>
		            </div>'; 
		            }else{
		            	$html['item'].='<table class="table table-bordered" style="margin-top:25px;">
		                <tbody> 
		                    <tr>
		                    <th style="text-align:center;">No checkout found.</th>
		                    </tr> 
		                 </tbody> 
		             </table>';
		            }

	 echo json_encode($html);
				
	}

	public function no_checkout() {
		
		$html['item']='';
		$html['item'].='<table class="table table-bordered" style="margin-top:25px;">
                <tbody> 
                    <tr>
                    <th style="text-align:center;">No checkout found.</th>
                    </tr> 
                 </tbody> 
             </table>';
		 echo json_encode($html);

	}
	public function no_select_checkout() {
		
		$html['item']='';

		 echo json_encode($html);

	}



	public function Checkout_Guest($booking_id){

		$today= date("Y-m-d");		
		if(count_rows("bookings", "booking_id = ".$booking_id." and  booking_status != '3' ")){

			$today= date("Y-m-d");
			$account_id=$this->session->userdata('account_id');
			$this->template->title('Guest List For Checkout','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('main_template','front');
			$this->template->build('checkout_list');
			$data['checkout_error'] = 1;
			$data['booking_id'] = $booking_id;
			$this->template->build('checkout_list',$data);	

		} else {

				$account_id=$this->session->userdata('account_id');
				$this->template->title('Guest Name - Checkout','Imanagemyhotel');
				$this->template->set('metaDesc','Imanagemyhotel');
				$this->template->set('metaKeyword','Imanagemyhotel');
				$this->template->set_layout('main_template','front');
				$this->template->build('checkout_guest');
				$data['booking_checkout']=$this->checkout_model->booking_checkout($account_id,$booking_id);
				$data['booking_expenses']=$this->checkout_model->booking_expenses($account_id,$booking_id);
				$data['booking_transaction']=$this->checkout_model->booking_transaction($account_id,$booking_id);
		 		$this->template->build('checkout_guest',$data);	
 		}


		
	}
	public function insert_checkout_details(){

		
		$booking_id=$this->input->post('booking_id');
		$checkout_remarks=$this->input->post('checkout_remarks');	

		$amount=$this->input->post('amount');
		$total_insert_count=count($amount);		
		$collection_point_id=$this->input->post('collection_point_id');
		$paymentType=$this->input->post('payment_type');
		$bankName=$this->input->post('bank_name');
		$cardNo=$this->input->post('card_no');
		$chequeNo=$this->input->post('cheque_no');
		$ddNo=$this->input->post('dd_no');
		$bankAccNo=$this->input->post('account_no');
		$ifsc=$this->input->post('ifsc_code');
		$expiryDate=$this->input->post('expiry_date');
		
		if($bankName ==""){ $bankName="N/A"; }
		if($chequeNo==""){ $chequeNo="N/A"; }
 		if($ddNo==""){ $ddNo="N/A"; }
 		if($cardNo==""){ $cardNo="N/A"; }
 		if($expiryDate==""){ $expiryDate="N/A"; }
 		if($bankAccNo==""){ $bankAccNo="N/A"; }
 		if($ifsc==""){ $ifsc="N/A"; }

	/////////////////// Update checkout date after checkout /////////////////

 		$checkout_date=getValue('bookings','checkout_date','booking_id ='.$booking_id);
 		$today_date=date('Y-m-d');
 		//echo $checkout_date.$today_date;
 		//die();
 		if ($checkout_date>$today_date) {

 		 			$checkout_date=array(
 					'booking_status' => '4',
					'checkout_date'=>date('Y-m-d')	
					);

	    		updateDataCondition('bookings',$checkout_date,'booking_id ='.$booking_id);
 			
 		 	} else { 		 	
 		 		$checkout_date=array(
 					'booking_status' => '4'
					);

	    		updateDataCondition('bookings',$checkout_date,'booking_id ='.$booking_id);

 		 	} 		
 
 		/////////////////// Insert checkout details after checkout into checkout details table /////////////////
 		
 		$data2=array(
					'booking_id'=>$booking_id,
					'checkout_remarks'=>$checkout_remarks,
					'user_id'=>$this->session->userdata('id')
					
					);
 		
		$checkout_details=insertValue('checkout_details',$data2);

	/////////////////// Insert checkout amount  details after checkout into transaction table /////////////////
		
		for ($i=0; $i < $total_insert_count; $i++) { 
			
			$checkout_data = array(
							'checkout_id'=>$checkout_details,
							'collection_point_id'=>$collection_point_id[$i],
							'advance'=>$amount[$i],
							'payment_type'=>$paymentType[$i],
							'payment_date'=>date('Y-m-d'),
							'bank_name'=>$bankName[$i],
							'cheque_no'=>$chequeNo[$i],
							'dd_no'=>$ddNo[$i],
							'card_no'=>$cardNo[$i],
							'account_no'=>$bankAccNo[$i],
							'ifsc_code'=>$ifsc[$i],
							'expiry_date'=>$expiryDate[$i]
						);
		$result=insertValue('transaction',$checkout_data);
			//print_r($checkout_data);
 			//echo "<br>";
		}
		
	/////////////////// Delete  row if checkout date is before today from booked_room_stay_date table /////////////////

  	$this->checkout_model->delete_staydate($booking_id);    
       	
	/////////////////// Insert checkout expenses  details after checkout into checkout_expenses table /////////////////	
        


			$expenses_item=$this->input->post('expenses_item');
			$expenses_price=$this->input->post('expenses_price');
			$expenses_date=$this->input->post('expenses_date');
		    $count1=count($expenses_item);

			 	for ($i=0; $i < $count1; $i++) { 

			 		if ($expenses_item[$i]!='' && $expenses_price[$i]!='') {

			 		$ex_date=dateformate(($expenses_date[$i]),"Y-m-d","-");		 		
			 		 $data = array(
									'checkout_expenses_item'=>$expenses_item[$i],
									'checkout_expenses_price'=>$expenses_price[$i],
									'checkout_expenses_date'=>$ex_date,
									'booking_id'=>$booking_id
								);
			 		insertValue('checkout_expenses',$data);
			 	}
		 	}		

	/////////////////// Fetch deta last checkout  bookings details  /////////////////		 	

	 	$account_id=$this->session->userdata('account_id');	 	
	 	$booking_checkout_details=$this->checkout_model->booking_checkout_details($account_id,$booking_id);


	 	  if(isset($booking_checkout_details)){
                     foreach($booking_checkout_details as $v){
                     	$booking_id=$v->booking_id;
                 		$name=$v->name;               
                 		$room_number=$v->room_number;
                    		 }
                     }
		//echo $name.$room_number;
	 	$this->session->set_flashdata('insert_success','<div class="alert alert-success alert-dismissable" style="text-align:center;">
    									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										 You have successfully Checkout '.$name.' with  '.$room_number.' </div>'
           					);	
		redirect(base_url().'checkout');
	}




	

}
