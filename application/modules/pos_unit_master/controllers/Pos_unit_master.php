<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_unit_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_unit_master_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		
		$data['allRecord']=getData("unit_master","unit_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('POS Unit Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_unit_master',$data);

				
	}

	public function addUnit(){
		$unit_name=$this->input->post('unit_name'); 
		$conversion_value=$this->input->post('conversion_value');
		$conversion_unit_id=$this->input->post('conversion_unit_id');
		$hotel_id=$this->session->userdata('hotel_id');

		if($conversion_unit_id!=''){
			$data=array('hotel_id'=>$hotel_id,
						'unit_name'=>$unit_name,
						'conversion_value'=>$conversion_value,
						'conversion_unit_id'=>$conversion_unit_id,
						'unit_status'=>'1'
						);
		}else{
			$data=array('hotel_id'=>$hotel_id,
						'unit_name'=>$unit_name,
						'conversion_value'=>$conversion_value,
						'conversion_unit_id'=>'0',
						'unit_status'=>'1'
						);

		}
		//print_r($data); die();
		$q = $this->db->get_where('unit_master',array('unit_name' => $unit_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('unit_name',$unit_name);
			  $this->db->update('unit_master',$data);

			  $this->session->set_flashdata('chkunitdata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Unit already exists.
            </div>');
			redirect('pos_unit_master#');

		}else{ 
			  $unit_id=insertValue('unit_master',$data);
		} 	
		
		
		$this->session->set_flashdata('addunitdata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the unit.
            </div>');
		
		
		redirect('pos_unit_master#');
		
	}


	public function editUnit($idu) { 
		$unit=getSingle('unit_master','unit_id = '.$idu,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Unit</h3>
			    </div>
		    <form method="post" action="'.base_url().'pos_unit_master/updateUnit">
			<input type="hidden" id="unitID" name="unitID" value="'.$unit->unit_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Unit Name<span class="error">*</span></label>
		              <input type="text" id="unit_name" name="unit_name" class="form-control" value="'.$unit->unit_name.'" required>
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-2" class="control-label">Conversion Value<span class="error">*</span></label>
		              <input type="text" id="conversion_value" name="conversion_value" class="form-control" value="'.$unit->conversion_value.'" required>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Conversion Unit</label>
		              '.get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","$unit->conversion_unit_id","conversion_unit_id","conversion_unit_id","","","","").'
		            </div>
		          </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_unit" id="edit_unit" class="btn btn-success waves-effect waves-light">Update Unit</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateUnit() {
		$unitID=$this->input->post('unitID'); 
		$unit_name=$this->input->post('unit_name'); 
		$conversion_value=$this->input->post('conversion_value');
		$conversion_unit_id=$this->input->post('conversion_unit_id');
		
		if($conversion_unit_id!=''){
			$data=array(
						'unit_name'=>$unit_name,
						'conversion_value'=>$conversion_value,
						'conversion_unit_id'=>$conversion_unit_id,
						'unit_status'=>'1'
						);
		}else{
			$data=array(
						'unit_name'=>$unit_name,
						'conversion_value'=>$conversion_value,
						'conversion_unit_id'=>'0',
						'unit_status'=>'1'
						);

		}

  		$unitUpdate=updateDataCondition('unit_master',$data,'unit_id = '.$unitID);
                          
  		$this->session->set_flashdata('msgUnitUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the unit.
            </div>');	
                          
  		redirect('pos_unit_master#');

	}


	public function unitDelete() { 
		
		$unit_id=$_POST['unit_id'];		
	
		$data['unit_status']='0';			
		
		$supplier_delete=updateDataCondition('unit_master',$data,'unit_id = '.$unit_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	












	

}
