<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Unit Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_units" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Units</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_unit" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Unit</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addunitdata"); ?>
            <?=$this->session->flashdata("chkunitdata"); ?> 
            <?=$this->session->flashdata("msgUnitUpdate"); ?>  
            <div class="tab-pane active" id="all_units"> 
            <!-- Row for Table Starts -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="font-weight:bold;margin-bottom:30px;">All Units</h2>
                    <table id="datatable" class="table table-striped table-bordered">
                        <?php 
                            if(isset($allRecord) && count($allRecord)>0){
                            ?>
                        <thead>
                            <tr>
                                <th>Unit Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                 
                        <tbody>
                            <?php 
                                foreach($allRecord as $unit):{
                              ?>
                            <tr>
                                <td><?=$unit->unit_name?></td>
                                <td>
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="#" data-toggle="modal" onclick="editUnit(<?=$unit->unit_id?>)"  data-target="#edit-unit">Edit</a></li>
                                        <li><a onclick="unitDelete(<?=$unit->unit_id?>)" href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?> 
                           </tbody>
                           <?php endforeach;  } else{ ?>
                            <tbody> 
                                <tr >
                                <th style="text-align:center;">You have not added any Unit Yet.</th>
                                </tr> 
                             </tbody> 
                             <?php } ?>
                    </table>

                </div>
            </div>
           <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_unit"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Unit</h2>
                <form name="unit_form" method="post" id="unit_form" action="<?php echo base_url(); ?>pos_unit_master/addUnit"> 
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Unit Name<span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="unit_name" name="unit_name" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Conversion Value<span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="conversion_value" name="conversion_value" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Conversion Unit</label>
                                    <div class="col-md-9">
                   <?=get_data_dropdown("unit_master","unit_id","unit_name","unit_status = '1'","","conversion_unit_id","conversion_unit_id","","","","")?>
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Unit</button>
                    </div>
                </div>
                </form> 
                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends --> 

<!-- Add Supplier Modal -->
<div id="edit-unit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


$(document).ready(function() {
    $("#unit_form").validate({
         rules: {
            
            unit_name: {
                required: true,
                noSpace: true
            },
            conversion_value: {
                required: true,
                number: true
            }
            
         },
         messages: {
            unit_name: {
                    required: "Please enter unit name"
                    
                },
            conversion_value: {
                    required: "Please enter conversion value",
                    number: "Please specify a number"
                }    
         },
        
    });
});


function editUnit(idu)
{
    var idu = idu;
    //alert(idu);
   
    $.ajax({
        url:"<?php echo site_url('pos_unit_master/editUnit');?>/"+idu,
        type: 'POST',
        dataType: "json",
        data: {'idu': idu},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-unit').html(data.item);
            
        }      
    });
}



$('#edit_unit').click(function(){

   var unitID = $("#unitID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_unit_master/updateUnit",
        type: 'POST',
        dataType: "json",
        data: {unitID : unitID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function unitDelete(unit_id) {
    //alert(unit_id); 
    swal({
        title: "Are you sure?",
        text: "This unit will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_unit_master/unitDelete",
            type: 'POST',
            dataType: "json",
            data: {'unit_id': unit_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}



</script>

