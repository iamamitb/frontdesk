<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class room_maintenance_model extends CI_Model {
	function getRoomInfo($hotel_id){
			$row=array();
			$sql="SELECT r.room_id,r.room_name,rn.room_number from rooms as r 
							join room_number as rn 
							on r.room_id=rn.room_id 
							where r.hotel_id=$hotel_id";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				foreach($query->result_array() as $val){
					$row[$val['room_id']]['room_name']=$val['room_name'];
					$row[$val['room_id']]['room_number'][]=$val['room_number'];
				}
			}
			return $row;
	}
	function roomid($maintenance_id,$roomid){
		 foreach($roomid['room_id'] as $key=>$value){
			 	$room_num_data2 = array(															
									'maintenance_id'=>$maintenance_id,
									'room_id'=>$value												
								);
						$room_maintenance_id=insertValue('room_id_maintenance',$room_num_data2);

						 foreach($roomid['room_number'][$value] as $key=>$value1){
							 	$room_num_data3 = array(															
									'room_maintenance_id'=>$room_maintenance_id,
									'room_number'=>$value1												
								);
						$room_number_id=insertValue('room_no_maintenance',$room_num_data3);
							$rm[]=$value1;
						 }
			 }

			 return $rm;
	}
		function date_info($maintenance_id,$date_info){
			  $date1=date($date_info['from_date']);
			 $date2=date($date_info['to_date']);
			 $date_diff=DateDiff($date1,$date2);
			for($i=0;$i<=$date_diff;$i++){
			 $date=date('Y-m-d', strtotime($date1. ' + '.$i.' days'));
			 $room_num_data4 = array(															
									'maintenance_id'=>$maintenance_id,
									'date'=>$date												
								);
			 $room_date_id=insertValue('room_maintenance_date',$room_num_data4);
			
			}
			$d1=strtotime($date1);
			$d2=strtotime($date2);
			return date("jS F Y",$d1) .' - '. date("jS F Y",$d2);
	}
			function getmaintenanceRoom($hotel_id){
				$row=array();
				$sql="SELECT rm.maintenance_id,rm.comment,rim.room_id,rnm.id,rnm.room_number from room_maintenance as rm
				join room_id_maintenance as rim on rm.maintenance_id=rim.maintenance_id
				join room_no_maintenance as rnm on rnm.room_maintenance_id=rim.room_maintenance_id
				where rm.hotel_id=$hotel_id";
			$query = $this->db->query($sql);
				if($query->num_rows() > 0){
					foreach($query->result_array() as $val){
						$row[]=	$val;
					}
				}
			return $row;
			}
			function getdateinfo($maintenance_id){
			$sql="SELECT MIN(date) as mi,max(date) as mx FROM `room_maintenance_date` WHERE `maintenance_id`=$maintenance_id";
				$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				foreach($query->result_array() as $val){
					$mi=strtotime($val['mi']);
					$mx=strtotime($val['mx']);
					return  date("jS F Y",$mi) .' - '. date("jS F Y",$mx);
				}
				}
			}
			function check($data){
		
		$from_date=dateformate($data['from_date'],"Y-m-d",$simbol="-");
		$to_date=dateformate($data['to_date'],"Y-m-d",$simbol="-");
		$hotel_id=$data['hotel_id'];
		 foreach($data['room_id'] as $key=>$value){
						 foreach($data['room_number'][$value] as $key=>$value){
							$rm[]=$value;
						 }
			 }
			$room_numbers=implode(",",$rm);
			$sql="SELECT room_number FROM `bookings` as b 
							join booked_room_detail as brd on b.booking_id=brd.booking_id 
							join booked_room as br on br.booked_room_id=brd.booked_room_id
							where br.room_number IN ($room_numbers)
							and b.checkout_date >= '$to_date' 
							and b.checkin_date <= '$to_date'
							and b.checkout_date >= '$from_date'
							and b.checkin_date <= '$from_date'
							and b.`hotel_id`=$hotel_id";
				$query = $this->db->query($sql);
					if($query->num_rows() > 0){
						foreach($query->result_array() as $key=>$val){
							
							$row[]=$val['room_number'];
						}
					}
				
				return $room_number=implode(",",$row);
			}
}
?>