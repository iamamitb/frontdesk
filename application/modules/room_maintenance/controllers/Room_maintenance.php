<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Room_maintenance extends MX_Controller {
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('room_maintenance_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in();
		is_privileges();
  		 }
	public function index()
	{
		$this->template->title('Room maintenance','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('room_maintenance');
	}
	public function roominfo(){
		$html['item']="";
		$hotel_id= $this->input->post('hotel_id');
		if(!empty($hotel_id)){
			$html['type']='sucess';
			$RoomInfo=$this->room_maintenance_model->getRoomInfo($hotel_id);
			$html['item'].='';
			foreach($RoomInfo as $key=>$value) {
					$html['item'].='<div class="col-md-3">
					<div class="panel panel-border panel-primary">
	                    <div class="panel-heading"> 
	                        <h3 class="panel-title">'.$value['room_name'].'</h3>
							<input type="hidden" name="room_id[]" value="'.$key.'"/>
	                    </div> 
	                    <div class="panel-body">'; 
						foreach($value['room_number'] as $key1=>$value1) {
	                    	$html['item'].='<div class="checkbox checkbox-success">
	                            <input id="checkbox3" class="chk" name="room_number['.$key.'][]" type="checkbox" value="'.$value1.'">
	                            <label for="checkbox3">
	                               '.$value1.'
	                            </label>
	                        </div>';
						}
	                    $html['item'].='</div> 
	                </div>
            	</div>';
			}
			$html['item'].='';
			}
			echo json_encode($html);
			die();
	}
	function addroommaintenance()
	{
		$date_range = explode(' - ',$this->input->post('date'));
		//print_r($date_range);
		$date_info['from_date'] =$date_range['0'];
		$date_info['to_date']=$date_range['1'];
	
			$maintenanceinfo['comment'] = ($this->input->post('comment'));
			$maintenanceinfo['hotel_id'] = ($this->input->post('hotel_id'));
			$roomid['room_id'] = ($this->input->post('room_id'));
			$roomid['room_number'] = ($this->input->post('room_number'));
			//print_r($roomid);
			//die();

			$maintenance_id=insertValue('room_maintenance',$maintenanceinfo);
			//echo $maintenance_id;
			//die();
			$rm=$this->room_maintenance_model->roomid($maintenance_id,$roomid);
			$dt=$this->room_maintenance_model->date_info($maintenance_id,$date_info);
			$room_no = implode(",", $rm);			
			$html['room_no']=$room_no;
			$html['dt']=$dt;
			echo json_encode($html);
			/*
			$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
											Room Number <b>'.$room_no. '</b> has been added to maintenance for <b>' .$dt.' </b></div>');
			//redirect("room_maintenance"); 	*/
	}
	function getmaintenanceRoom(){
		$html['item']="";
		$hotel_id = ($this->input->post('hotel_id'));
			if(!empty($hotel_id)){
			$hotel_name= getValue('hotels','hotel_name','hotel_id = '.$hotel_id );
			$RoomInfo=$this->room_maintenance_model->getmaintenanceRoom($hotel_id);
				if (!empty($RoomInfo)){
				$i=1;
				 $html['item'].="<thead>
								<tr>
									<th>Sl No</th>
									<th>Hotel Name</th>
									 <th>Room Type</th>
									<th>Room Number</th>
									<th>Maintenance Date</th>
									<th>Reason</th>
									<th>Manage</th>
								</tr>
							</thead> <tbody>";
						foreach($RoomInfo as $key=>$value) {
							$date=$this->room_maintenance_model->getdateinfo($value['maintenance_id']);
							 $room_name= trim(getValue('rooms','room_name','room_id = '.$value['room_id'] ));
							//$room_name1 = ($room_name1==$room_name) ? "" : $room_name;
							$room_name_ = ($room_name1==$room_name) ? "" : $room_name;
							$room_name1=$room_name;
									$html['item'].='<tr>
											<td>'.$i.'</td>
											<td>'.$hotel_name.'</td>
											<td>'.$room_name_.'</td>
											<td>'.$value['room_number'].'</td>
											<td>'.$date.'</td>  
											<td>'.$value['comment'].'</td> 
											<td><a href="javascript:void(0);" onclick="confirmDelete('.$value['id'].')">Remove</a></td>              
										</tr>';
										
								//$room_name = ($room_name==$room_name) ? "" : $room_name;
								$hotel_name = ($hotel_name==$hotel_name) ? "" : $hotel_name;
										$i++;
						}
				 $html['item'].="</tbody>";
				}
				else{
						$html['item']='<div class="alert alert-danger alert-dismissable" style="text-align:center;">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										You have not added any room to Maintenance yet</div>';
				}
			}
		echo json_encode($html);
			die();
	}
	function room_maintenanceDelete(){
		$id=$_POST['id'];		
		Delete_data('room_no_maintenance','id = '.$id);
		$html['type']='sucess';
		echo json_encode($html);
	}
	function check(){
		$data['hotel_id']=$this->input->post('hotel_id');
		$date_range = explode(' - ',$this->input->post('date'));
		$data['from_date'] =$date_range['0'];
		$data['to_date']=$date_range['1'];
		$data['room_id'] = $this->input->post('room_id');
		$data['room_number'] = $this->input->post('room_number');		
		$html['item']=$this->room_maintenance_model->check($data);
		$html['type']='sucess';
		echo json_encode($html);
	
	}
}