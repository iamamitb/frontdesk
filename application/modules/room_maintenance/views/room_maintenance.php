<link href="<?=base_url()?>themes/css/views/room_maintenance.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>themes/plugins/daterangepicker/css/daterangepicker.css" />
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Room Maintenance</h1></div>
</div>
<div class="row">
 <?=$this->session->flashdata("msg"); ?></div>
</div>
   <input type="hidden" id="no_of_hotels_hidden" value="<?=count_rows("hotels", "account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'")?>"> 
   
<!-- Page-Title Ends-->
<!-- Page content starts -->  
<div class="row">
	<ul class="nav nav-tabs navtab-bg"> 
        <li class="active"> 
            <a href="#maintenance" data-toggle="tab" aria-expanded="false"> 
                <span class="visible-xs"><i class="fa fa-home"></i></span> 
                <span class="hidden-xs">Maintenance</span> 
            </a> 
        </li> 
        <li class=""> 
            <a href="#view" data-toggle="tab" aria-expanded="true"> 
                <span class="visible-xs"><i class="fa fa-user"></i></span> 
                <span class="hidden-xs">View</span> 
            </a> 
        </li> 
    </ul> 
    <div class="tab-content"> 
		<div class="tab-pane active" id="maintenance">
       <div class="row">
       <form  method="post" id="roommaintenanceform">
          <div class="col-md-12">
            <div class="col-md-4" style="margin-left:40%;width:20%;margin-bottom:25px;">
              <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'",'0','hotel_id','hotel_id','onchange="getroom()"', "hotel_name"); ?>
              </div>
              </div>
              <div id="show">
              <div class="col-md-12">
					<div class="row">
                <div id="maintenance-div" >
                   </div>  
                  </div>
                  <div class="col-md-4">
						<div class="form-group">
                            <label for="">From Date<span class="error">*</span></label>
                            <input type="text" id="dt_new" class="form-control daterange" name="date"  placeholder="" required="required" />
                        </div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
                            <label for="">Reason</label>
                            <input type="text" name="comment" class="form-control" id="" placeholder="">
                        </div>
				</div>
					</div><div class="row" align="center">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" id="sbmt" >Submit Room For Maintenance</button>
                    </div>
                </form>
                </div>
       </div>
		</div>
		<div class="tab-pane" id="view"> 
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4" style="margin-left:40%;width:20%;margin-bottom:25px;">
						 <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'",'0','hotel_id1','hotel_id','onclick="getroominfo()"', "hotel_name"); ?>
	                </div>
	            </div>
			</div>
			<div class="row">
				<table class="table table-bordered" id="data-list">
                </table>
			</div>
		</div>
	</div>
</div>
 <!-- End row -->
<!-- Page content ends -->  
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?=base_url()?>themes/plugins/daterangepicker/js/moment.min.js"></script>
<script src="<?=base_url()?>themes/plugins/daterangepicker/js/daterangepicker.js"></script>
<script src="<?=base_url()?>themes/js/views/room_maintenance.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
