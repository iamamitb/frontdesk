<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('customer_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index()
	{
		
		$search = array();
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		$account_id=$this->session->userdata('account_id');
		if($action=='filter')
		$search=$_POST['filter'];
		$data['allCustomers'] = $this->customer_report_model->getCustomers($account_id,$search,$csv);

		$this->template->title('Customer Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('customer_report',$data);
	}

	public function print_report(){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
		$search=$_POST['filter'];
		$data['allCustomers']=$this->customer_report_model->getCustomers($account_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}


	public function editCustomer($custID) { 
		$customer=getSingle('guest_detail','guest_id = '.$custID,'','','','');
        
		$html['item']='<div class="modal-dialog"> 
        <div class="modal-content">
			<form method="post" action="'.base_url().'customer_report/updateCustomer">
			<input type="hidden" id="customerID" name="customerID" value="'.$customer->guest_id.'"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">Edit Customer Details</h4> 
            </div> 
            <div class="modal-body"> 
                <div class="row"> 
                    <div class="col-md-4"> 
                        <div class="form-group"> 
                            <label for="field-4" class="control-label">Name</label> 
                            <input type="text" id="customerName" name="customerName" value="'.$customer->name.'" class="form-control" required> 
                        </div> 
                    </div> 
                    <div class="col-md-4"> 
                        <div class="form-group"> 
                            <label for="field-5" class="control-label">Mobile Number</label> 
                            <input type="text" id="customerMobile" name="customerMobile" value="'.$customer->phone.'" class="form-control" required> 
                        </div> 
                    </div> 
                    <div class="col-md-4"> 
                        <div class="form-group"> 
                            <label for="field-6" class="control-label">Email address</label> 
                            <input type="email" id="customerEmail" name="customerEmail" value="'.$customer->email.'" class="form-control" required> 
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="form-group no-margin"> 
                            <label for="field-7" class="control-label">Address</label> 
                            <textarea class="form-control autogrow" id="customerAddress" name="customerAddress" placeholder="Write something about yourself" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px">'.$customer->address.'</textarea> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <button type="submit" id="editCust" class="btn btn-success">Update changes</button> 
            </div>
		</form>
        </div> 
    </div>';
		
		echo json_encode($html);

	}

	public function updateCustomer() { 
		$account_id=$this->session->userdata('account_id');
		$customerID=$this->input->post('customerID');
  		$customerName=$this->input->post('customerName');
  		$customerMobile=$this->input->post('customerMobile');
  		$customerEmail=$this->input->post('customerEmail');
  		$customerAddress=$this->input->post('customerAddress');
  		
  		$data=array('name'=>$customerName,
  					'address'=>$customerAddress,
  					'phone'=>$customerMobile,
  					'email'=>$customerEmail
					);	
  		$data['customerupdt'] = $this->customer_report_model->updateCustomer($data, $customerID);
		  $this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the customer information.
            </div>');	
                          
  		redirect('customer_report');

	}





	

}
