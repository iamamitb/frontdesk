<link href="<?=base_url()?>themes/css/views/customer_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Customer Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  

<!-- Panel Starts -->
<div class="row">
	<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Customer Report for your Account</h3>
    </div>
    <?=$this->session->flashdata("msg"); ?> 
    <div class="panel-body">
    <!-- Row for Custom Filter Starts -->	
        <?php if(isset($allCustomers) && count($allCustomers)>0){ ?>
    	<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
    		<div class="col-md-12">

    			<form action="" method="post" id="customerReportFrom" >
                <input name="action" type="hidden" value="filter" />
                <input name="csv" type="hidden" id="csv" value="" />
    			<div class="col-md-2">
        			<div class="form-group">
                    <label for="exampleInputEmail1">Mobile Number</label>
                    <input type="text" class="form-control" id="mibileNumber" value="<?php echo $mobile=(isset($_POST['filter']['g']['equal']['phone']))? $_POST['filter']['g']['equal']['phone'] : "" ;?>" name="filter[g][equal][phone]" placeholder="">
                	</div>
            	</div>

            	<div class="col-md-2">
                	<div class="form-group">
                    <label for="exampleInputEmail1">Customer Name</label>
                    <input type="text" class="form-control" id="customerName" value="<?php echo $cName=(isset($_POST['filter']['g']['equal']['name']))? $_POST['filter']['g']['equal']['name'] : "" ;?>" name="filter[g][equal][name]" placeholder="">
                	</div>
            	</div>

            	

            	<div class="col-md-2" style="top:25px;">
                	<div class="form-group">
                    <!-- <label for="exampleInputEmail1">Filter</label> -->
					<button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                	</div>
            	</div>
            </form>
    		</div>
    	</div>
        <?php } ?>
    	<!-- Row for Custom Filter Ends -->

    	<!-- Row for Table Starts -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php  
                    $c=0;                                                                                                             
                    if(isset($allCustomers) && count($allCustomers)>0){
                    ?> 
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Sl No.</th>
                            <th>Customer Name</th>
                            <th>Customer Address</th>
                            <th>Mobile Number</th>
                            <th>Email address</th>
                            <th>Edit Details</th>
                            
                        </tr>
                    </thead>
                    
                    <tbody>

                        <?php 
                        foreach($allCustomers as $customer):{
                        $c++;    
                         ?> 
                    	<tr id="<?php echo $customer->guest_id;?>">
                            <td><?php echo $c; ?></td>
                            <td><?php echo $customer->name; ?></td>
                            <td><?php echo $customer->address; ?></td>
                            <td><?php echo $customer->phone; ?></td>
                            <td><?php echo $customer->email; ?></td>
                            <td><a href="javascript:void(0)" onclick="editCustomer(<?php echo $customer->guest_id;?>)" data-toggle="modal" data-target="#customer-info-modal">Edit</a></td>
                        </tr>
                        <?php } endforeach;  ?>

                    </tbody>
                </table>

            </div>
        </div>
       <!-- Row for Table ends -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->

<div class="col-md-12" style="margin-top:25px;">
    <div align="center">
       <button id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No data found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>

<div id="customer-info-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">

</div><!-- /.modal -->


<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/js/views/customer_report.js"></script> 

<script type="text/javascript">

 $("#downloadReport").click(function() {
     $( "#csv" ).val(1);
     $( "#customerReportFrom" ).submit();
     $( "#csv" ).val('');
});

$("#printReport").click(function() {
    $( "#customerReportFrom" ).attr("target","_blank");
    $( "#customerReportFrom" ).attr("action","<?=base_url()?>customer_report/print_report");
    $( "#customerReportFrom" ).submit();
    $( "#customerReportFrom" ).removeAttr("target");
    $( "#customerReportFrom" ).attr("action"," ");
});


function editCustomer(custID)
{
    var custID = custID;
    //alert(custID);
   
   
    $.ajax({
        url:"<?php echo site_url('customer_report/editCustomer');?>/"+custID,
        type: 'POST',
        dataType: "json",
        data: {'custID': custID},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#customer-info-modal').html(data.item);
            
        }      
    });
}

$('#editCust').click(function(){

   var customerID = $("#customerID").val();
   //alert(customerID); 
   
    $.ajax({
        url:"<?php base_url() ?>customer_report/updateCustomer",
        type: 'POST',
        dataType: "json",
        data: {customerID : customerID},
        success:function(data)
        { 
          //alert(data); 
        }      
    });

});


</script>