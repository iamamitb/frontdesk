<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Customer Report</h1>
</div>

<div class="row">
	<table class="table table-bordered">
        <thead>
            <tr>
                <th>Sl No.</th>
                <th>Customer Name</th>
                <th>Customer Address</th>
                <th>Mobile Number</th>
                <th>Email address</th>
            </tr>
        </thead>
        <tbody>
            
            <?php 
            $c=0;
            if(count($allCustomers)>0):
                foreach($allCustomers as $customer):
                $c++;    
             ?> 
            <tr>
                <td><?php echo $c; ?></td>
                <td><?php echo $customer->name; ?></td>
                <td><?php echo $customer->address; ?></td>
                <td><?php echo $customer->phone; ?></td>
                <td><?php echo $customer->email; ?></td>
            </tr>
            <?php
             endforeach;
             endif;  
             ?> 
            
        </tbody>
    </table>

</div>