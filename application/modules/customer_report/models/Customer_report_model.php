<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_report_model extends CI_Model {

	public function getCustomers($account_id,$search=array(),$csv=0){
			
			$row=false;
			$sql="SELECT g.* FROM `guest_detail` as g 
					left JOIN `bookings` as b ON b.guest_id=g.guest_id
					left JOIN `hotels` as h ON b.hotel_id=h.hotel_id
					WHERE h.account_id=$account_id ";

			
			$where = array();
			if(!empty($search))
			{
			foreach($search as $db=>$dbarray)
			{
				foreach($dbarray as $type=>$typearray)
				{
					foreach($typearray as $newkey=>$val)
					{
						if($val){
						  if($type == 'like')
						  {
							  $where[] = $db.".".$newkey." like '%".$val."%' ";
						  }
						  elseif($type == 'from')
						  {
							  $where[] = $db.".".$newkey." >= '".$val."'";
						  }
						  elseif($type == 'to')
						  {
							  $where[] = $db.".".$newkey." <= '".$val."'";
						  }
						   elseif($type == 'from1')
							{		$datetime = strtotime($val);
							$d1 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." >= '".$d1."'";
						  }
						  elseif($type == 'to1')
						  {	$datetime = strtotime($val);
							$d2 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." <= '".$d2."'";
						  }
						  elseif($type == 'equal1')
						  {	$datetime = strtotime($val);
							$d2 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." = '".$d2."'";
						  }
						  elseif($type == 'equal')
						  {
							  $where[] = $db.".".$newkey." = '".$val."'";
						  }
						}
					}
				}
			
			}
			}

			if(count($where))
			{
				$sql.="and ".implode(' and ',$where);
			}

			$sql.=" GROUP BY g.guest_id";

			$query = $this->db->query($sql);

			if($csv!=0){
			download_report($query,"customer_report.csv");		 
			}
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $val)
				{
					$row[]=	$val;
				}
				return $row;
			}

		}






		public  function updateCustomer($data, $customerID){ 	 
	 	 $this->db->where('guest_id', $customerID); 
	 	 $this->db->update('guest_detail', $data);  
		 return;
		}

		
	
}
?>