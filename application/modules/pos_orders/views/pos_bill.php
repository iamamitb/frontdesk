<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title"><b>Bill</b></h1><p style="float:right;margin-right:20px;font-size:20px;font-weight:bold;">Bill Number<span class="badge badge-danger" style="font-size:20px;padding:10px;margin-left:10px;">32</span></p>
</div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">


    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#dine_in" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">Dine In</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#take_away" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Take Away</span> 
                </a> 
            </li> 

            <li class=""> 
                <a href="#room_service" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Room Service</span> 
                </a> 
            </li>
        </ul> 

        <div class="tab-content"> 

            <div class="tab-pane active" id="dine_in"> 

                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Room Number</label>
                                <select class="form-control"><option>Select Room Number</option><option>202</option><option>205</option><option>301</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Table Number</label>
                                <select class="form-control"><option>Select Table Number</option><option>1</option><option>2</option><option>3</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Guest Name</label>
                                <input type="text" class="form-control" id="field-2" placeholder=""> 
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile Number</label>
                                <input type="text" class="form-control" id="field-2" placeholder=""> 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Chinese Noodles</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>Egg Poach</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>Ice Cream</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>
                                    
                                  
                                    
                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                    
                    <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-3">Service Tax %<br><span style="font-weight:bold;font-size:30px;float:left;">5</span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-3" style="text-align:right;">Total Payable <br><span style="font-weight:bold;font-size:30px;float:right;">Rs. 500.59</span></div>
                        </div>
                    </div>
            

                    <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>

            <div class="tab-pane" id="room_service"> 

                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Room Number</label>
                                <select class="form-control"><option>Select Room Number</option><option>202</option><option>205</option><option>301</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Table Number</label>
                                <select class="form-control"><option>Select Table Number</option><option>1</option><option>2</option><option>3</option></select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Guest Name</label>
                                <input type="text" class="form-control" id="field-2" placeholder=""> 
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile Number</label>
                                <input type="text" class="form-control" id="field-2" placeholder=""> 
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Chinese Noodles</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>Egg Poach</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>Ice Cream</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>
                                    
                                  
                                    
                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                    
                    <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-3">Service Tax %<br><span style="font-weight:bold;font-size:30px;float:left;">5</span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-3" style="text-align:right;">Total Payable <br><span style="font-weight:bold;font-size:30px;float:right;">Rs. 500.59</span></div>
                        </div>
                    </div>
            

                    <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>

             <div class="tab-pane" id="take_away"> 

                <div class="row" style="margin-bottom: 20px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                    <div class="col-md-12">
                        
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Token Number</label>
                                <input type="text" class="form-control" id="field-2" placeholder=""> 
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="exampleInputEmail1"><b>Recent Tokens</b></label><br>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">15</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">42</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">9</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">21</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">37</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">24</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">17</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">11</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">3</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">23</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">27</span>
                                <span class="badge badge-primary" style="padding:10px;font-size:15px;cursor:pointer;">37</span>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>

                <!-- Row for Table Starts -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Sl.No</th>
                                        <th>Item Name</th>
                                        <th>Rate</th>
                                        <th>Quantity</th>
                                        <th>Vat %</th>
                                        <th>VAT total</th>
                                        <th>Amount</th>
                                        
                                    </tr>
                                </thead>

                         
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Chinese Noodles</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>Egg Poach</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>

                                    <tr>
                                        <td>3</td>
                                        <td>Ice Cream</td>
                                        <td>150</td>
                                        <td>3</td>
                                        <td>5</td>
                                        <td>200</td>
                                        <td>480</td>
                                    </tr>
                                    
                                  
                                    
                                    
                                </tbody>
                            </table>

                        </div>
                    </div>
                <!-- Row for Table ends  -->
                    
                    <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;">
                        <div class="col-md-12">
                            <div class="col-md-3">Service Tax %<br><span style="font-weight:bold;font-size:30px;float:left;">5</span></div>
                            <div class="col-md-3">Discount % <input type="text" class="form-control" id="field-2" placeholder="">  </div>
                            <div class="col-md-3">Discount Amount <input type="text" class="form-control" id="field-2" placeholder=""> </div>
                            <div class="col-md-3" style="text-align:right;">Total Payable <br><span style="font-weight:bold;font-size:30px;float:right;">Rs. 500.59</span></div>
                        </div>
                    </div>
            

                    <div class="col-md-12" style="margin-top:25px;text-align:center;"> <button class="btn btn-success btn-lg m-b-5">Print Bill</button></div>
            </div>
                
            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>