<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_store_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_store_master_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allRecord']=getData("pos_store","pos_store_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Store Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_store_master',$data);

				
	}

	public function addStore(){
		$pos_store_name=$this->input->post('pos_store_name'); 
		$pos_store_floor=$this->input->post('pos_store_floor');
		$pos_store_manager=$this->input->post('pos_store_manager');
		$hotel_id=$this->session->userdata('hotel_id');

		$data=array('hotel_id'=>$hotel_id,
					'pos_store_name'=>$pos_store_name,
					'pos_store_floor'=>$pos_store_floor,
					'pos_store_manager'=>$pos_store_manager,
					'pos_store_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('pos_store',array('pos_store_name' => $pos_store_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('pos_store_name',$pos_store_name);
			  $this->db->update('pos_store',$data);

			  $this->session->set_flashdata('chkStoreData','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Store name already exists.
            </div>');
			redirect('pos_store_master#');

		}else{  
			  $store_id=insertValue('pos_store',$data); 
		} 	
		
		
		$this->session->set_flashdata('addStoreData','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Store.
            </div>');
		
		
		redirect('pos_store_master#');
		
	}


	public function editStore($ids) { 
		$store=getSingle('pos_store','pos_store_id = '.$ids,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Store</h3>
			    </div>
		    <form method="post" id="store_form_edit" action="'.base_url().'pos_store_master/updateStore" >
			<input type="hidden" id="storeID" name="storeID" value="'.$store->pos_store_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Store Name <span class="error">*</span></label>
		              <input type="text" id="pos_store_name" name="pos_store_name" class="form-control" value="'.$store->pos_store_name.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Store Floor </label>
		              <input type="text" id="pos_store_floor" name="pos_store_floor" class="form-control error1" value="'.$store->pos_store_floor.'"  maxlength="12">
		              <span class="error_msg1" style="color:red"></span>
		            </div>
		          </div>
		        </div>

		        <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="field-1" class="control-label">Store Manager </label>
                                <input type="text" id="pos_store_manager" name="pos_store_manager" value="'.$store->pos_store_manager.'" class="form-control" >
                            </div>
                       </div>
                </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_store" id="edit_store" class="btn btn-success waves-effect waves-light">Update Store</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateStore() {
		$storeID=$this->input->post('storeID'); 
		$pos_store_name=$this->input->post('pos_store_name'); 
		$pos_store_floor=$this->input->post('pos_store_floor');
		$pos_store_manager=$this->input->post('pos_store_manager');
		
		$data=array(
					'pos_store_name'=>$pos_store_name,
					'pos_store_floor'=>$pos_store_floor,
					'pos_store_manager'=>$pos_store_manager,
					'pos_store_status'=>'1'
					);
		

  		$storeUpdate=updateDataCondition('pos_store',$data,'pos_store_id = '.$storeID);
                          
  		$this->session->set_flashdata('msgStoreUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the store.
            </div>');	
                          
  		redirect('pos_store_master#');

	}


	public function storeDelete() { 
		
		$store_id=$_POST['store_id'];		
	
		$data['pos_store_status']='0';			
		
		$store_delete=updateDataCondition('pos_store',$data,'pos_store_id = '.$store_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	






	

}
