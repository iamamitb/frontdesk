<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Store Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_material" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Store</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_material" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Store</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addStoreData"); ?>
            <?=$this->session->flashdata("chkStoreData"); ?> 
            <?=$this->session->flashdata("msgStoreUpdate"); ?> 
            <div class="tab-pane active" id="all_material"> 
                <!-- Row for Table Starts -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="font-weight:bold;margin-bottom:30px;">All Stores</h2>
                        <?php if(isset($allRecord) && count($allRecord)>0){ ?>
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Store Name</th>
                                    <th>Store Floor</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                     
                            <tbody>
                                <?php                                                                                                                
                                foreach($allRecord as $store):{
                                ?>
                                <tr>
                                    <td><?=$store->pos_store_name?></td>
                                    <td><?=$store->pos_store_floor?></td>
                                    <td>
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            
                                            <li><a href="#"  data-toggle="modal" onclick="editStore(<?=$store->pos_store_id?>)" data-target="#edit-store">Edit</a></li>
                                            <li><a onclick="storeDelete(<?=$store->pos_store_id?>)" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any Store.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>

                    </div>
                </div>
               <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_material"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Store</h2>
                <form name="store_form" method="post" id="store_form" action="<?php echo base_url(); ?>pos_store_master/addStore">
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Store Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="pos_store_name" name="pos_store_name" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Store Floor </label>
                                    <div class="col-md-9">
                                        <input type="text" id="pos_store_floor" name="pos_store_floor" class="form-control error" maxlength="4">
                                        <span class="error_msg" style="color:red;"></span>
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row" style="margin-top:30px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Store Manager </label>
                                    <div class="col-md-9">
                                        <input type="text" id="pos_store_manager" name="pos_store_manager" class="form-control" >
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Store</button>
                    </div>
                </div>
                </form>
                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<!-- modal --> 
<div id="edit-store" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


$(document).ready(function() {
    $("#store_form").validate({
         rules: {
            
            pos_store_name: {
                required: true,                     
                noSpace: true
            }
            
         },
         messages: {
            pos_store_name: {
                    required: "Please enter Store number"
                } 
         },
        
    });

});



$(document).on('keypress', '.error', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});


$(document).on('keypress', '.error1', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg1').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});


function editStore(ids)
{
    var ids = ids;
    //alert(ids);
   
    $.ajax({
        url:"<?php echo site_url('pos_store_master/editStore');?>/"+ids,
        type: 'POST',
        dataType: "json",
        data: {'ids': ids},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-store').html(data.item);
            
        }      
    });
}



$('#edit_store').click(function(){

   var storeID = $("#storeID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_store_master/updateStore",
        type: 'POST',
        dataType: "json",
        data: {storeID : storeID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});



function storeDelete(store_id) {
    //alert(store_id); 
    swal({
        title: "Are you sure?",
        text: "This Store will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_store_master/storeDelete",
            type: 'POST',
            dataType: "json",
            data: {'store_id': store_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}


</script>

