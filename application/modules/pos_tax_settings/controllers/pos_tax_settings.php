<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_tax_settings extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('Pos_tax_settings_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$hotel_id=$this->session->userdata('hotel_id');
		$data['taxSettings']=getSingle('pos_bill_design','hotel_id = '.$hotel_id,'','','','');
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Tax Settings','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_tax_settings',$data);

				
	}

	public function update_tax_settings() { 
		$pos_bill_design_id=$this->input->post('pos_bill_design_id');
		$vat=$this->input->post('vat');
		$alcholic_beverages_vat=$this->input->post('alcholic_beverages_vat');
		$service_tax=$this->input->post('service_tax');
		$service_charge=$this->input->post('service_charge');
  		$service_charge_for_room_service=$this->input->post('service_charge_for_room_service');
  		$vat_number=$this->input->post('vat_number');
  		$tin_number=$this->input->post('tin_number');
  		$kot_number=$this->input->post('kot_number');
  		
  		$data=array('pos_bill_vat_value'=>$vat,
  					'pos_bill_alcholic_beverages_vat'=>$alcholic_beverages_vat,
  					'pos_bill_service_tax_value'=>$service_tax,
  					'pos_bill_service_charge_value'=>$service_charge,
  					'pos_bill_room_service_charge_value'=>$service_charge_for_room_service,
  					'pos_bill_vat_number_value'=>$vat_number,
  					'pos_bill_tin_number_value'=>$tin_number,
  					'pos_bill_kot_number'=>$kot_number
					);	
  		$taxSettingsUpdate=updateDataCondition('pos_bill_design',$data,'pos_bill_design_id = '.$pos_bill_design_id);

		$this->session->set_flashdata('updttaxSettings','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the tax settings.
            </div>');	
                          
  		redirect('pos_tax_settings');

	}




	

}
