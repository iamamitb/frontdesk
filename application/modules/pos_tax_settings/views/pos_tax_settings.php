<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Tax Settings</h1></div>
</div>
<!-- Page-Title Ends-->
<?=$this->session->flashdata("updttaxSettings"); ?>
<!-- Page content starts -->  
<div class="row">
    <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Enter Tax Percentages</h3></div>
            <div class="panel-body">
                <?php
                if(isset($taxSettings)){ 
                 ?>
                <form name="tax_settings_form" method="post" id="tax_settings_form" action="<?php echo base_url(); ?>pos_tax_settings/update_tax_settings">
                <input type="hidden" id="pos_bill_design_id" name="pos_bill_design_id" value="<?=$taxSettings->pos_bill_design_id?>">     
                    <div class="form-group">
                        <label for="exampleInputEmail1">VAT % (Food & Non-alcholic beverages)</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_vat_value?>" id="vat" name="vat" maxlength="5">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">VAT % (Alcholic beverages)</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_alcholic_beverages_vat?>" id="alcholic_beverages_vat" name="alcholic_beverages_vat" maxlength="5">
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Service Tax %</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_service_tax_value?>" id="service_tax" name="service_tax" maxlength="5">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Service Charge %</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_service_charge_value?>" id="service_charge" name="service_charge" maxlength="5">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Service Charge % for Room Service</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_room_service_charge_value?>" id="service_charge_for_room_service" name="service_charge_for_room_service" maxlength="5">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">VAT Number (To be shown in Bill)</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_vat_number_value?>" id="vat_number" name="vat_number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">TIN Number (To be shown in Bill)</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_tin_number_value?>" id="tin_number" name="tin_number">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">KOT Number (To be shown in Bill)</label>
                        <input type="text" class="form-control" value="<?=$taxSettings->pos_bill_kot_number?>" id="kot_number" name="kot_number">
                    </div>
                    
                    <div align="center"><button type="submit" class="btn btn-success" style="">Save Changes</button></div>
                </form>
                <?php } ?>
            </div><!-- panel-body -->
        </div>

        </div>
        <div class="col-md-2"></div>

    </div>
                            
</div> <!-- End row -->
<!-- Page content ends -->  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>

<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?>



$(function()
{
    $("#tax_settings_form").validate(
      {
        rules: 
        {
          vat:{
            range:[0.01,100.99]
          },
          alcholic_beverages_vat:{
            range:[0.01,100.99]
          },
          service_tax:{
            range:[0.01,100.99]
          },
          service_charge:{
            range:[0.01,100.99]
          },
          service_charge_for_room_service:{
            range:[0.01,100.99]
          }
          
        }
      });   
});


</script>