<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Item Wise Sales</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Item Name </label>
                    <select class="form-control"><option>Select Item Name</option><option>Item 1</option><option>Item 2</option><option>Item 3</option></select>
                    </div>
                </div>
              

                <div class="col-md-2" style="top:25px;">
                    <div class="form-group">
                    <button class="btn btn-primary m-b-5">Filter Results</button>
                    </div>
                </div>




            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-bordered">
        <thead>
            <tr>
                <th>Date</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>

 
        <tbody>
            <tr>
                <td>23-02-2016</td>
                <td>Chinese Main Course</td>
                <td>5</td>
                <td>5600</td>
            </tr>

            <!-- <tr>
                <td colspan="3"><b>Total</b></td>
                <td>7890</td>
            </tr> -->
        </tbody>
    </table>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
