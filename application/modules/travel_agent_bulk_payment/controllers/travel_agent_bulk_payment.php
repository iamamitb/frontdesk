<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travel_agent_bulk_payment extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('travel_agent_bulk_payment_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index()
	{
		
		$account_id=$this->session->userdata('account_id');
		$data['countRecord']=count_rows('agents','account_id = '.$this->session->userdata('account_id')." and agent_status ='1' and agents_type ='1'");
		//echo $data['countRecord'];

		$this->template->title('Travel Agent Bulk Payment','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('travel_agent_bulk_payment',$data);

				
	}

	public function getTravelAgent(){
		$account_id=$this->session->userdata('account_id');
		$agent_id= $this->input->post('agent_id');
		$travelAgentBookingHistory=$this->travel_agent_bulk_payment_model->getBookingHistory($account_id,$agent_id);
		$cnt = 0;
		if(isset($agent_id)){
			$travelAgent=getSingle('agents','account_id = '.$account_id.' and agent_id ='.$agent_id,'','','','','');
			$html['item']='';
			$html['item'].='<div class="col-md-12" style="margin-bottom:50px;margin-top:30px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
        				<div class="col-md-1"></div>
        				<div class="col-md-2"><p><b>Travel Agent Name: </b><br>'.$travelAgent->agent_name.'</p></div>
        				<div class="col-md-2"><p><b>Address: </b><br>'.$travelAgent->agent_address.'</p></div>
        				<div class="col-md-2"><p><b>Contact Person: </b><br>'.$travelAgent->agent_contact_person.'</p></div>
        				<div class="col-md-2"><p><b>Phone Number: </b><br>'.$travelAgent->agent_phonenumber1.'</p></div>
        				<div class="col-md-2"><p><b>Email address: </b><br>'.$travelAgent->agent_email_address.'</p></div>
        				<div class="col-md-1"></div>
        			</div>
        			<div class="row" style="margin-left:10%;margin-right:10%;">
        			
        			<h3 class="panel-title" style="margin-bottom:20px;font-size:17px;">Travel Agent Booking History</h3>
        			<div class="table-responsive">';
        	$totalAmount=0; 
        	$totalTACAmount=0;	
        		
        	if(count($travelAgentBookingHistory)>0){	
			$html['item'].='<table class="table table-striped table-bordered table-hover dataTable">
							<thead>
								<tr><th>Serial Number</th><th>Guest Name</th><th>Booking Date</th><th>Booking Amount</th><th>TAC</th><th>Payable Amount</th></tr>
							</thead>
				            <tbody role="alert" aria-live="polite" aria-relevant="all">';
			$totalPaidByTA=0;	            
			foreach ($travelAgentBookingHistory as $key => $value1) {

			$cnt++;
			$travelAgentAmount=($value1->grand_total * $value1->agent_percentage)/100;
			$due=($travelAgentAmount - $value1->payment_amount);
			$totalAmount = $totalAmount + $value1->grand_total;	
			$totalTACAmount = $totalTACAmount + $travelAgentAmount;	
			$totalPaidByTA += $value1->payment_amount;	
			$html['item'].='<tr>
								<td>'.$cnt.'</td>
								<td>'.$value1->name.'</td>
								<td>'.date('d-M-Y', strtotime($value1->booking_date)).'</td>
								<td>'.$value1->grand_total.'</td>
								<td>'.$value1->agent_percentage.'</td>
								<td>'.$travelAgentAmount.'</td>
							</tr>';
					
			}

			$html['item'].='<tr>
	    					 <td colspan="4"><span style="float:right;"><b>Total: </b>'.$totalAmount.'</span></td>
	    					 <td colspan="6"><span style="float:right;"><b>TAC Total: </b>'.$totalTACAmount.'</span></td>
    						</tr>
    						<tr>
	    					 <td colspan="6"><span style="float:right;"><b>Total Paid By TA: </b>'.$totalPaidByTA.'</span></td>
    						</tr>
							</tbody>
						</table>';
			} else{
			$html['item'].='<table><tbody> 
			                <tr>
			                <th style="text-align:center;">No data found!</th>
			                </tr> 
			             </tbody> 
						</table>';	
						}					
								
		} 					
		echo json_encode($html);
	
	}	



	public function addPayment(){

		$date=$this->input->post('paymentDate');
		$paymentDate=dateformate($date,'Y-m-d',"-"); 
		$paymentDtl=$this->input->post('paymentDtl');
		$paymentAmt=$this->input->post('paymentAmt');

		$account_id=$this->session->userdata('account_id');
		$agent_id= $this->input->post('taID');

		$travelAgentBookingHistory=$this->travel_agent_bulk_payment_model->getBookingHistory($account_id,$agent_id);

		/*$remainingBalance=$paymentAmt;
		foreach($travelAgentBookingHistory as $bookingHistory){
			$bookingID=$bookingHistory->booking_id; 
			$dueAmt=($bookingHistory->grand_total - $bookingHistory->advance);
			if($remainingBalance!=0 && $remainingBalance>0 && $dueAmt>0 && $remainingBalance<=$dueAmt) 
			{
				$dueAmt=$remainingBalance; // then all value of remaining value go to $dueAmt variable 
				$remainingBalance=0; // overide the $remainingBalance to 0
				$this->travel_agent_bulk_payment_model->updatePayments($dueAmt,$bookingID); // update the booking table
			} else{
					
			    $remainingBalance-=$dueAmt; // deduction remaining to due amount
				$this->travel_agent_bulk_payment_model->updatePayments($dueAmt,$bookingID); // Update the booking table
					
			}
			if($remainingBalance==0) // if remaining balance is 0
			{
				break;	// break the loop
				
			}	

		}*/

		$bulkAmt=array(
						'agent_id'=>$agent_id,
						'payment_date'=>$paymentDate,
						'payment_details'=>$paymentDtl,
						'payment_amount'=>$paymentAmt
						);
								
								
		$this->travel_agent_bulk_payment_model->addBulkAmt($bulkAmt);	

		redirect('travel_agent_bulk_payment');
	}	







	

}
