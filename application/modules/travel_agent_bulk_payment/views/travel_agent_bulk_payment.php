<link href="<?=base_url()?>themes/css/views/travel_agent_bulk_payment.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Travel Agent Bulk Payment</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

	<div class="col-md-12">


		<div class="panel panel-default">
            <div class="panel-heading"> 
                <h3 class="panel-title">Travel Agent Bulk payment</h3> 
            </div> 
        
        	<div class="panel-body"> 
                
        		<div class="row">
	                <div class="form-group" style="margin-left:40%;width:20%;margin-bottom:25px;"> 
            		<?=get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id')." and agent_status ='1' and agents_type ='1'",'','agent_id','agent_id','onclick="getTravelAgent(this.value)"', "agent_name"); ?>        
	               	</div>
        		</div> <!-- End Row -->

        		<div id="travel_agent_list">
        		<div class="row"></div> <!-- End Row -->

        		
				</div>
        		<!-- Table ends -->


        		
        		</div>
        	</div>
        		<!--End Row -->
        		<?php if($countRecord>0) { ?>
        		<div class="row" style="margin-top:30px;margin-left:10%;"> 

        			<h3 class="panel-title" style="margin-bottom:20px;font-size:17px;">Payment Received From Travel Agent</h3>
        			<form method="post" action="<?php echo base_url(); ?>travel_agent_bulk_payment/addPayment">
        			<input type="hidden" value="" id="taID" name="taID">   
        			<div class="col-md-3">
	        			<div class="form-group">
	                        <label for="exampleInputEmail1">Date<span class="error">*</span></label>
	                        <input type="text" class="form-control" id="paymentDate" name="paymentDate" placeholder="" readonly required>
	                    </div>
                	</div>

                	<div class="col-md-3">
	                    <div class="form-group">
	                        <label for="exampleInputEmail1">Payment Details<span class="error">*</span></label>
	                        <input type="text" class="form-control" id="paymentDtl" name="paymentDtl" placeholder="" required>
	                    </div>
	                </div>

	                <div class="col-md-3">
	                     <div class="form-group">
	                        <label for="exampleInputEmail1">Amount Received<span class="error">*</span></label>
	                        <input type="text" class="form-control digit_only" id="paymentAmt" name="paymentAmt" placeholder="" required>
	                        <span class="error_msg" style="color:red"></span>
	                    </div>
                	</div>

                	<div class="col-md-3" style="top:25px;">
	                    <button type="submit" class="btn btn-default m-b-5">Enter Payment</button>
                	</div>
                </form>

        		</div> 
        		<?php } ?>
        		<!-- End of Row -->


        		</div>


            </div> 
        </div>

    </div>     

</div> <!-- End row -->
<!-- Page content ends --> 
<script src="<?=base_url()?>themes/js/jquery.min.js"></script> 
<script src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

$("#agent_id").change(function() {
	var agent_id=$("#agent_id").val();
	//alert(agent_id);
	$("#taID").val(agent_id);

	$.ajax({
		url: "<?=base_url()?>travel_agent_bulk_payment/getTravelAgent",
		type: 'POST',
		dataType: "json",
		data: {'agent_id': agent_id},  
		
		success: function(result) {

		$('#travel_agent_list').html(result.item);
	
		}
		
		});
	
	 });

 function getTravelAgent() {
	var agent_id=$("#agent_id").val();
	$("#taID").val(agent_id);
	$.ajax({
		url: "<?=base_url()?>travel_agent_bulk_payment/getTravelAgent",
		type: 'POST',
		dataType: "json",
		data: {'agent_id': agent_id},  
		
		success: function(result) {
		
		$('#travel_agent_list').html(result.item);
		
		}
		
		});
	
	 }



 $(document).ready(function(){
 
 	$("#paymentDate").datepicker({
      //maxDate: "02/26/2016"
	});


	<?php if($countRecord==0) { ?>
		
		swal({   
            title: "Alert!",   
            text: "Please Add Travel Agent First", 
            type: "error"    
        },
        function(){
    		window.location.href = '<?=base_url()?>travel_agent/';
		});   
	<?php } ?>
		
		
	
}); 



$(document).on('keypress', '.digit_only', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

        //display error message
        $('.error_msg').html("Digits Only").show().fadeOut("slow");
        return false;
    }
     
});


</script>