<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">


<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">VAT Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">From Date</label>
                    <input type="email" class="form-control" id="datepicker" placeholder="Enter Start Date">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">To Date</label>
                    <input type="email" class="form-control" id="datepicker2" placeholder="Enter End Date">
                    </div>
                </div>

                
                
              

                <div class="col-md-2" style="top:25px;">
                    <div class="form-group">
                    <button class="btn btn-primary m-b-5">Filter Results</button>
                    </div>
                </div>




            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Bill Number</th>
                    <th>Date</th>
                    <th>Total Amount</th>
                    <th>VAT</th>
                </tr>
            </thead>

     
            <tbody>
                <tr>
                    <td>678</td>
                    <td>23-02-2016</td>
                    <td>5000</td>
                    <td>400</td>
                    
                </tr>

                <tr>
                    <td></td><td></td>
                    <td><b>Total VAT</b></td>
                    <td>67</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Page content ends -->

<script src="../assets/js/jquery.min.js"></script>


<?php include('../include/pos-footer.php');?>