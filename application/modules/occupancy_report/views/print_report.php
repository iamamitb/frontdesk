<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Occupancy Report for <?php echo date("d-M-Y") ?></h1>
</div>

<div class="row" align="center">
<!-- <p><b>Filters: </b><span>Date: 23/02/2016 - 28/09/2016, Hotel: Anutri Beach Resort, Travel Agent: MakeMyTrip</span></p> -->
</div>

<div class="row">
	<table class="table table-bordered">
        <?php  
        $c=0;                                                                                                              
        if(isset($all_bookings) && count($all_bookings)>0){
        ?> 
        <thead>
            <tr>
                <th>Sl.No</th>
                <th>Hotel Name</th>
                <th>Booking ID</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Room Number</th>
                <th>Plan Type</th>
            </tr>
        </thead>
         <?php                                                                                                                
            foreach($all_bookings as $all_booking):{
            $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->travel_agent_id); 
            $c++;
            ?>  
        <tbody>
            
            <tr>
                <td><?=$c ?></td>
                <td><?=$all_booking->hotel_name ?></td>
                <td><?=$all_booking->booking_reference ?></td>
                <td><?=$all_booking->name ?></td>
                <td><?=$all_booking->phone ?></td>
                <td><?=$all_booking->room_number ?></td>
                <td><?=$all_booking->planTypName ?></td>
            </tr>
            <?php } ?>

            </tbody>
            <?php endforeach;  } else{  ?> 
            
         <tbody> 
            <tr >
            <th style="text-align:center;">No data found !</th>
            </tr> 
         </tbody> 
         <?php } ?>
    </table>

</div>