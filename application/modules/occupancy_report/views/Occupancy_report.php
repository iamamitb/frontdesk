<link href="<?=base_url()?>themes/css/views/occupancy_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Occupancy Report</h1></div>
</div>
<!-- Page-Title Ends-->


<!-- Page content starts -->  
<div class="row">
	<div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h3 class="panel-title">Occupancy Report for Today (20/7/2016)</h3>
                                    </div> -->

    <div class="panel-body">
	<!-- Row for Table Starts -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form action="" method="post" id="reportFrom" >
                <input name="action" type="hidden" value="filter" />
                <input name="csv" type="hidden" id="csv" value="" />    
                
                    <?php  
                    $c=0;                                                                                                         //print_r($all_bookings); die();     
                    if(isset($all_bookings) && count($all_bookings)>0){
                    ?>
                    <table id="datatable" class="table table-striped table-bordered"> 
                    <thead>
                        <tr>
                            <th>Sl.No</th>
                            <th>Hotel Name</th>
                            <th>Booking ID</th>
                            <th>Customer Name</th>
                            <th>Phone Number</th>
                            <th>Room Number</th>
                            <th>Plan Type</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php                                                                                                                
                        foreach($all_bookings as $all_booking):{
                            $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->travel_agent_id); 
                            $c++;
                        ?>  
                        <tr id="<?php echo $all_booking->booking_id;?>">
                            <td><?=$c ?></td>
                            <td><?=$all_booking->hotel_name ?></td>
                            <td><?=$all_booking->booking_reference ?></td>
                            <td><?=$all_booking->name ?></td>
                            <td><?=$all_booking->phone ?></td>
                            <td><?=$all_booking->room_number ?></td>
                            <td><?=$all_booking->planTypName ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">Action <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a onclick="alertBox(<?php echo $all_booking->booking_id;?>)" href="javascript:void(0)"><i class="fa fa-bed"></i><span class="link_text">Change Room</span></a></li>
                                        <li><a onclick="alertBox(<?php echo $all_booking->booking_id;?>)" href="javascript:void(0)"><i class="fa fa-calendar"></i><span class="link_text">Extend Booking</span></a></li>
                                        <li><a onclick="alertBoxCheckout(<?php echo $all_booking->booking_id;?>)" href="javascript:void(0)"><i class="fa fa-shopping-cart"></i><span class="link_text">Checkout</span></a></li>
                                        
                                    </ul>
                                </div>

                            </td>
                        </tr>
                        <?php } endforeach;  ?>

					</tbody>
                </table>
            </form>
            </div>
        </div>
       <!-- Row for Table ends Starts -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->

<div class="col-md-12">
    <div align="center">
        <button type="submit" id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button type="submit" id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>

<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">Today, you have no Occupancy.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>


<!-- Change Room Modal -->
<div class="modal fade change-room-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Change Room</h4>
            </div>
            <div class="modal-body">
                
                <!-- Start Row for Date. Put this div into Loop to show for each day -->
                <div class="row">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">21st February 2016</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Current Room Type</th>
                                        <th>Current Room Number</th>
                                        <th>New Room Type</th>
                                        <th>New Room Number</th>
                                        <th>Sell Price</th>
                                        <th>Extra Bed</th>
                                        <th>Extra Bed Price</th>
                                        <th>Extra Person</th>
                                        <th>Extra Person Charge</th>
                                        <th>Luxury Tax</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>AC Suite</option>
                                                <option>AC Double</option>
                                                <option>NON AC Single</option>
                                                <option>Super Special</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>203</option>
                                                <option>204</option>
                                                <option>205</option>
                                                <option>206</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td> 
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>  
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>AC Suite</option>
                                                <option>AC Double</option>
                                                <option>NON AC Single</option>
                                                <option>Super Special</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>203</option>
                                                <option>204</option>
                                                <option>205</option>
                                                <option>206</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td> 
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>  
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
                <!-- End Row -->
                <!-- Start Row for Date. Put this div into Loop to show for each day -->
                <div class="row">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">22nd February 2016</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Current Room Type</th>
                                        <th>Current Room Number</th>
                                        <th>New Room Type</th>
                                        <th>New Room Number</th>
                                        <th>Sell Price</th>
                                        <th>Extra Bed</th>
                                        <th>Extra Bed Price</th>
                                        <th>Extra Person</th>
                                        <th>Extra Person Charge</th>
                                        <th>Luxury Tax</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>AC Suite</option>
                                                <option>AC Double</option>
                                                <option>NON AC Single</option>
                                                <option>Super Special</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>203</option>
                                                <option>204</option>
                                                <option>205</option>
                                                <option>206</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td> 
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>  
                                    </tr>
                                    <tr>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td><input type="text" class="form-control" id="" placeholder="" readonly="readonly"></td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>AC Suite</option>
                                                <option>AC Double</option>
                                                <option>NON AC Single</option>
                                                <option>Super Special</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control">
                                                <option>Select</option>
                                                <option>203</option>
                                                <option>204</option>
                                                <option>205</option>
                                                <option>206</option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>
                                        <td><input type="text" class="form-control" id="" placeholder=""></td> 
                                        <td><input type="text" class="form-control" id="" placeholder=""></td>  
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
                <!-- End Row -->

                <!-- Start pricing difference -->
                <div class="row" id="net_payable">
                    <div class="panel panel-border panel-danger">
                        <div class="panel-heading"> 
                                    <h3 class="panel-title">Price Difference</h3> 
                                </div>
                        <div class="panel-body">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <label>Original Room Tariff</label><br>
                                    <span class="net_payable">4100</span>
                                </div>
                                <div class="col-md-4">
                                    <label>New Room Tariff</label><br>
                                    <span class="net_payable">5400</span>
                                </div>
                                
                                <div class="col-md-4">
                                    <label><b>Difference</b></label><br>
                                    <span class="net_payable" style="color:#ef5350 !important;">9900</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End Pricing difference -->

                <div class="row" align="center">
                    <button class="btn btn-primary btn-lg m-b-5">Move Room</button>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Extend Booking Modal -->
<div class="modal fade extend-room-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-lg" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title" id="myLargeModalLabel">Extend Booking</h4>
            </div>
            <div class="modal-body">
                <div class="row" align="center" style="margin-bottom:30px;">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Days to be extended?</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" value="">
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-success">Go</button>
                        </div>
                    </div>
                </div>

                <!-- Repeat this row for each day -->
                <div class="row">
                    <div class="panel panel-border panel-primary">
                        <div class="panel-heading"> 
                            <h3 class="panel-title">1st Day - 25/03/2016</h3> 
                        </div> 
                        <div class="panel-body"> 
                            <div class="row">
                                <div class="col-md-5">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Current Room</th>
                                                <th>New Room Type</th>
                                                <th>New Room</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>AC Deluxe - 107</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>AC Suite</option>
                                                        <option>AC Special Suite</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>103</option>
                                                        <option>204</option>
                                                        <option>305</option>
                                                        <option>402</option>
                                                        <option>501</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>AC Deluxe - 107</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>AC Suite</option>
                                                        <option>AC Special Suite</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>103</option>
                                                        <option>204</option>
                                                        <option>305</option>
                                                        <option>402</option>
                                                        <option>501</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>AC Deluxe - 107</td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>AC Suite</option>
                                                        <option>AC Special Suite</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control">
                                                        <option>103</option>
                                                        <option>204</option>
                                                        <option>305</option>
                                                        <option>402</option>
                                                        <option>501</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-7">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Room Tariff</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extra Bed</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Bed Charge</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extra Person</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Extra Person Charge</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Luxury Tax</label>
                                            <input type="text" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <!-- End row -->

                <!-- Start pricing difference -->
                <div class="row" id="net_payable">
                    <div class="panel panel-border panel-danger">
                        <div class="panel-heading"> 
                                    <h3 class="panel-title">Price Difference</h3> 
                                </div>
                        <div class="panel-body">
                            
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <label>Original Room Tariff</label><br>
                                    <span class="net_payable">4100</span>
                                </div>
                                <div class="col-md-4">
                                    <label>New Room Tariff</label><br>
                                    <span class="net_payable">5400</span>
                                </div>
                                
                                <div class="col-md-4">
                                    <label><b>Difference</b></label><br>
                                    <span class="net_payable" style="color:#ef5350 !important;">9900</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End Pricing difference -->

                <div class="row" align="center">
                    <button class="btn btn-primary btn-lg m-b-5">Extend Room</button>
                </div>


            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Extend Booking Modal ends -->

<!-- Page content ends --> 
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 
<!-- <script src="<?=base_url()?>themes/js/views/occupancy_report.js"></script>  -->
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
 $("#downloadReport").click(function() {
     $( "#csv" ).val(1);
     $( "#reportFrom" ).submit();
     $( "#csv" ).val('');
});

$("#printReport").click(function() {
    $( "#reportFrom" ).attr("target","_blank");
    $( "#reportFrom" ).attr("action","<?=base_url()?>occupancy_report/print_report");
    $( "#reportFrom" ).submit();
    $( "#reportFrom" ).removeAttr("target");
    $( "#reportFrom" ).attr("action"," ");
});

function alertBox(booking_id) {
    //alert(booking_id); 
    swal({
        title: "Are you sure?",
        text: "You will be redirected to the Reservation Page for editing the booking",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Go to Edit Booking Page",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>reservation/edit_booking/"+booking_id,
            type: 'POST',
            dataType: "json",
            data: {'booking_id': booking_id},  
            
        success: function(msg) {
                swal("Done!", "Moving to booking edit page", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            /*error: function (xhr, ajaxOptions, thrownError) {
                swal("Error!", "Please try again", "error");
            }*/
        });
    });
}




function alertBoxCheckout(booking_id) {
    //alert(booking_id); 
    swal({
        title: "Are you sure?",
        text: "You will be redirected to the Checkout Page",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Go to Checkout Page",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>checkout/"+booking_id,
            type: 'POST',
            dataType: "json",
            data: {'booking_id': booking_id},  
            
        success: function(msg) {
                swal("Done!", "Moving to Checkout page", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            /*error: function (xhr, ajaxOptions, thrownError) {
                swal("Error!", "Please try again", "error");
            }*/
        });
    });
}




</script>