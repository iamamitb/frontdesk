<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Occupancy_report_model extends CI_Model {

	function getreport($account_id,$csv=0){

	 	$row=false;
		$sql="SELECT b.*,gd.name,gd.phone,h.hotel_name,bm.room_number,pt.name as planTypName 
					FROM bookings b LEFT JOIN guest_detail gd ON b.guest_id=gd.guest_id
					LEFT JOIN booked_room_detail brd ON b.booking_id=brd.booking_id
					LEFT JOIN booked_room bm ON bm.booked_room_id=brd.booked_room_id
					LEFT JOIN hotels h ON b.hotel_id=h.hotel_id
					LEFT JOIN plan_type pt ON b.plan_type_id=pt.plan_type_id
					WHERE b.booking_status=3 and h.account_id=$account_id "; 
					

		//$sql="SELECT * FROM cancel_master ";			
								
		/*if($booking_id!=0)
		{
			$sql.=" and b.booking_id=$booking_id ";
		}*/

		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}
		//$sql.=" GROUP BY b.booking_id";
		$query = $this->db->query($sql);
		
		if($csv!=0){
			download_report($query,"occupancy_report.csv");		 
		}
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}
			return $row;
		}
		
		
	}	
		
	
}
?>