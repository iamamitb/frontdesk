<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Occupancy_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('occupancy_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index()
	{
		
		$account_id=$this->session->userdata('account_id');
		$csv=trim($this->input->post('csv'));
		
		$this->template->title('Occupancy Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$data['all_bookings']=$this->occupancy_report_model->getreport($account_id,$csv);
		//print_r($data['all_bookings']); die();
		$this->template->build('occupancy_report',$data);

				
	}
	
	public function print_report(){
		$account_id=$this->session->userdata('account_id');
		
		$data['all_bookings']=$this->occupancy_report_model->getreport($account_id);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}

}
