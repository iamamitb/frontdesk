<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Current Material Stock</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Material Name </label>
                    <select class="form-control"><option>Select Material Name</option><option>Material 1</option><option>Material 2</option><option>Material 3</option></select>
                    </div>
                </div>
              

                <div class="col-md-2" style="top:25px;">
                    <div class="form-group">
                    <button class="btn btn-primary m-b-5">Filter Results</button>
                    </div>
                </div>




            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Material Name</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    
                </tr>
            </thead>

     
            <tbody>
                <tr>
                    
                    <td>Aata Moyda</td>
                    <td>5</td>
                    <td>56</td>
                </tr>

                
            </tbody>
        </table>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>



<script src="<?=base_url()?>themes/plugins/datatables/datatables.js"></script>