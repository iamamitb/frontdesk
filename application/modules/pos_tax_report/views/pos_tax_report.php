<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">


<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Tax Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">
                <form action="" method="post" id="reportFrom" >
                <input name="action" type="hidden" value="filter" />
                <input name="csv" type="hidden" id="csv" value="" />
                
                <div class="col-md-2">
                    <div class="form-group">
                     <label for="exampleInputEmail1">From Date </label>
                     <input type="text" class="form-control" id="from_date" value="<?php echo $fromDate=(isset($_POST['filter']['p']['from1']['purchase_date']))? $_POST['filter']['p']['from1']['purchase_date'] : "" ;?>" name="filter[p][from1][purchase_date]" placeholder="" readonly>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">To Date </label>
                    <input type="text" class="form-control" id="to_date" value="<?php echo $toDate=(isset($_POST['filter']['p']['to1']['purchase_date']))? $_POST['filter']['p']['to1']['purchase_date'] : "" ;?>" name="filter[p][to1][purchase_date]" placeholder="" readonly>
                    </div>
                </div>

                <div class="col-md-4" style="top:25px;">
                    <div class="form-group">
                    <button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                    <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
                    </div>
                </div>
                
                </form>

            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Bill Number</th>
                    <th>Date</th>
                    <th>Total Amount</th>
                    <th>VAT</th>
                </tr>
            </thead>

     
            <tbody>
                <tr>
                    <td>678</td>
                    <td>23-02-2016</td>
                    <td>5000</td>
                    <td>400</td>
                    
                </tr>

                <tr>
                    <td></td><td></td>
                    <td><b>Total VAT</b></td>
                    <td>67</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<?php include('../include/pos-footer.php');?>

<script type="text/javascript">
$("#to_date").datepicker({ dateFormat: 'dd-mm-yy'});
$("#from_date").datepicker({ dateFormat: 'dd-mm-yy'}).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to_date").datepicker( "option", "minDate", minValue );
        });


//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_date").val("");
    $("#to_date").val("");
    $('#supplier_id option:selected').removeAttr('selected');
    $('#material_id option:selected').removeAttr('selected');
    window.location.reload(true);
});

$(document).ready(function(){
    var fromdate=$("#from_date").val();
    var todate=$("#to_date").val();
    var supplier_id=$("#supplier_id").val();
    var material_id=$("#material_id").val();
    if(fromdate != '' || todate != ''){
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }
});
</script>