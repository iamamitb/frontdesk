
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Supplier Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_supplier" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Suppliers</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_supplier" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Supplier</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addsupplierdata"); ?>  
            <?=$this->session->flashdata("chksupplierdata"); ?> 
            <?=$this->session->flashdata("msgSupplierUpdate"); ?> 
            <div class="tab-pane active" id="all_supplier"> 
                <!-- Row for Table Starts -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="font-weight:bold;margin-bottom:30px;">All Suppliers</h2>
                        <table id="datatable" class="table table-striped table-bordered">
                            <?php 
                            if(isset($allSupplier) && count($allSupplier)>0){
                            ?>
                            <thead>
                                <tr>
                                    <th>Supplier Name</th>
                                    <th>Supplier Phone Number</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                     
                            <tbody>
                                <?php 
                                    foreach($allSupplier as $supplier):{
                                  ?>
                                <tr>
                                    <td><?=$supplier->supplier_name?></td>
                                    <td><?=$supplier->phone?></td>
                                    <td>
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            
                                            <li><a href="#" data-toggle="modal" onclick="editSupplier(<?=$supplier->supplier_id?>)"  data-target="#edit-supplier">Edit</a></li>
                                            <li><a onclick="supplierDelete(<?=$supplier->supplier_id?>)" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?> 
                               </tbody>
                               <?php endforeach;  } else{ ?>
                                <tbody> 
                                    <tr >
                                    <th style="text-align:center;">You have not added any Supplier Yet.</th>
                                    </tr> 
                                 </tbody> 
                                 <?php } ?>
                        </table>

                    </div>
                </div>
               <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_supplier"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Supplier</h2>
                <form name="supplier_form" method="post" id="supplier_form" action="<?php echo base_url(); ?>pos_supplier_master/addSupplier"> 
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Supplier Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="supplier_name" name="supplier_name" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email </label>
                                    <div class="col-md-9">
                                        <input type="email" id="email" name="email" class="form-control">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone Number <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="phone" name="phone" class="form-control digit_only" maxlength="12" required>
                                        <span class="error_msg" style="color:red"></span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Mobile Number</label>
                                    <div class="col-md-9">
                                        <input type="text" id="mobile" name="mobile" class="form-control digit_only1" maxlength="12">
                                        <span class="error_msg1" style="color:red"></span>
                                    </div>
                            </div>
                        </div>
                        

                        
                    </div>
                </div>


                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <input type="text" id="address" name="address" class="form-control">
                                    </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Post Code</label>
                                    <div class="col-md-9">
                                        <input type="text" id="post_code" name="post_code" class="form-control digit_only2" maxlength="10">
                                        <span class="error_msg2" style="color:red"></span>
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">City</label>
                                    <div class="col-md-9">
                                        <input type="text" id="city" name="city" class="form-control">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Contact Person Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="contact_name" name="contact_name" class="form-control" required>
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class-"col-md-12">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                    <div class="col-md-9">
                                        <input type="text" id="website" name="website" class="form-control">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button type="submit" class="btn btn-success btn-lg m-b-5">Add Supplier</button>
                    </div>
                </div>

                </form> 



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  

<!-- Add Supplier Modal -->
<div id="edit-supplier" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal -->
<!-- End of Supplier Modal -->                                        


<!-- Sweet Alert used for deleting confirmation -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script src="<?=base_url()?>themes/js/views/pos_supplier_master.js"></script>

<script>

<?php //if($wizard_status==0){ ?>
        /*swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });*/
<?php //} ?>





function editSupplier(ids)
{
    var ids = ids;
    //alert(ids);
   
    $.ajax({
        url:"<?php echo site_url('pos_supplier_master/editSupplier');?>/"+ids,
        type: 'POST',
        dataType: "json",
        data: {'ids': ids},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-supplier').html(data.item);
            
        }      
    });
}



$('#edit_supplier').click(function(){

   var supplierID = $("#supplierID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_supplier_master/updateSupplier",
        type: 'POST',
        dataType: "json",
        data: {supplierID : supplierID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});



function supplierDelete(supplier_id) {
    //alert(supplier_id); 
    swal({
        title: "Are you sure?",
        text: "This supplier will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_supplier_master/supplierDelete",
            type: 'POST',
            dataType: "json",
            data: {'supplier_id': supplier_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}

</script>