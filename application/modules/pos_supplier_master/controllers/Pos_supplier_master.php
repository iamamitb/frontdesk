<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_supplier_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_supplier_master_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		
		$data['allSupplier']=getData("supplier","supplier_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('POS Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_supplier_master',$data);

				
	}

	public function addSupplier(){
		$supplier_name=$this->input->post('supplier_name'); 
		$phone=$this->input->post('phone');
		$address=$this->input->post('address');
		$mobile=$this->input->post('mobile');
		$city=$this->input->post('city');
		$contact_name=$this->input->post('contact_name');
		$email=$this->input->post('email');
		$website=$this->input->post('website');
		$post_code=$this->input->post('post_code');
		$hotel_id=$this->session->userdata('hotel_id');

		$data=array('hotel_id'=>$hotel_id,
					'supplier_name'=>$supplier_name,
					'contact_person'=>$contact_name,
					'address'=>$address,
					'city'=>$city,
					'post_code'=>$post_code,
					'phone'=>$phone,
					'mobile'=>$mobile,
					'email'=>$email,
					'website'=>$website,
					'supplier_status'=>'1'
					);
		//print_r($data); die();
		$q = $this->db->get_where('supplier',array('supplier_name' => $supplier_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('supplier_name',$supplier_name);
			  $this->db->update('supplier',$data);

			  $this->session->set_flashdata('chksupplierdata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Supplier already exists.
            </div>');
			redirect('pos_supplier_master#');

		}else{ 
			  $supplier_id=insertValue('supplier',$data);
		} 	
		
		
		$this->session->set_flashdata('addsupplierdata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the supplier.
            </div>');
		
		
		redirect('pos_supplier_master#');
		
	}


	public function editSupplier($ids) { 
		$supplier=getSingle('supplier','supplier_id = '.$ids,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Supplier</h3>
			    </div>
		    <form method="post" action="'.base_url().'pos_supplier_master/updateSupplier">
			<input type="hidden" id="supplierID" name="supplierID" value="'.$supplier->supplier_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Supplier Name<span class="error">*</span></label>
		              <input type="text" id="supplier_name" name="supplier_name" class="form-control" value="'.$supplier->supplier_name.'" required>
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-2" class="control-label">Email</label>
		              <input type="email" id="email" name="email" class="form-control" value="'.$supplier->email.'" >
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Phone Number<span class="error">*</span></label>
		              <input type="text" id="phone" name="phone" class="form-control digit_only3" value="'.$supplier->phone.'" maxlength="12" required>
		              <span class="error_msg3" style="color:red"></span>
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-2" class="control-label">Mobile Number</label>
		              <input type="text" id="mobile" name="mobile" class="form-control digit_only4" value="'.$supplier->mobile.'" maxlength="12">
		              <span class="error_msg4" style="color:red"></span> </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Address</label>
		              <input type="text" id="address" name="address" class="form-control"  value="'.$supplier->address.'" >
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Post Code</label>
		              <input type="text" id="post_code" name="post_code" class="form-control digit_only5"  value="'.$supplier->post_code.'" maxlength="10">
		              <span class="error_msg5" style="color:red"></span>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">City</label>
		              <input type="text" id="city" name="city" class="form-control"  value="'.$supplier->city.'" >
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Contact Person Name <span class="error">*</span></label>
		              <input type="text" id="contact_name" name="contact_name" class="form-control"  value="'.$supplier->contact_person.'" required>
		            </div>
		          </div>
		        </div>
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Website</label>
		              <input type="text" id="website" name="website" class="form-control"  value="'.$supplier->website.'">
		            </div>
		          </div>
		        </div>



		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_supplier" id="edit_supplier" class="btn btn-success waves-effect waves-light">Update Supplier</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}


	public function updateSupplier() {
		$supplierID=$this->input->post('supplierID'); 
		$supplier_name=$this->input->post('supplier_name'); 
		$phone=$this->input->post('phone');
		$address=$this->input->post('address');
		$mobile=$this->input->post('mobile');
		$city=$this->input->post('city');
		$contact_name=$this->input->post('contact_name');
		$email=$this->input->post('email');
		$website=$this->input->post('website');
		$post_code=$this->input->post('post_code');
		
		$data=array(
					'supplier_name'=>$supplier_name,
					'contact_person'=>$contact_name,
					'address'=>$address,
					'city'=>$city,
					'post_code'=>$post_code,
					'phone'=>$phone,
					'mobile'=>$mobile,
					'email'=>$email,
					'website'=>$website,
					'supplier_status'=>'1'
					);
		
		/*echo "<pre>";
		print_r($data); die();*/
  		$supplierUpdate=updateDataCondition('supplier',$data,'supplier_id = '.$supplierID);
                          
  		$this->session->set_flashdata('msgSupplierUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the supplier.
            </div>');	
                          
  		redirect('pos_supplier_master#');

	}



	public function supplierDelete() { 
		
		$supplier_id=$_POST['supplier_id'];		
	
		$data['supplier_status']='0';			
		
		$supplier_delete=updateDataCondition('supplier',$data,'supplier_id = '.$supplier_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	



	

}
