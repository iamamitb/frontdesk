<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
		$this->load->model('reservation_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;		
		is_logged_in();
		is_privileges();
   		 }

	public function index()
	{
		$search = array();
		//$search['bo']['from1']['checkin_date']=date('d-m-Y');
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		if($action=='filter')
		$search=$_POST['filter'];
		$this->template->title('Reservation Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('reservation_report');
		$data['all_bookings']=$this->reservation_report_model->getreport($account_id,0,$search,$csv);
		$this->template->build('reservation_report',$data);	

	}
	
	public function print_report($booking_id=0){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
		$search=$_POST['filter'];
		$data['all_bookings']=$this->reservation_report_model->getreport($account_id,$booking_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}

}
