<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_report_model extends CI_Model {

	function getreport($account_id=0,$booking_id=0,$search=array(),$csv=0)
	{
	 	$row=false;
			 $sql="SELECT bo.booking_reference,bo.booking_id, hotel_name,name,agent_id,phone,booking_date,grand_total,advance,(grand_total-advance) as due,checkin_date,checkout_date,plan_type_id,GROUP_CONCAT(room_number) as room_number,GROUP_CONCAT(room_name) as room_name,COUNT(room_number) as no_of_room from hotels as h 
								LEFT JOIN `bookings` as bo on h.hotel_id=bo.hotel_id
								LEFT JOIN  `booked_room_detail` as brd on bo.booking_id=brd.booking_id
								LEFT JOIN `transaction` as t on bo.booking_id=t.booking_id
								LEFT JOIN  `booked_room` as br on brd.booked_room_id=br.booked_room_id
								LEFT JOIN `rooms` as r on r.room_id=br.room_id
								LEFT JOIN `guest_detail` as gd on bo.guest_id=gd.guest_id 
								where h.account_id=$account_id AND bo.booking_status NOT IN (5,6) ";
								
			if($booking_id!=0)
				{
					$sql.=" and bo.booking_id=$booking_id ";
				}
		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y-m-d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}
		$sql.=" GROUP BY brd.booking_id";
		//echo $sql;
		$query = $this->db->query($sql);
		
		if($csv!=0){
			download_report($query,"reservation_report.csv");		 
		}
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}
			return $row;
		}
		
		
	}	
	
}
?>