<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
    @page {size: landscape}
}
</style>
<!-- Style for Printing ends -->
<div id="print"  >
<div class="row" >
    <h1 align="center">Reservation Report</h1>
</div>
<?php if(!empty($_POST)){ ?>
<div class="row" align="center">
 
</div>
<?php } ?>

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Booking ID</th>
                <th>Mobile Number</th>
                <th>Guest Name</th>
                <th>Agency Name</th>
                <th>Hotel Name</th>
                <th>Net Total</th>
                <th>Deposit</th>
                <th>Due</th>
                <th>Booking Date</th>
                <th>Checkin Date</th>
                
            </tr>
        </thead>
        <tbody>
            
        <?php                                                                                                                
        if(count($all_bookings)>0){        
        foreach($all_bookings as $all_booking):        
        {
       $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 

        
        ?>
        <tr>
                <td><?=$all_booking->booking_reference ?></td>
                <td><?=$all_booking->phone ?></td>
                <td><?=$all_booking->name ?></td>                
                <td><?=$agentname=($agent_name!="")?$agent_name:"Direct Booking"; ?></td>
                <td><?=$all_booking->hotel_name ?></td>
                <td><?=$all_booking->grand_total?></td>
                <td><?=$all_booking->advance?></td>
                <td><?=$all_booking->due?></td>
                <td><?=$all_booking->booking_date?></td>
                <td><?=$all_booking->checkin_date?></td>
            </tr>
             <?php } endforeach;  } ?>    
            
        </tbody>
    </table>

</div>
</div>