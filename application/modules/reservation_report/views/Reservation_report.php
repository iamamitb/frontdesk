<link href="<?=base_url()?>themes/css/views/reservation_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Reservation Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<!-- Panel Starts -->
<div class="row">
    <div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">Reservation Report for your Account</h3>
</div>                                  
        
<div class="panel-body">
<!-- Row for Custom Filter Starts -->   
<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
  <form action="" method="post" id="my_from" >
  <input name="action" type="hidden" value="filter" />
  <input name="csv" type="hidden" id="csv" value="" />
    <div class="col-md-12">

        <div class="col-md-2">
            <div class="form-group">
    <label for="exampleInputEmail1">From Date</label>
    <input type="text" class="form-control" id="from_booking_date" value="<?php echo $dt=(isset($_POST['filter']['bo']['from1']['checkin_date']))? $_POST['filter']['bo']['from1']['checkin_date'] : "" ;?>" name="filter[bo][from1][checkin_date]" placeholder="" readonly="readonly">
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
            <label for="exampleInputEmail1">To Date</label>
            <input type="text" class="form-control" value="<?php echo $dt1=(isset($_POST['filter']['bo']['to1']['checkin_date']))? $_POST['filter']['bo']['to1']['checkin_date'] : "" ;?>" id="to_booking_date" name="filter[bo][to1][checkin_date]" placeholder=""  readonly="readonly">
            </div>
        </div>

        <div class="col-md-1">
            <div class="form-group">
            <label for="exampleInputEmail1">Booking ID</label>
            <input type="text" class="form-control" value="<?php echo $bi=(isset($_POST['filter']['bo']['equal']['booking_reference']))? $_POST['filter']['bo']['equal']['booking_reference'] : "" ;?>" name="filter[bo][equal][booking_reference]" id="exampleInputEmail1" placeholder="">
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
            <?php  $hi=(isset($_POST['filter']['bo']['equal']['hotel_id']))? $_POST['filter']['bo']['equal']['hotel_id'] : "" ;?>
            <label for="exampleInputEmail1">Hotel Name</label>
            <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id'),$hi,'hotel_id',"filter[bo][equal][hotel_id]"); ?> 
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
            <label for="exampleInputEmail1">Agent</label>
               <?php  $tai=(isset($_POST['filter']['bo']['equal']['agent_id']))? $_POST['filter']['bo']['equal']['agent_id'] : "" ;?>
            <?=get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id'),$tai,'agent_id',"filter[bo][equal][agent_id]"); ?>
            </div>
        </div>

        <div class="col-md-3" style="top:25px;">
            <div class="form-group">
            <!-- <label for="exampleInputEmail1">Filter</label> -->
            <button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
            <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- Row for Custom Filter Ends -->

    <!-- Row for Table Starts -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <?php                                                                                                                
            if(count($all_bookings)>0)
            { ?>
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Mobile Number</th>
                        <th>Guest Name</th>
                        <th>Agency Name</th>
                        <th>Hotel Name</th>
                        <th>Net Total</th>
                        <th>Deposit</th>
                        <th>Due</th>
                        <th>Booking Date</th>
                        <th>Checkin Date</th>
                        <th>Manage</th>
                    </tr>
                </thead>

         
                <tbody>
                    <?php 
          
            foreach($all_bookings as $all_booking): 
        
            {
                $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 

            ?>
                    <tr>
                        <td><?=$all_booking->booking_reference ?></td>
                        <td><?=$all_booking->phone ?></td>
                        <td><?=$all_booking->name ?></td>
                        <td><?=$agentname=($agent_name!="")?$agent_name:"Direct Booking"; ?></td>
                        <td><?=$all_booking->hotel_name ?></td>
                        <td><?=$all_booking->grand_total?></td>
                        <td><?=$all_booking->advance?></td>
                        <td><?=$all_booking->due?></td>
                        <td><?=dateformate($all_booking->booking_date,"d-M-Y","-")?></td>
                        <td><?=dateformate($all_booking->checkin_date,"d-M-Y","-")?></td>
                        <td>
                            <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?=base_url()?>reservation_report/print_report/<?=$all_booking->booking_id ?>" target="_blank">Print</a></li>
                                <li><a href="<?=base_url()?>reservation/edit_booking/<?=$all_booking->booking_id ?>">Edit</a></li>
                              <!--  <li><a href="#">Pay Due</a></li>
                                <li><a href="#">Pay Due History</a></li>
                                <li><a href="#">Change Status</a></li>-->
                                <li class="divider"></li>
                                <li><a href="<?=base_url()?>sales_report/cancel_booking/<?=$all_booking->booking_id?>">Cancel Booking</a></li>
                            </ul>
                            </div>
                        </td>
                    </tr>
                   <?php } endforeach;  ?>    
                </tbody>
            </table>

        </div>
    </div>
   <!-- Row for Table ends -->
</div>
<div class="col-md-12" style="margin-top:25px;">
    <div align="center">
         <button id="print" class="btn btn-default m-b-5">Print Report</button>
        <button id="download" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
                                    <?php  } 
                                    else{?>
                                     <table class="table table-bordered" style="margin-top:25px;">
                                        <tbody> 
                                            <tr>
                                            <th style="text-align:center;">No reservations or bookings found. Go to the <a target="_blank" href="<?=base_url()?>reservation">Reservation</a> page and make a booking first.</th>
                                            </tr> 
                                         </tbody> 
                                     </table>
                                    <?php }?>
                                </div>   
</div> <!-- End row -->
<!-- Panel ends -->


<!-- Page content ends -->  
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/js/views/reservation_report.js"></script> 
<!-- Page content ends -->  
<script src="<?=base_url()?>/themes/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>