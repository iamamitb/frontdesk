<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other_income_model extends CI_Model {
	function getroomno($date,$hotel_id){
		$newDate = date("Y-m-d", strtotime($date));
		$row=array();
		$sql="select br.room_number from bookings as b
		join booked_room_detail as brd on b.booking_id=brd.booking_id
		join booked_room as br on br.booked_room_id=brd.booked_room_id
		join booked_room_stay_date as brsd on brsd.booked_room=br.id
		where b.hotel_id=$hotel_id and brsd.stay_date='".$newDate."' group by br.room_number";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val->room_number;
			}
		}
		//echo $this->db->last_query();
		return $row;

	}
	function getiteminfo($item_id=""){
		$row=array();
		$sql="select unit_price,in_stock  from other_item as ot
				LEFT OUTER JOIN other_item_booked as oib 
				on ot.item_id=oib.item_id
				where ot.item_id=$item_id
				and oib.status!='0'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row['no_of_item']=	$val->no;
				$row['use']=$val->u;
				$row['price']=	$val->unit_price;
				$row['in_stock']=	$val->in_stock;
			}
		}

		return $row;
	}
	
	function order_list($hotel_id=""){
		$row=array();
		 $sql="select item_name,room_number,price,no_of_item,total_amount,remarks,date,item_booked_id,in_stock,ot.item_id from other_item as ot
			 LEFT OUTER JOIN other_item_booked as oib on ot.item_id=oib.item_id 
			 where oib.hotel_id = $hotel_id and oib.status!='0'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}
		}

		return $row;
	}
	
	function getbookingid($roomno,$hotel_id){
		$row=array();
		 $sql="select b.booking_id from bookings as b
				join booked_room_detail as brd on b.booking_id=brd.booking_id
				join booked_room as br on br.booked_room_id=brd.booked_room_id
				where br.room_number and  b.hotel_id=$hotel_id and  br.room_number=$roomno";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row['booking_id']=	$val->booking_id;
			}
		}
			return $row;

	}
		
	
}
?>