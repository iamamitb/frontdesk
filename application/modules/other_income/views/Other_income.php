<link href="<?=base_url()?>themes/css/views/other_income.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Other Income</h1></div>
</div>
<div class="col-sm-12"><?=$this->session->flashdata("msg"); ?></div>
<!-- Page-Title Ends-->



<!-- Page content starts -->  
<div class="row">

<!-- Tabbed pane starts --> 
	<div class="col-md-12"> 
                                <ul class="nav nav-tabs navtab-bg"> 

                                    <li class="active"> 
                                        <a href="#other_income_list" data-toggle="tab" aria-expanded="true"> 
                                            <span class="visible-xs"><i class="fa fa-user"></i></span> 
                                            <span class="hidden-xs">Other Income List</span> 
                                        </a> 
                                    </li> 
                                    
                                </ul> 
			
                            
								<div class="tab-content">                                      
                                  

                                    <div class="tab-pane active" id="other_income_list"> 
                                    	<div class="col-md-12">
                                         <div class="form-group" style="margin-left:40%;width:20%;margin-bottom:25px;">
                                          <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'","",'hotel_id','hotel_id',"", "hotel_name"); ?>
                                            </div>
								            <div class="table-responsive">  
                                            <div  id="order_list">
                                            </div>
                                              <div align="center" style="margin-top:40px;">
                                        	<button class="btn btn-primary" data-toggle="modal" data-target="#add-other-income">Add Other Income</button>
                                        </div>
								            
                                              </div>
                                      
			 							</div> 
                                    </div>

                                </div>
	</div>
</div> <!-- End of row-->

<!-- Add Other Income Item Modal Starts -->
<div id="add-new-item" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
					<form action="" method="post">
                    <input name="action" type="hidden" value="add_stock" />
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h2 class="modal-title">Add New Item For Other Income</h2> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row">
                                                        <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Hotel Name <span class="error">*</span></label> 
                                                                    <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'","",'hotel_id2','hotel_id',"required aria-required='true'", "hotel_name"); ?>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Item Name <span class="error">*</span></label> 
                                                                    <input type="text" class="form-control" name="item_name" id="item_name" placeholder="" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Unit Price <span class="error">*</span></label> 
                                                                    <input type="text" class="form-control"  name="unit_price" id="unit_price"  placeholder="" required aria-required="true" onkeypress="return isNumber(event)"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row" align="center">
                                                            <div class="checkbox checkbox-success">
                                                                <input id="in_stock" value="1" name="in_stock" type="checkbox">
                                                                <label for="checkbox3">
                                                                    Stock Applicable?
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="row" align="center">

                                                            <div class="form-group" style="width:33%;" id="stock_qty"> 
                                                                    <label for="field-2" class="control-label">Total Stock in Hotel</label> 
                                                                    <input type="text" name="qty" class="form-control" id="field-2" placeholder="" onkeypress="return isNumber(event)"> 
                                                            </div>  
                                                        </div>

                                                    </div> 
                                                    <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="submit" id="submit" class="btn btn-success waves-effect waves-light">Save Item</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                           </form>
</div><!-- /.modal -->
<div id="edit-new-item" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>

<!-- Add Other Income Modal Starts -->
<div id="add-other-income" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none">
<form action="" method="post" id="other_item_form">
 <input name="action" type="hidden" value="add_item"  />
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h2 class="modal-title">Add Other Income</h2> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                        <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Hotel Name <span class="error">*</span></label> 
                                                                    <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'","",'hotel_id1','hotel_id',"onchange='getinformation()' required aria-required='true' ", "hotel_name"); ?>
                                                                </div> 
                                                            </div>
                                                        <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Date <span class="error">*</span></label> 
                                                               <input type="text" name="date" class="form-control DateFrom" required aria-required="true"  onchange="getinformation()" id="dt" placeholder="" />
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Item Name <span class="error">*</span></label> 
                                                                    <select class="form-control" id="item_id" required aria-required="true" name="item_id"><option value="">Select</option></select>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Room Number <span class="error">*</span></label> 
                                                                    <select class="form-control" id="room_number" name="room_number" required aria-required="true"><option value="">Select Room Number</option></select>
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Booking ID <span class="error">*</span></label> 
                                                                    <input type="text" name="booking_id" class="form-control" id="booking_id" placeholder="" required aria-required="true" readonly="readonly"> 
                                                                </div> 
                                                            </div> 

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Number of Days <span class="error">*</span></label> 
                                                                    <input type="text" name="days" class="form-control" onkeyup="total()" id="days" placeholder="" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                            
                                                        </div> 

                                                        <div class="row"> 
                                                           

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Unit Price <span class="error">*</span></label> 
                                                                    <input type="text" name="price" class="form-control" onkeyup="total()" id="price" placeholder="" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                             <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Number of Items <span class="error">*</span></label> 
                                                                    <input type="text" name="no_of_item" class="form-control" onkeyup="total()" id="no_of_item" placeholder="" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Available Items For Issue</label> 
                                                                    <input type="text" class="form-control" id="available" placeholder="" readonly="readonly"> 
                                                                </div> 
                                                            </div> 

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Currently In Use</label> 
                                                                    <input type="text" class="form-control" id="use" placeholder="" readonly="readonly"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Total Amount</label> 
                                                                    <input type="text" name="total_amount" class="form-control" id="total_amount" placeholder="" readonly="readonly" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                             
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Remarks</label> 
                                                                    <input type="text" class="form-control" name="remarks" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 


                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                        <button type="submit"  class="item btn btn-success waves-effect waves-light">Save Item</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                           
</form>
                                        </div><!-- /.modal -->
<div id="edit-other-income" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url()?>themes/js/views/other_income.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
