<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Other_income extends MX_Controller {
	
	    public function __construct(){
        parent::__construct();
		$this->load->model('other_income_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in();
		is_privileges(); 
   		 }

	public function index(){
		$action = $this->input->post('action');
		$account_id=$this->session->userdata('account_id');
		$hotel_id=$this->input->post('hotel_id');
		
		
			if($action=='add_stock')
				{
					$income=array();
					$income['hotel_id'] = $this->input->post('hotel_id');
					$income['item_name'] = $this->input->post('item_name');
					//$income['account_id'] = $account_id;
					$income['unit_price'] = $this->input->post('unit_price');
					$income['in_stock'] =(isset($_POST['in_stock'])) ? '1' : '0' ;
					$income['qty'] = $this->input->post('qty');
					insertValue('other_item',$income);
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					You are successfully add '.$this->input->post('item_name').' item in your inventory.
					</div>');
					redirect('other_income');			
							
				}
				else if($action=='update_stock'){
					$item_id=$this->input->post('item_id');
					$income=array();
					$income['item_name'] = $this->input->post('item_name');
					$income['unit_price'] = $this->input->post('unit_price');
					$income['qty'] = $this->input->post('qty');
					updateDataCondition('other_item',$income,"item_id = ".$item_id);
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					You are successfully Update '.$this->input->post('item_name').' item in your inventory.
					</div>');
					redirect('other_income');
							
				}
				else if($action=='add_item'){
					$incomeitem=array();
					$incomeitem['hotel_id'] = $this->input->post('hotel_id');
					$incomeitem['date'] = date("Y-m-d",strtotime($this->input->post('date')));
					$incomeitem['item_id'] = $this->input->post('item_id');
					$incomeitem['room_number'] = $this->input->post('room_number');
					$incomeitem['booking_id'] = $this->input->post('booking_id');
					$incomeitem['price'] = $this->input->post('price');
					$incomeitem['days'] = $this->input->post('days');
					$incomeitem['no_of_item'] = $this->input->post('no_of_item');
					$incomeitem['total_amount'] = $this->input->post('total_amount');
					$incomeitem['remarks'] = $this->input->post('remarks');
					insertValue('other_item_booked',$incomeitem);
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					You are successfully add new order againt <strong>'.$this->input->post('booking_id').'</strong> booking id.
					</div>');
					redirect('other_income');
				}
				else if($action=='edit_item'){
					$item_booked_id=$this->input->post('item_booked_id');
					$incomeitem=array();
					$incomeitem['price'] = $this->input->post('price');
					$incomeitem['days'] = $this->input->post('days');
					$incomeitem['no_of_item'] = $this->input->post('no_of_item');
					$incomeitem['total_amount'] = $this->input->post('total_amount');
					$incomeitem['remarks'] = $this->input->post('remarks');
					updateDataCondition('other_item_booked',$incomeitem,"item_booked_id = ".$item_booked_id);
					$this->session->set_flashdata('msg','<div class="alert alert-success alert-dismissable" style="text-align:center;">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					You are successfully Update order.
					</div>');
					redirect('other_income');
				}
				
		if($hotel_id!=""){
		$data['item_list']=getData("other_item","hotel_id = ".$hotel_id." and status!='0'");
		}
	
		//$data['item_booked_list']=getData("other_item_booked","hotel_id = ".$hotel_id);
		$this->template->title('Other Income','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('other_income',$data);
		

				
	}
	public function incomeDelete(){
		$item_id = $this->input->post('item_id');
		Delete_data("other_item","item_id = ".$item_id);
		$html['type']='sucess';
		echo json_encode($html);
	}
	public function itemdelete(){
	
		$item_id = $this->input->post('item_id');
		$data['status']='0';
		updateDataCondition("other_item",$data,"item_id = ".$item_id);
		$html['type']='sucess';
		echo json_encode($html);
	}
		public function bookeditemDelete(){
		$item_booked_id = $this->input->post('item_booked_id');
		Delete_data("other_item_booked","item_booked_id = ".$item_booked_id);
		$html['type']='sucess';
		echo json_encode($html);
	}	
		public function bookeditemreturn(){
		$item_booked_id = $this->input->post('item_booked_id');
		$data['status']='0';
		updateDataCondition("other_item_booked",$data,"item_booked_id = ".$item_booked_id);
		$html['type']='sucess';
		echo json_encode($html);
	}
	
	public function editincome(){
		 $item_id = $this->input->post('item_id');
		$item=getData("other_item","item_id = ".$item_id);
		//print_r($item);
		$html['type']='sucess';
		foreach($item as $value) {
		$slected = ($value->in_stock==1) ? 'checked="checked" disabled="disabled"' : '';
		$disabled = ($value->in_stock==1) ? '' : 'disabled="disabled"'  ;
		$html['item']='<form action="" method="post">
                    <input name="action" type="hidden" value="update_stock" />
					<input name="item_id" type="hidden" value="'.$value->item_id.'" />
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h2 class="modal-title">Edit Item For Other Income</h2> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Item Name<span class="error">*</span></label> 
                                                                    <input type="text" class="form-control" name="item_name" id="item_name" placeholder="" value="'.$value->item_name.'" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Unit Price<span class="error">*</span></label> 
                                                                    <input type="text" class="form-control"  name="unit_price" id="unit_price"  placeholder="" value="'.$value->unit_price.'" required aria-required="true"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row" align="center">
                                                            <div class="checkbox checkbox-success">
                                                                <input id="in_stock1" onclick="checkbox()" value="1" name="in_stock" type="checkbox" '.$slected . $disabled.' >
                                                                <label for="checkbox3">
                                                                    Stock Applicable?
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row" align="center">
                                                            <div class="form-group" style="width:33%;" id="stock_qty"> 
                                                                    <label for="field-2" class="control-label">Total Stock in Hotel</label> 
                                                                    <input type="text" name="qty" value="'.$value->qty.'" class="form-control" id="field-2" placeholder="" '.$disabled.'> 
                                                            </div>  
                                                        </div>

                                                    </div> 
                                                    <div class="modal-footer"> 
                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                            <button type="submit" class="btn btn-success waves-effect waves-light">Save Item</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                            </form>';
		}
		echo json_encode($html);
	}
	
	public function order_list(){
		$html['item']="";		
		$hotel_id = $this->input->post('hotel_id');
		if(!empty($hotel_id)){
			//$order_list=getData("other_item_booked","hotel_id = ".$hotel_id ." and status = '1'");
			$order_list=$this->other_income_model->order_list($hotel_id);
			//print_r($order_list);
			if(!empty($order_list)){
				$html['item'] .= '<table class="table table-striped table-bordered table-hover">
								        
								                    <thead>
								                        <tr><th>Sl. Number</th><th>Item Name</th><th>Room Number</th><th>Unit Price</th><th>Number of Item</th><th>Total Amount</th><th>Remarks</th><th>Date</th><th>Actions</th></tr>
								                    </thead>
								                                                
								                                                                                              
								                    <tbody>';
													$i=1;
				
					foreach($order_list as $val){
						$html['item'] .= '<tr>
								                        	<td>'.$i.'</td>
								                            <td>'.$val->item_name.'</td>
								                            <td>'.$val->room_number.'</td>
								                            <td>'.$val->price.'</td>
								                            <td>'.$val->no_of_item.'</td>
								                            <td>'.$val->total_amount.'</td>
								                            <td>'.$val->remarks.'</td>
								                            <td>'.dateformate($val->date).'</td>
								                            <td>
								                            	<div class="btn-group closed">
									                               	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Actions <span class="caret"></span></button>
									                                <ul class="dropdown-menu" role="menu">
										                  <li><a href="javascript:void(0)" onclick="edit_order('.$val->item_booked_id.');getiteminfo('.$val->item_id.')"   data-toggle="modal" data-target="#edit-new-item">Edit</a></li>';
														  if($val->in_stock!=0){
					 $html['item'] .= '<li><a href="javascript:void(0);" onclick="confirmreturn('.$val->item_booked_id.')">Return</a></li>';
														  }
					$html['item'] .= '<li><a href="javascript:void(0);" onclick="confirmDelete1('.$val->item_booked_id.')">Delete</a></li><!-- there is a JS file at the end of the page which takes this link action.-->
									                                </ul>
									                            </div>
								                            </td>
								                        </tr>'; 
														$i++;  
					}
					$html['item'] .= ' </tbody>
								                </table>';
			   } else  {
				  $html['item']='<div class="alert alert-danger alert-dismissable" style="text-align:center;">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								You have not added order yet</div>';
			   }
		}
		else{
			  $html['item']='<div class="alert alert-danger alert-dismissable" style="text-align:center;">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								Please select a hotel first</div>';
		}
			 
		$html['type']='sucess';
		echo json_encode($html);
		}
		
		public function getroomno(){		
		$date = dateformate($this->input->post('date'),"Y/m/d");
		$hotel_id = $this->input->post('hotel_id');
		$roomno=$this->other_income_model->getroomno($date,$hotel_id);		
			if(!empty($roomno)){
					$html['item'] = '<select class="form-control" onchange="getbooking()" required aria-required="true" id="room_number" name="room_number">';
					$html['item'] .= '<option value="">Select Room Number</option>';
					foreach($roomno as $val){
						//if($val[$field_id] == $select_id){$slected='selected="selected"';}else{$slected='';}'.$slected.'
						$html['item'] .= '<option value="'.$val.'" >'.$val.'</option>';   
					}
					$html['item'] .= '</select>';
			   } else  {
				   $html['item'] = '<select class="form-control" id="room_number" name="room_number" required aria-required="true"><option  value="">Select Room Number</option></select>';
			   }
			  $html['item1']=get_data_dropdown("other_item","item_id","item_name","hotel_id = ".$hotel_id,"",'item_id','item_id',"onchange='getiteminfo()' required aria-required='true'", "item_name");
		$html['type']='sucess';
		echo json_encode($html);
		}
		
	public function getiteminfo(){
		$item_id = $this->input->post('item_id');
		$iteminfo=$this->other_income_model->getiteminfo($item_id);
		$html['no_of_item']=$iteminfo['no_of_item'];
		$html['use']=$iteminfo['use'];
		$html['price']=$iteminfo['price'];
		$html['in_stock']=$iteminfo['in_stock'];
		$html['type']='sucess';
		echo json_encode($html);
	}
	public function getbookingid(){		
		$roomno = $this->input->post('room');
		$hotel_id = $this->input->post('hotel_id');
		$roomno=$this->other_income_model->getbookingid($roomno,$hotel_id);
		$html['booking_id']=$roomno['booking_id'];
		$html['type']='sucess';
		echo json_encode($html);
	}
	
		public function editorder(){
		 $item_booked_id = $this->input->post('item_booked_id');
		$item=getData("other_item_booked","item_booked_id = ".$item_booked_id);
		//print_r($item);
		$html['type']='sucess';
		foreach($item as $value) {
	
		$html['item']='<form action="" method="post" id="edit_other_item_form">
 <input name="action" type="hidden" value="edit_item" />
  <input name="item_booked_id" type="hidden" value="'.$value->item_booked_id.'" />
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h2 class="modal-title">Edit Other Income</h2> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                        <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Hotel Name</label> 
                                                                    '.get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status ='1'",$value->hotel_id,'hotel_id2','hotel_id',"disabled='disabled'", "hotel_name").'
                                                                </div> 
                                                            </div>
                                                        <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Date</label> 
                                                               <input type="text" name="date" value="'.dateformate($value->date,'m/d/Y').'" class="form-control DateFrom" disabled="disabled"  placeholder="" />
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Item Name</label> 
                                                                   '.get_data_dropdown("other_item","item_id","item_name","hotel_id = ".$value->hotel_id." ",$value->item_id,'item_id','item_id',"disabled='disabled'", "").'
                                                                </div> 
                                                            </div> 
									<input name="itm" type="hidden" value="'.$value->item_id.'" />
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Room Number</label> 
                                                     <input type="text" name="date" value="'.$value->room_number.'" class="form-control DateFrom" disabled="disabled" disabled="disabled" placeholder="" />
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Booking ID</label> 
                                                                    <input type="text" name="booking_id" class="form-control" id="booking_id" placeholder="" value="'.$value->booking_id.'" readonly="readonly"> 
                                                                </div> 
                                                            </div> 

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Number of Days<span class="error">*</span></label> 
                                                                    <input type="text" name="days" class="form-control" onchange="total()" id="days" value="'.$value->days.'"  placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                            
                                                        </div> 

                                                        <div class="row"> 
                                                           

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Unit Price<span class="error">*</span></label> 
                                                                    <input type="text" name="price" class="form-control" onchange="total()" id="price" placeholder="" value="'.$value->price.'" > 
                                                                </div> 
                                                            </div> 
                                                             <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Number of Items<span class="error">*</span></label> 
                                                                    <input type="text" name="no_of_item" class="form-control" onchange="total()" id="no_of_item" placeholder="" value="'.$value->no_of_item.'" > 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Available Items For Issue</label> 
                                                                    <input type="text" class="form-control" id="available" placeholder="" disabled="disabled"> 
                                                                </div> 
                                                            </div> 

                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Currently In Use</label> 
                                                                    <input type="text" class="form-control" id="use" placeholder="" disabled="disabled"> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Total Amount</label> 
                                                                    <input type="text" name="total_amount" class="form-control" id="total_amount" placeholder="" value="'.$value->total_amount.'" readonly="readonly"  > 
                                                                </div> 
                                                            </div> 
                                                             
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Remarks</label> 
                                                                    <input type="text" class="form-control" name="remarks" id="field-2" placeholder="" value="'.$value->remarks.'" > 
                                                                </div> 
                                                            </div> 
                                                        </div> 


                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                        <button id="save_item" type="submit" class="item btn btn-success waves-effect waves-light">Save Item</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                           
</form>';
		}
		echo json_encode($html);
	}
	

}
