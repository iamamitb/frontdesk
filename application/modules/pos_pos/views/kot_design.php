<link href="<?=base_url()?>themes/css/views/kot_design.css" rel="stylesheet" type="text/css">

<div class="row kot_design_total">
	<div class="row kot_design">
		<h1>Kitchen Order Ticket # <?=$allpos_order[0]->pos_id?></h1>
		<div class="row kot_header">
			<div class="col-md-12">
				<?php if($allpos_order[0]->order_type=='3'){ ?>
				<div class="col-md-6">
					Table: <?=$allpos_order[0]->pos_table_number?>
				</div>
				<?php }?>
				<?php if($allpos_order[0]->order_type=='3' || $allpos_order[0]->order_type=='2'  ){ ?>
				<div class="col-md-6">
					Steward: <?=$allpos_order[0]->steward_name?>
				</div>
				<?php }?>
				<?php if($allpos_order[0]->order_type=='2'){ ?>
				<div class="col-md-6">
					Room: <?=$allpos_order[0]->pos_room_no?>
				</div>
				<?php }?>
				<div class="col-md-12">
					Deliver: <?=date('jS F Y\,h:i A',strtotime($allpos_order[0]->pos_entry_time))?>
				</div>
				<div class="col-md-12">
					Ordered on: 23 April 2016, 13:52 PM
				</div>
			</div>
		</div>
		<table class="table table-striped">
	        <thead>
	            <tr>
	                <th>Sl No.</th>
	                <th>Item</th>
	                <th>Quantity</th>
	            </tr>
	        </thead>
	        <tbody>
	            <?php $cnt=1;if(isset($allpos_item_order)  && !empty($allpos_item_order)) {foreach($allpos_item_order as $item):?>

	            <tr>
	                <td><?=$cnt?></td>
	                <td><?=$item->menu_name?></td>
	                <td><?=$item->pos_order_quantity?></td>
	            </tr>
	            <?php $cnt++; endforeach; } ?> 
	        </tbody>
	    </table>
	</div>

	<div class="row kot_details">
		
		<p><b>Instructions:</b> <?=$allpos_order[0]->pos_special_instruction?></p>
	</div>
</div>