<!-- Page content starts -->  
<!-- Upper row-->
<div class="row" id="upper_row">
    <div class="col-md-12">
        <div class="col-md-5" style="margin-bottom: 5px;background: #E1EEF2;padding: 1%;border-radius: 5px;height: 100px;margin-right: 2%;width: 34%;">
            <div class="col-md-12" style="margin-top:18px;">
                <div class="col-md-3"><button class="btn btn-danger room_service" id="dine_in">Dine In</button></div>
                <div class="col-md-5"><button class="btn btn-danger room_service" id="room_service">Room Service</button></div>
                <div class="col-md-3"><button class="btn btn-inverse room_service" id="take_away">Take Away</button></div>
            </div>
        </div>
        <form id="general_information">
        <div class="col-md-7" style="margin-bottom: 5px;background: #E1EEF2;padding: 1%;border-radius: 5px;width:63%;">
        
            <div class="col-md-2" id="room_number" style="display:none;">
                <div class="form-group">
                    <label for="exampleInputEmail1">Room Number</label>
                    <select class="form-control" id="room_number_id"><option value="">Select Room Number</option>
                    <?php if(isset($today_occupancy)  && !empty($today_occupancy)) {foreach($today_occupancy as $today):?>
                    <option value="<?=$today->rooms?>"><?=$today->rooms?></option>
                    <?php  endforeach; } ?>
                    </select>
                <p style="color:red;" id="room_number_error"></p></div>
            </div>

            <div class="col-md-3" id="guest_name">
                <div class="form-group">
                    <label for="exampleInputEmail1">Guest name</label>
                    <input type="text" class="form-control general_information" id="guest_name_id" placeholder="" required aria-required="true">
                <p style="color:red;" id="guest_error"></p></div>
            </div>

            <div class="col-md-3" id="mobile">
                <div class="form-group">
                    <label for="exampleInputEmail1">Mobile Number</label>
                    <input type="text" class="form-control general_information" id="mobile_id" placeholder="">
                <p style="color:red;" id="mobile_error"></p></div>
            </div>

            
            <div class="col-md-2" id="steward" style="display:none;">
                <div class="form-group">
                    <label for="exampleInputEmail1">Steward </label>
                    <select class="form-control" id="steward_id"><option value="">Select Steward</option>
                    <?php if(isset($allsteward)  && !empty($allsteward)) {foreach($allsteward as $getsteward):?>
                    <option value="<?=$getsteward->steward_id?>"><?=$getsteward->steward_name?></option>
                    <?php  endforeach; } ?>
                    </select>
                <p style="color:red;" id="steward_error"></p></div>
            </div>

            <div class="col-md-2" id="booking_id" style="display:none;">
                <div class="form-group"> 
                    <label for="exampleInputEmail1">Booking ID</label>
                    <input type="text" class="form-control" id="bookingid" placeholder="" readonly>
                <p style="color:red;" id="bookingid_error"></p></div>
            </div>

             <div class="col-md-2" id="table_no" style="display:none;">
                <div class="form-group"> 
                    <label for="exampleInputEmail1">Table No</label>
                    <select class="form-control" id="table_no_id">
                    <option value="">select table</option>
                    <?php if(isset($alltable)  && !empty($alltable)) {foreach($alltable as $table):?>
                    <option value="<?=$table->pos_table_id?>"><?=$table->pos_table_number?></option>
                    <?php  endforeach; } ?> 
                    </select>
                <p style="color:red;" id="table_no_error"></p></div>
            </div>

        </div>
        </form>
    </div>                    
</div> <!-- End Upper row -->

<div class="row" >
    <div class="col-md-12">
        <div class="col-md-5" style="margin-bottom: 5px;background: #E1EEF2;padding: 1%;border-radius: 5px;min-height: 800px;margin-right: 2%;width: 34%;">
        
      <!--   <input class="form-control" style="margin-bottom:20px;" id="cname" name="name" type="text" required="" aria-required="true" placeholder="Enter name of menu items">
 -->
<select id="auto_complete_menu" class="form-control" >
<option value="">Enter name of menu items</option> 
<?php if(isset($allcategory)  && !empty($allcategory) && isset($allmenu_by_category)) {foreach($allcategory as $category):foreach($allmenu_by_category[$category->category_id] as $menu):?>
<option value="menu_<?=$menu->menu_item_id?>"><?=$menu->menu_name?></option>
 <?php  endforeach; endforeach; } ?> 
</select>   

            <div class="row" style="margin-top:20px;" >
            <!-- Order Start -->
                <div class="col-md-12">
                    <table class="table table-bordered" style="background:#f2f2f2;" id="item_add">
                                                    <thead>
                                                        <tr>
                                                            <th>Sl.no</th>
                                                            <th>Qty</th>
                                                            <th>Item</th>
                                                            <th>Rate</th>
                                                            <th>Price</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        
                                                        <tr>
                                                            <td>Total</td>
                                                            <td id="total_item"colspan="2"><b>Items:</b>0</td>
                                                            <td id="total_item_price"colspan="4"><b>Amount:</b> 0</td>

                                                        </tr>
                                                    </tfoot>

                                                     <tbody>
                                                        
                                                    </tbody>
                    </table>

                </div>
            <!-- Order End -->
            </div>

            <div class="row"  id="special_instruction_not_chargeable_div" style="display:none;">
                <div class="col-md-12">
                    <div class="col-md-6"><button type="button" class="btn btn-default" data-toggle="modal" id="add_special_instruction">Add Special Instruction</button></div>
                    <div class="col-md-6"><button type="button" class="btn btn-default" id="not_chargeable">Not Chargeable</button></div>
                </div>
            </div>

            <div class="row"  id="submit_order_div" style="margin-top: 30px;text-align:center;display:none;">
                <button class="btn btn-primary btn-lg m-b-5" data-toggle="modal" id="submit_order">Submit Order</button>

            </div>

            

        </div>
        <div class="col-md-7" style="margin-bottom: 5px;background: #E1EEF2;padding: 1%;border-radius: 5px;width:63%;s">
            
                <!-- Menu Categories Start -->
                <div class="col-md-12" style="margin-bottom:40px;">
                  <?php $cnt=0;$category_id=0; if(isset($allcategory)  && !empty($allcategory)) {foreach($allcategory as $category):?>
                   <button type="button" class="<?php echo isset($cnt) && $cnt==0?'btn btn-inverse btn-rounded category_button':'btn btn-danger btn-rounded category_button'; ?>" id="category_<?=$category->category_id?>"><?=$category->category_name?></button>
               <?php $category_id=isset($cnt) && $cnt==0?$category->category_id:$category_id;  $cnt=1; endforeach; } ?>
                <!--<button type="button" class="btn btn-inverse btn-rounded">Lunch</button>-->
                </div>
                <!-- Menu Categories End -->
            
                <!-- Menu Items Start -->
                <div class="col-md-12" id="menu_item">
                 <?php  if(isset($allmenu_by_category)  && !empty($allmenu_by_category)) {foreach($allmenu_by_category[$category_id] as $menu):?>
                    <button type="button" class="square" id="menu_<?=$menu->menu_item_id?>"><?=$menu->menu_name?></button>
                 <?php  endforeach; } ?> 
                </div>
                <!-- Menu Items End -->
        </div>
    </div>                    
</div> <!-- End row -->
<!-- Page content ends -->  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<link href="<?=base_url()?>themes/css/select2.css" rel="stylesheet" type="text/css">

<script src="<?=base_url()?>themes/js/select2.full.js"></script>

<script src="<?=base_url()?>themes/js/select2.js"></script>
<script src="<?=base_url()?>themes/js/views/pos_pos.js"></script>
<script>
allmenu_by_category= <?=json_encode($allmenu_by_category);?>;
today_occupancy=     <?=json_encode($today_occupancy);?>
</script>
 

<!-- Special Instructions Modal -->

<div id="add_special_instruction_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h4 class="modal-title">Add Special Instructions</h4> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        
                                                        <div class="row"> 
                                                            <div class="col-md-12"> 
                                                                <div class="form-group no-margin"> 
                                                                    <label for="field-7" class="control-label">Special Instructions</label> 
                                                                    <textarea class="form-control autogrow" id="special_instructions" placeholder="" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px"></textarea> 
                                                                    <input type="hidden" id="totel_list_item">
                                                             </div> 
                                                            </div> 
                                                        </div> 

                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                        <button type="button" class="btn btn-info waves-effect waves-light" data-dismiss="modal">Save</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

<!-- Special Instructions Modal Ends -->

<!-- KOT Modal -->

<div id="show_kot" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                            
                                                    <div class="modal-body"> 
                                                        
                                                        <div class="row">

                <div class="panel panel-border panel-inverse" style="margin-top:20px;margin-left:2%;margin-right:2%;">
                    <div class="panel-heading"> <h3 class="panel-title" style="text-align:center;font-size:25px;">KOT</h3></div> 
                    <div class="panel-body"> 
                        <p>
                            <table class="table table-bordered" id="kot_table">
                                
                                                        
                                                        <!-- <tr>
                                                            <th>Kot Number</th>
                                                            <td>143</td>
                                                            
                                                        </tr> -->

                                                        <tr>
                                                            <th>Date</th>
                                                            <td><?=date('d/m/Y')?></td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Time</th>
                                                            <td><?=date('h:i A')?></td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Room Number</th>
                                                            <td id='kot_room_number'></td>
                                                            
                                                        </tr>

                                                        <tr>
                                                            <th>Steward Name</th>
                                                            <td id='kot_stward'></td>
                                                            
                                                        </tr>

                                                      <tr>
                                                            <th>Table No</th>
                                                            <td id='kot_table_no'></td>
                                                            
                                                        </tr>    

                                                        <tr>
                                                            <th colspan="2" style="background-color:#f2f2f2;font-weight:bold;" >Order</th>

                                                        </tr>

                                                        <tr>
                                                            <th>Item</th>
                                                            <td>Qty</td>
                                                            
                                                        </tr>
                                                        <tbody id="item_add_body">
                                                        </tbody>
                                                        <tfoot> 
                                                        <tr>
                                                            <th colspan="2" style="background-color:#f2f2f2;font-weight:bold;">Special Instruction</th>
                                                        </tr>

                                                        <tr>
                                                            <th colspan="2" id="special_instruction_comment">Please treat this guest nicely, give him free cold drinks and show him the path to wash room</th>
                                                        </tr>
                                                        </tfoot>
                                                    
                                                </table>
                        </p>
                        
                    </div> 
                </div>


            </div>
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button> 
                                                        <button type="button" class="btn btn-success btn-lg" id='submit_kot'>Print KOT</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->
<!-- KOT Modal ends -->