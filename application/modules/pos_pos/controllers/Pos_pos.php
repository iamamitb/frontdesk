<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_pos extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_pos_model');
		$this->load->library('form_validation');
        date_default_timezone_set('Asia/Kolkata');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		//The function use pull all category
        $hotel_id=$this->session->userdata('hotel_id');
		$data['allcategory']=getAll('category_management',array('category_status'=>'1','hotel_id'=>$hotel_id),'category_id','asc','','');
        foreach($data['allcategory'] as $category)
        {
        	$data['allmenu_by_category'][$category->category_id]=getAll('menu_item',array('category_id'=>$category->category_id,'hotel_id'=>$hotel_id),'menu_item_id','asc','','');
        }
		$data['alltable']=getAll('table_master',array('hotel_id'=>$hotel_id,'pos_table_status'=>'1'),'pos_table_id','asc','','');
        $data['allsteward']=getAll('steward_master',array('hotel_id'=>$hotel_id,'steward_status'=>'1'),'steward_id','asc','','');
	    $data['today_occupancy'] = $this->pos_pos_model->getTodayOccupancy($hotel_id);
        $this->template->title('POS','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
        $this->template->build('pos_pos',$data);

				
	}

    public function kot_design($pos_id){

        $hotel_id=$this->session->userdata('hotel_id');
        $data['allpos_order'] = $this->pos_pos_model->allpos_order($pos_id,$hotel_id);
        $data['allpos_item_order'] = $this->pos_pos_model->allpos_item_order($pos_id,$hotel_id);
        $this->template->set_layout('kot_design_template','front');
        $this->template->build('kot_design',$data);

    }

	public function additem(){

		$hotel_id=$this->session->userdata('hotel_id');
        $pos_bill_no=0;
        $room_number=$this->input->post('room_number');
		$guest_name=$this->input->post('guest_name');
		$mobile=$this->input->post('mobile');
		$steward=$this->input->post('steward');
		$booking_id=$this->input->post('booking_id');
		$table_no=$this->input->post('table_no');
		$item_array=$this->input->post('item_array');
		$room_service=$this->input->post('room_service');
		$special_instruction=$this->input->post('special_instruction');
        $not_chargeable=$this->input->post('not_chargeable');
        $item_total_price=$this->input->post('item_total_price');
        $mobile_exit=getAll('pos_master',array('hotel_id'=>$hotel_id,'pos_entry_date'=>date('Y-m-d'),'pos_mobile'=>$mobile),'pos_id','desc','','');
        $alltax=getAll('pos_bill_design',array('hotel_id'=>$hotel_id),'pos_bill_design_id','asc','','');
        $total_vat=0;
        $total_service_tax=0;
        $total_service_charge=0;
        if(isset($alltax) && !empty($alltax))
        {
           foreach($alltax as $tax)
           {
            $pos_bill_alcholic_beverages_vat=$tax->pos_bill_alcholic_beverages_vat;
            $pos_bill_vat_number_value=$tax->pos_bill_vat_number_value;
            $pos_bill_service_tax_value=$tax->pos_bill_service_tax_value;
            $pos_bill_service_charge_value=$tax->pos_bill_service_charge_value;
           }
        }
        $last_pos_bill=getAll('pos_master',array('hotel_id'=>$hotel_id),'pos_bill','desc','1','');   
            if(isset($last_pos_bill) && !empty($last_pos_bill))
            {
               foreach($last_pos_bill as $getbill)
               {
                $bill_no=$getbill->pos_bill;
               }

            }

            if(isset($bill_no) && !empty($bill_no))
            {
            $pos_last_bill_no=(int)$bill_no+1;
            } 
            else
            {
               $pos_last_bill_no=1;  
            }
    
        
        if($room_service=="take_away")
        {
        $room_number=0;
        $steward=0;
        $booking_id=0;
        $table_no=0;
        $order_type=1;
        $pos_bill_no=$pos_last_bill_no;
        $last_token_number=getAll('pos_master',array('hotel_id'=>$hotel_id),'token_number','desc','1','');
        foreach($last_token_number as $last_token)
        {
        	$token_number_no=$last_token->token_number;
        }
        if(isset($token_number_no) && $token_number_no!="")
				{
					$token_number_no=(int)$token_number_no+1;
				}
				else
				{
					$token_number_no=1;
				}

        }
        else if($room_service=="room_service")
        {
        $table_no=0;	
        $order_type=2;
        $token_number_no=0;
        $room_exit=getAll('pos_master',array('hotel_id'=>$hotel_id,'pos_room_no'=>$room_number,'pos_payment_status'=>'pending'),'pos_id','desc','1','');
        if(isset($room_exit) && !empty($room_exit))
        {
           foreach($room_exit as $get_bill_no)
           {
            $pos_bill_no=$get_bill_no->pos_bill;
           }
        }
        else
        {
            $pos_bill_no=$pos_last_bill_no;
        }
        
        }
        else if($room_service=="dine_in")
        {
        $room_number=0;
        $booking_id=0;
        $order_type=3;
        $token_number_no=0;
        $table_exit=getAll('pos_master',array('hotel_id'=>$hotel_id,'pos_table_id'=>$table_no,'pos_payment_status'=>'pending'),'pos_id','desc','1','');
        if(isset($table_exit) && !empty($table_exit))
        {
           foreach($table_exit as $get_bill_no)
           {
            $pos_bill_no=$get_bill_no->pos_bill;
           }
        }
        else
        {
            $pos_bill_no=$pos_last_bill_no;
        }

        }
        else
        {
        $guest_name=0;
        $mobile=0;
        $room_number=0;
        $steward=0;
        $booking_id=0;
        $table_no=0;	
        }
        $pos_master_array=array(
        	'pos_bill'=>$pos_bill_no,
            'pos_guest_name'=>$guest_name,
        	'pos_mobile'=>$mobile,
        	'pos_steward_id'=>$steward,
        	'pos_room_no'=>$room_number,
        	'pos_booking_id'=>$booking_id,
        	'pos_entry_date'=>date('Y-m-d'),
            'pos_entry_time'=>date('H:i:s'),
        	'pos_special_instruction'=>$special_instruction,
        	'pos_not_chargeable'=>$not_chargeable,
        	'pos_steward_id'=>$steward,
        	'pos_table_id'=>$table_no,
            'order_type'=>$order_type,
            'token_number'=>$token_number_no,
            'pos_total'=>$item_total_price,
            'pos_due'=>$item_total_price,
            'hotel_id'=>$hotel_id,
          	);

        $pos_id=insertValue('pos_master',$pos_master_array);
        foreach($item_array as $item)
        {
             $pos_order_array=array(
        	'pos_id'=>$pos_id,
            'hotel_id'=>$hotel_id,
        	'pos_order_item_id'=>$item['item_id'],
        	'pos_order_quantity'=>$item['no_of_quantity'],
        	'pos_order_rate'=>$item['rate_per_quantity'],
        	'pos_order_total_price'=>$item['sub_total'],
        	); 
        $pos_order_id=insertValue('pos_order',$pos_order_array);
            

            if( $item['alcholoc_beverage']=='1')
            {
            $vat_amount=($item['sub_total']*$pos_bill_alcholic_beverages_vat)/100;
            }
            else
            {
            $vat_amount=($item['sub_total']*$pos_bill_vat_number_value)/100;
            }
   

            $total_vat=$total_vat+$vat_amount;
            //$total_service_tax=$total_service_tax+$service_tax;
            //$total_service_charge=$total_service_charge+$service_charge;
        }
        
            $total_service_tax=($item_total_price*$pos_bill_service_tax_value)/100;
            $total_service_charge=($item_total_price*$pos_bill_service_charge_value)/100;
        $pos_master_array_update=array(
            'pos_total_vat'=>$total_vat,
            'pos_total_service_tax'=>$total_service_tax,
            'pos_total_service_charge'=>$total_service_charge,
    );
       
    $result=updateDataCondition('pos_master', $pos_master_array_update,array('pos_id'=>$pos_id));
 	echo $pos_id;
    }   	

}
