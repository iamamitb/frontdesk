<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Material Issue</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">
                <form action="<?php echo base_url(); ?>pos_material_issue/add" method="post" id="reportFrom" >
                
                <div class="col-md-2">
                    <div class="form-group">
                     <label for="exampleInputEmail1">Store </label>
                     <?=get_data_dropdown('pos_store','pos_store_id','pos_store_name','pos_store_status=1',"","store","store[]","required","","","");?> 
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Material Name </label>
                     <?=get_data_dropdown('raw_material_master','material_id','material_name','material_status=1',"","material","material[]","required","","","");?>  
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Quantity</label>
                    <input type="text" class="form-control" id="quantity" name="quantity[]" >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Unit </label>
                     <?=get_data_dropdown('unit_master','unit_id','unit_name','unit_status=1',"","unit","unit[]","required","","","");?> 
                    </div>
                </div>
              

                <div class="col-md-4" style="top:25px;">
                    <div class="form-group">
                    <button type="submit" class="btn btn-primary m-b-5">Add to List</button>
                    </div>
                </div>

                </form>
            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
       
    <table id="datatable" class="table table-bordered">
        <thead>
        
            <tr>
                <th>Sl No.</th>
                <th>Material</th>
                <th>Quantity</th>
                <th>Unit</th>
                <th>Store</th>
                <th>Manage</th>
            </tr>
        </thead>

                                             
        <tbody>
            <?php //$i = 1; ?>

        <?php print_r($post_array); 
        
        ?>

           <?php for($i=0;$i<count($this->session->userdata('material'));$i++){ ?>
            <tr>
                <td><?php //echo $i++; ?></td>
                <td><?php echo $this->session->userdata(['material'][$i]);  ?></td>
                <td><?php echo $this->session->userdata('quantity');  ?></td>
                <td><?php echo $this->session->userdata('unit');  ?></td>
                <td><?php echo $this->session->userdata('store'); ?></td> 
                <td>Delete</td>
               
            </tr>
            
            <?php } ?>

        </tbody>
        </table>
        <div class="row" style="top:25px;" align="center">
            <div class="form-group">
                <button type="submit" class="btn btn-primary m-b-5">Submit Issuance</button>
            </div>
        </div>
        
    </div>
</div>
<!-- Page content ends -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script src="<?=base_url()?>themes/js/views/service_tax_report.js"></script> 
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<?php include('../include/pos-footer.php');?>

<script type="text/javascript">


</script>