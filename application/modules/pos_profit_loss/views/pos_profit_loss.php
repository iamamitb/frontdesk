<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Profit Loss Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
                                        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
                                            <div class="col-md-12">

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                    <label for="exampleInputEmail1">From Date</label>
                                                    <input type="text" class="form-control" id="datepicker" placeholder="Enter Start Date">
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                    <label for="exampleInputEmail1">To Date</label>
                                                    <input type="text" class="form-control" id="datepicker2" placeholder="Enter End Date">
                                                    </div>
                                                </div>

                                                
                                                
                                              

                                                <div class="col-md-2" style="top:25px;">
                                                    <div class="form-group">
                                                    <button class="btn btn-primary m-b-5">Filter Results</button>
                                                    </div>
                                                </div>




                                            </div>
                                        </div>
                                        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <table id="datatable" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Product Name</th>
                                                            <th>Quantity</th>
                                                            <th>Cost Price</th>
                                                            <th>Sell Price</th>
                                                            <th>Profit Loss</th>
                                                        </tr>
                                                    </thead>

                                             
                                                    <tbody>
                                                        <tr>
                                                            <td>23-02-2016</td>
                                                            <td>Chinese</td>
                                                            <td>5</td>
                                                            <td>4890</td>
                                                            <td>7890</td>
                                                            <td>3000</td>
                                                        </tr>

                                                        <tr>
                                                            <td></td><td></td>
                                                            <td><b>Total</b></td>
                                                            <td>5670</td>
                                                            <td>4563</td>
                                                            <td>4523</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url()?>themes/plugins/datatables/datatables.js"></script>
