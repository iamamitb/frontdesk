<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Category Management</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_category" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Categories</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_category" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Category</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addcategorydata"); ?>
            <?=$this->session->flashdata("chkcategorydata"); ?> 
            <?=$this->session->flashdata("msgCategoryUpdate"); ?> 
            <div class="tab-pane active" id="all_category"> 
            <!-- Row for Table Starts -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="font-weight:bold;margin-bottom:30px;">All Category</h2>
                    
                    <table id="datatable" class="table table-striped table-bordered">
                        <?php 
                            if(isset($allRecord) && count($allRecord)>0){
                            ?>
                        <thead>
                            <tr>
                                <th>Category ID</th>
                                <th>Category Name</th>
                                <th>Category Rank</th>
                                <th>Category Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <?php 
                            foreach($allRecord as $category):{
                          ?>
                        <tbody>
                            <tr>
                                <td><?=$category->category_id?></td>
                                <td><?=$category->category_name?></td>
                                <td><?=$category->category_rank?></td>
                                <td><?=$category->category_description?></td>
                                <td>
                                    <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="#" data-toggle="modal" onclick="editCategory(<?=$category->category_id?>)" data-target="#edit-category">Edit</a></li>
                                        <li><a onclick="categoryDelete(<?=$category->category_id?>)" href="javascript:void(0)">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>    
                        </tbody>
                        <?php endforeach;  } else{  ?> 
                        
                     <tbody> 
                        <tr >
                        <th style="text-align:center;">You have not added any Category Yet.</th>
                        </tr> 
                     </tbody> 
                     <?php } ?>
                    </table>

                </div>
            </div>
           <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_category"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Category</h2>
                <form name="category_form" method="post" id="category_form" action="<?php echo base_url(); ?>pos_category_management/addCategory">
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category Name <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="category_name" name="category_name" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category Description <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="category_description" name="category_description" required aria-required="true" class="form-control" >
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Category Rank <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="category_rank" name="category_rank" required aria-required="true" class="form-control">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Category</button>
                    </div>
                </div>
                </form> 



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<!-- modal --> 
<div id="edit-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>

<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 



$(document).ready(function() {
    $("#category_form").validate({
         rules: {
            
            category_name: {
                required: true,
                lettersonly: true,                     
                noSpace: true
            },
            category_description: {
                required: true
            },
            category_rank: {
                required: true,
                number: true
            }
            
         },
         messages: {
            category_name: {
                    required: "Please enter category name",
                    lettersonly: "only letters are allowed"
                },    
            category_description: {
                    required: "Please enter category description"
                },
            category_rank: {
                    required: "Please enter category rank",
                    number: "Please specify a number"
                }             
         },
        
    });

});

function editCategory(idc)
{
    var idc = idc;
    //alert(idc);
   
    $.ajax({
        url:"<?php echo site_url('pos_category_management/editCategory');?>/"+idc,
        type: 'POST',
        dataType: "json",
        data: {'idc': idc},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-category').html(data.item);
            
        }      
    });
}



$('#edit_category').click(function(){

   var categoryID = $("#categoryID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_category_management/updateCategory",
        type: 'POST',
        dataType: "json",
        data: {categoryID : categoryID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function categoryDelete(category_id) {
    //alert(category_id); 
    swal({
        title: "Are you sure?",
        text: "This category will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_category_management/categoryDelete",
            type: 'POST',
            dataType: "json",
            data: {'category_id': category_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}



</script>
