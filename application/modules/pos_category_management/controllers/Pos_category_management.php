<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_category_management extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_category_management_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allRecord']=getData("category_management","category_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Category Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_category_management',$data);

				
	}

	public function addCategory(){
		$category_name=$this->input->post('category_name'); 
		$category_description=$this->input->post('category_description');
		$category_rank=$this->input->post('category_rank');
		$hotel_id=$this->session->userdata('hotel_id');
		
		$data=array('hotel_id'=>$hotel_id,
					'category_name'=>$category_name,
					'category_description'=>$category_description,
					'category_rank'=>$category_rank,
					'category_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('category_management',array('category_name' => $category_name));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('category_name',$category_name);
			  $this->db->update('category_management',$data);

			  $this->session->set_flashdata('chkcategorydata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                Category already exists.
            </div>');
			redirect('pos_category_management#');

		}else{ 
			  $category_id=insertValue('category_management',$data);
		} 	
		
		
		$this->session->set_flashdata('addcategorydata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Category.
            </div>');
		
		
		redirect('pos_category_management#');
		
	}

	public function editCategory($idc) { 
		$category=getSingle('category_management','category_id = '.$idc,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Category</h3>
			    </div>
		    <form method="post" id="category_form_edit" action="'.base_url().'pos_category_management/updateCategory" >
			<input type="hidden" id="categoryID" name="categoryID" value="'.$category->category_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Name <span class="error">*</span></label>
		              <input type="text" id="category_name" name="category_name" class="form-control" value="'.$category->category_name.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Description<span class="error">*</span></label>
		              <input type="text" id="category_description" name="category_description" class="form-control" value="'.$category->category_description.'" required>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Rank <span class="error">*</span></label>
		              <input type="text" id="category_rank" name="category_rank" class="form-control" value="'.$category->category_rank.'" required>
		             </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_category" id="edit_category" class="btn btn-success waves-effect waves-light">Update Category</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateCategory() {
		$categoryID=$this->input->post('categoryID'); 
		$category_name=$this->input->post('category_name'); 
		$category_description=$this->input->post('category_description');
		$category_rank=$this->input->post('category_rank');
		
		$data=array(
					'category_name'=>$category_name,
					'category_description'=>$category_description,
					'category_rank'=>$category_rank,
					'category_status'=>'1'
					);

		//print_r($data); die();

  		$categoryUpdate=updateDataCondition('category_management',$data,'category_id = '.$categoryID);
                          
  		$this->session->set_flashdata('msgCategoryUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the category.
            </div>');	
                          
  		redirect('pos_category_management#');

	}


	public function categoryDelete() { 
		
		$category_id=$_POST['category_id'];		
	
		$data['category_status']='0';			
		
		$category_delete=updateDataCondition('category_management',$data,'category_id = '.$category_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	








	

}
