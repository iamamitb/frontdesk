<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pos_material_purchase_report_model extends CI_Model {
		
	
	function getreport($search=array(),$csv){

	 	$row=false;
		
					
		if($csv == ""){
				$sql="SELECT p.purchase_date,rmm.material_name,um.unit_name,pm.purchase_material_quantity,pm.purchase_material_rate,p.total_amount 
					FROM purchase_material pm LEFT JOIN purchase p ON p.purchase_id=pm.purchase_id
					LEFT JOIN raw_material_master rmm ON rmm.material_id=pm.material_id
					LEFT JOIN unit_master um ON um.unit_id=pm.unit_id
					LEFT JOIN supplier s ON s.supplier_id=p.supplier_id
					WHERE pm.purchase_material_status=1 and p.purchase_status=1 "; 
			}else if ($csv == 1) {
				$sql="SELECT p.purchase_date as 'Date',rmm.material_name as 'Item',um.unit_name as 'Unit',pm.purchase_material_quantity as 'Quantity',pm.purchase_material_rate as 'Rate',p.total_amount as 'Total' 
					FROM purchase_material pm LEFT JOIN purchase p ON p.purchase_id=pm.purchase_id
					LEFT JOIN raw_material_master rmm ON rmm.material_id=pm.material_id
					LEFT JOIN unit_master um ON um.unit_id=pm.unit_id
					LEFT JOIN supplier s ON s.supplier_id=p.supplier_id
					WHERE pm.purchase_material_status=1 and p.purchase_status=1 "; 
			}

		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}
		//$sql.=" GROUP BY b.booking_id";
		$query = $this->db->query($sql);
		
		if($csv!=0){
			download_report($query,"pos_daywise_purchase_report.csv");		 
		}
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}
			return $row;
		}
		
		
	}




	
}
?>