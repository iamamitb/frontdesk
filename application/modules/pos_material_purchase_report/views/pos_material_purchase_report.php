<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Material Purchase Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    
        <!-- Row for Custom Filter Starts -->   
        <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
            <div class="col-md-12">
                <form action="" method="post" id="reportFrom" >
                <input name="action" type="hidden" value="filter" />
                <input name="csv" type="hidden" id="csv" value="" />
                <div class="col-md-2">
                    <div class="form-group">
                     <label for="exampleInputEmail1">From Date </label>
                     <input type="text" class="form-control" id="from_date" value="<?php echo $fromDate=(isset($_POST['filter']['p']['from1']['purchase_date']))? $_POST['filter']['p']['from1']['purchase_date'] : "" ;?>" name="filter[p][from1][purchase_date]" placeholder="" readonly>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <label for="exampleInputEmail1">To Date </label>
                    <input type="text" class="form-control" id="to_date" value="<?php echo $toDate=(isset($_POST['filter']['p']['to1']['purchase_date']))? $_POST['filter']['p']['to1']['purchase_date'] : "" ;?>" name="filter[p][to1][purchase_date]" placeholder="" readonly>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                    <?php  $rmmID=(isset($_POST['filter']['rmm']['equal']['material_id']))? $_POST['filter']['rmm']['equal']['material_id'] : "" ;?>     
                    <label for="exampleInputEmail1">Material Name </label>
                     <?=get_data_dropdown("raw_material_master","material_id","material_name","material_status = 1",$rmmID,'material_id',"filter[rmm][equal][material_id]","","","",""); ?> 
                    </div>
                </div>


                <div class="col-md-2">
                    <div class="form-group">
                    <?php  $sID=(isset($_POST['filter']['s']['equal']['supplier_id']))? $_POST['filter']['s']['equal']['supplier_id'] : "" ;?>     
                    <label for="exampleInputEmail1">Supplier Name </label>
                     <?=get_data_dropdown("supplier","supplier_id","supplier_name","supplier_status = 1",$sID,'supplier_id',"filter[s][equal][supplier_id]","","","",""); ?> 
                    </div>
                </div>
              

                <div class="col-md-4" style="top:25px;">
                    <div class="form-group">
                    <button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                    <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
                    </div>
                </div>

                </form>
            </div>
        </div>
        <!-- Row for Custom Filter Ends -->
    

    <div class="row">
    <?php
      $totalQuantity=0;     
      if(isset($all_records) && count($all_records)>0){
      ?>    
    <table id="datatable" class="table table-bordered">
        <thead>
            <tr>
                <th>Date</th>
                <th>Item</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Total</th>
            </tr>
        </thead>

                                             
        <tbody>
            <?php                                                                                                                
            foreach($all_records as $record):{
            ?>  
            <tr>
                <td><?=date('d-M-Y', strtotime($record->purchase_date))?></td>
                <td><?=$record->material_name ?></td>
                <td><?=$record->unit_name ?></td>
                <td><?=$record->purchase_material_quantity ?></td>
                <td><?=$record->purchase_material_rate ?></td>
                <td><?=$record->purchase_material_quantity * $record->purchase_material_rate ?></td>
                <?php $qty=$record->purchase_material_quantity * $record->purchase_material_rate;
                 $totalQuantity += $qty ;?>
            </tr>
            <?php } endforeach;  ?>
            <tr><td></td><td></td><td></td><td></td>
                <td><b style="float:right;">Total Quantity</b></td>
                    <td><?=$totalQuantity?></td>
            </tr>
        </tbody>
        </table>
        <?php  } else{ ?>
         <table class="table table-bordered" style="margin-top:25px;">
            <tbody> 
                <tr>
                <th style="text-align:center;">No data found.</th>
                </tr> 
             </tbody> 
         </table>
        <?php } ?>
    </div>
</div>
<!-- Page content ends -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/js/views/service_tax_report.js"></script> 
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<?php include('../include/pos-footer.php');?>

<script type="text/javascript">

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 

$("#to_date").datepicker({ dateFormat: 'dd-mm-yy'});
$("#from_date").datepicker({ dateFormat: 'dd-mm-yy'}).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to_date").datepicker( "option", "minDate", minValue );
        });


//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_date").val("");
    $("#to_date").val("");
    $('#supplier_id option:selected').removeAttr('selected');
    $('#material_id option:selected').removeAttr('selected');
    window.location.reload(true);
});

$(document).ready(function(){
    var fromdate=$("#from_date").val();
    var todate=$("#to_date").val();
    var supplier_id=$("#supplier_id").val();
    var material_id=$("#material_id").val();
    if(fromdate != '' || todate != '' || material_id != '' || supplier_id != ''){
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }
});

</script>