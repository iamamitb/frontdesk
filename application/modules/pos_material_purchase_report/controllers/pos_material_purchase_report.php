<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pos_material_purchase_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_material_purchase_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$search = array();
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		if($action=='filter')
		$search=$_POST['filter']; 

		$data['all_records']=$this->pos_material_purchase_report_model->getreport($search,$csv);
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		
		$this->template->title('Material Purchase','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_material_purchase_report',$data);

				
	}
	

}
