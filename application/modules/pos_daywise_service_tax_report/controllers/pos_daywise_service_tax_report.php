<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_daywise_service_tax_report extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_daywise_service_tax_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$this->template->title('Service Tax Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_daywise_service_tax_report');

				
	}
	

}
