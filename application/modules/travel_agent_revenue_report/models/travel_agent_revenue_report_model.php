<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Travel_agent_revenue_report_model extends CI_Model {


	function getreport($account_id,$search=array(),$csv,$travelAgent){

	 	$row=false;
		 $sql="SELECT b.booking_reference,t.grand_total,b.agent_percentage,b.booking_id,b.booking_date,ta.agent_name,ta.agent_id,h.hotel_name 
		 			FROM transaction t RIGHT JOIN bookings b ON b.booking_id=t.booking_id 
			 			RIGHT JOIN agents ta ON b.agent_id=ta.agent_id 
			 			RIGHT JOIN hotels h ON b.hotel_id=h.hotel_id 
			 			WHERE ta.account_id=$account_id "; 		
								
		
				
		/*if($booking_id!=0)
		{
			$sql.=" and b.booking_id=$booking_id ";
		}*/

		if($travelAgent!=0)
		{
			$sql.=" and b.agent_id=$travelAgent ";
		}

		$where = array();
		
		if(!empty($search))
		{
		foreach($search as $db=>$dbarray)
		{
			foreach($dbarray as $type=>$typearray)
			{
				foreach($typearray as $newkey=>$val)
				{
					if($val){
					  if($type == 'like')
					  {
						  $where[] = $db.".".$newkey." like '%".$val."%' ";
					  }
					  elseif($type == 'from')
					  {
						  $where[] = $db.".".$newkey." >= '".$val."'";
					  }
					  elseif($type == 'to')
					  {
						  $where[] = $db.".".$newkey." <= '".$val."'";
					  }
					   elseif($type == 'from1')
						{		$datetime = strtotime($val);
						$d1 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." >= '".$d1."'";
					  }
					  elseif($type == 'to1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." <= '".$d2."'";
					  }
					    elseif($type == 'equal1')
					  {	$datetime = strtotime($val);
						$d2 = date('Y/m/d', $datetime);
						  $where[] = $db.".".$newkey." = '".$d2."'";
					  }
					  elseif($type == 'equal')
					  {
						  $where[] = $db.".".$newkey." = '".$val."'";
					  }
					}
				}
			}
		
		}
		}
		if(count($where))
		{
			$sql.="and ".implode(' and ',$where);
		}
		$sql.=" GROUP BY b.booking_id";
		$query = $this->db->query($sql);
		
		if($csv!=0){
			download_report($query,"travel_agent_revenue_report.csv");		 
		}
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $val)
			{
				$row[]=	$val;
			}
			return $row;
		}
		
		
	}	

		
	
}
?>