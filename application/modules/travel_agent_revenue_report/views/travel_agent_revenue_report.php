<link href="<?=base_url()?>themes/css/views/travel_agent_revenue_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Travel Agent Revenue Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<!-- Panel Starts -->
<div class="row">
	<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">Travel Agent Report for your Account</h3>
</div>
<div class="panel-body">

<!-- Row for Custom Filter Starts -->
<?php //if(isset($all_bookings) && count($all_bookings)>0){ ?>  	
<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
    
	<div class="col-md-12">
        <form action="" method="post" id="TAReportForm" >
        <input name="action" type="hidden" value="filter" />
        <input name="csv" type="hidden" id="csv" value="" />
		<div class="col-md-2">
			<div class="form-group">
            <label for="exampleInputEmail1">From Date</label>
            <input type="text" id="from_booking_date" value="<?php echo $dt=(isset($_POST['filter']['b']['from1']['booking_date']))? $_POST['filter']['b']['from1']['booking_date'] : "" ;?>" name="filter[b][from1][booking_date]" class="form-control" placeholder="Enter Start Date">
        	</div>
    	</div>

    	<div class="col-md-2">
        	<div class="form-group">
            <label for="exampleInputEmail1">To Date</label>
            <input type="text" value="<?php echo $dt1=(isset($_POST['filter']['b']['to1']['booking_date']))? $_POST['filter']['b']['to1']['booking_date'] : "" ;?>" id="to_booking_date" name="filter[b][to1][booking_date]" class="form-control" placeholder="Enter End Date">
        	</div>
    	</div>

    	<div class="col-md-2">
			<div class="form-group">
            <label for="exampleInputEmail1">Travel Agent</label>
            <?php  $tai=(isset($_POST['filter']['b']['equal']['agent_id']))? $_POST['filter']['b']['equal']['agent_id'] : "" ;?>
            <?=get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id'),$tai,'agent_id',"filter[b][equal][agent_id]"); ?>
        	</div>
    	</div>

    	<div class="col-md-2">
        	<div class="form-group">
            <label for="exampleInputEmail1">Booking ID</label>
            <input type="text" value="<?php echo $bi=(isset($_POST['filter']['b']['equal']['booking_reference']))? $_POST['filter']['b']['equal']['booking_reference'] : "" ;?>" name="filter[b][equal][booking_reference]" class="form-control" placeholder="Enter Booking ID"> 
        	</div>
    	</div>
    	<div class="col-md-2">
			<div class="form-group">
            <label for="exampleInputEmail1">Hotel Name</label>
            <?php  $hi=(isset($_POST['filter']['b']['equal']['hotel_id']))? $_POST['filter']['b']['equal']['hotel_id'] : "" ;?>
            <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id'),$hi,'hotel_id',"filter[b][equal][hotel_id]"); ?>
        	</div>
    	</div>

    	<div class="col-md-2" style="top:25px;">
        	<div class="form-group">
            <!-- <label for="exampleInputEmail1">Filter</label> -->
			<button type="submit" name="showResult" class="btn btn-primary m-b-5">Filter Results</button>
        	</div>
    	</div>
    </form>
	</div>
</div>
<?php //} ?>
<!-- Row for Custom Filter Ends -->

<!-- Row for Table Starts -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php if(isset($all_bookings) && count($all_bookings)>0){ ?> 
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Hotel Name</th>
                    <th>Agent Name</th>
                    <th>Booking Amount</th>
                    <th>TAC</th>
                    <th>Agent Payable Amount</th>
                    <th>Booking Date</th>
                </tr>
            </thead>
        <tbody>
        <?php                                                                                                                
            foreach($all_bookings as $all_booking):{
            $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 
           ?>     
        <tr>
            <td><?=$all_booking->booking_reference ?></td>
            <td><?=$all_booking->hotel_name ?></td>
            <td><?=$all_booking->agent_name ?></td>
            <td><?=$all_booking->grand_total ?></td>
            <td><?=$all_booking->agent_percentage ?>%</td>
            <td>
                <?php
                $bookingAmt=$all_booking->grand_total;
                $tac=$all_booking->agent_percentage;
                $percentageAmt=($bookingAmt * $tac)/100;
                //$amt=$bookingAmt + $percentageAmt;
                echo $percentageAmt;
                ?>
            </td>
            <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
        </tr>
        <?php } endforeach;  ?>

        </tbody>
    </table>

    </div>
    </div>
   <!-- Row for Table ends -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->

<div class="col-md-12" style="margin-top:25px;">
    <div align="center">
        <button id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No data found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>

<!-- Page content ends -->  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<!-- Controller Specific JS file, place it after Sweet alert plugin is called -->
<script src="<?=base_url()?>themes/js/views/travel_agent_revenue_report.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
// check in and check out date picker...
    $("#to_booking_date").datepicker({ dateFormat: 'dd-mm-yy', maxDate:0});
    $("#from_booking_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate:0}).bind("change",function(){
        var minValue = $(this).val();
        minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
        minValue.setDate(minValue.getDate()+1);
        $("#to_booking_date").datepicker( "option", "minDate", minValue );
    })
     $("#downloadReport").click(function() {
             $( "#csv" ).val(1);
             $( "#TAReportForm" ).submit();
             $( "#csv" ).val('');
       });
        $("#printReport").click(function() {
            $( "#TAReportForm" ).attr("target","_blank");
            $( "#TAReportForm" ).attr("action","<?=base_url()?>travel_agent_revenue_report/print_report");
            $( "#TAReportForm" ).submit();
            $( "#TAReportForm" ).removeAttr("target");
            $( "#TAReportForm" ).attr("action"," ");
       });

</script>


