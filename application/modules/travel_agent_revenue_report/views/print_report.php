<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Travel Agent Revenue Report</h1>
</div>
<?php
if(!empty($_POST)){   
$agent_name=(($_POST['filter']['b']['equal']['agent_id'])!="")? getValue('agents','agent_name',"agent_id = ".$_POST['filter']['b']['equal']['agent_id'] ) : "" ;    
$hotel_name=(($_POST['filter']['b']['equal']['hotel_id'])!="")? getValue('hotels','hotel_name',"hotel_id = ".$_POST['filter']['b']['equal']['hotel_id'] ) : "" ;
?>     
<div class="row" align="center">
<p><b>Filters: </b><span>Date: <?php echo $dt1=(isset($_POST['filter']['b']['from1']['booking_date']))? $_POST['filter']['b']['from1']['booking_date'] : "" ;?> - <?php echo $dt2=(isset($_POST['filter']['b']['to1']['booking_date']))? $_POST['filter']['b']['to1']['booking_date'] : "" ;?>, Hotel: <?=$hotel_name?>, Travel Agent: <?=$agent_name?></span></p>
</div>
<?php } ?>

<div class="row">
	<table class="table table-bordered">
        <?php  
            if(isset($all_bookings) && count($all_bookings)>0){
            ?> 
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Hotel Name</th>
                    <th>Agent Name</th>
                    <th>Booking Amount</th>
                    <th>TAC</th>
                    <th>Agent Payable Amount</th>
                    <th>Booking Date</th>
                </tr>
            </thead>
            <?php                                                                                                                
            foreach($all_bookings as $all_booking):{
            $agent_name=getValue('agents','agent_name',"agent_id = ".$all_booking->agent_id); 
           ?> 

    <tbody>
        <tr>
            <td><?=$all_booking->booking_reference ?></td>
            <td><?=$all_booking->hotel_name ?></td>
            <td><?=$all_booking->agent_name ?></td>
            <td><?=$all_booking->grand_total ?></td>
            <td><?=$all_booking->agent_percentage ?>%</td>
            <td>
                <?php
                $bookingAmt=$all_booking->grand_total;
                $tac=$all_booking->agent_percentage;
                $percentageAmt=($bookingAmt * $tac)/100;
                //$amt=$bookingAmt + $percentageAmt;
                echo $percentageAmt;
                ?>
            </td>
            <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
        </tr>
        <?php } ?>

        </tbody>
        <?php endforeach;  } else{  ?> 
        
     <tbody> 
        <tr >
        <th style="text-align:center;">No data found!</th>
        </tr> 
     </tbody> 
     <?php } ?>
    </table>

</div>