<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pos_purchase_and_stock_entry_model extends CI_Model {
		
	public function getPurchaseEntry(){
		
		$data=$this->db->query("select * from purchase p LEFT JOIN purchase_material pm ON p.purchase_id=pm.purchase_id 
			LEFT JOIN supplier s ON s.supplier_id=p.supplier_id 
			LEFT JOIN raw_material_master rmm ON rmm.material_id=pm.material_id
			LEFT JOIN unit_master um ON um.unit_id=pm.unit_id
			WHERE p.purchase_status='1' AND pm.purchase_material_status='1'
			GROUP BY p.purchase_id");
		return $data->result();
	}	
}
?>