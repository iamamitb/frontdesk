<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Purchase and Stock Entry</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_purchase" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">Purchase History</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_purchase" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Purchase Entry</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addPurchaseData"); ?>
            <?=$this->session->flashdata("msgPurchaseUpdate"); ?>
            <div class="tab-pane active" id="all_purchase"> 
                <!-- Row for Table Starts -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="font-weight:bold;margin-bottom:30px;">All Purchase</h2>
                        <?php if(isset($allRecord) && count($allRecord)>0){ ?> 
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Supplier Name</th>
                                    <th>Material Name</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Rate</th>
                                    <th>Amount</th> 
                                    <th>Date</th>
                                    <th>Challan Number</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                     
                            <tbody>
                            <?php                                                                                                                
                            foreach($allRecord as $purchase_data):{
                            ?>    
                                <tr>
                                    <td><?=$purchase_data->supplier_name?></td>
                                    <td><?=$purchase_data->material_name?></td>
                                    <td><?=$purchase_data->unit_name?></td>
                                    <td><?=$purchase_data->purchase_material_quantity?></td>
                                    <td><?=$purchase_data->purchase_material_rate?></td>
                                    <td><?=$purchase_data->purchase_material_amount?></td>
                                    <td><?=date('d-M-Y', strtotime($purchase_data->purchase_date))?></td>
                                    <td><?=$purchase_data->purchase_bill_number?></td>
                                    <td>
                                        <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" data-toggle="modal" onclick="editPurchase(<?=$purchase_data->purchase_id?>)" data-target="#edit-purchase">Edit</a></li>
                                            <li><a onclick="purchaseDelete(<?=$purchase_data->purchase_id?>)" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                        </div>
                                    </td>
                                </tr>
                               <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any purchase entry.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>

                    </div>
                </div>
               <!-- Row for Table ends -->
            </div>

            <div class="tab-pane" id="add_purchase"> 
                <form name="purchase_form" method="post" id="purchase_form" action="<?php echo base_url(); ?>pos_purchase_and_stock_entry/addPurchase">
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Purchase</h2>

                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Supplier Name <span class="error">*</span></label>
                                <div class="col-md-9">
                                <?=get_data_dropdown("supplier","supplier_id","supplier_name","supplier_status = '1'","","supplier_id","supplier_id","required","","","")?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Purchase Date <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="purchase_date" name="purchase_date" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:30px;">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Challan Number <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="bill_number" name="bill_number" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row" style="margin-top:30px;">
                    <h3 style="font-weight:bold;margin-bottom:60px;text-align:center;">Add Material</h3>
                    <input type="hidden" value="1" id="rowID" name="rowID"/>
                    <div class="row"> 
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Material <span class="error">*</span></label>
                                <?=get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","","materials_1","materials[]","required","","","matClss");?> 
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Store <span class="error">*</span></label>
                                <?=get_data_dropdown("pos_store","pos_store_id","pos_store_name","pos_store_status='1'","","store_1","store[]","required","","","");?> 
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Quantity <span class="error">*</span></label>
                                <input type="text" id="qty_1" name="qty[]" onkeyup="calAmt()" required class="form-control error1"><span class="error_msg1" style="color:red;"></span>
                            </div>

                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Unit <span class="error">*</span></label>
                                <?=get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","","units_1","units[]","required","","","");?> 
                            </div>

                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Rate <span class="error">*</span></label>
                                <input type="text" id="rate_1" name="rate[]" onkeyup="calAmt()" required class="form-control error2">
                                <span class="error_msg2" style="color:red;"></span>
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Amount <span class="error">*</span></label>
                                <input type="text" id="amount_1" name="amount[]" required class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-1"> <br>
                            <button type="button" class="btn btn-success" onclick="addMoreRows(this.form);">+</button>
                        </div>
                    </div>
                </div>
                <div id="addedRows"></div>
                </div>
                
<div class="row" style="margin-top:30px;">
    <div class="row"> 
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="form-group">
                <label>Discount % <span class="error">*</span></label>
                <input type="text" id="purchase_discount" name="purchase_discount" required class="form-control error7"><span class="error_msg7" style="color:red;"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Paid Amount <span class="error">*</span></label>
                <input type="text" id="paid_amount" name="paid_amount" required class="form-control error8"><span class="error_msg8" style="color:red;"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Total Amount <span class="error">*</span></label>
                <input type="text" id="total_amount" name="total_amount" required class="form-control" readonly>
            </div>
        </div>
    </div>
</div>           
</div>

    <div class="row">
    
        <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
            <button class="btn btn-success btn-lg m-b-5">Add Purchase Entry</button>
        </div>
    </div>
    </form>



    </div>
</div>
</div>
<!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<!-- modal --> 
<div id="edit-purchase" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal --> 
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url(); ?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


<?php if($countRecordMaterial==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Material Entry found.To use this section, please go to POS Raw Material Master and add Material information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_raw_material_master';
        });
<?php } ?> 

<?php if($countRecordUnit==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Unit Entry found.To use this section, please go to POS Unit Master and add Unit information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_unit_master';
        });
<?php } ?>   

<?php if($countRecordSupplier==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "No Supplier Entry found.To use this section, please go to POS Supplier and add Supplier information", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>pos_supplier_master';
        });
<?php } ?>


$(document).on('keypress', '.error1', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg1').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error2', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg2').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error3', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg3').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error4', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg4').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error5', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg5').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error6', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg6').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error7', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg7').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error8', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg8').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error9', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg9').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});

$(document).on('keypress', '.error10', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg10').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});


$("#purchase_date").datepicker({
    maxDate: '0', minDate: '-3'
});

function editDatePickr(id){
    //alert(id);
    $("#"+id).datepicker({
      //maxDate: "02/26/2016"
    });
}

$(document).ready(function() {
    $("#purchase_form").validate({
         rules: {
            
            supplier_id: {
                required: true
            },
            purchase_date: {
                required: true
            },
            bill_number: {
                required: true
            }
            
         },
         messages: {
            supplier_id: {
                    required: "Please select a supplier"
                },    
            purchase_date: {
                    required: "Please enter purchase date"
                },
            bill_number: {
                    required: "Please enter bill number"
                }         
         },
        
    });

});



function calAmt()
{
   var rowid=document.getElementById("rowID").value;
    var sum =0;
     
     if(qty!=''){
       for (var j = 1; j <= rowid; j++) {

            var qty = $('#qty_'+j).val(); 
            var rt = $("#rate_"+j).val();
            /*alert(qty);
            alert(rt);*/
                if(qty != 0 && qty != '' && rt != 0 && rt != ''){
                    result=(parseInt(rt)*parseInt(qty));
                    $('#amount_'+j).val(result);
                    sum=parseInt(sum)+parseInt(result);
                   // alert(result);
                }  
        }
        $('#total_amount').val(sum);
    }        
}



$(document).on('keyup', '.editRate', function(){
    calEditAmt();
});

function calEditAmt()
{
   var editRowid=document.getElementById("rowEditID").value;
    
       for (var v = 1; v <= editRowid; v++) {

            var qty = $('#qnty_'+v).val(); 
            var rt = $("#rateEdt_"+v).val();
            //alert(qty);
            //alert(rt);
             
                if(qty != 0 && qty != '' && rt != 0 && rt != ''){
                    result=(parseInt(rt)*parseInt(qty));
                    $('#amnt_'+v).val(result);
                     //sumE=parseInt(sumE)+parseInt(result);
                    //alert(result);
                    
                }  
        }
       
        
      
}

//================ Add rows ====================

 function addMoreRows(frm) {
    var rowCount = document.getElementById("rowID").value;

    rowCount ++;

     var materialName='<?php echo get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","","materials_'+rowCount+'","materials[]","required","","","matClss") ?>';
     var storeName='<?php echo get_data_dropdown("pos_store","pos_store_id","pos_store_name","pos_store_status='1'","","store_'+rowCount+'","store[]","required","","","") ?>';
     var UnitName='<?php echo get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","","units_'+rowCount+'","units[]","required","","","") ?>';
     

     var recRow = '<div class="row" id="'+rowCount+'" style="margin-top:15px;"><div class="col-md-12"><div class="col-md-3"><div class="form-group">'+ materialName +'</div></div><div class="col-md-3"><div class="form-group">'+ storeName +'</div></div><div class="col-md-1"><div class="form-group"><input type="text" class="form-control error3" id="qty_'+rowCount+'" name="qty[]" required><span class="error_msg3" style="color:red;"></span></div></div><div class="col-md-1"><div class="form-group">'+ UnitName +'</div></div><div class="col-md-1"><div class="form-group"><input type="text" class="form-control error4" id="rate_'+rowCount+'" name="rate[]" onkeyup="calAmt()" required><span class="error_msg4" style="color:red;"></span></div></div><div class="col-md-2"><div class="form-group"><input type="text" class="form-control" id="amount_'+rowCount+'" name="amount[]" required readonly></div></div><div class="col-md-1"><button type="button" class="btn btn-success"  style="margin-right:7px;" onclick="addMoreRows(this.form);" >+</button><button type="button"  class="btn btn-danger" onclick="removeRow('+rowCount+');calAmt();" >-</button></div></div></div>';

    jQuery('#addedRows').append(recRow);

    $('#rowID').val(rowCount);

}


function removeRow(removeNum) {
    //alert(removeNum);
    jQuery("#"+removeNum).remove();

    var rowCount = document.getElementById("rowID").value;

    rowCount --;
    $('#rowID').val(rowCount);
}



function editPurchase(idp)
{
    var idp = idp;
   
    $.ajax({
        url:"<?php echo site_url('pos_purchase_and_stock_entry/editPurchase');?>/"+idp,
        type: 'POST',
        dataType: "json",
        data: {'idp': idp},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-purchase').html(data.item);
            
        }      
    });
}



$('#edit_purchase').click(function(){

   var purchaseID = $("#purchaseID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_purchase_and_stock_entry/updatePurchase",
        type: 'POST',
        dataType: "json",
        data: {purchaseID : purchaseID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function purchaseDelete(purchase_id) {
    //alert(purchase_id); 
    swal({
        title: "Are you sure?",
        text: "This purchase entry will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_purchase_and_stock_entry/purchaseDelete",
            type: 'POST',
            dataType: "json",
            data: {'purchase_id': purchase_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}



</script>
