<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_purchase_and_stock_entry extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_purchase_and_stock_entry_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);

		$data['allRecord']=$this->pos_purchase_and_stock_entry_model->getPurchaseEntry();
		$data['countRecordMaterial']=count_rows("raw_material_master","material_status = '1'");
        $data['countRecordUnit']=count_rows("unit_master","unit_status = '1'");
        $data['countRecordSupplier']=count_rows("supplier","supplier_status = '1'");

		$this->template->title('Purchase and Stock Entry','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_purchase_and_stock_entry',$data);
				
	}

	public function addPurchase(){
		$supplier_id=$this->input->post('supplier_id'); 
		$purchase_date=$this->input->post('purchase_date');
		$date=dateformate($purchase_date,'Y-m-d h:m:i',"-"); 
		$bill_number=$this->input->post('bill_number');
		$total_amount=$this->input->post('total_amount');
		$purchase_discount=$this->input->post('purchase_discount');
		$paid_amount=$this->input->post('paid_amount');
		$hotel_id=$this->session->userdata('hotel_id');

		$data=array('hotel_id'=>$hotel_id,
					'supplier_id'=>$supplier_id,
					'purchase_date'=>$date,
					'purchase_bill_number'=>$bill_number,
					'total_amount'=>$total_amount,
					'purchase_discount'=>$purchase_discount,
					'paid_amount'=>$paid_amount,
					'purchase_status'=>'1'
					);
		
		//print_r($data); die();
		  
			$insertID=insertValue('purchase',$data); 

			$purchase_id=$insertID;
			$material_id=$this->input->post('materials');
			$pos_store_id=$this->input->post('store');
			$unit_id=$this->input->post('units');
			$quantity=$this->input->post('qty');
			$rate=$this->input->post('rate');
			$amount=$this->input->post('amount');
			$record_count=count($material_id);	
			//print_r($ingredientInfo); die();

			for ($i=0; $i < $record_count; $i++) { 
			
				$purchase_material_data = array('hotel_id'=>$hotel_id,
												'purchase_id'=>$purchase_id,
												'material_id'=>$material_id[$i],
												'pos_store_id'=>$pos_store_id[$i],
												'unit_id'=>$unit_id[$i],
												'purchase_material_rate'=>$rate[$i],
												'purchase_material_quantity'=>$quantity[$i],
												'purchase_material_amount'=>$amount[$i],
												'purchase_material_status'=>'1'
											);
				$result=insertValue('purchase_material',$purchase_material_data);
					
			}
		 	
		
		
		$this->session->set_flashdata('addPurchaseData','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Purchase Data.
            </div>');
		
		
		redirect('pos_purchase_and_stock_entry#');
		
	}

	public function editPurchase($idp) { 
		$purchase=getSingle('purchase','purchase_id = '.$idp,'','','','');
		
		$purchaseInfo=getData('purchase_material','purchase_id = '.$idp);
		$html['item']='';
		$html['item'].='<div class="modal-dialog" style="width:85%;">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Purchase</h3>
			    </div>
		    <form method="post" id="purchase_form_edit" action="'.base_url().'pos_purchase_and_stock_entry/updatePurchase" >
			<input type="hidden" id="purchaseID" name="purchaseID" value="'.$purchase->purchase_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		        <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Supplier Name <span class="error">*</span></label>
		              '.get_data_dropdown("supplier","supplier_id","supplier_name","supplier_status='1'","$purchase->supplier_id","supplier_id","supplier_id","required","","","").'
		            </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Purchase Date <span class="error">*</span></label>
		              <input type="text" id="purchase_date_edit" name="purchase_date" class="form-control" value="'.$purchase->purchase_date.'" onclick="editDatePickr(this.id)" required>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		        <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Challan Number <span class="error">*</span></label>
		              <input type="text" id="bill_number" name="bill_number" class="form-control" value="'.$purchase->purchase_bill_number.'" required>
		            </div>
		          </div>
		        </div>

		        <div class="row" style="margin-top:30px;">
                    <h3 style="font-weight:bold;margin-bottom:60px;text-align:center;">Add Ingredients</h3>';

		    if(isset($purchaseInfo) && count($purchaseInfo)>0){
		    	$k=1;
		    $html['item'].='<div class="row">
                        <div class="col-md-12">
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Material <span class="error">*</span></label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Store <span class="error">*</span></label>
                                </div>
                            </div>

                             <div class="col-md-1">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Quantity <span class="error">*</span></label>
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="form-group">
                                <label for="exampleInputEmail1">Unit <span class="error">*</span></label>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Rate <span class="error">*</span></label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Amount <span class="error">*</span></label>
                                </div>
                            </div>

                            <!-- <div class="col-md-1">
                                <br>
                                 <button type="button" class="btn btn-success" onclick="editRows(this.form);" >Add</button> 
                            </div> -->
                        </div>
                    </div>';	
            foreach ($purchaseInfo as $key => $valueI) {    $c=COUNT($purchaseInfo);
		     $html['item'].='<div class="row" id="'.$k.'">
		     			<input type="hidden" value="'.$c.'" id="rowEditID" name="rowEditID"/>
		     			<input type="hidden" value="'.$valueI->purchase_material_id.'" id="purchaseMaterialID" name="purchaseMaterialID[]"/>
                        <div class="col-md-12">
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.get_data_dropdown("raw_material_master","material_id","material_name","material_status='1'","$valueI->material_id","material_$k","materials[]","required","","","").'    
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    '.get_data_dropdown("pos_store","pos_store_id","pos_store_name","pos_store_status='1'","$valueI->pos_store_id","store_$k","store[]","required","","","").'    
                                </div>
                            </div>

                             <div class="col-md-1">
                                <div class="form-group">
                                    <input type="text" class="form-control editRate error6" id="qnty_'.$k.'" name="qty[]" value="'.$valueI->purchase_material_quantity.'"  required>
                                	<span class="error_msg6" style="color:red;"></span>
                                </div>
                            </div>

                             <div class="col-md-2">
                                <div class="form-group">
                                    '.get_data_dropdown("unit_master","unit_id","unit_name","unit_status='1'","$valueI->unit_id","unit_$k","units[]","required","","","").'        
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <input type="text" class="form-control editRate error5" id="rateEdt_'.$k.'" name="rate[]" value="'.$valueI->purchase_material_rate.'"  required>
                                    <span class="error_msg5" style="color:red;"></span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                   <input type="text" class="form-control" id="amnt_'.$k.'" name="amount[]" value="'.$valueI->purchase_material_amount.'"  required readonly>
                                </div>
                            </div>

                            <!-- <div class="col-md-1">
                                <br>
                                 <button type="button" class="btn btn-success" onclick="editRows(this.form);" >Add</button> 
                            </div> -->
                        </div>
                    </div>'; 
             $k++;          
		     }}else{
		    /*$html['item'].='<div class="row" style="margin-top:30px;">No Data Found!</div>';*/
		     }   
		    $html['item'].='<div id="editedRows"></div>
		    <div class="row" style="margin-top:30px;">
		    <div class="row"> 
		    <div class="col-md-12">
		        <div class="col-md-4">
		            <div class="form-group">
		                <label>Discount % <span class="error">*</span></label>
		                <input type="text" id="purchase_discount" name="purchase_discount" value="'.$purchase->purchase_discount.'" required class="form-control error9"><span class="error_msg9" style="color:red;"></span>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="form-group">
		                <label>Paid Amount <span class="error">*</span></label>
		                <input type="text" id="paid_amount" name="paid_amount" value="'.$purchase->paid_amount.'" required class="form-control error10"><span class="error_msg10" style="color:red;"></span>
		            </div>
		        </div>
		        <div class="col-md-4">
		            <div class="form-group">
		                <label>Total Amount <span class="error">*</span></label>
		                <input type="text" id="total_amount" name="total_amount" value="'.$purchase->total_amount.'" required class="form-control" readonly>
		            </div>
		        </div>
		    </div>
		</div>           
		</div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_purchase" id="edit_purchase" class="btn btn-success waves-effect waves-light">Update Purchase Entry</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}


	public function updatePurchase() {
		$purchaseID=$this->input->post('purchaseID'); 
		$supplier_id=$this->input->post('supplier_id'); 
		$purchase_date=$this->input->post('purchase_date');
		$date=dateformate($purchase_date,'Y-m-d h:m:i',"-"); 
		$bill_number=$this->input->post('bill_number');
		$total_amount=$this->input->post('total_amount');
		$purchase_discount=$this->input->post('purchase_discount');
		$paid_amount=$this->input->post('paid_amount');

		$data=array(
					'supplier_id'=>$supplier_id,
					'purchase_date'=>$date,
					'purchase_bill_number'=>$bill_number,
					'total_amount'=>$total_amount,
					'purchase_discount'=>$purchase_discount,
					'paid_amount'=>$paid_amount,
					'purchase_status'=>'1'
					);


		//print_r($data); die();

  		$purchaseUpdate=updateDataCondition('purchase',$data,'purchase_id = '.$purchaseID);

			$purchaseMaterialID=$this->input->post('purchaseMaterialID');
			$material_id=$this->input->post('materials');
			$pos_store_id=$this->input->post('store');
			$unit_id=$this->input->post('units');
			$quantity=$this->input->post('qty');
			$rate=$this->input->post('rate');
			$amount=$this->input->post('amount');
			$record_count=count($material_id);	
			//echo $record_count; die();
			
			for ($i=0; $i < $record_count; $i++) { 

				$purchase_material_data = array(
								'material_id'=>$material_id[$i],
								'pos_store_id'=>$pos_store_id[$i],
								'unit_id'=>$unit_id[$i],
								'purchase_material_rate'=>$rate[$i],
								'purchase_material_quantity'=>$quantity[$i],
								'purchase_material_amount'=>$amount[$i],
								'purchase_material_status'=>'1'
							);

				$purchase_material_update=updateDataCondition('purchase_material',$purchase_material_data,'purchase_material_id = '.$purchaseMaterialID[$i]); 
				//echo $this->db->last_query();
				
			} 
			
	               
  		$this->session->set_flashdata('msgPurchaseUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the purchase entry.
            </div>');	
                          
  		redirect('pos_purchase_and_stock_entry#');

	}


	public function purchaseDelete() { 
		
		$purchase_id=$_POST['purchase_id'];		
		$data['purchase_status']='0';			
		$purchase_delete=updateDataCondition('purchase',$data,'purchase_id = '.$purchase_id);
		
		$purchaseMaterialData['purchase_material_status']='0';			
		$purchase_material_delete=updateDataCondition('purchase_material',$purchaseMaterialData,'purchase_id = '.$purchase_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	
	

}
