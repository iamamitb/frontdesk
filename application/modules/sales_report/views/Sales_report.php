<link href="<?=base_url()?>themes/css/views/sales_report.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Sales Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  

<!-- Panel Starts -->
<div class="row">
    <div class="panel panel-default">
<div class="panel-heading">
    <h3 class="panel-title">Sales Report for your Account</h3>
</div>
 <?=$this->session->flashdata("addmsgPayDue"); ?>
 
<div id="alert_msg"></div>  
<div class="panel-body">
    
    <!-- Row for Custom Filter Starts -->
    <?php //if(isset($all_bookings) && count($all_bookings)>0){ ?>    
    <div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
        <div class="col-md-12">
            <form action="" method="post" id="salesReport" >
            <input name="action" type="hidden" value="filter" />
            <input name="csv" type="hidden" id="csv" value="" />
            <div class="col-md-2">
                <div class="form-group">
                    <label for="exampleInputEmail1">From Date</label>
                    <input type="text" value="<?php echo $fromDate=(isset($_POST['filter']['b']['from1']['checkin_date']))? $_POST['filter']['b']['from1']['checkin_date'] : "" ;?>" name="filter[b][from1][checkin_date]" id="from_booking_date" class="form-control" placeholder="" readonly>
                       
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                <label for="exampleInputEmail1">To Date</label>
                <input type="text" value="<?php echo $toDate=(isset($_POST['filter']['b']['to1']['checkin_date']))? $_POST['filter']['b']['to1']['checkin_date'] : "" ;?>" name="filter[b][to1][checkin_date]" id="to_booking_date" placeholder="" class="form-control" readonly>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                <label for="exampleInputEmail1">Travel Agent</label>
                <?php  $ta=(isset($_POST['filter']['b']['equal']['agent_id']))? $_POST['filter']['b']['equal']['agent_id'] : "" ;?>
                <?=get_data_dropdown("agents","agent_id","agent_name","account_id = ".$this->session->userdata('account_id')." and agent_status='1' and agents_type='1'",$ta,'agent_id',"filter[b][equal][agent_id]"); ?>
                </div>
            </div>

            <div class="col-md-1">
                <div class="form-group">
                <label for="exampleInputEmail1">Users</label>
                <?php  $uID=(isset($_POST['filter']['b']['equal']['users_id']))? $_POST['filter']['b']['equal']['users_id'] : "" ;?>  
                <?=get_data_dropdown("users","id","full_name","account_id = ".$this->session->userdata('account_id')." and status='1'",$uID,'id',"filter[b][equal][users_id]","","","",""); ?> 
                  
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                <label for="exampleInputEmail1">Hotel Name</label>
                <?php  $hID=(isset($_POST['filter']['h']['equal']['hotel_id']))? $_POST['filter']['h']['equal']['hotel_id'] : "" ;?>  
                <?=get_data_dropdown("hotels","hotel_id","hotel_name","account_id = ".$this->session->userdata('account_id')." and hotels_status='1'",$hID,'hotel_id',"filter[h][equal][hotel_id]","","","",""); ?> 
                                                                  
                </div>
            </div>

            <div class="col-md-3" style="top:25px;">
                <div class="form-group">
                <button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
                </div>
            </div>
            
            </form>
        </div>
    </div>
    <?php //} ?>
    <!-- Row for Custom Filter Ends -->

<!-- Row for Table Starts -->
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php if(isset($all_bookings) && count($all_bookings)>0){ ?> 
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Mobile Number</th>
                    <th>Guest Name</th>
                    <th>Agency Name</th>
                    <th>Hotel Name</th>
                    <th>Grand Total</th>
                    <th>Deposit</th>
                    <th>Due</th>
                    <th>Booking Date</th>
                    <th>Checkin Date</th>
                    <th>Checkout Date</th>
                    <th>Status</th>
                    <th>Manage</th>
                </tr>
            </thead>

                                             
                <tbody>
                <?php                                                                                                                
                foreach($all_bookings as $all_booking):{
                    $agent_id=$all_booking->agent_id; 
                    $booking_id=$all_booking->booking_id;
                    $dueAmt=$all_booking->grand_total - $all_booking->totalAdvance;
                    
                ?>
                <tr>
                    <td><?=$all_booking->booking_reference ?></td>
                    <td><?=$all_booking->phone ?></td>
                    <td><?=$all_booking->name ?></td>
                    <td><?=getValue("agents","agent_name","agent_id = ".$agent_id);?></td>
                    <td><?=$all_booking->hotel_name ?></td>
                    <td><?=$all_booking->grand_total?></td>
                    <td><?=$all_booking->totalAdvance?></td>
                    <td><?=$dueAmt?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->checkin_date)); ?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->checkout_date));?></td>
                    <td><?php if($all_booking->booking_status=='1'){
                                echo "Booked";
                            }elseif($all_booking->booking_status=='2'){
                                echo "Hold";
                            }elseif($all_booking->booking_status=='3'){
                                echo "Checkin";
                            }elseif($all_booking->booking_status=='4'){
                                echo "Checkout";
                            }elseif($all_booking->booking_status=='5'){
                                echo "No Show";
                            }elseif($all_booking->booking_status=='6'){
                                echo "Cancel";
                            }

                        ?></td>
                    <td>
                        <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="<?=base_url()?>bill/<?=$all_booking->booking_id?>" target="_blank">Print Bill</a></li>
                            <?php if($all_booking->booking_status=='1' || $all_booking->booking_status=='3'){ ?>
                            <li><a href="<?=base_url()?>reservation/edit_booking/<?=$all_booking->booking_id?>" target="_blank">Edit</a></li>
                            <?php } ?>
                            <?php if($dueAmt>0){ ?>
                            <li><a href="#" data-toggle="modal" data-target="#pay-due-modal" onclick="payDue(<?php echo "{$all_booking->booking_id},{$all_booking->agent_id}";?>)"> Pay Due</a></li>
                            <?php } ?>
                            <?php if($all_booking->totalAdvance>0){ ?>    
                            <li><a href="#" data-toggle="modal" data-target="#paydue-history-modal" onclick="payDueHistory(<?php echo $all_booking->booking_id;?>)">Pay Due History</a></li>
                            <?php }
                             if($all_booking->booking_status=='2'){ ?>
                            <li><a href="#" data-toggle="modal" data-target="#change-status-modal" onclick="changeStatus(<?php echo $all_booking->booking_id;?>)">Change Status</a></li>
                            <?php } ?>
                            <li class="divider"></li>
                            <?php if($all_booking->booking_status==1 || $all_booking->booking_status==2){ ?>
                            <li><a href="<?=base_url()?>sales_report/cancel_booking/<?=$all_booking->booking_id?>" target="_blank">Cancel Booking</a></li>
                            <?php } ?>
                        </ul>
                        </div>
                    </td>
                </tr>
               <?php } endforeach;  ?>   
            </tbody>
        </table>

    </div>
</div>
<!-- Row for Table ends -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->

<div class="col-md-12" style="margin-top:25px;">
    <div align="center">
        <button id="printReport" class="btn btn-default m-b-5" >Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No data found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>

<!-- Page content ends -->

<!-- Change Status modal starts -->
<div id="change-status-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
       <div class="modal-content"> 
        <div class="modal-header"> 
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
            <h3 class="modal-title"><b>Change Status</b></h3> 
        </div>
        <input type="hidden" value="" id="booking_id" name="booking_id"> 
        <div class="checkbox checkbox-danger">
            <input id="book" type="checkbox"  >
            <label for="checkbox3">Book</label>
        </div>
        <div class="checkbox checkbox-warning">
           <input id="hold" type="checkbox" >
            <label for="checkbox5">Hold</label>
        </div>

        
        <div id="success_msg">
        </div>    
        


    </div>
</div>
</div>
<!-- Change Status modal ends -->

<!-- Pay Due History modal starts -->
<div id="paydue-history-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- Pay Due History modal ends -->

<!-- Pay Due modal starts -->   
<div id="pay-due-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h3 class="modal-title"><b>Pay Due Amount</b></h3> 
            </div>

            <form name="payDueform" id="payDueform"> 
            <input type="hidden" value="" id="taID" name="taID">
            <input type="hidden" value="" id="bookingID" name="bookingID">    
            <div class="modal-body"> 
                <div class="row"> 

                   
                <div class="row"> 
                     <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="field-1" class="control-label">Grand Total <span class="error">*</span></label> 
                            <input type="text" class="form-control" id="grandTotal" name="grandTotal" value="" readonly>
                        </div> 
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="field-1" class="control-label">Due <span class="error">*</span></label> 
                            <input type="text" class="form-control" id="due" name="due" value="<?=$amount->due; ?>" readonly>
                        </div> 
                    </div>
                </div>
                 
                    
                <div class="row"> 
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="field-1" class="control-label">Amount <span class="error">*</span></label> 
                            <input type="text" class="form-control digit_only amount_check" id="payDueAmt" name="payDueAmt" required>
                            <span class="error_msg number_msg" style="color:red"></span> 
                        </div> 
                    </div> 
                    <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="field-2" class="control-label">Date <span class="error">*</span></label> 
                            <input type="text" class="form-control" id="payDueDt" name="payDueDt" > 
                        </div> 
                    </div>
                <div>

                <div class="row"> 
                     <div class="col-md-6"> 
                        <div class="form-group"> 
                            <label for="field-1" class="control-label">Type</label> 
                            <?php echo get_payment_type_list(); ?> 
                        </div> 
                    </div>
                </div>

                
                <!-- Payment type bank related fields starts -->
                <div class="col-md-6" >
                  <div id="cheque_details" >
                    <div class="col-md-6" id="chq_details">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Cheque Number </label>
                        <input type="text" class="form-control only_digit" id="cheque_no" name="cheque_no" placeholder="Cheque Number" maxlength="10" >
                      </div>
                    </div>
                    <div class="col-md-6" id="dd_details">
                      <div class="form-group">
                        <label for="field-1" class="control-label">DD Number </label>
                        <input type="text" class="form-control only_digit" id="dd_no" name="dd_no" placeholder="DD Number" maxlength="10">
                      </div>
                    </div>
                    
                  </div>
                  <div id="cc_details" >
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Card Number </label>
                        <input type="text" class="form-control only_digit" name="card_no" id="card_no" placeholder="Card Number" maxlength="10">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Expiry Date </label>
                        <input type="text" class="form-control" name ="expiry_date" id="expiry_date" placeholder="Expiry Date mm/yy">
                      </div>
                    </div>
                  </div>
                  <div id="neft_details" >
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Account Number </label>
                        <input type="text" class="form-control only_digit" name="bank_account_no" id="bank_account_no" placeholder="Account Number" maxlength="10">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">IFSC Code</label>
                        <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="IFSC Code">
                      </div>
                    </div>
                   
                  </div>

                  <div class="col-md-6" id="bank_details">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Bank Name </label>
                        <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name">
                      </div>
                    </div>


                </div>
                <!-- Payment type bank related fields ends -->

                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="form-group"> 
                            <label for="field-2" class="control-label">Remarks</label>
                            <textarea name="payDueRmks" id="payDueRmks" class="form-control autogrow"></textarea> 
                            
                        </div> 
                    </div>
                </div>
                </div> 
             
            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                <button type="button" id="payduebtn" class="btn btn-success waves-effect waves-light">Save changes</button> 
            </div> 
        </form>
        </div> 
    </div>
</div>
<!-- Pay Due modal ends -->




  
  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script> 
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
    // check in and check out date picker...
        $("#payDueDt").datepicker({ dateFormat: 'dd-mm-yy', maxDate:0 });
        $("#to_booking_date").datepicker({ dateFormat: 'dd-mm-yy'});

        $("#from_booking_date").datepicker({ dateFormat: 'dd-mm-yy' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to_booking_date").datepicker( "option", "minDate", minValue );
        });

        $("#downloadReport").click(function() {
             $( "#csv" ).val(1);
             $( "#salesReport" ).submit();
             $( "#csv" ).val('');
       });
        $("#printReport").click(function() {
            $( "#salesReport" ).attr("target","_blank");
            $( "#salesReport" ).attr("action","<?=base_url()?>sales_report/print_report");
            $( "#salesReport" ).submit();
            $( "#salesReport" ).removeAttr("target");
            $( "#salesReport" ).attr("action"," ");
       });


    $(function () {
         $('#row_bnkDtl').hide();
         $('#payment_type').change(function () {
             $('#row_bnkDtl').hide();
             if (this.options[this.selectedIndex].value == 'cheque') {
                 $('#row_bnkDtl').show();
             }
         });
    });

    function payDue(bkid,taid){
        var bkid = bkid;
        var taid = taid;
        $("#bookingID").val(bkid);
        $("#taID").val(taid);

        $.ajax({
            url:"<?php echo site_url('sales_report/fetchPayDue');?>/",
            type: 'POST',
            data: {'booking_id': bkid}, 
            dataType: "json", 
            success:function(data)
            { 
                //alert(data['grand_total']) ;
                $("#grandTotal").val(data['grand_total']); 
                $("#due").val(data['due']); 
            }      
        });

    }

      

    
       


    $('#payduebtn').click(function() {
        var bookingID = $('#bookingID').val();
        var grandTotal = $('#grandTotal').val();
        var payDueAmt = $('#payDueAmt').val();
        var payDueDt = $('#payDueDt').val();
        var payDueRmks = $('#payDueRmks').val();
        var paymentType = $('#payment_type').val();
        var bankName = $('#bank_name').val();
        var cardNo = $('#card_no').val();
        var chequeNo = $('#cheque_no').val();
        var ddNo = $('#dd_no').val();
        var bankAccNo = $('#bank_account_no').val();
        var ifsc = $('#ifsc_code').val();
        var expiryDate = $('#expiry_date').val();

        var dataString = 'bookingID='+ bookingID
                        + '&grandTotal=' +grandTotal
                        + '&payDueAmt=' + payDueAmt
                        + '&payDueDt=' + payDueDt
                        + '&payDueRmks=' + payDueRmks
                        + '&paymentType=' + paymentType
                        + '&bankName=' + bankName
                        + '&cardNo=' + cardNo
                        + '&chequeNo=' + chequeNo
                        + '&ddNo=' + ddNo
                        + '&bankAccNo=' + bankAccNo
                        + '&ifsc=' + ifsc
                        + '&expiryDate=' + expiryDate

       
        $.ajax({
            type: 'POST',
            url:"<?php echo site_url('sales_report/addPayDue');?>/",
            data: dataString,
            dataType: "json",
            success:function(data)
            { 
                if(data.msgType=='alert'){
                $('#pay-due-modal').modal("hide");
                swal({
                        title: "Information",
                        text: "You do not have any due.",
                        type: "info",
                        closeOnConfirm: false,
                        },
                        function(){
                        window.location = "<?=base_url()?>sales_report/";
                     }); 
                 }else if(data.msgType=='addmsg'){
                        window.location = "<?=base_url()?>sales_report/";
                }           
            }      
        });

    }); 



    function payDueHistory(bid){
        var bid = bid;
        //alert(bid);
        $.ajax({
            url:"<?php echo site_url('sales_report/displayPayDueHistory');?>/"+bid,
            type: 'POST',
            dataType: "json",
            data: {'bid': bid},  
            success:function(data)
            { 
                //alert(data) ; 
                $('#paydue-history-modal').html(data.item); 
            }      
        });
    } 

    function changeStatus(bookingid){
        var bookingid = bookingid;
        $("#booking_id").val(bookingid);
        $.ajax({
            url:"<?php echo site_url('sales_report/changeStatus');?>/"+bookingid,
            type: 'POST',
            data: {'bookingid': bookingid},  
            success:function(data)
            { 
                //alert(data) ;
                $('#book').prop('checked', false);
                $('#hold').prop('checked', false); 
                if(data==1){
                    $('#book').prop('checked', true);
                    $('#hold').prop('disabled', true);
                } else if(data==2){
                    $('#hold').prop('checked', true);
                }
            }      
        });
    }

    $('#book').change(function() {
        $('#hold').prop('checked', false);
        $('#hold').prop('disabled', true);
        var bkingID=$("#booking_id").val();
        $.ajax({
            url:"<?php echo site_url('sales_report/updateStatus');?>/"+bkingID,
            type: 'POST',
            dataType: "json",
            data: {'bkingID': bkingID},  
            success:function(data)
            { 
                $('#success_msg').html('<div class="alert alert-success alert-dismissable" style="text-align:center;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>You have successfully changed the booking status.</div>');

            }      
        });

    });   




$(document).ready(function(){
        $("#card_no").mask("9999-9999-9999-9999"); // 
        $("#expiry_date").mask("99/99"); //


        var base_url = $.cookie("base_url");

        $("#cheque_details").hide();
        $("#dd_details").hide();
        $("#chq_details").hide();   
        $("#cc_details").hide();
        $("#neft_details").hide();
        $("#bank_details").hide();

        $('#payment_type').change(function() {
            //alert("change on select..")
            if($('#payment_type').val() != ""){
                /// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
                var payment_type = $('#payment_type').val();

                if(payment_type == "cash" || payment_type == "payment_by_cash" )
                    $("#cheque_details").hide();                    
                    $("#dd_details").hide();
                    $("#chq_details").hide();
                    $("#cc_details").hide();
                    $("#neft_details").hide();  
                    $("#bank_details").hide();    
                
                if(payment_type == "cheque" || payment_type == "demand_draft" || payment_type == "credit_card" || payment_type == "debit_card" || payment_type == "neft" || payment_type == "rtgs"){
                    //$("#bank_details_central_reservation").show();
                    if(payment_type == "cheque" ){
                        $("#cheque_details").show();
                        $("#chq_details").show();
                        $("#bank_details").show();
                        $("#dd_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").hide();      
                    }
                    if(payment_type == "demand_draft" ){
                        $("#cheque_details").show();
                        $("#dd_details").show();
                        $("#bank_details").show();
                        $("#chq_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").hide();
                             
                    }
                    if(payment_type == "credit_card" || payment_type == "debit_card" ){
                        $("#cheque_details").hide();
                        $("#dd_details").hide();
                        $("#chq_details").hide();
                        $("#cc_details").show();
                        $("#neft_details").hide(); 
                        $("#bank_details").hide();      
                    }
                    if(payment_type == "neft" || payment_type == "rtgs" ){
                        $("#cheque_details").hide();
                        $("#dd_details").hide();
                        $("#chq_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").show(); 
                        $("#bank_details").show();     
                    }
                }
                                        

            } else {
            
                $("#cheque_details").hide();                    
                $("#dd_details").hide();
                $("#chq_details").hide();
                $("#cc_details").hide();
                $("#neft_details").hide(); 
                $("#bank_details").hide();     
            }
            
        });
});


$(".digit_only").keypress(function (e) {
    var due = $('#due').val();
    var payDueAmt = $('#payDueAmt').val();
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $('.error_msg').html("Digits Only").show().fadeOut("slow");
        return false;
    } 
});
    


 $(".amount_check").keyup(function(e) {
        var due = parseFloat($('#due').val());
        var payDueAmt = parseFloat($('#payDueAmt').val());
        //alert(due);
        //alert(payDueAmt);
         if(payDueAmt > due){
            $('#payDueAmt').val('');
            $('#payDueAmt').focus();
            $('.number_msg').html("Value can not be greater than due").show().fadeOut(3000);
            return false;
         }
   });



//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_booking_date").val("");
    $("#to_booking_date").val("");
    $('#hotel_id option:selected').removeAttr('selected');
    $('#agent_id option:selected').removeAttr('selected');
    $('#id option:selected').removeAttr('selected');
    window.location.reload(true);
});

$(document).ready(function(){
    var frombookingdate=$("#from_booking_date").val();
    var tobookingdate=$("#to_booking_date").val();
    var hotelid=$("#hotel_id").val();
    var agentid=$("#agent_id").val();
    var usersid=$("#id").val();
    if(frombookingdate != '' || tobookingdate != '' || hotelid != '' || agentid != '' || usersid != ''){    
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }
});


</script>



