<link href="<?=base_url()?>themes/css/views/cancel_booking.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title"><b>Cancel Booking</b></h1></div>
</div>
<!-- Page-Title Ends-->
<?=$this->session->flashdata("msgCancelBooking"); ?> 
<div class="row">
	<div class="col-md-12">
		<div class="col-md-8">
            <table class="table table-bordered">
                <?php
                if(isset($getBookedRoomDetail) && count($getBookedRoomDetail)>0){
                $day=1;
                $total=0; 
                ?>        
                <thead>
                    <tr>
                        <th>
                            <div class="checkbox">
                                <!-- <input id="" type="checkbox">
                                <label for="">
                                    <b>Check to Cancel All</b>
                                </label> -->
                            </div>
                        </th>
                        <th>Room Number</th>
                        <th>Room Type</th>
                        <th>Base Tariff</th>
                        <th>Extra Bed</th>
                        <th>Extra Charge</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php 
                        foreach($getBookedRoomDetail as $bookedRoomDetail){
                            $booking_id=$bookedRoomDetail->booking_id;
                        ?>
                    <tr class="alert alert-warning"><td colspan="6" align="center">Day <?php echo $day ?></td></tr>     
                    <tr>
                        <td><?php echo date('d-M-Y', strtotime($bookedRoomDetail->stay_date))?></td>
                        <td>
                            <div>
                            <!--<div class="checkbox">
                                 <input id="checkbox1" type="checkbox"> -->
                                <label for="checkbox1">
                                    <?=$bookedRoomDetail->room_number?>
                                </label>
                            </div>
                        </td>
                        <td>
                            <span><?=$bookedRoomDetail->room_name?></span>
                        </td>
                        <td>
                            <?php echo $bookedRoomDetail->base_price;
                                  $total=$total+$bookedRoomDetail->base_price;
                            ?>
                        </td>
                        <td><?=$bookedRoomDetail->extra_bed?></td>
                        <td><?=$bookedRoomDetail->extra_bed_charge?></td>
                    </tr>
                    <?php $day++; } } else{ ?>
                    <tr ><th style="text-align:center;">No data found !</th></tr>
                    <?php } ?>


                    <tr><td colspan="6" align="center" style="font-size:25px;font-weight:bold;">Total: <?=$total?></td></tr>
                    
                    <form name="calcelform" id="calcelform" > 
                    <input type="hidden" value="<?=$booking_id?>" id="bookingID" name="bookingID">   
                    <tr>
                        <td colspan="6" align="center">
                            <div class="form-group">
                                <label for="" class="col-sm-5 control-label">Refund Amount<span class="error">*</span></label>
                                <div class="col-sm-5">
                                  <input type="text" class="form-control" name="refund_amount" id="refund_amount" placeholder="" required="true">
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="6" align="center">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Cancellation Reason</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="cancellation_reason" id="cancellation_reason"></textarea>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="6" align="center">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Collection Point<span class="error">*</span></label>
                                <div class="col-md-10">
                                    <?php 
 echo get_data_dropdown('collection_point','collection_point_id','collection_point_name','account_id = '.$this->session->userdata('account_id').' and collection_point_status=1',"","collectionPt","collectionPt","","","","");
                                    ?>
                                   
                                </div>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="6" align="center">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Payment Type<span class="error">*</span></label>
                                <div class="col-sm-10">
                                    <?php echo get_payment_type_list(); ?> 
                                </div>
                            </div>



                <!-- Payment type bank related fields starts -->
                <div class="col-md-6" >
                  <div id="cheque_details" >
                    <div class="col-md-6" id="chq_details">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Cheque Number </label>
                        <input type="text" class="form-control only_digit" id="cheque_no" name="cheque_no" placeholder="Cheque Number" maxlength="10" >
                      </div>
                    </div>
                    <div class="col-md-6" id="dd_details">
                      <div class="form-group">
                        <label for="field-1" class="control-label">DD Number </label>
                        <input type="text" class="form-control only_digit" id="dd_no" name="dd_no" placeholder="DD Number" maxlength="10">
                      </div>
                    </div>
                    
                  </div>
                  <div id="cc_details" >
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Card Number </label>
                        <input type="text" class="form-control only_digit" name="card_no" id="card_no" placeholder="Card Number" maxlength="10">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Expiry Date </label>
                        <input type="text" class="form-control" name ="expiry_date" id="expiry_date" placeholder="Expiry Date mm/yy">
                      </div>
                    </div>
                  </div>
                  <div id="neft_details" >
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-1" class="control-label">Account Number </label>
                        <input type="text" class="form-control only_digit" name="bank_account_no" id="bank_account_no" placeholder="Account Number" maxlength="10">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="field-2" class="control-label">IFSC Code</label>
                        <input type="text" class="form-control" name ="ifsc_code" id="ifsc_code" placeholder="IFSC Code">
                      </div>
                    </div>
                   
                  </div>

                  <div class="col-md-6" id="bank_details">
                      <div class="form-group">
                        <label for="field-2" class="control-label">Bank Name </label>
                        <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name">
                      </div>
                    </div>


                </div>
                <!-- Payment type bank related fields ends -->




                        </td>
                    </tr>

                    <tr>
                        <td colspan="6" align="center">
                            <button type="button" id="cancelBooking" class="btn btn-primary">Do Cancel</button>
                            <button type="button" class="btn btn-default"><a href="<?=base_url()?>postpone_booking/<?=$booking_id?>" target="_blank">Postpone Booking</a></button>
                        </td>
                    </tr>


            </tbody>        

            </table>    
		</div>
        <?php

        //print_r($getBookingDetail);
        ?>
		<div class="col-md-4"><br>
			<table class="table table-bordered">
                <tbody>
                    <?php if(isset($getBookingDetail)){ 
                        foreach($getBookingDetail as $bookingDetail){
                        ?> 
                	<tr>
                        <td>Guest Name</td>
                        <td><?=$bookingDetail->name?></td>
                    </tr>
                    <tr>
                        <td>Room Rent</td>
                        <td><?=$bookingDetail->total_room_tariff?></td>
                    </tr>
                    <tr>
                        <td>Service Tax</td>
                        <td><?=$bookingDetail->service_tax?></td>
                    </tr>
                    <tr>
                        <td>Luxury Tax</td>
                        <td><?=$bookingDetail->total_luxury_tax?></td>
                    </tr>
                    <tr>
                        <td>Net Total</td>
                        <td><?=$bookingDetail->grand_total?></td>
                    </tr>
                    <tr>
                        <td>Advance Payment</td>
                        <td><?=$bookingDetail->advance?></td>
                    </tr>
                    <tr>
                        <td>Due</td>
                        <td><?=$bookingDetail->due?></td>
                    </tr>
                    <tr>
                        <td>Travel Agent</td>
                        <td><?=$bookingDetail->agent_name?></td>
                    </tr>
                    <tr>
                        <td>TAC Paid</td>
                        <td>
                            <?php
                            $bookingAmt=$bookingDetail->grand_total;
                            $tac=$bookingDetail->agent_percentage;
                            $percentageAmt=($bookingAmt * $tac)/100;
                            //$amt=$bookingAmt + $percentageAmt;
                            echo $percentageAmt;
                            ?>
                        </td>
                    </tr>
                    <?php } } ?>
                </tbody>
            </table>

		</div>
	</div>
</div>

<script src="<?=base_url()?>themes/js/jquery.min.js"></script> 
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/jquery.maskedinput.min.js" ></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url(); ?>themes/js/views/cancel_booking.js" ></script>
