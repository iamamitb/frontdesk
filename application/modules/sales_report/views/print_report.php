<!-- Style for page display -->
<style>
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Sales Report</h1>
</div>
<?php if(!empty($_POST)){ ?>
<div class="row" align="center">
<?php  
$agent_name=(($_POST['filter']['b']['equal']['agent_id'])!="")? getValue('agents','agent_name',"agent_id = ".$_POST['filter']['b']['equal']['agent_id'] ) : "" ;    
$hotel_name=(($_POST['filter']['b']['equal']['hotel_id'])!="")? getValue('hotels','hotel_name',"hotel_id = ".$_POST['filter']['b']['equal']['hotel_id'] ) : "" ;
?>    
<!-- <p><b>Filters: </b><span>Date: <?php echo $dt1=(isset($_POST['filter']['b']['from1']['checkin_date']))? $_POST['filter']['b']['from1']['checkin_date'] : "" ;?> - <?php echo $dt2=(isset($_POST['filter']['b']['to1']['checkin_date']))? $_POST['filter']['b']['to1']['checkin_date'] : "" ;?>, Hotel: <?=$hotel_name?>, Travel Agent: <?=$agent_name?></span></p> -->
</div>
<?php } ?>

<div class="row">
	<?php if(isset($all_bookings) && count($all_bookings)>0){ ?> 
        <table id="datatable" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Mobile Number</th>
                    <th>Guest Name</th>
                    <th>Agency Name</th>
                    <th>Hotel Name</th>
                    <th>Grand Total</th>
                    <th>Deposit</th>
                    <th>Due</th>
                    <th>Booking Date</th>
                    <th>Checkin Date</th>
                    <th>Checkout Date</th>
                    <th>Status</th>
                </tr>
            </thead>

                                             
                <tbody>
                <?php                                                                                                                
                foreach($all_bookings as $all_booking):{
                    $agent_id=$all_booking->agent_id; 
                    $booking_id=$all_booking->booking_id;
                    $dueAmt=$all_booking->grand_total - $all_booking->totalAdvance;

                ?>
                <tr>
                    <td><?=$all_booking->booking_reference ?></td>
                    <td><?=$all_booking->phone ?></td>
                    <td><?=$all_booking->name ?></td>
                    <td><?=getValue("agents","agent_name","agent_id = ".$agent_id);?></td>
                    <td><?=$all_booking->hotel_name ?></td>
                    <td><?=$all_booking->grand_total?></td>
                    <td><?=$all_booking->totalAdvance?></td>
                    <td><?=$dueAmt?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->booking_date)); ?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->checkin_date)); ?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->checkout_date));?></td>
                    <td><?php if($all_booking->booking_status=='1'){
                                echo "Booked";
                            }elseif($all_booking->booking_status=='2'){
                                echo "Hold";
                            }elseif($all_booking->booking_status=='3'){
                                echo "Checkin";
                            }elseif($all_booking->booking_status=='4'){
                                echo "Checkout";
                            }elseif($all_booking->booking_status=='5'){
                                echo "No Show";
                            }elseif($all_booking->booking_status=='6'){
                                echo "Cancel";
                            }

                        ?></td>
                </tr>
               <?php } endforeach;  ?>   
            </tbody>
        </table>
        <?php  } else{ ?>
         <table class="table table-bordered" style="margin-top:25px;">
            <tbody> 
                <tr>
                <th style="text-align:center;">No data found.</th>
                </tr> 
             </tbody> 
         </table>
        <?php } ?>

</div>