<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_report_model extends CI_Model {

  
    public function getBookings($account_id,$search=array(),$csv){
            
            $row=false;
            
            if($csv == ""){
                $sql="SELECT  b.booking_reference,b.booking_id, b.booking_date,b.checkin_date, b.checkout_date,h.hotel_name, g.name,g.phone,t.grand_total,SUM(t.advance) as totalAdvance,t.due,a.agent_name,b.agent_id,b.users_id,b.booking_status
                    FROM  bookings b, hotels h, guest_detail g, users u, transaction t, agents a
                    where b.hotel_id=h.hotel_id 
                    and g.guest_id=b.guest_id
                    and b.booking_id=t.booking_id
                    and b.users_id=u.id
                    and b.agent_id=a.agent_id 
                    and h.account_id=$account_id ";
              }else if ($csv == 1) {
                $sql="SELECT  b.booking_reference as 'Booking ID',b.booking_date as 'Booking Date',b.checkin_date as 'Checkin Date', b.checkout_date as 'Checkout Date',h.hotel_name as 'Hotel Name', g.name as 'Guest Name',g.phone as 'Mobile Number',t.grand_total as 'Grand Total',SUM(t.advance) as 'Deposit',t.due as 'Due',a.agent_name as 'Agency Name',b.booking_status as 'Status'
                    FROM  bookings b, hotels h, guest_detail g, users u, transaction t, agents a
                    where b.hotel_id=h.hotel_id 
                    and g.guest_id=b.guest_id
                    and b.booking_id=t.booking_id
                    and b.users_id=u.id
                    and b.agent_id=a.agent_id 
                    and h.account_id=$account_id ";
              }        

                  
            $where = array();
            if(!empty($search))
            {
            foreach($search as $db=>$dbarray)
            {
                foreach($dbarray as $type=>$typearray)
                {
                    foreach($typearray as $newkey=>$val)
                    {
                        if($val){
                          if($type == 'like')
                          {
                              $where[] = $db.".".$newkey." like '%".$val."%' ";
                          }
                          elseif($type == 'from')
                          {
                              $where[] = $db.".".$newkey." >= '".$val."'";
                          }
                          elseif($type == 'to')
                          {
                              $where[] = $db.".".$newkey." <= '".$val."'";
                          }
                           elseif($type == 'from1')
                            {       $datetime = strtotime($val);
                            $d1 = date('Y/m/d', $datetime);
                              $where[] = $db.".".$newkey." >= '".$d1."'";
                          }
                          elseif($type == 'to1')
                          { $datetime = strtotime($val);
                            $d2 = date('Y/m/d', $datetime);
                              $where[] = $db.".".$newkey." <= '".$d2."'";
                          }
                          elseif($type == 'equal1')
                          { $datetime = strtotime($val);
                            $d2 = date('Y/m/d', $datetime);
                              $where[] = $db.".".$newkey." = '".$d2."'";
                          }
                          elseif($type == 'equal')
                          {
                              $where[] = $db.".".$newkey." = '".$val."'";
                          }
                        }
                    }
                }
            
            }
            }

            if(count($where))
            {
                $sql.="and ".implode(' and ',$where);
            }

            $sql.=" group by t.booking_id";

            $query = $this->db->query($sql);

            if($csv!=0){
            download_report($query,"sales_report.csv");        
            }
            
            if($query->num_rows() > 0)
            {
                foreach($query->result() as $val)
                {
                    $row[]= $val;
                }
                return $row;
            }

        }


    public function insertPayDue($data){
      $this->db->insert('transaction',$data);
      $sql = $this->db->last_query();
      //echo $sql;
    }    


    public  function bookingDetail($bkingid){ 
      $data=$this->db->query("select b.*,a.agent_name,g.name from bookings b,agents a,guest_detail g  where a.agent_id=b.agent_id and b.guest_id=g.guest_id and b.booking_id='".$bkingid."'");
      return $data->result();
    }  

    public  function roomDetail($bkingid){ 
      $data=$this->db->query("select b.*,br.*,brd.*,r.*,brsd.* from bookings b,booked_room_detail brd,booked_room br,rooms r,booked_room_stay_date brsd  where b.booking_id=brd.booking_id and brd.booked_room_id=br.booked_room_id and r.room_id=br.room_id and br.id=brsd.booked_room and b.booking_id='".$bkingid."' and b.booking_status!='4' and b.booking_status!='5' and b.booking_status!='6'");
      return $data->result();
    } 

    public  function getDueAmount($bookingID){ 
      $data=$this->db->query("select grand_total,advance,due from transaction where booking_id='".$bookingID."' and transaction_type='CR' order by transaction_id desc LIMIT 1 ");
      //echo $this->db->last_query();
      return $data->row();

    } 
    
    public  function getTotalAdvance($bookingID){ 
      $data=$this->db->query("select SUM(advance) as totalAdvance from transaction where booking_id='".$bookingID."' and transaction_type='CR'");
      //echo $this->db->last_query();
      return $data->row();

    } 
  
}
?>