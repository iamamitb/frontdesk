<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_report extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('sales_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		//is_logged_in_admin_profile("login"); 
   		 }

	public function index() {
		$search = array();
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		$account_id=$this->session->userdata('account_id');
		if($action=='filter')
		$search=$_POST['filter'];
		
		$data['all_bookings'] = $this->sales_report_model->getBookings($account_id,$search,$csv);
		
		$this->template->title('Sales Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('sales_report',$data);	
	}

	public function print_report(){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
		$search=$_POST['filter'];
		$data['all_bookings']=$this->sales_report_model->getBookings($account_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}

	public function fetchPayDue(){
		$booking_id=$this->input->post('booking_id');
		$due_amount = $this->sales_report_model->getDueAmount($booking_id);
		echo json_encode($due_amount);
		//print_r($due_amount);
	}

	public function addPayDue(){
		$bookingID=$this->input->post('bookingID');
		$payDueAmt=$this->input->post('payDueAmt');
		$grandTotal=$this->input->post('grandTotal');
		$payDueDt=$this->input->post('payDueDt');
		$date=dateformate($payDueDt,'Y-m-d',"-"); 
		$payDueRmks=$this->input->post('payDueRmks');
		$paymentType=$this->input->post('paymentType');
		$bankName=$this->input->post('bankName');
		$cardNo=$this->input->post('cardNo');
		$chequeNo=$this->input->post('chequeNo');
		$ddNo=$this->input->post('ddNo');
		$bankAccNo=$this->input->post('bankAccNo');
		$ifsc=$this->input->post('ifsc');
		$expiryDate=$this->input->post('expiryDate');

		$advance=$this->sales_report_model->getTotalAdvance($bookingID);
		//echo "Advance = ".$advance->totalAdvance;
		$due=$grandTotal - (($advance->totalAdvance)+$payDueAmt);
		
		if($bankName ==""){ $bankName="N/A"; }
		if($chequeNo==""){ $chequeNo="N/A"; }
 		if($ddNo==""){ $ddNo="N/A"; }
 		if($cardNo==""){ $cardNo="N/A"; }
 		if($expiryDate==""){ $expiryDate="N/A"; }
 		if($bankAccNo==""){ $bankAccNo="N/A"; }
 		if($ifsc==""){ $ifsc="N/A"; }
		
		$data=array(
					'booking_id'=>$bookingID,
					'payment_date'=>$date,
					'grand_total'=>$grandTotal,
					'advance'=>$payDueAmt,
					'due'=>$due,
					'payment_type'=>$paymentType,
					'bank_name'=>$bankName,
					'cheque_no'=>$chequeNo,
					'dd_no'=>$ddNo,
					'card_no'=>$cardNo,
					'expiry_date'=>$expiryDate,
					'account_no'=>$bankAccNo,
					'ifsc_code'=>$ifsc,
					'remarks'=>$payDueRmks
                    );
		
		//print_r($data);	
		$transaction_id=insertValue('transaction',$data);

		$fetchDue= getValue('transaction','due','transaction_id = '.$transaction_id);
		//echo $fetchDue;
		//exit();
		if($fetchDue==0){
			$html['type']='sucess';
			$html['msgType']='alert';
		} else{

			$this->session->set_flashdata('addmsgPayDue','<div class="alert alert-success alert-dismissable" style="text-align:center;">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                You have successfully added the due amount.
	            </div>');
			
			$html['msgType']='addmsg';

		}
		
		echo json_encode($html);
	}



	public function displayPayDueHistory($bid) { 
		$payDueHistory=getData('transaction','booking_id = '.$bid);

		$payDueAmountCheck=getValue('transaction','advance','booking_id = '.$bid);

		//echo $payDueAmountCheck;
        
		$html['item']='<div class="modal-dialog modal-full"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h3 class="modal-title"><b>Pay Due History</b></h3> 
            </div> 
            <div class="modal-body"> 
                <div class="row">
                <table class="table table-bordered">';
                 $totalPaidAmount=0;          
        if(isset($payDueHistory) && $payDueAmountCheck>0){

        $html['item'].='<thead>
                <tr>
                <th>Payment Date</th>
                <th>Payment Amount</th>
                <th>Payment Type</th>
                <th>Bank</th>
                <th>Cheque No</th>
                <th>DD No</th>
                <th>Card No</th>
                <th>Expiry Date</th>
                <th>Bank Account No</th>
                <th>IFSC Code</th>
                <th>Remarks</th>
                </tr>
                </thead>
                <tbody>';
       
        foreach ($payDueHistory as $key => $value1) {

	        $paidAmount = $value1->advance; 
	        $paymentType = $value1->payment_type;  
	        if($paymentType=='demand_draft') { $paymentType="Demand Draft"; }   
	        if($paymentType=='credit_card') { $paymentType="Credit Card"; } 
	        if($paymentType=='debit_card') { $paymentType="Debit Card"; } 
	        if($paymentType=='neft') { $paymentType="NEFT"; } 
	        if($paymentType=='rtgs') { $paymentType="RTGS"; } 
	        if($paymentType=='payment_by_company') { $paymentType="Payment by compnay"; } 
	        if($paymentType=='cheque') { $paymentType="Cheque"; } 
	        if($paymentType=='cash') { $paymentType="Cash"; }

        $html['item'].='<tr>
                        <td>'.date('d-M-Y', strtotime($value1->payment_date)).'</td>
                        <td><span style="float:right;">'.$paidAmount.'</span></td>
                        <td>'.$paymentType.'</td>
                        <td>'.$value1->bank_name.'</td>
                        <td>'.$value1->cheque_no.'</td>
                        <td>'.$value1->dd_no.'</td>
                        <td>'.$value1->card_no.'</td>
                        <td>'.$value1->expiry_date.'</td>
                        <td>'.$value1->account_no.'</td>
                        <td>'.$value1->ifsc_code.'</td>
                        <td>'.$value1->remarks.'</td>
                    </tr>';
        			$totalPaidAmount = $totalPaidAmount + $paidAmount;
         			}
        
    	$html['item'].='<tr>
	    					<td colspan="2"><span style="float:right;"><b>Total: </b>'.$totalPaidAmount.'</span></td>
	    					<td colspan="9"></td>
    					</tr>
    			</tbody>';
    	}else{
    	$html['item'].='<tbody> 
                    <tr >
                    <th style="text-align:center;">No data found!</th>
                    </tr> 
                 </tbody>';
                  } 
        $html['item'].='</table>
            </div> 
        </div> 
        <div class="modal-footer"> 
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
        </div> 
        </div> 
    </div>';
		
		echo json_encode($html);

	}


	public function changeStatus(){
		$bookingid=$this->input->post('bookingid');
		$data['getBookingStatus'] = getValue('bookings','booking_status','booking_id = '.$bookingid);
		echo $data['getBookingStatus'];
	
	}

	public function updateStatus(){
		$bkingID=$this->input->post('bkingID');
		$data['booking_status']='1';
		$statusChanged=updateDataCondition('bookings',$data,'booking_id = '.$bkingID);

		$html['type']='sucess';
		echo json_encode($html);
	}

	public function cancel_booking($bkingid){

		$data['getBookingDetail']=$this->sales_report_model->bookingDetail($bkingid);

		$data['getBookedRoomDetail']=$this->sales_report_model->roomDetail($bkingid);

		//print_r($data['getBookingDetail']);
		
		$this->template->title('Cancel Booking','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('cancel_booking',$data);
	}

	public function cancelBooking(){
		$bookingID=$this->input->post('bookingID');
		$refundAmt=$this->input->post('refundAmt');
		$cancellationReason=$this->input->post('cancellationReason');
		$collectionPt=$this->input->post('collectionPt');

		$paymentType=$this->input->post('paymentType');
		$bankName=$this->input->post('bankName');
		$cardNo=$this->input->post('cardNo');
		$chequeNo=$this->input->post('chequeNo');
		$ddNo=$this->input->post('ddNo');
		$bankAccNo=$this->input->post('bankAccNo');
		$ifsc=$this->input->post('ifsc');
		$expiryDate=$this->input->post('expiryDate');
		
		if($bankName ==""){ $bankName="N/A"; }
		if($chequeNo==""){ $chequeNo="N/A"; }
 		if($ddNo==""){ $ddNo="N/A"; }
 		if($cardNo==""){ $cardNo="N/A"; }
 		if($expiryDate==""){ $expiryDate="N/A"; }
 		if($bankAccNo==""){ $bankAccNo="N/A"; }
 		if($ifsc==""){ $ifsc="N/A"; }

 		$data2=array(
					'booking_id'=>$bookingID,
					'collection_point_id'=>$collectionPt,
					'user_id'=>$this->session->userdata('id'),
					'cancel_date'=>date('Y-m-d'),
					'cancel_reason'=>$cancellationReason
					);

		$cancel_id=insertValue('cancel_booking',$data2);
	
		$data=array(
					'cancel_id'=>$cancel_id,
					'advance'=>$refundAmt,
					'payment_type'=>$paymentType,
					'bank_name'=>$bankName,
					'cheque_no'=>$chequeNo,
					'dd_no'=>$ddNo,
					'card_no'=>$cardNo,
					'expiry_date'=>$expiryDate,
					'account_no'=>$bankAccNo,
					'ifsc_code'=>$ifsc,
					'transaction_type'=>'DR'
					);

		$result=insertValue('transaction',$data);

		$data1['booking_status']='6';		
		
		$updateBookingStatus=updateDataCondition('bookings',$data1,'booking_id = '.$bookingID);
		
		$roomDelete=Delete_data('booked_room_detail','booking_id = '.$bookingID);

		$this->session->set_flashdata('msgCancelBooking','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully cancelled the booking.
            </div>');

		redirect('sales_report/cancel_booking');
	
	}
	

}
