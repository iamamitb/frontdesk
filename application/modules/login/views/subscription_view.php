<div class="alert alert-info" style="text-align:center;">
    <b>Your subscription period for using iManagemyhotel Frontdesk and POS is over. You need to renew your subscription and enter an activation code below to continue.</b>
</div>

 <?=$this->session->flashdata("error_msg"); ?>

<div class="wrapper-page">




            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body"><img src="<?=base_url()?>themes/images/immh-logo2.png">
<title>Login to iManageMyHotel</title>

<!-- Page content starts -->  
<form class="form-horizontal m-t-20" action="<?=base_url()?>/login/subscription" method="post">
                    
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Enter Activation Code</label>
                            <input class="form-control input-lg" type="text" required="required" name="renewal_code">
                             <?php echo form_error('renewal_code'); ?>
                        </div>
                    </div>

                  

                    
                    
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Renew Subscription</button>
                        </div>
                    </div>


</form> 

<!-- Page content ends -->  
