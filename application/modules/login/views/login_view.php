<?=$this->session->flashdata("success_msg"); ?>
<div class="wrapper-page">

            <!-- Incorrect password or email address Div. Show it via AJAX, originally display none -->
          <?=$this->session->flashdata("msg"); ?>
          <?=$this->session->flashdata("error_msg"); ?>
            <!-- Incorrect password div ends -->
<div id="forgot_pass_show" class="alert alert-danger alert-dismissable hide" style="text-align:center;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    Please contact your system administrator and request him to reset your password from Users
</div>
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-body"><img src="<?=base_url()?>themes/images/immh-logo2.png">
<title>Login to iManageMyHotel</title>

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="page-title" style="text-align: center;text-transform: uppercase;font-size: 24px;font-weight: bold;margin-top: 24px;margin-bottom:0px;">Login</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<form class="form-horizontal m-t-20" action="" method="post">
                    
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control input-lg" type="text" required name="email" placeholder="Email address">
                             <?php echo form_error('email'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                         
                        <input class="form-control input-lg" type="password" required name="password" placeholder="Password">
                        <?php echo form_error('password'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30" align="center">
                        <div class="col-sm-12" id="forgot_pass">
                            <a href="#"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                        
                    </div>
</form> 

<!-- Page content ends --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script> 
<script>
$(document).on('click', '#forgot_pass', function(e){
     $("#forgot_pass_show").removeClass("hide");
       });

</script>
