<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	
	    public function __construct() {
        parent::__construct();
		$this->load->model('login_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
    }

	public function index()
	{
			$this->template->title('Login','Imanagemyhotel');
			$this->template->set('metaDesc','Imanagemyhotel');
			$this->template->set('metaKeyword','Imanagemyhotel');
			$this->template->set_layout('login_template','front');
			$this->template->build('login_view');

				$this->form_validation->set_error_delimiters("<div class='error' align='center' style='margin-top:5px;'>", "</div>");
				//$this->form_validation->set_rules('email','Email id',  'trim|required|valid_email');
				//$this->form_validation->set_message('valid_email', 'Must be a valid email address');
				$this->form_validation->set_rules('password','Password', 'trim|required');
					if( $this->form_validation->run() == FALSE )
					{
							$this->template->title('Login','Imanagemyhotel');
							$this->template->set('metaDesc','Imanagemyhotel');
							$this->template->set('metaKeyword','Imanagemyhotel');
							$this->template->set_layout('login_template','front');
							$this->template->build('login_view');
					
					}
					else 
					{
						if($query = $this->login_model->user_check())
						  	{
								if($query = $this->login_model->validate()) {
									
										if ($this->login_model->renewal_date()) {

											redirect("dashboard");
										}	else {

											redirect("login/subscription");
										}								

								} else {

								 $this->session->set_flashdata('msg','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
		                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		                Username or Password does not match.
		            </div>');	
							redirect('login');	
								}
							}
						  else
						  	{
					   $this->session->set_flashdata('error_msg','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                This Username does not exist.
            </div>');	
					redirect('login');						  
						  	}
					}
	}
	function logout()

	{
		$this->session->sess_destroy();
		redirect("login");

	}

	public function subscription() {
		$this->template->title('Subscription Period Over','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('login_template','front');
		$this->template->build('subscription_view');

		$this->form_validation->set_error_delimiters("<div class='error' align='center' style='margin-top:5px;'>", "</div>");
				$this->form_validation->set_rules('renewal_code','renewal code', 'trim|required');
					if( $this->form_validation->run() == FALSE ) {

							$this->template->title('Subscription Period Over','Imanagemyhotel');
							$this->template->set('metaDesc','Imanagemyhotel');
							$this->template->set('metaKeyword','Imanagemyhotel');
							$this->template->set_layout('login_template','front');
							$this->template->build('subscription_view');
					
					} else {

						

						if($query = $this->login_model->renewal_check()) {

									if($query = $this->login_model->renewal_update()) {

									$hotel_id=$this->session->userdata('hotel_id'); 
									$frontdesk_renewal_days=getValue('hotels','frontdesk_renewal_days','hotel_id = '.$hotel_id);
									$frontdesk_renewal_date=getValue('hotels','frontdesk_renewal_date','hotel_id = '.$hotel_id);
									$renewal_date = date("d-M-Y", strtotime($frontdesk_renewal_date ));

										$this->session->set_flashdata('success_msg','<div class="alert alert-success" style="text-align:center;">
    <b>Your subscription was successfully renewed for '.$frontdesk_renewal_days.' days. Your subscription will expire on '.$renewal_date.'</b>
</div>');	
									redirect(base_url().'login');

									} 															

								} else {

									 $this->session->set_flashdata('error_msg','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <b>Activation Code Incorrect.</b><br><br>Please contact iManagemYhotel Support and get your correct activation code.
</div>');	
									redirect(base_url().'login/subscription');
								}

					}

	}
	

}
