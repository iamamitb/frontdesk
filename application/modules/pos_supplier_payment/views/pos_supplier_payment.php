<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">


<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Supplier Payment</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">


    <div class="row">
        
        <table id="datatable" class="table table-bordered">
            <thead>
                <tr>
                    <th>Bill Number</th>
                    <th>Voucher Number</th>
                    <th>Amount</th>
                    <th>Date</th>
                    
                </tr>
            </thead>

     
            <tbody>
                <tr>
                    <td>190909</td>
                    <td>763899</td>
                    <td>6700</td>
                    <td>4-02-2016</td>
                </tr>

                
            </tbody>
        </table>
    </div>

    <div align="center"><button class="btn btn-success" data-toggle="modal" data-target="#supplier-payment">Add Supplier Payment</button></div>


</div>
<!-- Page content ends -->
<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>


<div id="supplier-payment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content"> 
                                                    <div class="modal-header"> 
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                        <h3 class="modal-title"><b>Add Supplier Payment</b></h3> 
                                                    </div> 
                                                    <div class="modal-body"> 
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Supplier Name</label> 
                                                                    <select class="form-control"><option>Select Supplier Name</option><option>Supplier 1</option><option>Supplier 2</option><option>Supplier 3</option></select>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Voucher Number</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Bill Number</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div>
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Total Amount</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Discount</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Previous Payment</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                         
                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Amount Due</label> 
                                                                    <input type="text" class="form-control" id="field-1" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Current Payment</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 

                                                        <div class="row"> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-1" class="control-label">Date Of Payment</label> 
                                                                    <input type="text" class="form-control" id="datepicker" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                            <div class="col-md-6"> 
                                                                <div class="form-group"> 
                                                                    <label for="field-2" class="control-label">Comments</label> 
                                                                    <input type="text" class="form-control" id="field-2" placeholder=""> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                       
                                                         
                                                    </div> 
                                                    <div class="modal-footer"> 
                                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
                                                        <button type="button" class="btn btn-success waves-effect waves-light">Issue</button> 
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div><!-- /.modal -->

