<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wizard extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('wizard_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allCategory']=getData('category_management','category_status = 1');
		$data['allSteward']=getData('steward_master','steward_status = 1');
		$data['allTable']=getData('table_master','pos_table_status = 1');
		$this->template->title('Wizard','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('wizard',$data);

				
	}


	public function addTable(){
			$table_number=$this->input->post('table_number');
			$no_of_seats=$this->input->post('no_of_seats');
			$record_count=count($table_number);	
			$hotel_id=$this->session->userdata('hotel_id');
			
			for ($i=0; $i < $record_count; $i++) { 
			
				$wizard_table_data = array('hotel_id'=>$hotel_id,
										'pos_table_number'=>$table_number[$i],
										'no_of_seats'=>$no_of_seats[$i],
										'pos_table_status'=>'1'
										);
				//print_r($wizard_table_data); 
				$result=insertValue('table_master',$wizard_table_data);
				//echo $this->db->last_query();	
			}
		 	
		
		$this->session->set_flashdata('addtabledata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added POS Table.
            </div>');
		
		
		redirect('wizard');

	}

	public function addBasicDetail(){

		$hotel_id=$this->session->userdata('hotel_id');
		$restaurant_name=$this->input->post('restaurant_name'); 
        $restaurant_address=$this->input->post('restaurant_address');
        $restaurant_phone=$this->input->post('restaurant_phone');
        $vat_value=$this->input->post('vat_value');
        $service_charge=$this->input->post('service_charge');
        $room_service_charge=$this->input->post('room_service_charge');
        $vat_number=$this->input->post('vat_number');
        $tin_number=$this->input->post('tin_number');
        $service_tax_number=$this->input->post('service_tax_number');

        $vat_flag=$this->input->post('vat_flag');
        $service_tax_flag=$this->input->post('service_tax_flag');
        $service_charge_for_room_service_flag=$this->input->post('service_charge_for_room_service_flag');
        $tin_number_flag=$this->input->post('tin_number_flag');
        $vat_number_flag=$this->input->post('vat_number_flag');
        $date_time_flag=$this->input->post('date_time_flag');
        $bill_number_flag=$this->input->post('bill_number_flag');
        $table_number_flag=$this->input->post('table_number_flag');
        $room_number_flag=$this->input->post('room_number_flag');
        $kot_number_flag=$this->input->post('kot_number_flag');
        $token_number_flag=$this->input->post('token_number_flag');

        if($vat_flag ==""){ $vat_flag="0"; }
        if($service_tax_flag ==""){ $service_tax_flag="0"; }
        if($service_charge_for_room_service_flag ==""){ $service_charge_for_room_service_flag="0"; }
        if($tin_number_flag ==""){ $tin_number_flag="0"; }
        if($vat_number_flag ==""){ $vat_number_flag="0"; }
        if($date_time_flag ==""){ $date_time_flag="0"; }
        if($bill_number_flag ==""){ $bill_number_flag="0"; }
		if($table_number_flag ==""){ $table_number_flag="0"; }
        if($room_number_flag ==""){ $room_number_flag="0"; }
        if($kot_number_flag ==""){ $kot_number_flag="0"; }
        if($token_number_flag ==""){ $token_number_flag="0"; }

        $hotel_data=array(
        			'hotel_id'=>$hotel_id,
                    'pos_bill_restaurant_name'=>$restaurant_name,
                    'pos_bill_restaurant_address'=>$restaurant_address,
                    'pos_bill_restaurant_phone'=>$restaurant_phone,
                    'pos_bill_vat_value'=>$vat_value,
                    'pos_bill_service_charge_value'=>$service_charge,
                    'pos_bill_room_service_charge_value'=>$room_service_charge,
                    'pos_bill_vat_number_value'=>$vat_number,
                    'pos_bill_tin_number_value'=>$tin_number,
                    'pos_bill_service_tax_number'=>$service_tax_number,
                    'pos_bill_vat_flag'=>$vat_flag,
                    'pos_bill_service_tax_flag'=>$service_tax_flag,
                    'pos_bill_tin_number_flag'=>$tin_number_flag,
                    'pos_bill_vat_number_flag'=>$vat_number_flag,
                    'pos_bill_date_time_flag'=>$date_time_flag,
                    'pos_bill_number_flag'=>$bill_number_flag,
                    'pos_bill_table_number_flag'=>$table_number_flag,
                    'pos_bill_room_number_flag'=>$room_number_flag,
                    'pos_bill_kot_flag'=>$kot_number_flag,
                    'pos_bill_token_number_flag'=>$token_number_flag,
                    'pos_bill_room_service_charge_flag'=>$service_charge_for_room_service_flag
                    );
        
       // print_r($hotel_data); 
       $insertID=insertValue('pos_bill_design',$hotel_data);
		 
		$this->session->set_flashdata('addBasicDetaildata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added basic details.
            </div>');
		
		
		redirect('wizard');

	}


	public function addSteward(){
			$hotel_id=$this->session->userdata('hotel_id');
			$steward_name=$this->input->post('steward_name'); 
			$steward_phone=$this->input->post('steward_phone');
			$record_count=count($steward_name);	
			//print_r($steward_name); die();

			for ($i=0; $i < $record_count; $i++) { 
			
				$wizard_steward_data = array('hotel_id'=>$hotel_id,
										'steward_name'=>$steward_name[$i],
										'steward_phone_no'=>$steward_phone[$i],
										'steward_status'=>'1'
										);
				$result=insertValue('steward_master',$wizard_steward_data);
					
			}
		 	
		
		
		$this->session->set_flashdata('addstewarddata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added POS Steward.
            </div>');
		
		
		redirect('wizard');

	}

	public function addCategory(){
		$category_name=$this->input->post('category_name'); 
		$category_description=$this->input->post('category_description');
		$category_rank=$this->input->post('category_rank');
		$hotel_id=$this->session->userdata('hotel_id');
		$data=array('hotel_id'=>$hotel_id,
					'category_name'=>$category_name,
					'category_description'=>$category_description,
					'category_rank'=>$category_rank,
					'category_status'=>'1'
					);
		
		 
		$category_id=insertValue('category_management',$data);
		
		$this->session->set_flashdata('addcategorydata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Category.
            </div>');
		
		
		redirect('wizard');
		
	}

	public function addMenu(){
		$hotel_id=$this->session->userdata('hotel_id');
		$category_id=$this->input->post('category_id'); 
		$menu_name=$this->input->post('menu_name');
		$selling_price=$this->input->post('selling_price');
		$cost_price=$this->input->post('cost_price');
		$menu_rank=$this->input->post('menu_rank');
		$add_vat=$this->input->post('add_vat');
		$menu_description=$this->input->post('menu_description');
		
		$data=array('hotel_id'=>$hotel_id,
					'category_id'=>$category_id,
					'menu_name'=>$menu_name,
					'selling_price'=>$selling_price,
					'cost_price'=>$cost_price,
					'menu_rank'=>$menu_rank,
					'add_vat'=>$add_vat,
					'menu_description'=>$menu_description,
					'menu_status'=>'1'
					);
		
		 
		$insert_id=insertValue('menu_item',$data);
		
		$this->session->set_flashdata('addmenudata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the Menu.
            </div>');
		
		
		redirect('wizard');
		
	}


	public function editTable($idt) { 
		$table=getSingle('table_master','pos_table_id = '.$idt,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Table</h3>
			    </div>
		    <form method="post" id="table_form_edit" action="'.base_url().'wizard/updateTable" >
			<input type="hidden" id="tableID" name="tableID" value="'.$table->pos_table_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Table Number<span class="error">*</span></label>
		              <input type="text" id="table_number" name="table_number" class="form-control" value="'.$table->pos_table_number.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Number of Seats<span class="error">*</span></label>
		              <input type="text" id="no_of_seats" name="no_of_seats" class="form-control digit_only1" value="'.$table->no_of_seats.'" required>
		              <span class="error_msg1" style="color:red"></span>
		            </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_table" id="edit_table" class="btn btn-success waves-effect waves-light">Update Table</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateTable() {
		$tableID=$this->input->post('tableID'); 
		$table_number=$this->input->post('table_number'); 
		$no_of_seats=$this->input->post('no_of_seats');
		
		$data=array(
					'pos_table_number'=>$table_number,
					'no_of_seats'=>$no_of_seats,
					'pos_table_status'=>'1'
					);

  		$tableUpdate=updateDataCondition('table_master',$data,'pos_table_id = '.$tableID);
                          
  		$this->session->set_flashdata('msgTableUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the Table.
            </div>');	
                          
  		redirect('wizard');

	}

	public function tableDelete() { 
		
		$table_id=$_POST['table_id'];		
	
		$data['pos_table_status']='0';			
		
		$table_delete=updateDataCondition('table_master',$data,'pos_table_id = '.$table_id);

		$html['type']='sucess';
		echo json_encode($html);
	}


	//=========================================================================================================

	public function editSteward($ids) { 
		$steward=getSingle('steward_master','steward_id = '.$ids,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Steward</h3>
			    </div>
		    <form method="post" id="steward_form_edit" action="'.base_url().'wizard/updateSteward" >
			<input type="hidden" id="stewardID" name="stewardID" value="'.$steward->steward_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Steward Name <span class="error">*</span></label>
		              <input type="text" id="steward_name" name="steward_name" class="form-control" value="'.$steward->steward_name.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Steward Phone Number <span class="error">*</span></label>
		              <input type="text" id="steward_phone" name="steward_phone" class="form-control digit_only1" value="'.$steward->steward_phone_no.'" required maxlength="12">
		              <span class="error_msg1" style="color:red"></span>
		            </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_steward" id="edit_steward" class="btn btn-success waves-effect waves-light">Update Steward</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateSteward() {
		$stewardID=$this->input->post('stewardID'); 
		$steward_name=$this->input->post('steward_name'); 
		$steward_phone=$this->input->post('steward_phone');
		
		$data=array(
					'steward_name'=>$steward_name,
					'steward_phone_no'=>$steward_phone,
					'steward_status'=>'1'
					);
		

  		$stewardUpdate=updateDataCondition('steward_master',$data,'steward_id = '.$stewardID);
                          
  		$this->session->set_flashdata('msgStewardUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the steward.
            </div>');	
                          
  		redirect('wizard');

	}


	public function stewardDelete() { 
		
		$steward_id=$_POST['steward_id'];		
	
		$data['steward_status']='0';			
		
		$steward_delete=updateDataCondition('steward_master',$data,'steward_id = '.$steward_id);

		$html['type']='sucess';
		echo json_encode($html);
	}

	//=========================================================================================================

	public function editCategory($idc) { 
		$category=getSingle('category_management','category_id = '.$idc,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Category</h3>
			    </div>
		    <form method="post" id="category_form_edit" action="'.base_url().'wizard/updateCategory" >
			<input type="hidden" id="categoryID" name="categoryID" value="'.$category->category_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Name <span class="error">*</span></label>
		              <input type="text" id="category_name" name="category_name" class="form-control" value="'.$category->category_name.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Description<span class="error">*</span></label>
		              <input type="text" id="category_description" name="category_description" class="form-control" value="'.$category->category_description.'" required>
		             </div>
		          </div>
		        </div>

		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Category Rank <span class="error">*</span></label>
		              <input type="text" id="category_rank" name="category_rank" class="form-control" value="'.$category->category_rank.'" required>
		             </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_category" id="edit_category" class="btn btn-success waves-effect waves-light">Update Category</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateCategory() {
		$categoryID=$this->input->post('categoryID'); 
		$category_name=$this->input->post('category_name'); 
		$category_description=$this->input->post('category_description');
		$category_rank=$this->input->post('category_rank');
		
		$data=array(
					'category_name'=>$category_name,
					'category_description'=>$category_description,
					'category_rank'=>$category_rank,
					'category_status'=>'1'
					);

		//print_r($data); die();

  		$categoryUpdate=updateDataCondition('category_management',$data,'category_id = '.$categoryID);
                          
  		$this->session->set_flashdata('msgCategoryUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the category.
            </div>');	
                          
  		redirect('wizard');

	}


	public function categoryDelete() { 
		
		$category_id=$_POST['category_id'];		
	
		$data['category_status']='0';			
		
		$category_delete=updateDataCondition('category_management',$data,'category_id = '.$category_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	

	//=========================================================================================================

	public function updateWizardStatus() { 
		$account_id=$this->session->userdata('id');
		$data['wizard_status']='1';			
		
		$wizard_status=updateDataCondition('account',$data,'account_id = '.$account_id);

		$html['type']='sucess';
		echo json_encode($html);
		
	}

	


	

}
