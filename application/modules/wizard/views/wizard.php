
<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>themes/plugins/jquery.steps/demo/css/jquery.steps.css">
<link href="<?=base_url()?>themes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/css/core.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/css/components.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/css/responsive.css" rel="stylesheet" type="text/css">
<script src="<?=base_url()?>themes/js/modernizr.min.js"></script>
<style>
.navbar-default {background-color: #FFFFFF;border:none;} div.btn-group.btn-group-justified{margin-top:7px;}
.wizard > .content {background: #ffffff;min-height: 900px;padding: 20px;}
.wizard > .content > .body label {font-weight: normal;}
.wizard > .content > .body input {background: #FFF;}
..form-control{background: #FFF;}
.wizard.vertical > .content {
    display: inline;
    float: left;
    margin: 0 2.5% 0.5em 2.5%;
    width: 65%;
    background: #eee;
    border-radius: 4px;
}
</style>

<!-- Page content starts --> 

<!-- Row for Wizard Starts -->
<div class="row">
    <div class="col-md-12">
        
       
<div class="">
    <h3>Action Required</h3>
    <p>Please complete the following set up wizard to set up the software for your restaurant.</p>
     <!-- Vertical Steps Example -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <!-- <div class="panel-heading"> 
                <h3 class="panel-title">Vertical Steps Example</h3> 
            </div>  -->
<?=$this->session->flashdata("addBasicDetaildata"); ?>  
<?=$this->session->flashdata("addtabledata"); ?> 
<?=$this->session->flashdata("msgTableUpdate"); ?> 
<?=$this->session->flashdata("addstewarddata"); ?>
<?=$this->session->flashdata("msgStewardUpdate"); ?>
<?=$this->session->flashdata("addcategorydata"); ?>
<?=$this->session->flashdata("msgCategoryUpdate"); ?>
<?=$this->session->flashdata("addmenudata"); ?>         
<div class="panel-body"> 
    <div id="wizard-vertical">
        <h3>Basic Details</h3>
        <section>
            <div class="row">
            <form method="post" id="basic_info" action="<?php echo base_url(); ?>wizard/addBasicDetail">
                <div class="col-md-12">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Name of the Restaurant <span class="error">*</span></label>
                            <input type="text" id="restaurant_name" name="restaurant_name" class="form-control" required aria-required="true">
                            <span id="restaurant-name-error" class="wizard-error"></span>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Phone Number Of the Restaurant <span class="error">*</span></label>
                            <input type="text" id="restaurant_phone" name="restaurant_phone" class="form-control" required aria-required="true">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">

                     <div class="form-group">
                        <label for="">Address of the Restaurant <span class="error">*</span></label>
                        <textarea id="restaurant_address" name="restaurant_address" class="form-control" rows="2" required aria-required="true"></textarea>
                    </div>
                </div>

                <div class="col-md-12">

                    <div class="col-md-4">
                        <div class="form-group">
                        <label for="">Vat % <span class="error">*</span></label>
                        <input type="text" id="vat_value" name="vat_value" class="form-control" required aria-required="true">
                        </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Service Charge % <span class="error">*</span></label>
                            <input type="text" id="service_charge" name="service_charge" class="form-control" required aria-required="true">
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="">Service Charge % for Room Service <span class="error">*</span></label>
                            <input type="text" id="room_service_charge" name="room_service_charge" class="form-control" required aria-required="true">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">

                    <div class="col-md-4">
                        <div class="form-group">
                             <label for="">Vat Number </label>
                            <input type="text" id="vat_number" name="vat_number" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">TIN Number </label>
                            <input type="text" id="tin_number" name="tin_number" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Service Tax Number </label>
                            <input type="text" id="service_tax_number" name="service_tax_number" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">

                    <div class="col-md-6">
                        <div class="checkbox checkbox-success">
                            <input id="vat_flag" name="vat_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show VAT % In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="service_tax_flag" name="service_tax_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Service Tax % In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="service_charge_for_room_service_flag" name="service_charge_for_room_service_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Room Service Charge % In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="tin_number_flag" name="tin_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show TIN Number In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="kot_number_flag" name="kot_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show KOT Number In Bill
                            </label>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="checkbox checkbox-success">
                            <input id="vat_number_flag" name="vat_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show VAT Number In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="date_time_flag" name="date_time_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Date and Time In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="bill_number_flag" name="bill_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Bill Number in Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="table_number_flag" name="table_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Table Number In Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="room_number_flag" name="room_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Room Number in Bill
                            </label>
                        </div>

                        <div class="checkbox checkbox-success">
                            <input id="token_number_flag" name="token_number_flag" type="checkbox" value="1">
                            <label for="checkbox3">
                                Show Token Number In Bill (For Take away)
                            </label>
                        </div>
                    </div>

                </div>
                <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Submit</button>
                </form>
            </div>
        </section>

                                            

<h3>Add Table</h3>
<section>
    <!-- <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Table Number 9 was added Successfully
    </div> -->
    
    <h3>Add Table</h3>
    <form name="table_form" method="post" id="table_form" action="<?php echo base_url(); ?>wizard/addTable">
    <input type="hidden" value="1" id="rowID" name="rowID"/>
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Table Number <span class="error">*</span></label>
                        <input type="text" id="table_number_1" name="table_number[]" class="form-control" required aria-required="true">
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Number of Seats <span class="error">*</span></label>
                        <input type="text" id="no_of_seats_1" name="no_of_seats[]" class="form-control" required aria-required="true">
                    </div> 
                </div>

                <div class="col-md-4"><br><br>
             <button type="button" id="add_table_btn" onclick="addMoreRows(this.form);" class="btn btn-success btn-sm m-b-5">+</button> 
                </div>

            </div>
        </div>
        <div id="addedRows"></div>

        <div class="row">
            <b><a href="#" style="float:right;" data-toggle="modal" data-target="#table-modal">View Table List</a></b>
        </div>
        <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Submit</button>
        </form> 
                

</section>
<h3>Add Steward</h3>
<section>

    <h3>Add Steward</h3>
    <form method="post" id="steward_form" action="<?php echo base_url(); ?>wizard/addSteward">
    <input type="hidden" value="1" id="rowStewardID" name="rowStewardID"/>
    <!-- <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Steward Shyam was added successfully
    </div> -->
    <div class="row">
            <div class="col-md-12">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Steward Name <span class="error">*</span></label>
                        <input type="text" id="steward_name_1" name="steward_name[]" class="form-control" required aria-required="true">
                    </div> 
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Steward Phone Number <span class="error">*</span></label>
                        <input type="text" id="steward_phone_1" name="steward_phone[]" class="form-control" required aria-required="true">
                    </div> 
                </div>

                <div class="col-md-4"><br><br>
                <button type="button" onclick="addMoreStewardRows(this.form);" class="btn btn-success btn-sm m-b-5">+</button>
                </div>

            </div>
        </div>
        <div id="addedStewardRows"></div>

         <div class="row">
            <b><a href="#" style="float:right;" data-toggle="modal" data-target="#steward-modal">View Steward List</a></b>
        </div>
    <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Submit</button>
  </form>
</section>
<h3>Add Category</h3>
<section>

    <h3 style="margin-bottom:30px;">Add Category</h3>
        <!-- <div class="alert alert-warning alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        Category Was added successfully
        </div> -->
<form method="post" id="category_form" action="<?php echo base_url(); ?>wizard/addCategory">    
<div class="row">
    <div class-"col-md-12">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Category Name <span class="error">*</span></label>
                    <div class="col-md-9">
                        <input type="text" id="category_name" name="category_name" required aria-required="true" class="form-control">
                    </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Category Description <span class="error">*</span></label>
                    <div class="col-md-9">
                        <input type="text" id="category_description" name="category_description" required aria-required="true" class="form-control">
                    </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Category Rank <span class="error">*</span></label>
                    <div class="col-md-9">
                        <input type="text" id="category_rank" name="category_rank" required aria-required="true" class="form-control">
                    </div>
            </div>
        </div>
        
    </div>
</div>

    <div class="row">
        <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
            <button type="submit" class="btn btn-success btn-lg m-b-5">Add Category</button>
        </div>
    </div>
   
</form>
        <div class="row">
            <b><a href="#" style="float:right;" data-toggle="modal" data-target="#category-modal">View Category List</a></b>
        </div>
</section>
    <h3>Add Menu Items</h3>
    <section>
   
        <h3>Add Menu Items</h3>
        <!-- <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            Menu Was added successfully
            </div> -->

        <form method="post" id="menu_form" action="<?php echo base_url(); ?>wizard/addMenu">
        <div class="row">
            <div class="form-group">
                <label class="col-md-3 control-label">Category Name <span class="error">*</span></label>
                <div class="col-md-9">
                 <?=get_data_dropdown("category_management","category_id","category_name","category_status = '1'","","category_id","category_id","required","","","")?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group">
                <div class="form-group">
                    <label class="col-md-3 control-label">Menu Name <span class="error">*</span></label>
                    <div class="col-md-9">
                        <input type="text" id="menu_name" name="menu_name" required aria-required="true" class="form-control">
                    </div>
                </div>
            </div>
        </div>

    <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label">Selling Price <span class="error">*</span></label>
                <div class="col-md-9">
                    <input type="text" id="selling_price" name="selling_price" required aria-required="true" class="form-control">
                </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label">Cost Price <span class="error">*</span></label>
                <div class="col-md-9">
                    <input type="text" id="cost_price" name="cost_price" required aria-required="true" class="form-control">
                </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label">Menu Rank <span class="error">*</span></label>
                <div class="col-md-9">
                    <input type="text" id="menu_rank" name="menu_rank" required aria-required="true" class="form-control">
                </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <label class="col-md-3 control-label">Add VAT <span class="error">*</span></label>
                <div class="col-md-9">
                    <select class="form-control" id="add_vat" name="add_vat" required aria-required="true">
                        <option value="">Select</option>
                        <option value="Yes">Yes</option><option value="No">No</option>
                    </select>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="col-md-3 control-label">Menu Description <span class="error">*</span></label>
                <div class="col-md-9">
                    <input type="text" id="menu_description" name="menu_description" required aria-required="true" class="form-control">
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Submit</button>
   </form>
    
</section>
    <h3>Finish</h3>
    <section>
    <div class="form-group clearfix">
    <div class="col-lg-12">
        <div>
            <label>
                Thanks for completing the wizard. You can now start using POS for your restaurant.
            </label>
        </div>
        <a href="<?=base_url()?>pos_pos">
        <button type="submit" id="wizard_finish" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Go to POS</button>
        </a>
    </div>
    
    </div>
    </section>
    </div> <!-- End #wizard-vertical -->
    </div>  <!-- End panel-body -->
    </div> <!-- End panel -->

    </div> <!-- end col -->

    </div> <!-- End row -->
</div>
</div>

</div>
<!-- Page content ends -->  










<!-- Category Modal starts -->
<div id="category-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog modal-full"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">All Categories</h4> 
            </div> 
            <div class="modal-body"> 
                <div class="row"> 
                <?php if(isset($allCategory) && count($allCategory)>0){ ?> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Category Rank</th>
                                <th>Category Description</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php                                                                                                       foreach($allCategory as $each_category):{
                            ?> 
                            <tr>
                                <td><?=$each_category->category_name?></td>
                                <td><?=$each_category->category_rank?></td>
                                <td><?=$each_category->category_description?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                        
                                        <ul class="dropdown-menu" role="menu">

                                        <li><a href="#" data-toggle="modal" onclick="editCategory(<?=$each_category->category_id?>)" data-target="#edit-category">Edit</a></li>
                                        <li><a onclick="categoryDelete(<?=$each_category->category_id?>)" href="javascript:void(0)">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any Category Yet.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>
                </div> 
            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <button type="button" class="btn btn-success">Save changes</button> 
            </div> 
        </div> 
    </div>
</div><!-- /.modal -->
<!-- modal --> 
<div id="edit-category" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal -->
<!-- Category modal ends -->


<!-- Table Modal -->
<div id="table-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">All Tables</h4> 
            </div> 
            <div class="modal-body"> 
                <div class="row"> 
                <?php if(isset($allTable) && count($allTable)>0){ ?> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Table Number</th>
                                <th>Number of Seats</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php                                                                                                       foreach($allTable as $each_table):{
                            ?>
                            <tr>
                                <td><?=$each_table->pos_table_number?></td>
                                <td><?=$each_table->no_of_seats?></td>
                                <td>
                                <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    
                                    <li><a href="#"  data-toggle="modal" onclick="editTable(<?=$each_table->pos_table_id?>)" data-target="#edit-table">Edit</a></li>
                                    <li><a onclick="tableDelete(<?=$each_table->pos_table_id?>)" href="javascript:void(0)">Delete</a></li>
                                </ul>
                                </div>
                            </td>
                            </tr>
                            <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any Table Yet.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>
                </div> 
            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <button type="button" class="btn btn-success">Save changes</button> 
            </div> 
        </div> 
    </div>
</div><!-- /.modal -->
<!-- Edit Table modal --> 
<div id="edit-table" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- Edit table modal ends -->
<!-- Table modal ends -->

<!-- Steward Modal -->
<div id="steward-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                <h4 class="modal-title">All Stewards</h4> 
            </div> 
            <div class="modal-body"> 
                <div class="row">
                <?php if(isset($allSteward) && count($allSteward)>0){ ?> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Steward Name</th>
                                <th>Steward Phone Number</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php                                                                                                       foreach($allSteward as $each_steward):{
                            ?>
                            <tr>
                                <td><?=$each_steward->steward_name?></td>
                                <td><?=$each_steward->steward_phone_no?></td>
                                <td>
                                <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    
                                    <li><a href="#"  data-toggle="modal" onclick="editSteward(<?=$each_steward->steward_id?>)" data-target="#edit-steward">Edit</a></li>
                                    <li><a onclick="stewardDelete(<?=$each_steward->steward_id?>)" href="javascript:void(0)">Delete</a></li>
                                </ul>
                                </div>
                            </td>
                            </tr>
                            <?php } endforeach;  ?> 
                            </tbody>
                        </table>
                        <?php  } else{ ?>
                     <table class="table table-bordered" style="margin-top:25px;">
                        <tbody> 
                            <tr>
                            <th style="text-align:center;">You have not added any Steward Yet.</th>
                            </tr> 
                         </tbody> 
                     </table>
                    <?php } ?>
                </div> 
            </div> 
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <button type="button" class="btn btn-success">Save changes</button> 
            </div> 
        </div> 
    </div>
</div><!-- /.modal -->
<!-- modal --> 
<div id="edit-steward" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal -->
<!-- Steward modal ends -->




<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<!--Form Wizard-->
<script src="<?=base_url()?>themes/plugins/jquery.steps/build/jquery.steps.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js"></script> -->

<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<!--wizard initialization-->
<script src="<?=base_url()?>themes/pages/jquery.wizard-init.js" type="text/javascript"></script>
<script src="<?=base_url()?>themes/js/views/wizard.js"></script>


