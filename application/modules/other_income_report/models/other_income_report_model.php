<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other_income_report_model extends CI_Model {

		public function getBookings($account_id,$search=array(),$csv=0){
			
			$row=false;
			$sql="SELECT  oi.*,oib.*,b.booking_reference  FROM  other_item oi RIGHT JOIN other_item_booked oib ON oi.item_id=oib.item_id
						RIGHT JOIN hotels h ON oib.hotel_id=h.hotel_id
						RIGHT JOIN bookings b ON oib.booking_id=b.booking_id
						where h.account_id=$account_id ";

			$where = array();
			if(!empty($search)){
			foreach($search as $db=>$dbarray){
				foreach($dbarray as $type=>$typearray){
					foreach($typearray as $newkey=>$val){
						if($val){
						  if($type == 'like')
						  {
							  $where[] = $db.".".$newkey." like '%".$val."%' ";
						  }
						  elseif($type == 'from')
						  {
							  $where[] = $db.".".$newkey." >= '".$val."'";
						  }
						  elseif($type == 'to')
						  {
							  $where[] = $db.".".$newkey." <= '".$val."'";
						  }
						   elseif($type == 'from1')
							{		$datetime = strtotime($val);
							$d1 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." >= '".$d1."'";
						  }
						  elseif($type == 'to1')
						  {	$datetime = strtotime($val);
							$d2 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." <= '".$d2."'";
						  }
						  elseif($type == 'equal1')
						  {	$datetime = strtotime($val);
							$d2 = date('Y/m/d', $datetime);
							  $where[] = $db.".".$newkey." = '".$d2."'";
						  }
						  elseif($type == 'equal')
						  {
							  $where[] = $db.".".$newkey." = '".$val."'";
						  }
						}
					}
				}
			
			}
			}

			if(count($where))
			{
				$sql.="and ".implode(' and ',$where);
			}

			$sql.=" order by oib.booking_id";

			$query = $this->db->query($sql);

			if($csv!=0){
			download_report($query,"other_income_report.csv");		 
			}
			
			if($query->num_rows() > 0)
			{
				foreach($query->result() as $val)
				{
					$row[]=	$val;
				}
				return $row;
			}

		}	
}
?>