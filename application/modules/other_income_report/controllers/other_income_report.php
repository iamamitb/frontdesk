<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Other_income_report extends MX_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('other_income_report_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in();
		is_privileges();
   	}

	public function index(){
		$search = array();
		$action=trim($this->input->post('action'));
		$csv=trim($this->input->post('csv'));
		$account_id=$this->session->userdata('account_id');
		if($action=='filter')
		$search=$_POST['filter'];
		$data['all_bookings'] = $this->other_income_report_model->getBookings($account_id,$search,$csv);
		$this->template->title('Other Income Report','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('main_template','front');
		$this->template->build('other_income_report',$data);
	}
	
	public function print_report(){
		$search = array();	
		$account_id=$this->session->userdata('account_id');
		$action=trim($this->input->post('action'));	
		if($action=='filter')
		$search=$_POST['filter'];
		$data['all_bookings']=$this->other_income_report_model->getBookings($account_id,$search);
		$this->template->set_layout('print_template','front');
		$this->template->build('print_report',$data);
	}	
}
