<!-- Style for page display -->
<style>
html{background-color: #FFF;}
.row{margin-left:5%;margin-right:5%;}
/*span{margin-left:50px;text-align:center;width:80%;}*/
</style>
<!-- Page display style ends  -->

<!-- Style for Printing -->
<style>
@media print {
	@page {size: landscape}
}
</style>
<!-- Style for Printing ends -->

<div class="row">
	<h1 align="center">Other Income Report</h1>
</div>
<?php
if(!empty($_POST)){
$item_name=(($_POST['filter']['oi']['equal']['item_id'])!="")? getValue('other_item','item_name',"item_id = ".$_POST['filter']['oi']['equal']['item_id'] ) : "" ;
?>
<div class="row" align="center">
<p><b>Filters: </b><span>Date: <?php echo $dt1=(isset($_POST['filter']['oib']['from1']['date']))? $_POST['filter']['oib']['from1']['date'] : "" ;?> - <?php echo $dt2=(isset($_POST['filter']['oib']['to1']['date']))? $_POST['filter']['oib']['to1']['date'] : "" ;?>, Item: <?=$item_name?> </span></p>
</div>
<?php } ?>

<div class="row">
	<table class="table table-bordered">
            <?php                                                                                                                
            if(isset($all_bookings) && count($all_bookings)>0){
            ?> 
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Item Name</th>
                    <th>Room Number</th>
                    <th>Unit Price</th>
                    <th>Item Number</th>
                    <th>Remarks</th>
                    <th>Date</th>
                    <th>Total Price</th>
                    <th>Status</th>
                </tr>
            </thead>

            <?php foreach($all_bookings as $all_booking):{ ?> 
            <tbody>
                <tr>
                    <td><?=$all_booking->booking_id ?></td>
                    <td><?=$all_booking->item_name ?></td>
                    <td><?=$all_booking->room_number ?></td>
                    <td><?=$all_booking->unit_price ?></td>
                    <td><?=$all_booking->no_of_item ?></td>
                    <td><?=$all_booking->remarks ?></td>
                    <td><?php echo date('d-M-Y', strtotime($all_booking->date)); ?></td>
                    <td><?=$all_booking->total_amount ?></td>
                    <td><?=$all_booking->status ?></td>
                </tr>
                <?php } ?>    
            </tbody>
            <?php endforeach;  } else{  ?> 
            
         <tbody> 
            <tr >
            <th style="text-align:center;">No data found !</th>
            </tr> 
         </tbody> 
         <?php } ?>
        </table>

</div>