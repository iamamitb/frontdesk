<link href="<?=base_url()?>themes/css/views/other_income.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Other Income Report</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<!-- Panel Starts -->
<div class="row">
	<div class="panel panel-default">
<div class="panel-heading">
    <h3 class="panel-title">Other Income Report for your Account</h3>
</div>
<div class="panel-body">
	
	<!-- Row for Custom Filter Starts -->	
    <?php //if(isset($all_bookings) && count($all_bookings)>0){ ?>
	<div class="row" style="margin-bottom: 50px;background: #E1EEF2;padding: 1%;border-radius: 5px;"> 
		<div class="col-md-12">
            <form action="" method="post" id="reportFrom" >
            <input name="action" type="hidden" value="filter" />
            <input name="csv" type="hidden" id="csv" value="" />
			<div class="col-md-3">
    			<div class="form-group">
                <label for="exampleInputEmail1">From Date</label>
                <input type="text" id="from_date" readonly="" value="<?php echo $fromDate=(isset($_POST['filter']['oib']['from1']['date']))? $_POST['filter']['oib']['from1']['date'] : "" ;?>" name="filter[oib][from1][date]" class="form-control" placeholder="Enter Start Date">
            	</div>
        	</div>

        	<div class="col-md-3">
            	<div class="form-group">
                <label for="exampleInputEmail1">To Date</label>
                <input type="text" id="to_date" readonly="" value="<?php echo $toDate=(isset($_POST['filter']['oib']['to1']['date']))? $_POST['filter']['oib']['to1']['date'] : "" ;?>" name="filter[oib][to1][date]" class="form-control" placeholder="Enter End Date">
            	</div>
        	</div>

        	<div class="col-md-3">
    			<div class="form-group">
                <label for="exampleInputEmail1">Select Item</label>
                <?php  $oi=(isset($_POST['filter']['oi']['equal']['item_id']))? $_POST['filter']['oi']['equal']['item_id'] : "" ;?>
                <?=get_data_dropdown("other_item","item_id","item_name","status = '1'",$oi,'item_id',"filter[oi][equal][item_id]"); ?> 
            	</div>
        	</div>
        	
        	<div class="col-md-3" style="top:25px;">
            	<div class="form-group">
                <!-- <label for="exampleInputEmail1">Filter</label> -->
				<button type="submit" class="btn btn-primary m-b-5">Filter Results</button>
                <button type="cancel" id="clear_data" class="btn btn-primary m-b-5">Clear Filters</button>
                </div>
        	</div>
        </form>
		</div>
	</div>
    <?php //} ?>
	<!-- Row for Custom Filter Ends -->

    	<!-- Row for Table Starts -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if(isset($all_bookings) && count($all_bookings)>0){ ?>
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Booking ID</th>
                            <th>Item Name</th>
                            <th>Room Number</th>
                            <th>Unit Price</th>
                            <th>Item Number</th>
                            <th>Remarks</th>
                            <th>Date</th>
                            <th>Total Price</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php                                                                                                                
                        foreach($all_bookings as $all_booking):{
                        ?> 
                        <tr>
                            <td><?=$all_booking->booking_reference ?></td>
                            <td><?=$all_booking->item_name ?></td>
                            <td><?=$all_booking->room_number ?></td>
                            <td><?=$all_booking->unit_price ?></td>
                            <td><?=$all_booking->no_of_item ?></td>
                            <td><?=$all_booking->remarks ?></td>
                            <td><?php echo date('d-M-Y', strtotime($all_booking->date)); ?></td>
                            <td><?=$all_booking->total_amount ?></td>
                            <td><?=$all_booking->status ?></td>
                        </tr>
                       <?php } endforeach;  ?>
                    </tbody>
                </table>

            </div>
        </div>
       <!-- Row for Table ends Starts -->
    </div>
</div>   
</div> <!-- End row -->
<!-- Panel ends -->
<div class="col-md-12">
    <div align="center">
        <button id="printReport" class="btn btn-default m-b-5">Print Report</button>
        <button id="downloadReport" class="btn btn-default m-b-5">Download Report in Excel</button>
    </div>
</div>
<?php  } else{ ?>
 <table class="table table-bordered" style="margin-top:25px;">
    <tbody> 
        <tr>
        <th style="text-align:center;">No data found.</th>
        </tr> 
     </tbody> 
 </table>
<?php } ?>

<!-- Page content ends -->  

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$("#to_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate:0});
$("#from_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate:0}).bind("change",function(){
        var minValue = $(this).val();
        minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
        minValue.setDate(minValue.getDate()+1);
        $("#to_date").datepicker( "option", "minDate", minValue );
    })

 $("#downloadReport").click(function() {
     $( "#csv" ).val(1);
     $( "#reportFrom" ).submit();
     $( "#csv" ).val('');
});

$("#printReport").click(function() {
    $( "#reportFrom" ).attr("target","_blank");
    $( "#reportFrom" ).attr("action","<?=base_url()?>other_income_report/print_report");
    $( "#reportFrom" ).submit();
    $( "#reportFrom" ).removeAttr("target");
    $( "#reportFrom" ).attr("action"," ");
});

//it will clear all form filed data and reload the page ..
$("#clear_data").click(function() {
    $("#from_date").val("");
    $("#to_date").val("");
    $('#item_id option:selected').removeAttr('selected');
    window.location.reload(true);
});
</script>