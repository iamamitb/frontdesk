<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">

<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Bill Design</h1></div>
</div>
<?=$this->session->flashdata("updateBillData"); ?>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">
    <div class="col-md-12">
        <!-- Bill Parameters -->
        <div class="col-md-6"> 
        <?php if(isset($getBillDesignData)){
            foreach($getBillDesignData as $billDesignData){ 
               $vat_flag=$billDesignData->pos_bill_vat_flag; 
               $service_tax_flag=$billDesignData->pos_bill_service_tax_flag; 
               $service_charge_for_room_service_flag=$billDesignData->pos_bill_room_service_charge_flag; 
               $tin_number_flag=$billDesignData->pos_bill_tin_number_flag; 
               $vat_number_flag=$billDesignData->pos_bill_vat_number_flag; 
               $date_time_flag=$billDesignData->pos_bill_date_time_flag; 
               $bill_number_flag=$billDesignData->pos_bill_number_flag; 
               $table_number_flag=$billDesignData->pos_bill_table_number_flag; 
               $room_number_flag=$billDesignData->pos_bill_room_number_flag; 
               $kot_number_flag=$billDesignData->pos_bill_kot_flag; 
               $token_number_flag=$billDesignData->pos_bill_token_number_flag; 
            ?> 
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Design Your Bill</h3> 
                </div>

                <div class="panel-body"> 
                <form id="bill_design_form">
                <input type="hidden" id="pos_bill_design_id" name="pos_bill_design_id" value="<?=$billDesignData->pos_bill_design_id?>"> 
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name of Restaurant <span class="error">*</span></label>
                        <input type="text" value="<?=$billDesignData->pos_bill_restaurant_name?>" id="restaurant_name" name="restaurant_name" class="form-control" placeholder="" required aria-required="true">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Address of Restaurant</label>
                        <input type="text" value="<?=$billDesignData->pos_bill_restaurant_address?>" id="restaurant_address" name="restaurant_address" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Phone Number of Restaurant</label>
                        <input type="text" value="<?=$billDesignData->pos_bill_restaurant_phone?>" id="restaurant_phone" name="restaurant_phone" class="form-control" >
                       <!-- <span class="error_msg" style="color:red"></span> -->
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Header Message</label>
                        <input type="textarea" value="<?=$billDesignData->pos_bill_header_message?>" id="header_message" name="header_message" class="form-control" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Footer Message</label>
                        <input type="textarea" value="<?=$billDesignData->pos_bill_footer_message?>" id="footer_message" name="footer_message" class="form-control" placeholder="">
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="vat_flag" name="vat_flag" type="checkbox" value="vat_flag" <?php if(isset($vat_flag) && $vat_flag == 1){ echo 'checked="checked"';}?>>

                        <label for="checkbox3">
                            Show VAT % In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="service_tax_flag" name="service_tax_flag" type="checkbox" value="service_tax_flag" <?php if(isset($service_tax_flag) && $service_tax_flag == 1){ echo 'checked="checked"';}?>>
                        <label for="checkbox3">
                            Show Service Tax % In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success"> 
                        <input id="service_charge_for_room_service_flag" name="service_charge_for_room_service_flag" type="checkbox" value="service_charge_for_room_service_flag" <?php if(isset($service_charge_for_room_service_flag) && $service_charge_for_room_service_flag == 1){ echo 'checked="checked"';}?>>
                        <label for="checkbox3">
                            Show Room Service Charge % In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="tin_number_flag" name="tin_number_flag" type="checkbox" value="tin_number_flag" <?php if(isset($tin_number_flag) && $tin_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show TIN Number In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="vat_number_flag" name="vat_number_flag" type="checkbox" value="vat_number_flag" <?php if(isset($vat_number_flag) && $vat_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show VAT Number In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="date_time_flag" name="date_time_flag" type="checkbox" value="date_time_flag" <?php if(isset($date_time_flag) && $date_time_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show Date and Time In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="bill_number_flag" name="bill_number_flag" type="checkbox" value="bill_number_flag" <?php if(isset($bill_number_flag) && $bill_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show Bill Number in Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="table_number_flag" name="table_number_flag" type="checkbox" value="table_number_flag" <?php if(isset($table_number_flag) && $table_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show Table Number In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="room_number_flag" name="room_number_flag" type="checkbox" value="room_number_flag" <?php if(isset($room_number_flag) && $room_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show Room Number in Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="kot_number_flag" name="kot_number_flag" type="checkbox" value="kot_number_flag" <?php if(isset($kot_number_flag) && $kot_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show KOT Number In Bill
                        </label>
                    </div>

                    <div class="checkbox checkbox-success">
                        <input id="token_number_flag" name="token_number_flag" type="checkbox" value="token_number_flag" <?php if(isset($token_number_flag) && $token_number_flag == 1){ echo 'checked="checked"';}?> >
                        <label for="checkbox3">
                            Show Token Number In Bill (For Take away)
                        </label>
                    </div>

                    <div align="center">
                        <button type="submit" id="add_bill_design" name="add_bill_design" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Preview Bill</button>
                    </div>

                <form>  
                </div> 
            </div>
            <div class="row" align="center">
                <button type="submit" id="save_bill_data" name="save_bill_data" class="btn btn-success waves-effect waves-light m-b-5" style="margin-top:30px;">Save Bill Data</button>
            </div>
            <?php } } ?>
        </div>
        
        <!-- Bill Preview -->
        <?php  //echo count($getBillData);
       // if(isset($_POST['add_bill_design'])){ ?> 
        <div class="col-md-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Preview Your Bill</h3> 
                </div> 
                <div class="panel-body" id="bill_preview"> 

                    
                    
                </div> 
            </div>
        </div>
        <?php //} else{ ?>
        <!-- <div class="col-md-6">
            <div class="panel panel-border panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Preview Your Bill</h3> 
                </div> 
                <div class="panel-body"> 

                    <div class="restaurant_bill">
                        <div class="row">
                            <div class="restaurant_name">To see the preview of your bill please add bill data..</div>
                        </div>
                    </div>
                </div> 
            </div>
        </div> -->
        <?php //} ?>
        <!-- End Bill Preview -->
        <!-- ================ -->
    </div>
                            
</div> <!-- End row -->
<!-- Page content ends --> 

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?=base_url()?>themes/plugins/jquery-ui/jquery-ui.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script src="<?=base_url()?>themes/js/views/pos_bill_design.js"></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?>

</script>  