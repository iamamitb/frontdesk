<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_bill_design extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_bill_design_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		
		$data['getBillData']=getSingle('pos_bill_design','','','','','');
        //echo count($data['getBillData']);
        $account_id=$this->session->userdata('id');
        $hotel_id=getValue('hotels','hotel_id','account_id = '.$account_id);
        
        $data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
        $data['getBillDesignData']=getData('pos_bill_design','hotel_id = '.$hotel_id);
        //print_r($data['getBillDesignData']); die();
        $this->template->title('Bill Design','Imanagemyhotel');
        $this->template->set('metaDesc','Imanagemyhotel');
        $this->template->set('metaKeyword','Imanagemyhotel');
        $this->template->set_layout('pos_template','front');
        $this->template->build('pos_bill_design',$data);
				
	}


	public function previewBillDesign(){
		$restaurant_name=$this->input->post('restaurant_name'); 
		$restaurant_address=$this->input->post('restaurant_address');
		$restaurant_phone=$this->input->post('restaurant_phone');
		$header_message=$this->input->post('header_message');
		$footer_message=$this->input->post('footer_message');
		$vat_flag=$this->input->post('vat_flag');
		$service_tax_flag=$this->input->post('service_tax_flag');
		$service_charge_for_room_service_flag=$this->input->post('service_charge_for_room_service_flag');
		$tin_number_flag=$this->input->post('tin_number_flag');
		$vat_number_flag=$this->input->post('vat_number_flag');
		$date_time_flag=$this->input->post('date_time_flag');
		$bill_number_flag=$this->input->post('bill_number_flag');
		$table_number_flag=$this->input->post('table_number_flag');
		$room_number_flag=$this->input->post('room_number_flag');
		$kot_number_flag=$this->input->post('kot_number_flag');
		$token_number_flag=$this->input->post('token_number_flag');

		$data=array(
					'pos_bill_restaurant_name'=>$restaurant_name,
					'pos_bill_restaurant_address'=>$restaurant_address,
					'pos_bill_restaurant_phone'=>$restaurant_phone,
					'pos_bill_header_message'=>$header_message,
					'pos_bill_footer_message'=>$footer_message,
					'pos_bill_vat_flag'=>$vat_flag,
					'pos_bill_service_tax_flag'=>$service_tax_flag,
					'pos_bill_tin_number_flag'=>$tin_number_flag,
					'pos_bill_vat_number_flag'=>$vat_number_flag,
					'pos_bill_date_time_flag'=>$date_time_flag,
					'pos_bill_number_flag'=>$bill_number_flag,
					'pos_bill_table_number_flag'=>$table_number_flag,
					'pos_bill_room_number_flag'=>$room_number_flag,
					'pos_bill_kot_flag'=>$kot_number_flag,
					'pos_bill_token_number_flag'=>$token_number_flag,
					'pos_bill_room_service_charge_flag'=>$service_charge_for_room_service_flag
					);
		
		  
		//$insertID=insertValue('pos_bill_design',$data); 

		//$billData=getSingle('pos_bill_design','pos_bill_design_id = '.$insertID,'','','','');
       
		$html['item'].='<div class="restaurant_bill">
                        <div class="row"><p align="center">TAX INVOICE</p>
                            <div class="restaurant_name">'.$restaurant_name.'</div>
                        </div>';
        if($restaurant_phone!=''){                
        $html['item'].='<div class="row">
                            <div class="restaurant_phone_number"><i class="ion-ios7-telephone"></i>'.$restaurant_phone.'</div>
                        </div>';
            }
        if($restaurant_address!=''){                 
        $html['item'].='<div class="row">
                            <div class="restaurant_address">'.$restaurant_address.'</div>
                        </div>';
            } 
        if($header_message!=''){                
        $html['item'].='<div class="row">
                            <div class="restaurant_header">'.$header_message.'</div>
                        </div>';
            }               
        
        $html['item'].='<div class="row" style="font-family:courier;text-transform:uppercase;font-size:12px;margin-bottom:20px;">
                            <div class="col-md-12">';
                if($bill_number_flag==1){
                $html['item'].='<div class="col-md-6">
                                    Bill No 1756
                                </div>';
                }                
                if($date_time_flag==1){    
                $html['item'].='<div class="col-md-6">
                                    23-5-2016 03:39 PM
                                </div>';
                }                 
                if($table_number_flag==1){                
                $html['item'].='<div class="col-md-6">
                                    Table: 14
                                </div>';
                }                 
                if($room_number_flag==1){                
                $html['item'].='<div class="col-md-6">
                                    Room: 107
                                </div>';
                }                 
                $html['item'].='</div>
                        </div>

                        <div class="row">
                            <table class="restaurant_table" style="text-transform:uppercase;width:330px;">
                                
                                <tr style="border-top:1px dashed #000;border-bottom:1px dashed #000;font-weight:bold;color:#000;">
                                    <td style="width:170px;">Description</td>
                                    <td>Qty</td>
                                    <td>Rate</td>
                                    <td>Amt</td>
                                </tr>
                                <tr style="border-bottom:1px dashed #565656;height:30px;">
                                    <td>Chicken Kosha</td>
                                    <td>3</td>
                                    <td>100</td>
                                    <td>300.00</td>
                                </tr>
                                    
                                <tr style="border-bottom:1px dashed #565656;height:30px;" >
                                    <td>Hyderabadi Biriyani</td>
                                    <td>3</td>
                                    <td>180</td>
                                    <td>540.00</td>
                                </tr>

                                <tr style="border-bottom:1px dashed #565656;height:30px;">
                                    <td>Mexican Salad With Gravy</td>
                                    <td>3</td>
                                    <td>180</td>
                                    <td>540.80</td>
                                </tr>

                                <tr style="border-bottom:1px dashed #565656;height:50px;">
                                    <td>Total</td>
                                    <td>9</td>
                                    <td></td>
                                    <td>1540.80</td>
                                </tr>';

                                if($vat_flag==1){
                                    $html['item'].='<tr style="height:30px;">
                                    <td colspan="3">VAT 14.5% on 1540.80</td>
                                    <td>1600.80</td>
                                </tr>';
                                }

                                if($service_tax_flag==1){
                                    $html['item'].='<tr style="height:30px;">
                                    <td colspan="3">Service Tax 5.8% on 1540.80</td>
                                    <td>60.80</td>
                                </tr>';
                                }

                                if($service_charge_for_room_service_flag==1){
                                    $html['item'].='<tr style="height:30px;">
                                    <td colspan="3">Service Charge Room Service 5.8% on 1540.80</td>
                                    <td>60.80</td>
                                </tr>';
                                }

                                $html['item'].='<tr style="height:30px">
                                    <td colspan="3">Round Off</td>
                                    <td>-0.32</td>
                                </tr>

                                <tr style="border-top:1px dashed #000;border-bottom:1px dashed #000;height:50px;">
                                    <td colspan="3">Total Payable</td>
                                    <td>1620</td>
                                </tr>


                            </table>
                           
                                   
                                   
                        </div>

                        <div class="row" style="border-top:1px solid #f2f2f2;border-bottom:1px solid #f2f2f2;text-align:center;margin-bottom:10px;">
                            <p>';
                            if($vat_number_flag==1){    
                            $html['item'].='VAT No. - 1238765678';
                            }
                            if($tin_number_flag==1){ 
                            $html['item'].='<br>TIN No. - 00876786546';
                            }
                            if($kot_number_flag==1){ 
                            $html['item'].='<br>KOT No. - 11432445345';
                            }
            $html['item'].='</p>
                        </div>';
            if($footer_message!=''){             
            $html['item'].='<div class="row" style="text-align:center;">'.$footer_message.'</div>';
                }
            $html['item'].='</div>';
		
                    echo json_encode($html);
		/*$this->session->set_flashdata('addBillData','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the pos bill design.
            </div>');
		
		
		redirect('pos_bill_design#');*/
		
	}


    public function updateBillDesign(){
        $pos_bill_design_id=$this->input->post('pos_bill_design_id'); 
        $restaurant_name=$this->input->post('restaurant_name'); 
        $restaurant_address=$this->input->post('restaurant_address');
        $restaurant_phone=$this->input->post('restaurant_phone'); 
        $header_message=$this->input->post('header_message');
        $footer_message=$this->input->post('footer_message');
        $vat_flag=$this->input->post('vat_flag');
        $service_tax_flag=$this->input->post('service_tax_flag');
        $service_charge_for_room_service_flag=$this->input->post('service_charge_for_room_service_flag');
        $tin_number_flag=$this->input->post('tin_number_flag');
        $vat_number_flag=$this->input->post('vat_number_flag');
        $date_time_flag=$this->input->post('date_time_flag');
        $bill_number_flag=$this->input->post('bill_number_flag');
        $table_number_flag=$this->input->post('table_number_flag');
        $room_number_flag=$this->input->post('room_number_flag');
        $kot_number_flag=$this->input->post('kot_number_flag');
        $token_number_flag=$this->input->post('token_number_flag');
        $hotel_id=$this->session->userdata('hotel_id');

        $data=array(
                    'pos_bill_restaurant_name'=>$restaurant_name,
                    'pos_bill_restaurant_address'=>$restaurant_address,
                    'pos_bill_restaurant_phone'=>$restaurant_phone,
                    'pos_bill_header_message'=>$header_message,
                    'pos_bill_footer_message'=>$footer_message,
                    'pos_bill_vat_flag'=>$vat_flag,
                    'pos_bill_service_tax_flag'=>$service_tax_flag,
                    'pos_bill_tin_number_flag'=>$tin_number_flag,
                    'pos_bill_vat_number_flag'=>$vat_number_flag,
                    'pos_bill_date_time_flag'=>$date_time_flag,
                    'pos_bill_number_flag'=>$bill_number_flag,
                    'pos_bill_table_number_flag'=>$table_number_flag,
                    'pos_bill_room_number_flag'=>$room_number_flag,
                    'pos_bill_kot_flag'=>$kot_number_flag,
                    'pos_bill_token_number_flag'=>$token_number_flag,
                    'pos_bill_room_service_charge_flag'=>$service_charge_for_room_service_flag
                    );
        
        /*print_r($data); 
        echo "pos_bill_design_id=".$pos_bill_design_id;*/
        updateDataCondition("pos_bill_design",$data,"pos_bill_design_id = ".$pos_bill_design_id);

        $this->session->set_flashdata('updateBillData','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the pos bill design data.
            </div>');
        
        
        redirect('pos_bill_design#');

    }    
  



	

}
