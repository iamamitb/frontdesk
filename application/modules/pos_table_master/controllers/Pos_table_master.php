<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos_table_master extends MX_Controller {
	
	    public function __construct()
		 {
        parent::__construct();
        $this->load->helper('utility');
		$this->load->model('pos_table_master_model');
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
		is_logged_in(); 
   		 }

	public function index()
	{
		$data['allRecord']=getData("table_master","pos_table_status = '1'");
		$account_id=$this->session->userdata('id');
		$data['wizard_status']=getValue('account','wizard_status','account_id = '.$account_id);
		$this->template->title('Table Master','Imanagemyhotel');
		$this->template->set('metaDesc','Imanagemyhotel');
		$this->template->set('metaKeyword','Imanagemyhotel');
		$this->template->set_layout('pos_template','front');
		$this->template->build('pos_table_master',$data);
				
	}

	public function addTable(){
		$table_number=$this->input->post('table_number'); 
		$no_of_seats=$this->input->post('no_of_seats');
		$hotel_id=$this->session->userdata('hotel_id');

		$data=array('hotel_id'=>$hotel_id,
					'pos_table_number'=>$table_number,
					'no_of_seats'=>$no_of_seats,
					'pos_table_status'=>'1'
					);
		
		//print_r($data); die();
		$q = $this->db->get_where('table_master',array('pos_table_number' => $table_number));
		
		if ( $q->num_rows() > 0 ){
			  $this->db->where('pos_table_number',$table_number);
			  $this->db->update('table_master',$data);

			  $this->session->set_flashdata('chktabledata','<div class="alert alert-danger alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                POS Table already exists.
            </div>');
			redirect('pos_table_master#');

		}else{ 
			  $material_id=insertValue('table_master',$data);
		} 	
		
		
		$this->session->set_flashdata('addtabledata','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully added the POS Table.
            </div>');
		
		
		redirect('pos_table_master#');
		
	}

	public function editTable($idt) { 
		$table=getSingle('table_master','pos_table_id = '.$idt,'','','','');
		
		$html['item']='<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			      <h3 class="modal-title">Edit Table</h3>
			    </div>
		    <form method="post" id="table_form_edit" action="'.base_url().'pos_table_master/updateTable" >
			<input type="hidden" id="tableID" name="tableID" value="'.$table->pos_table_id.'"> 
		      <div class="modal-body">
		        <div class="row">
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Table Number<span class="error">*</span></label>
		              <input type="text" id="table_number" name="table_number" class="form-control" value="'.$table->pos_table_number.'" required>
		             </div>
		          </div>
		          <div class="col-md-6">
		            <div class="form-group">
		              <label for="field-1" class="control-label">Number of Seats<span class="error">*</span></label>
		              <input type="text" id="no_of_seats" name="no_of_seats" class="form-control digit_only1" value="'.$table->no_of_seats.'" required>
		              <span class="error_msg1" style="color:red"></span>
		            </div>
		          </div>
		        </div>
		        
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
		        <button type="submit" name="edit_table" id="edit_table" class="btn btn-success waves-effect waves-light">Update Table</button>
		      </div>
		    </form>
		  </div>
		</div>';
		
		echo json_encode($html);

	}

	public function updateTable() {
		$tableID=$this->input->post('tableID'); 
		$table_number=$this->input->post('table_number'); 
		$no_of_seats=$this->input->post('no_of_seats');
		
		$data=array(
					'pos_table_number'=>$table_number,
					'no_of_seats'=>$no_of_seats,
					'pos_table_status'=>'1'
					);

  		$tableUpdate=updateDataCondition('table_master',$data,'pos_table_id = '.$tableID);
                          
  		$this->session->set_flashdata('msgTableUpdate','<div class="alert alert-success alert-dismissable" style="text-align:center;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have successfully updated the Table.
            </div>');	
                          
  		redirect('pos_table_master#');

	}


	public function tableDelete() { 
		
		$table_id=$_POST['table_id'];		
	
		$data['pos_table_status']='0';			
		
		$table_delete=updateDataCondition('table_master',$data,'pos_table_id = '.$table_id);

		$html['type']='sucess';
		echo json_encode($html);
	}	




	

}
