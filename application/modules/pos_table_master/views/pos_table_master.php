<link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<!-- Page-Title -->
<div class="row">
<div class="col-sm-12"><h1 class="pull-left page-title">Table Master</h1></div>
</div>
<!-- Page-Title Ends-->

<!-- Page content starts -->  
<div class="row">

    <!-- Tabbed pane starts --> 
    <div class="col-md-12"> 
         
        <ul class="nav nav-tabs navtab-bg"> 
            <li class="active"> 
                <a href="#all_material" data-toggle="tab" aria-expanded="false"> 
                    <span class="visible-xs"><i class="fa fa-home"></i></span> 
                    <span class="hidden-xs">All Tables</span> 
                </a> 
            </li> 
                
            <li class=""> 
                <a href="#add_material" data-toggle="tab" aria-expanded="true"> 
                    <span class="visible-xs"><i class="fa fa-user"></i></span> 
                    <span class="hidden-xs">Add Table</span> 
                </a> 
            </li> 
        </ul> 

        <div class="tab-content"> 
            <?=$this->session->flashdata("addtabledata"); ?>
            <?=$this->session->flashdata("chktabledata"); ?> 
            <?=$this->session->flashdata("msgTableUpdate"); ?> 
            <div class="tab-pane active" id="all_material"> 
            <!-- Row for Table Starts -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 style="font-weight:bold;margin-bottom:30px;">All Tables</h2>
                    <table id="datatable" class="table table-striped table-bordered">
                        <?php 
                            if(isset($allRecord) && count($allRecord)>0){
                            ?>
                        <thead>
                            <tr>
                                <th>Table Number</th>
                                <th>Number of Seats</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <?php 
                            foreach($allRecord as $table):{
                          ?> 
                        <tbody>
                        <tr>
                            <td><?=$table->pos_table_number?></td>
                            <td><?=$table->no_of_seats?></td>
                            <td>
                                <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Manage <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    
                                    <li><a href="#"  data-toggle="modal" onclick="editTable(<?=$table->pos_table_id?>)" data-target="#edit-table">Edit</a></li>
                                    <li><a onclick="tableDelete(<?=$table->pos_table_id?>)" href="javascript:void(0)">Delete</a></li>
                                </ul>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>    
                        </tbody>
                        <?php endforeach;  } else{  ?> 
                        
                     <tbody> 
                        <tr >
                        <th style="text-align:center;">You have not added any Table Yet.</th>
                        </tr> 
                     </tbody> 
                     <?php } ?>
                    </table>

                </div>
            </div>
           <!-- Row for Table ends Starts -->
            </div>

            <div class="tab-pane" id="add_material"> 
                <h2 style="font-weight:bold;margin-bottom:60px;">Add Table</h2>
                <form name="table_form" method="post" id="table_form" action="<?php echo base_url(); ?>pos_table_master/addTable">
                <div class="row">
                    <div class-"col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Table Number <span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="table_number" name="table_number" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Number of Seats<span class="error">*</span></label>
                                    <div class="col-md-9">
                                        <input type="text" id="no_of_seats" name="no_of_seats" class="form-control" required aria-required="true">
                                    </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" style="margin-top:45px;text-align:center;"> 
                        <button class="btn btn-success btn-lg m-b-5">Add Table</button>
                    </div>
                </div>
                </form> 
                



            </div>
        </div>
    </div>
    <!-- Tabbed pane Ends -->
                            
</div> <!-- End row -->
<!-- Page content ends -->  
<!-- modal --> 
<div id="edit-table" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
</div>
<!-- /.modal -->

<script src="<?=base_url()?>themes/js/jquery.min.js"></script>
<script src="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.min.js"></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/jquery.validate.min.js'></script>
<script src='<?=base_url()?>themes/plugins/jquery-validation/dist/additional-methods.js'></script>
<script>

<?php if($wizard_status==0){ ?>
        swal({   
            title: "Master Entry Needed",   
            text: "You need to fill important details about your restaurant first.", 
            type: "warning"    
        },
    function(){
            window.location.href = '<?=base_url()?>wizard';
        });
<?php } ?> 


$(document).ready(function() {
    $("#table_form").validate({
         rules: {
            
            table_number: {
                required: true,                     
                noSpace: true
            },
            no_of_seats: {
                required: true,
                number: true
            }
            
         },
         messages: {
            table_number: {
                    required: "Please enter table number"
                },    
            no_of_seats: {
                    required: "Please enter no of seats",
                    number: "Please specify a number"
                }        
         },
        
    });

});



$(document).on('keypress', '.digit_only1', function(e){    
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('.error_msg1').html("Digits Only").show().fadeOut("slow");
        return false;
    }     
});


function editTable(idt)
{
    var idt = idt;
    //alert(idt);
   
    $.ajax({
        url:"<?php echo site_url('pos_table_master/editTable');?>/"+idt,
        type: 'POST',
        dataType: "json",
        data: {'idt': idt},  
        success:function(data)
        { 
            //alert(data) ; 
            $('#edit-table').html(data.item);
            
        }      
    });
}



$('#edit_table').click(function(){

   var tableID = $("#tableID").val();
   
    $.ajax({
        url:"<?php base_url() ?>pos_table_master/updateTable",
        type: 'POST',
        dataType: "json",
        data: {tableID : tableID},
        success:function(data)
        { 
          alert(data); 
        }      
    });
});


function tableDelete(table_id) {
    //alert(table_id); 
    swal({
        title: "Are you sure?",
        text: "This POS table will be deleted and cannot be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
         }, function (isConfirm) {
            //alert(isConfirm); 
        if (!isConfirm) return;

        $.ajax({
            url: "<?=base_url()?>pos_table_master/tableDelete",
            type: 'POST',
            dataType: "json",
            data: {'table_id': table_id},  
            
        success: function(msg) {
            swal("Done!", "It was succesfully deleted!", "success");
                setTimeout(function(){
                window.location.reload(1);
                }, 1000);
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error"); 
                
            }
        });
    });
}





</script>
