<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/*************************check for login*************************************************/
	function is_logged_in() {
	    $CI =& get_instance();
	    $is_logged_in_admin = $CI->session->userdata('is_logged_in');
		$CI->session->userdata('id');		
		 if($is_logged_in_admin != true )
			redirect('login');	
		 
	}

/*************************Hotel Limit left for any account*************************************************/	
	function get_hotel_limit_left($account_id)  { 		
		if(!empty($account_id)) { 
			$hotel_limit = getValue('account', 'hotel_limit', 'account_id = '.$account_id) ; 		   
			$total = count_rows("hotels", "account_id = ".$account_id." and hotels_status = '1'") ;								   
			return ($hotel_limit-$total);					  
		}
		 
	}
 /**************************************************************************/
	function random_gen($length){
	  	$random= "";
	  	srand((double)microtime()*1000000);
	  	$char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	 	// $char_list .= "abcdefghijklmnopqrstuvwxyz";
	  	$char_list .= "1234567890";
	  	// Add the special characters to $char_list if needed
	
	  	for($i = 0; $i < $length; $i++) {    
		 	$random .= substr($char_list,(rand()%(strlen($char_list))), 1);  
	  	}  
	  	return $random;
	}

/**************************************************************************/
	function get_state_list($state=''){ 		
		$row =array("Andaman And Nicobar"=>"Andaman And Nicobar",
					"Andhra Pradesh"=>"Andhra Pradesh",
					"Arunachal Pradesh"=>"Arunachal Pradesh",
					"Assam"=>"Assam","Bihar"=>"Bihar",
					"Chandigarh"=>"Chandigarh",
					"Chhattisgarh"=>"Chhattisgarh",
					"Dadra And Nagar Haveli"=>"Dadra And Nagar Haveli",
					"Daman And Diu"=>"Daman And Diu",
					"Goa"=>"Goa","Gujarat"=>"Gujarat",
					"Haryana"=>"Haryana",
					"Himachal Pradesh"=>"Himachal Pradesh",
					"Jammu And Kashmir"=>"Jammu And Kashmir",
					"Jharkhand"=>"Jharkhand",
					"Kerala"=>"Kerala",
					"Karnataka"=>"Karnataka",
					"Madhya Pradesh"=>"Madhya Pradesh",
					"Maharashtra"=>"Maharashtra",
					"Manipur"=>"Manipur",
					"Meghalaya"=>"Meghalaya",
					"Mizoram"=>"Mizoram",
					"Nagaland"=>"Nagaland",
					"New Delhi"=>"New Delhi",
					"Orissa"=>"Orissa",
					"Pondicherry"=>"Pondicherry",
					"Punjab"=>"Punjab",
					"Rajasthan"=>"Rajasthan",
					"Sikkim"=>"Sikkim",
					"Tamil Nadu"=>"Tamil Nadu",
					"Telangana"=>"Telangana",
					"Tripura"=>"Tripura",
					"Uttar Pradesh"=>"Uttar Pradesh",
					"Uttrakhand"=>"Uttrakhand",
					"West Bengal"=>"West Bengal");
		$html ='<select name="state"  class="form-control"  required aria-required="true">';
		$html .='<option value="">Select State here</option>';
		foreach($row as $val){
			if($val==$state){
				$slected='selected="selected"';
			}else{
				$slected='';
			}
			$html .='<option value="'.$val.'" '.$slected.'>'.$val.'</option>';   
		}
   		$html .='</select>';
   		return $html;
	}
/**************************************************************************/	
	function get_payment_type_list($payment_type = '',$required = ''){ 
		$row =array("cash"=>"Cash",
			 "cheque"=>"Cheque",
			 "demand_draft"=>"Demand Draft",
			 "credit_card"=>"Credit Card",
			 "debit_card"=>"Debit Card",
			 "neft"=>"NEFT",
			 "rtgs"=>"RTGS",
			 "payment_by_company"=>"Payment by compnay"
			 );
		if($required == '')
			$html .='<select name="payment_type" id="payment_type"  class="form-control" >';   
		else  
			$html .='<select name="payment_type" id="payment_type"  class="form-control"  required aria-required="true">'; 
		$html .='<option value="">Please select payment type</option>';
		foreach($row as $key=>$val){
			if($payment_type == $key)
				$html .='<option value="'.$key.'" selected="selected" >'.$val.'</option>';   
			else  
				$html .='<option value="'.$key.'" >'.$val.'</option>'; 
		}
		$html .='</select>';
		return $html;
	}
/**************************************************************************/	
	function get_id_card_type_list($card_type = ''){ 
		$row =array("driving_licence"=>"Driving Licence",
					"passport"=>"Passport",
					"voter_card"=>"Voter Card",
					"aadhar_card"=>"Aadhar Card",
					"pan_card"=>"PAN Card",
					"others"=>"Others"
					);
		$html ='<select name="id_card_type" id="id_card_type"  class="form-control"  required aria-required="true">';
//		$html .='<option value="">Please select id card type</option>';
		foreach($row as $key=>$val){
			if($card_type == $key)
				$html .='<option value="'.$key.'" selected="selected" >'.$val.'</option>'; 
			else  
				$html .='<option value="'.$key.'" >'.$val.'</option>'; 
		}
		$html .='</select>';
		return $html;
	}
/**************************************************************************/	
	function get_food_plan($hotel_id="",$plan="ep,ap,cp,map"){ 
		if(!empty($hotel_id)){ 
		$CI = & get_instance(); 
		$query=$CI->db->query("SELECT count(id) as c FROM `plan_type` pt 
							   	join hotel_plan_type hpt on
								pt.`plan_type_id`=hpt.`plan_type_id`
								where hpt.hotel_id='$hotel_id' and pt.name='$plan'");
		$row = $query->row();		
		$hotel_plan=$row->c;
		$attr=($hotel_plan == '1') ? "" : "disabled";
		return $attr;
		}
	}
/**************************************************************************/
	function dateformate($datetime,$formate="",$simbol=""){ 
		$formate=($formate=='')?"d-m-Y":$formate;
		$simbol=($simbol=='')?".":$simbol;	 
		$datetime = strtotime($datetime);
		$mysqldate = date($formate, $datetime);
		return  str_replace("-",$simbol,$mysqldate);	
	
	}
/**************************************************************************/
		function get_net_rate($price="",$general_percentage="",$pricenew=""){ 
		if(!empty($general_percentage)){ 
			if(!empty($pricenew)){ 
			$net_price=$pricenew-($pricenew*$general_percentage/100);
			return round($net_price, 0 , PHP_ROUND_HALF_EVEN);
			
			}
			else {
			$net_price=$price-($price*$general_percentage/100);
			return round($net_price, 0, PHP_ROUND_HALF_EVEN);
			}
		}else{
			return $price;
		}
	}	  
/**************************************************************************/

// Function added by Ashwani Kumar...

// define a function which will return a list of record in the form of drop down list...

	function get_data_dropdown($table_name, $field_id, $field_name, $where="", $select_id="", $id="", $name="", $attr="", $order_by_fld="", $order_by="",$class=""){	 
		if(!empty($table_name) && !empty($field_id) && !empty($field_name)) { 
			//get main CodeIgniter object
			$CI = & get_instance();       
	       	$CI->db->select($field_id.','.$field_name);
	       	$CI->db->from($table_name);
			if($where!="")
	       	$CI->db->where($where);
	        if($order_by_fld != '')
				$CI->db->order_by($order_by_fld,$order_by);
			$query = $CI->db->get(); 
			$row = $query->result_array();
			if(!empty($row)){
				$html = '<select class="form-control '.$class.'" id="'.$id.'" name="'.$name.'" '.$attr.'>';
				$html .= '<option value="">Select</option>';
				foreach($row as $val){
					if($val[$field_id] == $select_id){$slected='selected="selected"';}else{$slected='';}
					$html .= '<option value="'.$val[$field_id].'" '.$slected.'>'.$val[$field_name].'</option>';   
				}
				$html .= '</select>';
		   } else  {
			   $html = "";
		   }
		   return $html;
		}

	}

############################### Get Value from Database for specific field ##########
	
	function getValue($tableName, $field_name, $param){
		//get main CodeIgniter object
		$CI = & get_instance();       
		$CI->db->select($field_name);
		$query = $CI->db->get_where($tableName, $param);
		$row = $query->row_array();
		return count($row)?$row[$field_name]:"";
	}
	
	############################### Return array from specific table ###################
	
	function getData($tableName, $param){
		//get main CodeIgniter object
		$CI = & get_instance();       
		$query = $CI->db->get_where($tableName, $param);
		return $query->result();
	}	

############################### Return number of rows ########################	
	
	function count_rows($tableName, $param) {
		//get main CodeIgniter object
		$CI = & get_instance();       
		if(isset($param) && $param!="")
			$CI->db->where($param);
		$query = $CI->db->get($tableName);
		$CI->db->last_query();;
		return $query->num_rows();
	}
	
	############################### Return a array from specific table ##################
	function getSingle($table,$where_clause,$order_by_fld,$order_by,$limit,$offset) {
		//get main CodeIgniter object
		$CI = & get_instance();       
        if($where_clause != '')
        	$CI->db->where($where_clause);
        if($order_by_fld != '')
			$CI->db->order_by($order_by_fld,$order_by);
        if($limit != '' && $offset !='')
            $CI->db->limit($limit,$offset);		
        $CI->db->select('*');
        $CI->db->from($table);
        $query = $CI->db->get();  
         return $query->row(); 
	}	   

	############################### Return a array from specific table ##################
	function getAll($table,$where_clause="",$order_by_fld="",$order_by="",$limit="",$offset="") {
		//get main CodeIgniter object
		$CI = & get_instance();       
		if($where_clause != '')
			$CI->db->where($where_clause);
        if($order_by_fld != '')
		    $CI->db->order_by($order_by_fld,$order_by);
		if($limit != '')
		    $CI->db->limit($limit,$offset);		
		$CI->db->select('*');
		$CI->db->from($table);
		$query = $CI->db->get();  
		//echo $this->db->last_query();
		 return $query->result(); 
	}
	
	 ############################## Return result from specific table ##################
	
	function getResult($tableName){
		//get main CodeIgniter object
		$CI = & get_instance();       
		$query = $CI->db->get($tableName);
		return $query->result();
	}
			
	############################## Insert array for specific table #############
	function insertValue($table, $row)	{
		//get main CodeIgniter object
		$CI = & get_instance();       
		$str = $CI->db->insert_string($table, $row);        
		$query = $CI->db->query($str);    
		$insertid = $CI->db->insert_id();
		return $insertid;
	}
			
	############### Delete Record from a table ########################
	function Delete_data($table,$where)	{
		//get main CodeIgniter object
		$CI = & get_instance();       
		$CI->db->delete($table, $where); 
	}

	############################### Update table record ###################
	
	function updateData($id, $tableName, $data , $autoCol = 'id') {
		//get main CodeIgniter object
		$CI = & get_instance();       
		$CI->db->where($autoCol ,  $id);
		$CI->db->update($tableName, $data);
		return TRUE;
	}

	############################### Update table record ###################
	
	function updateDataCondition($tableName, $data, $where)	{
		//get main CodeIgniter object
		$CI = & get_instance();       
//		$CI->db->where('id', $id);
		$CI->db->where($where);
		$CI->db->update($tableName, $data);
		//echo $CI->db->last_query(); die();
		return TRUE;
	}
		
	############################### Return an array of specific fields from specific table ##################
	function getAllFields($table, $fields="", $where_clause="", $order_by_fld="", $order_by="", $limit="", $offset="") {
		//get main CodeIgniter object
		$CI = & get_instance();       
		if($fields != '')
			$CI->db->select($fields);
		else
			$CI->db->select('*');
		if($where_clause != '')
			$CI->db->where($where_clause);
		if($order_by_fld != '')
			$CI->db->order_by($order_by_fld,$order_by);
		if($limit != '' && $offset !='')
			$CI->db->limit($limit,$offset);		
		$CI->db->from($table);
		$query = $CI->db->get();  
		 return $query->result(); 
	}
/**************************************************************************/
	function get_meal_plans($hotel_id='',$attr="") { 
		$CI = & get_instance();
		$query = $CI->db->query("select * from plan_type order by plan_type_id");
		$row = $query->result_array();
		$slected='';
		if($hotel_id!=""){
			$query1 = $CI->db->query("select plan_type_id from hotel_plan_type where hotel_id=$hotel_id");
			$row1 = $query1->result_array();
		}
	   	$html="";
		   foreach($row as $val){
			   if($hotel_id!=""){
				foreach($row1 as $val1){
					if($val['plan_type_id']==$val1['plan_type_id']){
						$slected='checked="checked"'; break;
					}else{
						$slected='';
					}
				}}
		$html .='<div class="col-sm-3"><input type="checkbox" id="checkbox1" name="meal_plans[]" value='.$val['plan_type_id'].' '.$attr.' '.$slected.'><label for="checkbox1">'.$val['name'].'</label></div>';   
	   }
	   return $html;
	}

	############################### Return an array of specific fields from specific table ##################

	function unique_sort($arrs, $id) {
		$unique_arr = array();
	   	foreach ($arrs as $key=>$arr) {
		 	if (!in_array($arr[$id], $unique_arr)) {
				$unique_arr[] = $arr[$id];
			}
		}
		sort($unique_arr);
		return $unique_arr;
	}
	############### get the day difference ##################
	function DateDiff($startDate,$endDate){
		$startDateX = explode("/", $startDate);
		$endDateX =  explode("/", $endDate);
		$date1 =  mktime(0, 0, 0, $startDateX[0],$startDateX[1],$startDateX[2]); //m/d/y
		$date2 =  mktime(0, 0, 0, $endDateX[0],$endDateX[1],$endDateX[2]); //m/d/y
		$interval =($date2 - $date1)/(3600*24);
		return  $interval ;
	}

	############### get the day difference ##################
//	both date must be in d-m-Y format only	
	function DaysDiff($startDate,$endDate){
		$startDateX = explode("-", $startDate);
		$endDateX =  explode("-", $endDate);
		$date1 =  mktime(0, 0, 0, $startDateX[1], $startDateX[0], $startDateX[2]); //d-m-Y
		$date2 =  mktime(0, 0, 0, $endDateX[1], $endDateX[0], $endDateX[2]);//d-m-Y
		$interval =($date2 - $date1)/(3600*24);
		// returns numberofdays
		return  $interval ;
	}
	############################### download report ##################

	function download_report($query,$filename="report.csv") {
	   $CI = & get_instance();
	   $CI->load->dbutil();
			 $CI->load->helper('download');
	   $delimiter = ",";
	   $newline = "\r\n";
	   header('Content-Type: application/csv'); 
	   header('Content-Disposition: attachment; filename="' . $filename . '"'); 
	   $data = $CI->dbutil->csv_from_result($query, $delimiter, $newline);
	   force_download($filename, $data);
	}
############################### module name ##################
 	function module_name(){
		$refs = array();
		$list = array();
		$items= getData('menu','status = 1');
    	foreach($items as $data){			
			$thisref = &$refs[ $data->menu_id ];
			$thisref['menu_id'] = $data->menu_id;
			$thisref['menu_name'] = $data->menu_name;
			$thisref['menu_url'] = $data->menu_url;
		   	if (empty($data->parent_id)){
			   $list[ $data->menu_id ] = &$thisref;
		   	}else{
			   $refs[ $data->parent_id ]['children'][ $data->menu_id ] = &$thisref;
		   	}		
		}
			return $list;
 	}
############################### create menu as user ##################		
	function create_menu($modules,$role=array()){	
		$html = "\n<ul>\n";
		foreach ($modules as $key=>$val){
			if(in_array($val['menu_id'],$role))	{ 
				$url=($val['menu_url']!="") ? base_url().$val['menu_url'] : "javascript:void(0)";
				$html .= '<li><a href="'.$url.'" >'.$val['menu_name'];
				if (array_key_exists('children', $val)){
					$html .= create_menu($val['children'],$role);
				}
				$html .= "</a></li>\n";
			}
		}
		$html .= "</ul>\n";
		return $html;
	}
############################### create menu for admin ##################		
	
	function create_menu_admin($arr){			
		$html = "\n<ul>\n";
		foreach ($arr as $key=>$v){
			$url=($v['menu_url']!="") ? base_url().$v['menu_url'] : "javascript:void(0)";
			$html .= '<li><a href="'.$url.'">'.$v['menu_name'];
			if (array_key_exists('children', $v)){
				$html .= create_menu_admin($v['children']);
			}
			$html .= "</a></li>\n";
		}
		$html .= "</ul>\n";
		return $html;
	}
############################## check privileges ################################################
    function is_privileges(){
		$CI =& get_instance();
		$id=$CI->session->userdata('id');
		$email=$CI->session->userdata('email');
		$actype=count_rows("account","account_email = '".$email."'");
		if($actype<=0){
			$menu_url=$CI->router->class;
			$menu_id=getValue("menu","menu_id","menu_url = '".$menu_url."'");
			$role=json_decode(getValue("user_role","role_priviledge","id = ".$id));
			$newrole=$role->menu_name;	
			if(!in_array($menu_id,$newrole))
				redirect('dashboard');
		}		
	}
############################## added contry rop down helper ...##############################
	
    function country_dropdown( $name="country", $top_countries=array(), $selection=NULL, $show_all=TRUE  )  {
        // You may want to pull this from an array within the helper
        $CI =& get_instance();
    	
    	$countries = $CI->config->load('country_list');

        $html = "<select name='{$name}' id='{$name}'  {$selection} class='form-control'>";
        $selected = NULL;
        if(in_array($selection,$top_countries))  {
            $top_selection = $selection;
            $all_selection = NULL;
        }else  {
            $top_selection = NULL;
            $all_selection = $selection;
        }

        if(!empty($top_countries))  {
            foreach($top_countries as $value)  {
                if(array_key_exists($value, $countries))  {
                    if($value === $top_selection)  {
                        $selected = "SELECTED";
                    }
                    $html .= "<option value='{$value}' {$selected}>{$countries[$value]}</option>";
                    $selected = NULL;
                }
            }
        }

        if($show_all)  {
            foreach($countries as $key => $country)  {
                if($key === $all_selection)  {
                    $selected = "SELECTED";
                }
                $html .= "<option value='{$key}' {$selected}>{$country}</option>";
                $selected = NULL;
            }
        }

        $html .= "</select>";
        return $html;
    }	

//  get the country name from country drop down list...this function will be used to show nationality..
	function get_country_name($country_code)  {
        // You may want to pull this from an array within the helper
        $CI =& get_instance();
    	
    	$countries = $CI->config->load('country_list');

        foreach($countries as $key => $country)  {
            if($key === $country_code)  {
                return $country;
            }
        }
    }
	
	function SendEmail($from,$frommane,$to,$subject,$message)  {
		$CI =& get_instance();
		$CI->email->from($from,$frommane);
		$CI->email->to($to);
		//$CI->email->cc('another@another-example.com');
		//$CI->email->bcc('them@their-example.com');
		$CI->email->subject($subject);
		$CI->email->message($message);
		$CI->email->send();
	}

/**************************************************************************/
	function get_hotels($role_id='',$attr="") { 
		$CI = & get_instance();
		$query = $CI->db->query("select hotel_id,hotel_name from hotels where account_id = ".$CI->session->userdata('account_id')." order by hotel_name");
		$row = $query->result_array();
		
		$slected='';
		if($role_id!=""){
			$query1 = $CI->db->query("select hotel_id from user_role where role_id=$role_id");
			$row12 = $query1->row_array();
			$row1=explode(",",$row12['hotel_id']);
		}
	   	$html="";
		   foreach($row as $val){
			   if($role_id!=""){
				foreach($row1 as $val1){
					if($val['hotel_id']==$val1){
						$slected='checked="checked"'; break;
					}else{
						$slected='';
					}
				}}
		$html .='<div class="col-sm-3"><div class="checkbox checkbox-success"><input type="checkbox" id="checkbox1" name="hotel_id[]" value='.$val['hotel_id'].' '.$attr.' '.$slected.'><label for="checkbox1">'.$val['hotel_name'].'</label></div></div>';   
	   }
	   return $html;
	}			
?>