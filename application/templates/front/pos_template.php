<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <title><?php echo $template['title'];?></title>
        <link rel="shortcut icon" href="<?=base_url()?>themes/images/favicon_1.ico">
        <link href="<?=base_url()?>themes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <link href="<?=base_url()?>themes/css/slicknav.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/pos.css" rel="stylesheet" type="text/css">
        <script src="<?=base_url()?>themes/js/modernizr.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                <a href="<?php echo base_url(); ?>" class="logo"><img src="<?=base_url()?>themes/images/immh-logo2.png"> </a>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                             <ul class="nav navbar-nav navbar-right pull-right">
                              <!-- Toggle Starts -->
                        
                          <div class="btn-group btn-group-justified" style="width:250px;float:left;">
                              <a class="btn btn-success" role="button" style="opacity:0.5;" href="<?php echo base_url(); ?>dashboard">FRONTDESK</a>
                              <a class="btn btn-danger" role="button" href="<?php echo base_url(); ?>posdashboard">POS</a>
                          </div>
                        
                        <!-- Toggle Ends -->
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true" style="text-transform: uppercase;"> <?php echo $this->session->userdata('user_name'); ?> <i class="ion-arrow-down-b" style="font-size: 15px;color: #565656;margin-left: 5px;"></i></a>
                                    <ul class="dropdown-menu" style="margin-top:10px;">
                                    <li><a href="javascript:void(0)"> Account ID: <?php echo $this->session->userdata('id'); ?> </a></li>
                                    <li><a href="javascript:void(0)"> Hotel Name: <?php echo $this->session->userdata('hotel_name'); ?> </a></li>
                                        <li><a href="<?php echo base_url(); ?>profile"> Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>login/logout"> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
          

            <!-- Have added the manual menu but you need to replace it with dynamic menu. Look at the bottom of the page for instructions -->

             <!-- Custom menu starts -->
                
                    <div class="custom_menu">
                    <ul>
                      <li><a href="posdashboard">POS Dashboard</a></li>
                      <li>Master
                        <ul>
                        <?php $account_id=$this->session->userdata('id');
    $wizard_status=getValue('account','wizard_status','account_id = '.$account_id);
    if($wizard_status=='1'){
    ?>
                          <li><a href="pos_supplier_master">Supplier Master</a></li> <?php }else{ ?> 
                            <li><a href="wizard">Supplier Master</a></li>
                          <?php } ?>
                          <li><a href="pos_unit_master">Unit Master</a></li> 
                          <li><a href="pos_raw_material_master">Raw Material Master</a></li> 
                          <li><a href="pos_store_master">Store Master</a></li> 
                          <li><a href="pos_tax_settings">Tax Settings</a></li> 
                          <li><a href="pos_table_master">Table Master</a></li> 
                          <li><a href="pos_steward_master">Steward Master</a></li> 
                          <li><a href="pos_bill_design">Bill Design</a></li> 
                          
                        </ul>
                      </li>
                      <li>POS
                        <ul>
                          <li><a href="pos_pos">POS</a></li>
                          <li><a href="pos_bill">Bill</a></li> 
                        </ul>
                      </li>
                      <li><a href="pos_category_management">Category Management</a></li>
                      <li><a href="pos_menu_management">Menu Management</a></li>
                      <li>Material Management
                        <ul>
                          <li><a href="pos_purchase_and_stock_entry">Purchase</a></li>
                          <li><a href="pos_material_issue">Material Issue</a></li> 
                          <li><a href="pos_supplier_payment">Supplier Payment</a></li>
                        </ul>
                      </li>
                      <li>Reports
                        <ul>
                          <li><a href="pos_material_purchase_report">Material Purchase Report</a></li>
                          <!-- <li><a href="pos_daywise_purchase">Daywise Purchase</a></li> 
                          <li><a href="pos_supplier_wise_purchase">Supplier Wise Purchase</a></li>  
                          <li><a href="pos_daywise_sales">Day Wise Sales</a></li> 
                          <li><a href="pos_itemwise_sales">Item Wise Sales</a></li> 
                          <li><a href="pos_current_material_stock">Current Material Stock</a></li> 
                          <li><a href="pos_profit_loss">Profit/Loss</a></li> -->
                          <li><a href="pos_daywise_issue">Material Issue Report</a></li> 
                          <li><a href="pos_daywise_service_tax_report">POS Sales Report</a></li> 
                          <li><a href="pos_tax_report">POS Tax Report</a></li>
                        </ul>
                      </li>
                      <li>System
                        <ul>
                          <!-- <li><a href="user_management.php">User Management</a></li>  -->
                          <li><a href="pos_settings.php">Settings</a></li> 
                          
                        </ul>
                      </li>
                     
                      
                    </ul>
                    </div>
                <!-- Custom menu ends -->
                
            <div class="content-page">
                <div class="content">
                    <div class="container">
<?php echo $template['body']; ?>
</div> <!-- container -->
                </div> <!-- content -->
<!-- page content ends -->
<!-- Enter footer here -->
            </div>
<!-- Enter Right Sidebar here -->
        </div>

 <!-- Footer starts -->

<!--<div class="footer">
  <div align="center">
    Your system was last synced on 15 March 2016 at 9:40 PM. <b><a href="#">Sync Now</a></b>
  </div>

  <div class="loader">
    <div class="progress-bar"><div class="progress-stripes"></div><div class="percentage">0%</div></div>
  </div>

  <div align="center"><span>Sync in progress...</span></div>
</div> -->

<!-- Footer ends -->  

        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>
        <!-- Main  -->
        
        <script src="<?=base_url()?>themes/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>themes/js/detect.js"></script>
        <script src="<?=base_url()?>themes/js/fastclick.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.blockUI.js"></script>
        <script src="<?=base_url()?>themes/js/waves.js"></script>
        <script src="<?=base_url()?>themes/js/wow.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.scrollTo.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.app.js"></script>
        <script src="<?=base_url()?>themes/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="<?=base_url()?>themes/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>themes/plugins/datatables/dataTables.bootstrap.js"></script>
     <script src="<?=base_url()?>themes/js/jquery.slicknav.min.js"></script>
         <script src="<?=base_url()?>themes/js/jquery.cookie.js"></script>
         <script type="text/javascript">
                    $(document).ready(function() {
         // add a cookie for base url...
                // the purpose of this cookie would be to get the base url through out the application 
                // by using cookie we can get the base url on each page's js file...so that ini different js file we will have a base url and it will help to use ajax....
                $.cookie("base_url", "<?=base_url();?>");
                // add a cookie for account id ...
                // the purpose of this cookie would be to get the logged in account id through out the application 
                // by using cookie we can get the account id on each page's js file...so that in different js file we will have a base url and account id it will help to use ajax....
                $.cookie("account_id", "<?=$this->session->userdata('account_id'); ?>");
            $(function() {
                  $('#nav > ul').slicknav({
                    prependTo:'#mobile-menu'
                  });
                });
                $('#datatable').dataTable();
                $('#datatable2').dataTable(); //just in case page has multiple data tables.
                $('#datatable3').dataTable();
                $('#datatable4').dataTable();
                $('#datatable5').dataTable();
                $('#datatable6').dataTable();               
                jQuery('#timepicker').timepicker({defaultTIme: false});
                jQuery('#timepicker2').timepicker({defaultTIme: false});                
            });
            function yScroll(){
              var navigation = document.getElementById("nav");
              var yPos = window.pageYOffset;

              if (yPos > 66) {
                  navigation.style.top = "0px";
                  navigation.style.position = "fixed";
                  navigation.style.width = "91.5%";
                  navigation.style.zIndex="999";
                  navigation.style.left = "0";
                  navigation.style.right = "0";
                  navigation.style.margin = "auto";
                  
              } else{
                  navigation.style.top = "0px";
                  navigation.style.position = "relative";
                  navigation.style.width = "initial";
                  navigation.style.left = "initial";
                  navigation.style.right = "initial";
                  navigation.style.margin = "inherit";
                  navigation.style.zIndex="999";
                  navigation.style.marginLeft = "30px";
                  navigation.style.marginRight = "30px";
                  
              };
            }
            window.addEventListener("scroll", yScroll);
            
        </script>
  </body>
</html>


<!-- Code for custom menu, added new table for POS menu and added functions in UTILITY helper 
  but its not working. You need to fix it and replace the manual menu above -->

 <!-- Get the code from this page http://codepaste.net/hpkw5w -->           