<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <title><?php echo $template['title'];?></title>

        <link rel="shortcut icon" href="<?=base_url()?>themes/images/favicon_1.ico">  

        <link href="<?=base_url()?>themes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/responsive.css" rel="stylesheet" type="text/css">

        <script src="<?=base_url()?>themes/js/modernizr.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        
    </head>
    <body style="background-color:#FFF;">
<?php  echo $template['body']; ?>
</div>                                 
                
            </div>
        </div>


        <!-- Main  -->
        <script src="<?=base_url()?>themes/js/jquery.min.js"></script>
        <script src="<?=base_url()?>themes/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>themes/js/detect.js"></script>
        <script src="<?=base_url()?>themes/js/fastclick.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.blockUI.js"></script>
        <script src="<?=base_url()?>themes/js/waves.js"></script>
        <script src="<?=base_url()?>themes/js/wow.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.scrollTo.min.js"></script>

        <script src="<?=base_url()?>themes/js/jquery.app.js"></script>
	
	</body>
</html>
