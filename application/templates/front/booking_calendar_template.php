<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <title><?php echo $template['title'];?></title>
        <link rel="shortcut icon" href="<?=base_url()?>themes/images/favicon_1.ico">
        <link href="<?=base_url()?>themes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/core.css" rel="stylesheet" type="text/css">
        <!-- CSS Written specially for Booking Calendar. Do not modify this file. If needed override  CSS in booking_calendar.css -->
        <link href="<?=base_url()?>themes/css/booking_calendar.css" rel="stylesheet" type="text/css">
        <!-- Special CSS Ends -->
        <link href="<?=base_url()?>themes/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <script src="<?=base_url()?>themes/js/jquery.min.js"></script>
        <script src="<?=base_url()?>themes/js/modernizr.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                <a href="<?php echo base_url(); ?>" class="logo"><img src="<?=base_url()?>themes/images/immh-logo2.png"> </a>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"> <?php echo $this->session->userdata('email'); ?> <i class="ion-arrow-down-b" style="font-size: 15px;color: #565656;margin-left: 5px;"></i></a>
                                    <ul class="dropdown-menu" style="margin-top:10px;">
                                    <li><a href="javascript:void(0)"> Account ID: <?php echo $this->session->userdata('id'); ?> </a></li>
                                        <li><a href="<?php echo base_url(); ?>profile"> Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>login/logout"> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            <!-- Custom menu starts -->
                     <?php	$email=$this->session->userdata('email'); 
			  		$id=$this->session->userdata('id');
					$modules=module_name();
					$actype=count_rows("account","account_email = '".$email."'");
					$role=json_decode(getValue("user_role","role_priviledge","id = ".$id));
					$newrole=$role->menu_name;?>
                    <div class="custom_menu">                  
                    <?php if($actype>0)
					echo create_menu_admin($modules);
					else
					echo create_menu($modules,$newrole);?>
					
                    </div>
                <!-- Custom menu ends -->
            <div class="content-page">
                <div class="content">
                    <div class="container">
<?php echo $template['body']; ?>
</div> <!-- container -->
                </div> <!-- content -->
<!-- page content ends -->
<!-- Enter footer here -->
            </div>
<!-- Enter Right Sidebar here -->
        </div>
        <!-- END wrapper -->
       
        <!-- Main  -->

        <script src="<?=base_url()?>themes/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>themes/js/detect.js"></script>
        <script src="<?=base_url()?>themes/js/fastclick.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.blockUI.js"></script>
        
        <script src="<?=base_url()?>themes/js/wow.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.scrollTo.min.js"></script>
         

<!-- //by R-->
<script type="text/javascript" src="<?=base_url()?>themes/js/redips-drag-min.js"></script>
<script type="text/javascript" src="<?=base_url()?>themes/js/booking_calendar.js"></script>
 <script src="<?=base_url()?>themes/js/jquery.cookie.js"></script>
         <script type="text/javascript">
                    $(document).ready(function() {
				 // add a cookie for base url...
                // the purpose of this cookie would be to get the base url through out the application 
                // by using cookie we can get the base url on each page's js file...so that ini different js file we will have a base url and it will help to use ajax....
                $.cookie("base_url", "<?=base_url();?>");
                // add a cookie for account id ...
                // the purpose of this cookie would be to get the logged in account id through out the application 
                // by using cookie we can get the account id on each page's js file...so that in different js file we will have a base url and account id it will help to use ajax....
                $.cookie("account_id", "<?=$this->session->userdata('account_id'); ?>");
			$('html').on('click','.all-collapse',function(e){
			e.preventDefault();
			var num = +$(this).attr("data-value");
			if (num=="0") {
				$(this).attr('data-value', '1');
				$(this).addClass('arrow-mover');
				$( ".expand_all" ).html( "Close All Room Types<span class='collapse-arrow'></span>" );
				$('.all-open-close').addClass('in');
				$('[data-toggle="collapse"]').addClass('arrow-mover');
			}
			else if (num=="1") {
			  $(this).attr('data-value', '0');
			  $(this).removeClass('arrow-mover');
			  $('.all-open-close').removeClass('in');
			  $( ".expand_all" ).html( "Expand All Room Types<span class='collapse-arrow'></span>" );
			  $('[data-toggle="collapse"]').removeClass('arrow-mover');
			}
  });
		$('[data-toggle="collapse"]').on('click', function() {
		  $(this).toggleClass('arrow-mover');
		});
		
		$('.initcell').on('click', function(){
		  $(this).toggleClass('green-white');
		  var greencount = $('td.green-white').length;
		  if (greencount>0) {
				$('.book-btn').addClass('glow-btn');
				$('.book-btn').removeClass('sa-alert');
				$('.book-btn').attr('data-target','#book-room-modal');
		  }
		  else if(greencount==0){
				$('.book-btn').removeClass('glow-btn');
				$('.book-btn').addClass('sa-alert');
				$('.book-btn').attr('data-target','');
		  }
		});
		$('td').on('mouseover mouseout', function(){
		  $(this).prevAll().addBack()
		  .add($(this).parent().prevAll()
		  .children(':nth-child(' + ($(this).index() + 1) + ')')).toggleClass('hover');
		});
		
		var s = $(".top-calender-bar");
		var pos = s.position();     
		var tableOffset = $(".top-calender-bar").offset().top;
		var $header = $("#view > thead").clone();
		var $fixedHeader = $("#header-fixed").append($header);
		
		
		$(window).scroll(function() {
		  var windowpos = $(window).scrollTop();
		  if (windowpos >= 1) {
			s.addClass("topstick");
			$('body').css({
			  'padding-top': s.height()+'px',
			});
			$("#header-fixed").css({
			  'width': $('#view').width()+'px',
			  'top': s.height()+'px',
			})
			$fixedHeader.show();
		
		  } else {
			 s.removeClass("topstick");
			 $('body').css({
			  'padding-top': '0',
			});
			 $fixedHeader.hide();
		  }
		});
	});
</script>

	</body>
</html>