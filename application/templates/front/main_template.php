<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="description" content="">
        <title><?php echo $template['title'];?></title>
        <link rel="shortcut icon" href="<?=base_url()?>themes/images/favicon_1.ico">
        <link href="<?=base_url()?>themes/css/bootstrap.min.css" rel="stylesheet" type="text/css">
         <link href="<?=base_url()?>themes/css/slicknav.min.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/pages.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/menu.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>themes/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
        <script src="<?=base_url()?>themes/js/modernizr.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                <a href="<?php echo base_url(); ?>" class="logo"><img src="<?=base_url()?>themes/images/immh-logo2.png"> </a>
                </div>
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                             <ul class="nav navbar-nav navbar-right pull-right">
                              <!-- Toggle Starts -->
                        
                          <div class="btn-group btn-group-justified" style="width:250px;float:left;">
                              <a class="btn btn-success" role="button" href="<?php echo base_url(); ?>dashboard">FRONTDESK</a>
                              <a class="btn btn-danger" role="button" style="opacity:0.5;" href="<?php echo base_url(); ?>posdashboard">POS</a>
                          </div>
                        
                        <!-- Toggle Ends -->
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true" style="text-transform: uppercase;"> <?php echo $this->session->userdata('user_name'); ?> <i class="ion-arrow-down-b" style="font-size: 15px;color: #565656;margin-left: 5px;"></i></a>
                                    <ul class="dropdown-menu" style="margin-top:10px;">
                                    <li><a href="javascript:void(0)"> Account ID: <?php echo $this->session->userdata('id'); ?> </a></li>
                                    <li><a href="javascript:void(0)"> Hotel Name: <?php echo $this->session->userdata('hotel_name'); ?> </a></li>
                                        <li><a href="<?php echo base_url(); ?>profile"> Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>login/logout"> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <!-- Top Bar End -->
            <!-- Custom menu starts -->
              <?php $email=$this->session->userdata('email'); 
            $id=$this->session->userdata('id');

            $modules=module_name();
            $actype=count_rows("account","account_email = '".$email."'");?>         
                     <div id="mobile-menu"></div>
                    <div class="custom_menu" id="nav">                  
                    <?php if($actype>0)
            echo create_menu_admin($modules);
          else{
            $role=json_decode(getValue("user_role","role_priviledge","id = ".$id));
            $newrole=$role->menu_name;
            echo create_menu($modules,$newrole);}?>
          
          
                    </div>
                <!-- Custom menu ends -->
            <div class="content-page">
                <div class="content">
                    <div class="container">
<?php echo $template['body']; ?>
</div> <!-- container -->
                </div> <!-- content -->
<!-- page content ends -->
<!-- Enter footer here -->
            </div>
<!-- Enter Right Sidebar here -->
        </div>
        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>
        <!-- Main  -->
        
        <script src="<?=base_url()?>themes/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>themes/js/detect.js"></script>
        <script src="<?=base_url()?>themes/js/fastclick.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.slimscroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.blockUI.js"></script>
        <script src="<?=base_url()?>themes/js/waves.js"></script>
        <script src="<?=base_url()?>themes/js/wow.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.nicescroll.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.scrollTo.min.js"></script>
        <script src="<?=base_url()?>themes/js/jquery.app.js"></script>
        <script src="<?=base_url()?>themes/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <script src="<?=base_url()?>themes/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>themes/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>themes/plugins/datatables/dataTables.bootstrap.js"></script>
     <script src="<?=base_url()?>themes/js/jquery.slicknav.min.js"></script>
         <script src="<?=base_url()?>themes/js/jquery.cookie.js"></script>
         <script type="text/javascript">
                    $(document).ready(function() {
         // add a cookie for base url...
                // the purpose of this cookie would be to get the base url through out the application 
                // by using cookie we can get the base url on each page's js file...so that ini different js file we will have a base url and it will help to use ajax....
                $.cookie("base_url", "<?=base_url();?>");
                // add a cookie for account id ...
                // the purpose of this cookie would be to get the logged in account id through out the application 
                // by using cookie we can get the account id on each page's js file...so that in different js file we will have a base url and account id it will help to use ajax....
                $.cookie("account_id", "<?=$this->session->userdata('account_id'); ?>");
            $(function() {
                  $('#nav > ul').slicknav({
                    prependTo:'#mobile-menu'
                  });
                });
                $('#datatable').dataTable();
                $('#datatable2').dataTable(); //just in case page has multiple data tables.
                $('#datatable3').dataTable();
                $('#datatable4').dataTable();
                $('#datatable5').dataTable();
                $('#datatable6').dataTable();               
                jQuery('#timepicker').timepicker({defaultTIme: false});
                jQuery('#timepicker2').timepicker({defaultTIme: false});                
            });
            /*function yScroll(){
              var navigation = document.getElementById("nav");
              var yPos = window.pageYOffset;

              if (yPos > 66) {
                  navigation.style.top = "0px";
                  navigation.style.position = "fixed";
                  navigation.style.width = "91.5%";
                  navigation.style.zIndex="999";
                  navigation.style.left = "0";
                  navigation.style.right = "0";
                  navigation.style.margin = "auto";
                  
              } else{
                  navigation.style.top = "0px";
                  navigation.style.position = "relative";
                  navigation.style.width = "initial";
                  navigation.style.left = "initial";
                  navigation.style.right = "initial";
                  navigation.style.margin = "inherit";
                  navigation.style.zIndex="999";
                  navigation.style.marginLeft = "30px";
                  navigation.style.marginRight = "30px";
                  
              };
            }*/
            //window.addEventListener("scroll", yScroll);
            
        </script>
  </body>
</html>