	// defiine a global variable to use the number of total more room addedd...
	var no = 1 ; 
	var no_of_day;
	var tax_amount = 0;
	var discount_amount = 0;
	var total_amount = 0;
	var due_amount = 0;
 	var total_room_tariff = 0;

	$(document).ready(function(){
//		alert("page loaded..");
//		$("#message").hide();
//		$(".travel").hide();
//		$(".corporate").hide();
//		$("#discount").hide();
		$(".bank_name_status").hide();
		$("#phone").mask("9999999999"); // mobile no validation..
		$("#travel_agent_phonenumber1").mask("9999999999"); // mobile no validation..
		$("#travel_agent_phonenumber2").mask("9999999999"); // mobile no validation..
		$("#card_no").mask("9999-9999-9999-9999"); // 
		$("#expiry_date").mask("99/99"); // 
	 	var base_url = $.cookie("base_url");
	 	var account_id = $.cookie("account_id");; // global decalration so dat we can use it any where..
//		by default we will hide payment type and date...if a guest is paying some amount in advance then only payment type and date will be visible...so on key up in advance filed we will mark it visible...
		$(".advance_payment").hide();		
		
// write a validation code which will restrict not to add a space bar at first character..

		$(document).on('keydown', '.required', function(e){ 
            if (e.which === 32 &&  e.target.selectionStart === 0){
            	swal({   
            		title: "", 
            		type: "error",
            		text: "First character cannot be blank!",   
            		timer: 1000,   
            		showConfirmButton: false 
            	});            	
           		return false;             
            } 
        });

// write a validation which will accept only numbers in fileds values. But 0 shouldn't be allowed at 1st place.
		
		$(document).on('keypress', '.only_digit', function(e){ 
		   	if (this.value.length == 0 && e.which == '48' ){
			  return false;
		   	} else if (e.which < 48 || e.which > 57 ){
				return false;		   
		   	}
        });

// write a validation which will check if an advance filed have some values more than 0 then it will display payment type and payment date...otherwise it will keep hiding these fileds...
		
		$(document).on('keyup', '#advance', function(e){ 
		   	if (this.value.length > 0 && this.value > 0 ){
			  	$(".advance_payment").show();
		   	} else {
				$(".advance_payment").hide();		   
		   	}
        });

// write a validation which will accept only characters in fileds values. 

		$(".only_character").keypress(function (e){
	        var code =e.keyCode || e.which;
	        if((code<65 || code>90)&&(code<97 || code>122)&&code!=32&&code!=46){
				swal("","Only alphabates are allowed.","error");
	          	return false;
	        }
	    });		

		// on the basis of discount type selection we need to display discount amount ..
		// by default discount amount will be hidden..		
		$('input[type=radio][name=discount_type]').change(function () {			
			var discount_type = $(this).val();
			if(discount_type != "" ){
				$("#discount").show();
				if(discount_type == "percentage" ){
					$("#discount_amount").attr("maxlength",2);
					$("#discount_amount").val('');
				}
				if(discount_type == "cash" ){
					$("#discount_amount").attr("maxlength",5);
					$("#discount_amount").val('');
				}
				calculate_total_room_tarrif(1);// calculate total room price after change of price.. 
			}

        });
		
		$(".existing_customer").click(function(){
			//alert("clk on close btn");			
			$('#ext_customer').prop('checked', false);
		});

		// check in and check out date picker...
		
		$("#payment_date").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 });

		$("#checkout_date" ).datetimepicker({dateFormat: 'dd-mm-yy', minDate: 0, controlType: 'select',	oneLine: true,	timeFormat: 'hh:mm tt',altField: "#check_out_time" }).bind("change", function (e) {
            var maxDate = $(this).val();
            maxDate = $.datepicker.parseDate("dd-mm-yy", maxDate);
            maxDate.setDate(maxDate.getDate());
            $(".stay_date_from").datepicker( "option", "maxDate", maxDate );
            $(".stay_date_to").datepicker( "option", "maxDate", maxDate );
            $("#payment_date").datepicker( "option", "maxDate", maxDate );			
		}); 

		$(document).on('focus', '.stay_date_from', function(){
			var id = $(this).attr("id");
			var no = id.substring(15);//stay_date_from_1,stay_date_to_1
///			var minDate = $("#"+id).val();
			var minDate = $("#checkin_date").val();
			var maxDate = $("#checkout_date").val();
	//		alert(id+no+minDate+maxDate);
			
			$("#"+id).datepicker({ dateFormat: 'dd-mm-yy', minDate: minDate, maxDate: maxDate,controlType: 'select',	oneLine: true}).bind("change",function(){
	            var minDate = $(this).val();
	            minDate = $.datepicker.parseDate("dd-mm-yy", minDate);
	            minDate.setDate(minDate.getDate()+1);
				$("#stay_date_to_"+no).datetimepicker( "option", "minDate", minDate );
	           	calculate_total_room_tarrif(1);// calculate total room price after change of price..
	        });
		});

		$(document).on('focus', '.stay_date_to', function(){
			var id = $(this).attr("id");
			var no = id.substring(13);
			var maxDate = $("#checkout_date").val();
			var minDate = $("#stay_date_from_"+no).val();
	//		alert(id+no+minDate+maxDate);
			
			$("#"+id).datepicker({ dateFormat: 'dd-mm-yy', minDate: minDate, maxDate: maxDate, controlType: 'select',	oneLine: true}).bind("change",function(){
	           calculate_total_room_tarrif(1);// calculate total room price after change of price..
	        });
		});


		// write a code which will be used in a case if a hotel is having 5 room types..but at the time of reservation we have used only 3 room types......sso when at the time to edit a booking we will get an option to add one more room....once you will click on that button we need to fetch data from controller...by considering only remaining room types and the other thing will be same like remove and add room...

		$(document).on('click', '.add_one_more_room', function(){ 
        	//alert("clk on a m r");
        	var no_of_div = $("#no_of_div").val();
        	var hotel_id = $("#hotel_id").val();
        	var booking_id = $("#booking_id").val();
			var dataString = 'no_of_div='+no_of_div+'&hotel_id='+hotel_id+'&booking_id='+booking_id;
        	var stay_date_from = $("#stay_date_from_"+no_of_div).val();
        	var stay_date_to = $("#stay_date_to_"+no_of_div).val();
        	var no_of_rooms = $("#no_of_rooms_"+no_of_div).val();
        	if(stay_date_from != "" && stay_date_to != "" && no_of_rooms != ""){
				var dataString = 'no_of_div='+no_of_div+'&hotel_id='+hotel_id+'&booking_id='+booking_id;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/get_remaining_room_type/",
		 			success:function(return_data){
	//	 				$(".add_one_more_room").hide();
		 				$("#add_room").append(return_data);
		 				no_of_div++;
		 				$("#no_of_div").val(no_of_div);
		 			}
				});        		
        	}else{
				swal("","Please enter required field's value before adding new room.","error");
				return false;        		
        	}

 		});

		// dynamically create and delete for select room for check in...
        var no_of_checkin_room = 1;

		$(document).on('click', '.remove_room', function(){ 
        	var id = $(this).attr('id');
        	var no_of_div = $("#no_of_div").val();
        	// now we need to check a condition that a user can't delete any div element from middle...so it must be from last div only...
        	if(no_of_div == id){
	        	$("#"+id).remove();	
				--no_of_div;
				$("#no_of_div").val(no_of_div);
				calculate_total_room_tarrif(1);// calculate total room price after change of price..        		
        	} else{
        		// it means its deleting from middle of the record....so we need to stop them to delete it from midle..
        		swal("", "Please remove from last records only. You can't remove from middle.", "error");		
           		return false; 
        	}
        });
		

		
		// write a code to get the guest details on the basis of mobile no or boooking id and fiil up that values in respected fileds..
		
		$("#fill_guest_details").click(function(){
//			alert("clkc on submit btn...");
			var mobile_no = $("#mobile").val();
			
			if(mobile_no == "" ){
				swal("","Please enter either mobile no ","error")
				return false;				
			} else {
			// it means both or either one field is filed up..
			// so on the basis of filled filed's value we need to decide whether we are going to fetch the value by using mobile no or by booking id..
			// if both fileds are fillled up then it must be valid mobile no and booking id..
			// now we need to test whether both fileds are filled up or not 
				// it means mobile fileds have values...
				var dataString = 'mobile_no='+mobile_no;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/get_guest_detail_mobile/",
		 			success:function(return_data){
		 				if(return_data == "0"){
							swal("","Please check your mobile number.","error")		 					
		 					$("#mobile").val('');
		 					$("#mobile").focus();
		 					return false;
		 				} else {
			 				var return_data = $.parseJSON(return_data);
	//							alert(return_data);
							$('#guest_name').val(return_data[0]);
							$('#phone').val(return_data[1]);
							$('#address').val(return_data[2]);
							$('#email').val(return_data[3]);
							$('#guest_id').val(return_data[4]);
							$("#existing-customer-modal").modal('toggle');		 					
		 				}
		 			}
				});
			}
		});

		// By default meal plan of hotel will be hidden..
		// it will be enabled or shown after selecting a hotel from drop down list....
		// meal plan will be displayed on the basis of hotel and avialibility of meal plan for that hotel..
		$("#meal_plan_type").hide();

		$('#hotel_id').change(function() {
			//alert("change on select..")
			if($('#hotel_id').val() != ""){
				/// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
				var hotel_id = $('#hotel_id').val();
				
				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_meal_plan/"+hotel_id,
		 			success:function(return_data){
//						alert(return_data);
						$("#meal_plan_type").show();
						$('#meal_plan').html('');
						$('#meal_plan').html(return_data);
		 			}
				});

				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_hotel_room_type/"+hotel_id,
		 			success:function(return_data){
//						alert(return_data);						
						$('#room_type').html('');
						$('#room_type').html(return_data);
		 			}
				});

				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_hotel_info/"+hotel_id,
		 			success:function(return_data){
						var return_data = $.parseJSON(return_data);		 				
//						alert(return_data);						
						$('#service_tax').val(return_data[0]);
						$('#service_charge').val(return_data[1]);
		 			}
				});

			} else {
				$("#meal_plan_type").hide();
			}
		    
		});

	// write a code to register a travel agent by clicking the button..after registering an agent it must be selected in the drop down list..
		
		$("#add_travel_agent").click(function(){
//			alert("clkc on submit btn...");
			var travel_agent_name = $("#travel_agent_name").val().trim();
			var travel_agent_address = $("#travel_agent_address").val().trim();
			var travel_agent_phonenumber1 = $("#travel_agent_phonenumber1").val().trim();
			var travel_agent_phonenumber2 = $("#travel_agent_phonenumber2").val().trim();
			var travel_agent_contact_person = $("#travel_agent_contact_person").val().trim();
			var travel_agent_email_address = $("#travel_agent_email_address").val().trim();
			var travel_agent_website = $("#travel_agent_website").val().trim();
			
			
			if(travel_agent_name == "" || travel_agent_address == "" || travel_agent_phonenumber1 == "" || travel_agent_contact_person == ""){
				swal("","Please fill out all required fields","error")				
				return false;				
			} else {
			// it means all required fields are filled up ..
			// now we need to test for valid email and website address...			
				if(travel_agent_email_address != "" ){
					var email_filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
					if (!email_filter.test(travel_agent_email_address)) {						
						swal("","Please enter a valid email address.","error");						
						$("#travel_agent_email_address").val('');
						$("#travel_agent_email_address").focus();
						return false;
					}
				}					
				if(travel_agent_website != ""){
					var url_filter = /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
					if (!url_filter.test(travel_agent_website)) {
						swal("","Please enter a valid website url. Your website url must start with http://www. or https://www.","error");						
						$("#travel_agent_website").val('');
						$("#travel_agent_website").focus();
						return false;
					}
				}
				// now we need to test the unique travel agent name	from db table..
				// if the same travel agent name is already registered in table thn we need to display an error message..

				$.ajax({
		 			type:"POST",
		 			data:travel_agent_name,
		 			url:base_url+"reservation/is_agent_name/"+travel_agent_name+"/"+account_id+"/1",
		 			success:function(return_data){
//						alert(return_data);
						if(return_data == 1){
							// it means the current travel agent name is already in db table..so we will avoid the current agent name..
							swal("","This travel agent name is already in our record. Please use another travel agent name.","error");						
							$("#travel_agent_name").val('');
							$("#travel_agent_name").focus();
							return false;
						} 				
		 			}
				});

				// it means all validations are done now we need to insert the records in DB table ...
				var dataString = 'account_id='+account_id+'&travel_agent_name='+travel_agent_name+'&travel_agent_address='+travel_agent_address+'&travel_agent_phonenumber1='+travel_agent_phonenumber1+'&travel_agent_phonenumber2='+travel_agent_phonenumber2+'&travel_agent_email_address='+travel_agent_email_address+'&travel_agent_website='+travel_agent_website+'&travel_agent_contact_person='+travel_agent_contact_person;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/add_travel_agent/",
		 			success:function(return_data){
//						alert(return_data);
						// now we have inserted the record in table....
						// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
						var travel_agent_id = return_data;
						var dataString = 'travel_agent_id='+travel_agent_id;
						$.ajax({
				 			type:"POST",
				 			data:dataString,
				 			url:base_url+"reservation/get_all_travel_agent_list/",
				 			success:function(return_data){
//								alert(return_data);
								$("#travel_agent_list").html('');
								$("#travel_agent_list").html(return_data);
												
				 			}
						});
		 			}
				});
				// now we need to close the travel agent registration modal.
				$("#travel-agent-modal").modal('toggle');				
				
			}
		});

	 	// call the modal to display the no of available room for selected hotel, plan etc..		
		
		$(document).on('change', '.room_id', function(){
			// get id attribute values on the basis of class name click..
			var select_id = $(this).attr('id');
			var room_no = [];
			// we need to get the no of row for check in room...it will be id values like 1,2 etc....
			// it will be in form of room_id_1.....
			no = select_id.substring(8);
			if($('#'+select_id).val() != ""){
				var room_id = $('#'+select_id).val();
				// now we need to write a condition where we will chck whether we have entered checked in date or not..
				// if checked in date and meal plan is not entered then we will alert an error message..
				var checkin_date = $('#checkin_date').val();
				var checkout_date = $('#checkout_date').val();
				var plan_type_id = $('#plan_type_id').val();
				var occupancy = $('input[name=occupancy\\['+no+'\\]\\[\\]]:checked').val();
				var travel_agent_id = $('#travel_agent_id').val();
				var corporate_agent_id = $('#corporate_agent_id').val();
//				var room_no = $('input[name=room_no\\[\\]]').val();
//				var selected_room_id = $('input[name=room_id\\[\\]]').val();
				$('input[name^="room_no"]').each(function() {
					if($(this).val() != "")
				    	room_no.push($(this).val());
				});				
				
				if(checkin_date != "" && plan_type_id != "" && checkout_date != "" ){
					var dataString = 'room_id='+room_id+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date+'&account_id='+account_id+'&room_no='+room_no+'&no='+no;
					$.ajax({
			 			type:"POST",
			 			data:dataString,
			 			url:base_url+"reservation/get_hotel_room/",
			 			success:function(return_data){
//							alert(return_data);
							//  now we need to check whether we have any vacant room or not....
							if(return_data == ""){// it means there is no any room vacant....so we will just display a message..
								$('#room-number-selection-modal').modal('hide');
								// remove all previous values if anything is there...
								$("#room_rate_"+no).val('');
								$("#net_rate_"+no).val('');
								$("#extra_bed_price_"+no).val('');
								$("#extra_person_charge_"+no).val('');
								$("#luxury_tax_"+no).val('');
								$("#luxury_tax_amount").val('');
								swal("","Please select any other room type and date. All rooms are booked under this type and date.","error");
								$("#room_id_"+no+" > option").removeAttr("selected");
								return false;
							}else {								
								// now we have inserted the record in table....
								// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
								$("#room_number").html('');
								$("#room_number").html(return_data);												

								// get rooom price on the basis of meal plan and room type and occupancy...					
								var dataString = 'room_id='+room_id+'&plan_type_id='+plan_type_id+'&occupancy='+occupancy+'&travel_agent_id='+travel_agent_id+'&corporate_agent_id='+corporate_agent_id+'&no='+no+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date;
								$.ajax({
						 			type:"POST",
						 			data:dataString,
						 			url:base_url+"reservation/get_room_plan_price/",
						 			success:function(return_data){
						 				var return_data = $.parseJSON(return_data);
//										alert(return_data);
										// now we have inserted the record in table....
										// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
										$("#room_rate_"+no).val('');
										$("#room_rate_"+no).val(return_data[0]);												
										$("#net_rate_"+no).val('');
										$("#net_rate_"+no).val(return_data[0]);												
										$("#extra_bed_price_"+no).val('');
										$("#extra_bed_price_"+no).val(return_data[1]);												
										$("#extra_person_charge_"+no).val('');
										$("#extra_person_charge_"+no).val(return_data[2]);	
										$("#luxury_tax_"+no).val('');
										$("#luxury_tax_"+no).val(return_data[3]);												
										$("#luxury_tax_amount").val('');
										$("#luxury_tax_amount").val(return_data[3]);	

						 			}
								});

							 	$('#room-number-selection-modal').modal();

							 	calculate_total_room_tarrif(1);
							}
			 			}
					});
				} else {
					$("#room_id_"+no+" > option").removeAttr("selected");
					swal("","Please enter your check in date and select meal plan type.","error");					
					return false;
				}
			}
		    
		});

		// on the basis of agent selection we need to display agent ..
		// by default travel agent will be selected..
		// corporate agent will be displayed if its selected..
		$('input[type=radio][name=agent]').change(function () {			
			var agent = $(this).val();
			if(agent != "" && agent == "travel" ){
				$(".travel").show();
				$(".corporate").hide();
			} else if(agent != "" && agent == "corporate" ){
				$(".travel").hide();
				$(".corporate").show();
			}

        });

		// we need to write a jquery code to add and remove a css class by using toggle class..
		// this will be used to select and deselect a room number..
		$(document).on('click', '.room_number_bg', function(){
			// get unique room id or get the id for clicked fileds..
			var room_number = $(this).attr('id');
//			$("#"+room_number).removeClass("btn-default");
  			$("#"+room_number).toggleClass("btn-inverse");
		});

		$(document).on('click', '.add_more_room_number', function(){ 
        	//alert("clk on a m r");
        	var id = $(this).attr("id");
			var no = id.substring(21); // add_more_room_number_1
        	
        	var room_id = $("#room_id_"+no).val();
        	var hotel_id = $("#hotel_id").val();
           	var checkin_date = $("#checkin_date").val();
           	var checkout_date = $("#checkout_date").val();
			var plan_type_id = $('#plan_type_id').val();
			var occupancy = $('input[name=occupancy\\['+no+'\\]\\[\\]]:checked').val();
//			var travel_agent_id = $('#travel_agent_id').val();
			var room_no = [];

// 			$('input[name^="room_number\\['+no+'\\]\\[\\]"]').each(function() {
// 				if($(this).val() != ""){
// //					alert(no+$(this).val());
// 			    	room_no.push([no,$(this).val()]);
// 				}
// 			});				

			$('input[name^="room_no"]').each(function() {
				if($(this).val() != "")
			    	room_no.push($(this).val());
			});				

			if(checkin_date != "" && plan_type_id != "" && checkout_date != "" ){
				var dataString = 'room_id='+room_id+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date+'&account_id='+account_id+'&room_no='+room_no+'&no='+no;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/get_hotel_room/",
		 			success:function(return_data){
//						alert(return_data);
						//  now we need to check whether we have any vacant room or not....
						if(return_data == ""){// it means there is no any room vacant....so we will just display a message..
							$('#room-number-selection-modal').modal('hide');
							swal("", "Please select a room from another type and date. All rooms are booked under this type and date.", "error");							
							return false;
						}else {								
							// now we have inserted the record in table....
							// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
							$("#room_number").html('');
							$("#room_number").html(return_data);												

						 	$('#room-number-selection-modal').modal();
						 	calculate_total_room_tarrif(1);
						}
		 			}
				});
			} else {
				$("#room_id_"+no+" > option").removeAttr("selected");
				swal("","Please enter your check in date and select meal plan type.","error")					
				return false;
			}

 		});

		// write a function which will calculate the total number of room selected on the basis on inverse class..

		

		$("#select_room").click(function(){
			var total_selected_room = 0;
			var room_id = [];
			var no = $("#no").val();
			var room_no = [];			
					
			$('.btn-inverse').each(function() { 
			  	var id = $(this).attr('id');
			  	if(id != undefined) {
			    	room_id.push(id);			    	
			    	total_selected_room++;
			  	}
			});
//			alert(total_selected_room+"   "+room_id+"   "+room_id);
			// now we need to insert total selected room values to the respective text box..
			$("#no_of_rooms_"+no).val(total_selected_room);
			$("#room-number-selection-modal").modal('toggle');
			$("#room_no_"+no).val(room_id);
			$("#display_room_no_"+no).text(room_id);
		 	// now we need to calculate the total room tarrif cost...
		 	// it will be based on net rate * total no of selected rooms * no of day/night stayed in hotel +
		 	// extra bed * extra bed price * no of day/night stayed in hotel +
		 	// extra person * extra person charge * no of day/night stayed in hotel	

		 	calculate_total_room_tarrif(1);

		});

		// for payment type we need by default hide all bank related html..
		// we will shoow and hide accordingly on the basis of payment type selected from drop down..

		$("#cheque_details_central_reservation").hide();
		$("#dd_details_central_reservation").hide();
		$("#chq_details_central_reservation").hide();	
		$("#cc_details_central_reservation").hide();
		$("#neft_details_central_reservation").hide();

		$('#payment_type').change(function() {
			//alert("change on select..")
			if($('#payment_type').val() != ""){
				/// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
				var payment_type = $('#payment_type').val();

				if(payment_type == "cash" || payment_type == "payment_by_company" ){

					$("#cheque_details_central_reservation").hide();					
					$("#dd_details_central_reservation").hide();
					$("#chq_details_central_reservation").hide();
					$("#cc_details_central_reservation").hide();
					$("#neft_details_central_reservation").hide();
					$(".bank_name_status").hide();											
				}
				
				if(payment_type == "cheque" || payment_type == "demand_draft" || payment_type == "credit_card" || payment_type == "debit_card" || payment_type == "neft" || payment_type == "rtgs"){
					//$("#bank_details_central_reservation").show();
					if(payment_type == "cheque" ){
						$("#cheque_details_central_reservation").show();
						$(".bank_name_status").show();
						$("#chq_details_central_reservation").show();
						$("#dd_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").hide();		
					}
					if(payment_type == "demand_draft" ){
						$("#cheque_details_central_reservation").show();
						$("#dd_details_central_reservation").show();
						$(".bank_name_status").show();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").hide();		
					}
					if(payment_type == "credit_card" || payment_type == "debit_card" ){
						$("#cheque_details_central_reservation").hide();
						$("#dd_details_central_reservation").hide();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").show();
						$("#neft_details_central_reservation").hide();
						$(".bank_name_status").hide();		
					}
					if(payment_type == "neft" || payment_type == "rtgs" ){
						$("#cheque_details_central_reservation").show();
						$(".bank_name_status").show();
						$("#dd_details_central_reservation").hide();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").show();		
					}
				}
										

			} else {
			
				$("#cheque_details_central_reservation").hide();					
				$("#dd_details_central_reservation").hide();
				$("#chq_details_central_reservation").hide();
				$("#cc_details_central_reservation").hide();
				$("#neft_details_central_reservation").hide();
				$(".bank_name_status").hide();		
			}
		    
		});			
		
	});

	// we need to write a jquery code to add and remove a css class by using toggle class..
	// this will be used to select and deselect a room number..
	$(document).on('keyup', '.calculate_room_tarrif', function(){

		calculate_total_room_tarrif(1);// calculate total room price after change of price..

	});

	// write a code to calculate the total amount oon the basis of discount, service tax, charge , misc charges, advance etc...
	$(document).on('keyup', '.calculate_total_amount', function(){
		
		var discount_amount = 0;
		var tax_amount = 0;
		var total_amount = 0;
		var grand_total = 0;
		var due = 0;

		// readonly fileds values...
		var total_room_tariff = $("#total_room_tariff").val();
		var luxury_tax_amount = $("#luxury_tax_amount").val();
		var tax_amount = $("#tax_amount").val();
		var total_amount = $("#total_amount").val();
		var grand_total = $("#grand_total").val();
		var due = $("#due").val();
		// editable fields value....
		var discount_amount = $("#discount_amount").val();
		var discount_type = $('input[type=radio][name=discount_type]:checked').val();
		var service_tax = $("#service_tax").val();
		var service_charge = $("#service_charge").val();
		var misc_charge = $("#misc_charge").val();
		var advance = $("#advance").val();


		// before proceeding to the calculation of amount we need to make sure that total room tariff, total amount, grant total fields values are not empty...

		if($.isNumeric(total_room_tariff) && total_room_tariff != 0 && total_room_tariff != "" && $.isNumeric(total_amount) && total_amount != 0 && total_amount != "" && $.isNumeric(grand_total) && grand_total != 0 && grand_total != "" ) {

		 	if(discount_amount != "" && discount_amount != 0 && discount_type != "" ){
		 		if(discount_type == "percentage")
		 			discount_amount = parseInt(((discount_amount * total_room_tariff) / 100));
		 		else if(discount_type == "cash"){
		 			// discount amount must not be more than total room tarrif....
		 			if(parseInt(discount_amount) <= parseInt(total_room_tariff))
		 				discount_amount = parseInt(discount_amount);
		 			else{
		 				swal("","Please check discount amount value. It must not be more than total room tarrif","error");		 		
				 		$("#discount_amount").val('');
				 		$("#discount_amount").focus();
		 			}
		 		}
		 	} 

			tax_amount = 0;
		 	if(luxury_tax_amount != "" && luxury_tax_amount != 0 ){
		 		tax_amount = parseInt(luxury_tax_amount);
		 	} 
		 	if(service_tax != "" && service_tax != 0 ){
		 		tax_amount = parseInt(tax_amount) + parseInt(((service_tax * total_room_tariff) / 100));
		 	} 
		 	if(service_charge != "" && service_charge != 0 ){
		 		tax_amount = parseInt(tax_amount) + parseInt(((service_charge * total_room_tariff) / 100));
		 	} 
		 	total_amount = parseInt(tax_amount) + parseInt(total_room_tariff) - parseInt(discount_amount);
		 	grand_total = total_amount;
		 	
		 	if(misc_charge != "" && misc_charge != 0 ){
		 		grand_total = parseInt(total_amount) + parseInt(misc_charge);
//		 		alert(grand_total);				
		 	} 
		 	due = grand_total;
		 	
		 	if(advance != "" && advance != 0 && advance < grand_total){
		 		due = grand_total - advance;
		 	} else if(advance > grand_total){
//		 		due = grand_total;	
				swal("","Please check advance amount value.","error")	 		
		 		$("#advance").val('');
		 		$("#advance").focus();
//		 		return false;
		 	}							 	
			$("#tax_amount").val(tax_amount.toFixed(2));
			$("#total_amount").val(total_amount.toFixed(2));
			$("#grand_total").val(grand_total.toFixed(2));
			$("#due").val(due.toFixed(2));	 		
		}
		calculate_total_room_tarrif(1);// calculate total room price after change of price..
	});

	// we will define a global function which will be used to calculate the date differences...
	// this calculation will be done on the basis of no of day, net rate, extra bed price etc....

	function calculate_total_room_tarrif(no){

	 	// now we need to calculate the total room tarrif cost...
	 	// it will be based on net rate * total no of selected rooms * no of day/night stayed in hotel +
	 	// extra bed * extra bed price * no of day/night stayed in hotel +
	 	// extra person * extra person charge * no of day/night stayed in hotel	
//	 	alert(" no = "+no);
		var checkin_date = $('#stay_date_from_'+no).val();
		var checkout_date = $('#stay_date_to_'+no).val();
	 	var net_rate = $("#net_rate_"+no).val();
	 	var extra_bed = $("#extra_bed_"+no).val();
	 	var extra_bed_price = $("#extra_bed_price_"+no).val();
	 	var extra_person = $("#extra_person_"+no).val();
	 	var extra_person_charge = $("#extra_person_charge_"+no).val();
	 	var luxury_tax = $("#luxury_tax_"+no).val();
	 	var no_of_rooms = $("#no_of_rooms_"+no).val();
	 	var luxury_tax_amount = $("#luxury_tax_amount").val();
	 	var service_tax = $("#service_tax").val();
	 	var service_charge = $("#service_charge").val();
	 	var discount_amount = $("#discount_amount").val();
	 	var discount_type = $('input[type=radio][name=discount_type]:checked').val();
	 	var misc_charge = $("#misc_charge").val();
	 	var advance = $("#advance").val();
	 	var no_of_room = $("#no_of_div").val();
	 	var total_room_tariff = 0;
	 	var luxury_tax_amount = 0;

		if(checkin_date != "" && checkout_date != "" && typeof(net_rate) != "undefined" && net_rate != "" && no_of_room != "" && typeof(no_of_room) != "undefined"){
//	we need to write a logic where we will find out how many more rooms are added.....
//  by default one room will be there...
//  so on the basis of no of more rooom added we need to get all the live values from all fields nad calculate the total cost..
			for (i = 1; i <= no_of_room; i++) { 
				// we need to write a jquery code to get the date differences...
				no_of_day = daydiff(parseDate($('#stay_date_from_'+i).val()), parseDate($('#stay_date_to_'+i).val()));
			 	net_rate = $("#net_rate_"+i).val();
			 	no_of_rooms = $("#no_of_rooms_"+i).val();
			 	extra_bed = $("#extra_bed_"+i).val();
			 	extra_bed_price = $("#extra_bed_price_"+i).val();
			 	extra_person = $("#extra_person_"+i).val();
			 	extra_person_charge = $("#extra_person_charge_"+i).val();
			 	luxury_tax = $("#luxury_tax_"+i).val();
//	alert("nor = "+i+" net_rate = "+net_rate+" no_of_room = "+no_of_rooms+" no_of_day = "+no_of_day);
				if(typeof(net_rate) != "undefined" && net_rate != "" && net_rate != 0 && no_of_rooms != "" && typeof(no_of_rooms) != "undefined") {
				 	total_room_tariff = total_room_tariff + (net_rate * no_of_rooms * no_of_day);
				 	
				 	if(extra_bed != "" && extra_bed != 0 && extra_bed_price != "" && extra_bed_price != 0 ){
				 		total_room_tariff = total_room_tariff + (extra_bed * extra_bed_price * no_of_day);
				 	} 
				 	if(extra_person != "" && extra_person != 0 && extra_person_charge != "" && extra_person_charge != 0 ){
				 		total_room_tariff = total_room_tariff + (extra_person * extra_person_charge * no_of_day);
				 	} 
				 	if(luxury_tax != "" && luxury_tax != 0 ){
				 		luxury_tax_amount = luxury_tax_amount + (((luxury_tax * net_rate) / 100) * no_of_rooms * no_of_day);
				 	} 
				}
			}
			tax_amount = 0;
		 	if($.isNumeric(total_room_tariff)){
		 		$("#total_room_tariff").val(total_room_tariff.toFixed(2));

			 	// discount calculation....
			 	if(discount_amount != "" && discount_amount != 0 && discount_type != ""){
			 		if(discount_type == "percentage")
			 			discount_amount = parseInt(((discount_amount * total_room_tariff) / 100));
			 		else if(discount_type == "cash"){
			 			// discount amount must not be more than total room tarrif....
			 			if(parseInt(discount_amount) <= parseInt(total_room_tariff))
			 				discount_amount = parseInt(discount_amount);
			 			else{
			 				swal("","Please check discount amount value. It must not be more than total room tarrif","error");		 		
							$("#discount_amount").val('');
							$("#discount_amount").focus();
//					 		return false;
			 			}
			 		}
			 	} 
				if($("#discount_amount").val() != "" && $("#discount_amount").val() != 0 ){	
				 	total_amount = parseInt(total_room_tariff) - parseInt($("#discount_amount").val());
				}else {
					total_amount = parseInt(total_room_tariff) ;
				}
				// luxury tax calculation....
			 	if(luxury_tax_amount != "" && luxury_tax_amount != 0 ){
			 		tax_amount = parseInt(luxury_tax_amount);
			 	} 
			 	if(service_tax != "" && service_tax != 0 ){
			 		tax_amount = parseInt(tax_amount) + parseInt(((service_tax * total_room_tariff) / 100));
			 	} 
			 	if(service_charge != "" && service_charge != 0 ){
			 		tax_amount = parseInt(tax_amount) + parseInt(((service_charge * total_room_tariff) / 100));
			 	} 

			 	$("#luxury_tax_amount").val(luxury_tax_amount.toFixed(2));
				if(tax_amount != "" && tax_amount != 0 ){
				 	$("#tax_amount").val(tax_amount.toFixed(2));
				 	total_amount = parseInt(tax_amount) + parseInt(total_amount);
				}			 	

			 	if(misc_charge != "" && misc_charge != 0 )
			 		grand_total = parseInt(total_amount) + parseInt(misc_charge);
			 	else
			 		grand_total = parseInt(total_amount);

				$("#grand_total").val(grand_total.toFixed(2));
				
			 	if(advance != "" && advance != 0 && advance < grand_total){
			 		due_amount = parseInt(grand_total) - parseInt(advance);
			 		$("#due").val(due_amount.toFixed(2));
			 	} else if(advance > grand_total){
			 		$("#due").val(grand_total.toFixed(2));
//			 		alert(advance+"  "+grand_total);
			 		swal("","Please check advance amount value.","error")
					$("#advance").val('');
					$("#advance").focus();
//					return false;
			 	}else {
			 		$("#due").val(grand_total.toFixed(2));				 		
			 	}	
				$("#total_amount").val(total_amount.toFixed(2));
			}			
		}	

	}

//		write a function which will work on chnage event of occupancy radio button...
// 		this code will work on change event on radio button...
//		if htel name, meal plan and room type is not selected or not entered.. it wont work....

	function occupancy(no) {

		var room_id = $('#room_id_'+no).val();
		var base_url = $.cookie("base_url");
		var occupancy = $('input[name=occupancy\\['+no+'\\]\\[\\]]:checked').val();
		var plan_type_id = $('#plan_type_id').val();
		var travel_agent_id = "";
		var corporate_agent_id = "";
		var checkin_date = $('#checkin_date').val();
		var checkout_date = $('#checkout_date').val();
		// we need to check a condition where we will check whether any agent is selected or not...if agent is selected....then whcich either travel or corporate...
		if($("input:radio[name='agent']").is(":checked")) {
			//it means agent radio button is checked....
			var agent_type = $("input[name='agent']:checked").val();
			if(agent_type == "travel")
				travel_agent_id = $('#travel_agent_id').val();
			else if(agent_type == "corporate")
				corporate_agent_id = $('#corporate_agent_id').val();
		}

		if(room_id != "" && plan_type_id != "" ){

			var dataString = 'room_id='+room_id+'&plan_type_id='+plan_type_id+'&occupancy='+occupancy+'&travel_agent_id='+travel_agent_id+'&corporate_agent_id='+corporate_agent_id+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date;
			$.ajax({
	 			type:"POST",
	 			data:dataString,
	 			url:base_url+"reservation/get_room_plan_price/",
	 			success:function(return_data){
	 				var return_data = $.parseJSON(return_data);
//						alert(return_data);
					// now we have inserted the record in table....
					// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
					$("#room_rate_"+no).val('');
					$("#room_rate_"+no).val(return_data[0]);
					$("#net_rate_"+no).val('');
					$("#net_rate_"+no).val(return_data[0]);														
					$("#extra_bed_price_"+no).val('');
					$("#extra_bed_price_"+no).val(return_data[1]);
					$("#extra_person_charge_"+no).val('');
					$("#extra_person_charge_"+no).val(return_data[2]);			
					$("#luxury_tax_"+no).val('');
					$("#luxury_tax_"+no).val(return_data[3]);
					
					calculate_total_room_tarrif(1); // calculate total room price after change of price..
	 			}
			});
			
		}else {
			swal("","Please check hotel name, meal plan and room type selected values and try again.","error");
			return false;
		}			

	}

//		we need to write a function where we will check whether any rooom number is checked or unchecked...if room number check box is checked it will add 1 in no of rooms ..if unchecked it will deduct 1 from no of rooms... this will work oon clcik event....on click event we need to pass 2 parameters..so that we can track on which room number we have clicked......

	function room_number(no,no_of_checkbox) {

		if($('#room_number_'+no_of_checkbox).is(':checked')){
			// it means earlier this room number was not checked...but after click its checked...now we need to add 1..
			var no_of_selected_room = $("#no_of_rooms_"+no).val();
			no_of_selected_room = parseInt(no_of_selected_room) + parseInt(1);
			$("#no_of_rooms_"+no).val(no_of_selected_room);
		}else {
			// it means earlier this room number was checked...but after click its unchecked...now we need to substract 1..
			var no_of_selected_room = $("#no_of_rooms_"+no).val();
			no_of_selected_room = parseInt(no_of_selected_room) - parseInt(1);
			// while marking a check box as uncheck we need to check that we can't uncheck all check box..atlist one checked check box must be there...
			// if(no_of_selected_room == 0 && no_of_checkbox == 1){
			// 	// it means there is only one check box and a user is marking it as uncheck..we need to stop it here only...its not allowed...
			// 	swal("", "Only alphabates are allowed.", "error");
	  //         	return false;

			// }
			$("#no_of_rooms_"+no).val(no_of_selected_room);
		}
		calculate_total_room_tarrif(1); // calculate total room price after change of price..
	}

// write a function which will calculate the date difference between two dates....
// input date format must be in d-m-Y format only...
// if we are passing any other format we need to pass it to y-m-d date fromat to date function...
// converting the date in required format...

	function parseDate(str) {
	    var mdy = str.split('-')
	    return new Date(mdy[2], mdy[1]-1, mdy[0]);
	    // default date format = y-m-d
	}
// calculate the date difference...

	function daydiff(first, second) {
	    return Math.round((second-first)/(1000*60*60*24));
	}		