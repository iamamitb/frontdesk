$(document).ready(function() {

	var fromdate=$("#from_booking_date").val();
    var todate=$("#to_booking_date").val();
    var agent_id=$("#agent_id").val();
    var hotel_id=$("#hotel_id").val();
    var booking_id=$("#booking_reference").val();
    if(fromdate != '' || todate != '' || agent_id != '' || hotel_id != '' || booking_id != ''){
        $("#clear_data").show();
    }else{
        $('#clear_data').hide();
    }

    
	var base_url = $.cookie("base_url");
	        $("#to_booking_date").datepicker({ dateFormat: 'dd-mm-yy'});
        $("#from_booking_date").datepicker({ dateFormat: 'dd-mm-yy'}).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("dd-mm-yy", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to_booking_date").datepicker( "option", "minDate", minValue );
        })
		 $("#download").click(function() {
				 $( "#csv" ).val(1);
				 $( "#my_from" ).submit();
				 $( "#csv" ).val('');
           });
		    $("#print").click(function() {
				$( "#my_from" ).attr("target","_blank");
				$( "#my_from" ).attr("action",base_url+"reservation_report/print_report");
				$( "#my_from" ).submit();
				$( "#my_from" ).removeAttr("target");
				$( "#my_from" ).attr("action"," ");
           });
		   
		   
		    $("#my_from").validate({
			     rules: {
					
					"filter[bo][equal][booking_reference]": {
											
						 number: true
					},
					
					
			     },
			     messages: {
						
					
						
			     },
				
		});
});


$("#clear_data").click(function() {
    $("#from_booking_date").val("");
    $("#to_booking_date").val("");
    $("#booking_reference").val("");
    $('#hotel_id option:selected').removeAttr('selected');
    $('#agent_id option:selected').removeAttr('selected');
    window.location.reload(true);
}); 

$(document).ready(function(){
    
}); 