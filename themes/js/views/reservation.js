	// defiine a global variable to use the number of total more room addedd...
	var no = 0 ; 
	var no_of_day;
	var tax_amount = 0;
	var discount_amount = 0;
	var total_amount = 0;
	var due_amount = 0;
 	var total_room_tariff = 0;

	$(document).ready(function(){
//		alert("page loaded..");
//		$("#message").hide();
		$(".travel").hide();
		$(".corporate").hide();
		$("#discount").hide();
		$(".bank_name_status").hide();
		$("#mobile").mask("9999999999"); // mobile no validation..
		$("#phone").mask("9999999999"); // mobile no validation..
		$("#travel_agent_phonenumber1").mask("9999999999"); // mobile no validation..
		$("#travel_agent_phonenumber2").mask("9999999999"); // mobile no validation..
		$("#card_no").mask("9999-9999-9999-9999"); // 
		$("#expiry_date").mask("99/99"); // 
	 	var base_url = $.cookie("base_url");
	 	var account_id = $.cookie("account_id"); // global decalration so dat we can use it any where..
//		by default we will hide payment type and date...if a guest is paying some amount in advance then only payment type and date will be visible...so on key up in advance filed we will mark it visible...
		$(".advance_payment").hide();		

// write a validation code which will restrict not to add a space bar at first character..

		$(document).on('keydown', '.required', function(e){ 
            if (e.which === 32 &&  e.target.selectionStart === 0){
            	swal({   
            		title: "", 
            		type: "error",
            		text: "First character cannot be blank!",   
            		timer: 1000,   
            		showConfirmButton: false 
            	});            	
           		return false;             
            } 
        });
    
// write a validation which will accept only numbers in fileds values. But 0 shouldn't be allowed at 1st place.
		
		$(document).on('keypress', '.only_digit', function(e){ 
		   	if (this.value.length == 0 && e.which == '48' ){
			  	return false;
		   	} else if (e.which < 48 || e.which > 57 ){
				return false;		   
		   	}
        });

// write a validation which will check if an advance filed have some values more than 0 then it will display payment type and payment date...otherwise it will keep hiding these fileds...
		
		$(document).on('keyup', '#advance', function(e){ 
		   	if (this.value.length > 0 && this.value > 0 ){
			  	$(".advance_payment").show();
		   	} else {
				$(".advance_payment").hide();		   
		   	}
        });

// write a validation which will accept only characters in fileds values. 

		$(".only_character").keypress(function (e){
	        var code =e.keyCode || e.which;
	        if((code<65 || code>90)&&(code<97 || code>122)&&code!=32&&code!=46){
	          	swal("", "Only alphabates are allowed.", "error");
	          	return false;
	        }
	    });		

		// on the basis of discount type selection we need to display discount amount ..
		// by default discount amount will be hidden..		
		$('input[type=radio][name=discount_type]').change(function () {			
			var discount_type = $(this).val();
			if(discount_type != "" ){
				$("#discount").show();
				if(discount_type == "percentage" ){
					$("#discount_amount").attr("maxlength",2);
					$("#discount_amount").val('');
				}
				if(discount_type == "cash" ){
					$("#discount_amount").attr("maxlength",5);
					$("#discount_amount").val('');
				}
				calculate_total_room_tarrif(1);// calculate total room price after change of price.. 
			}

        });
		
		$(".existing_customer").click(function(){
			//alert("clk on close btn");			
			$('#ext_customer').prop('checked', false);
			$("#guest_name").val('');
			$("#address").val('');
			$("#phone").val('');
			$("#email").val('');
		});

		// check in and check out date picker...
		
		$("#checkout_date").datetimepicker({ dateFormat: 'dd-mm-yy', minDate: 0, controlType: 'select',	oneLine: true,	timeFormat: 'hh:mm tt',altField: "#check_out_time"}).bind("change",function(){
            // var maxDate = $(this).val();
            // maxDate = $.datepicker.parseDate("dd-mm-yy", maxDate);
            // maxDate.setDate(maxDate.getDate());
            // $(".stay_date_from").datepicker( "option", "maxDate", maxDate );
            // $(".stay_date_to").datepicker( "option", "maxDate", maxDate );
            var maxDate = $(this).val();
            maxDate = $.datepicker.parseDate("dd-mm-yy", maxDate);
            maxDate.setDate(maxDate.getDate());
            $("#payment_date").datepicker( "option", "maxDate", maxDate );

            calculate_total_room_tarrif(no);// calculate total room price after change of price..
         });

        $("#checkin_date").datetimepicker({ dateFormat: 'dd-mm-yy', minDate: 0, controlType: 'select',	oneLine: true,	timeFormat: 'hh:mm tt',altField: "#check_in_time"}).bind("change",function(){
            var minDate = $(this).val();
            minDate = $.datepicker.parseDate("dd-mm-yy", minDate);
            minDate.setDate(minDate.getDate()+1);
            $("#checkout_date").datetimepicker( "option", "minDate", minDate );
            // var stayDate = $(this).val();
            // stayDate = $.datepicker.parseDate("dd-mm-yy", stayDate);
            // stayDate.setDate(stayDate.getDate());
            // $(".stay_date_from").datepicker( "option", "minDate", stayDate );
            
            calculate_total_room_tarrif(no);// calculate total room price after change of price..
        });				

		$("#payment_date").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 });

		// $(".stay_date_from").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0, controlType: 'select',oneLine: true}).bind("change",function(){
  //           var minDate = $(this).val();
  //           minDate = $.datepicker.parseDate("dd-mm-yy", minDate);
  //           minDate.setDate(minDate.getDate()+1);
  //           $(".stay_date_to").datepicker( "option", "minDate", minDate );
  //        });

  //       $(".stay_date_to").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0, controlType: 'select',	oneLine: true}).bind("change",function(){
  //       });

		$(document).on('focus', '.stay_date_from', function(){
           	var minDate = $("#checkin_date").val();
           	var maxDate = $("#checkout_date").val();
           	if(minDate != "" && maxDate != "") {
				$(this).datepicker({ dateFormat: 'dd-mm-yy', minDate: minDate, maxDate: maxDate, controlType: 'select',oneLine: true}).bind("change",function(){
		            calculate_total_room_tarrif(1);// calculate total room price after change of price..
	          	});     
           	} else {
           		swal("", "Please enter check in and check out date first.", "error");
           		return false;
           	}

        });

		$(document).on('focus', '.stay_date_to', function(){ 
			var id = $(this).attr("id");
			no = id.substring(13);
           	var minDate = $("#stay_date_from_"+no).val();
           	var maxDate = $("#checkout_date").val();
           	if(minDate != "" && maxDate != "") {
				$(this).datepicker({ dateFormat: 'dd-mm-yy', minDate: minDate, maxDate: maxDate, controlType: 'select',	oneLine: true}).bind("change",function(){
		            calculate_total_room_tarrif(1);// calculate total room price after change of price..			
				});        	
           	} else {
           		swal("", "Please enter check in and check out date first.", "error");           		
           		return false;
           	}
        });
		// dynamically create and delete for select room for check in...
		
        var no_of_checkin_room = 1;
		
		$(document).on('click', '.add_more_room', function(){ 
//        	alert("clk on a m r");
        	var no_of_room = $("#no_of_room").val();
        	
        	// before adding more room option we ned to make sure that stay date from and to is filled up.....

        	if($("#stay_date_from_"+no_of_room).val() != "" && $("#stay_date_to_"+no_of_room).val() != "" && $("#hotel_id").val() != "" ){
				no_of_room++;	
	        	
	        	$("#add_room").append('<div class="row select_room" id="'+no_of_room+'"  style="background: #f4f4f4;padding-top:20px;padding-bottom: 35px;border-radius:5px;margin-top:5px;"><input type="hidden" value="" id="room_no_'+no_of_room+'" name="room_no[]" ><div class="row"><div class="col-md-12"><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Room Type <span class="error">*</span> </label><div class="col-sm-6" ><select class="form-control room_id" id="room_id_'+no_of_room+'" data-toggle="modal" data-target="#" name="room_id[]" required ></select></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Number of Rooms <span class="error">*</span> </label><div class="col-sm-6"><input type="text" class="form-control" id="no_of_rooms_'+no_of_room+'" name="no_of_rooms[]" readonly required="" placeholder=""></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Room Rate <span class="error">*</span> </label><div class="col-sm-6"><input type="text" class="form-control" id="room_rate_'+no_of_room+'" name="room_rate[]" readonly required="" placeholder=""></div></div></div><div class="col-md-3"><span style="position:absolute;top:6px;">Occupancy <span class="error">*</span> </span><div class="radio radio-success radio-inline" style="margin-left:80px;"><input type="radio" value="single" name="occupancy['+no_of_room+'][]" checked="checked" onclick="occupancy('+no_of_room+');"><label >Single</label></div><div class="radio radio-success radio-inline"><input type="radio" value="double" name="occupancy['+no_of_room+'][]" onclick="occupancy('+no_of_room+');"><label >Double</label></div></div></div></div><div class="row"><div class="col-md-12"><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Net Rate <span class="error">*</span> </label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="net_rate_'+no_of_room+'" name="net_rate[]" placeholder="" maxlength="5"></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Extra Bed</label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_'+no_of_room+'" name="extra_bed[]" placeholder="" maxlength="5"></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Extra Bed Price</label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_bed_price_'+no_of_room+'" name="extra_bed_price[]" placeholder="" maxlength="5"></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Extra Person</label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_'+no_of_room+'" name="extra_person[]" placeholder="" maxlength="5"></div></div></div></div></div><div class="row"><div class="col-md-12"><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Extra Person Charge</label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="extra_person_charge_'+no_of_room+'" name="extra_person_charge[]" placeholder="" maxlength="5"></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Luxury Tax (%)</label><div class="col-sm-6"><input type="text" class="form-control only_digit calculate_room_tarrif" id="luxury_tax_'+no_of_room+'" name="luxury_tax[]" placeholder="" maxlength="2"></div></div></div><div class="col-md-6"><div align="center"><button type="button" class="btn btn-success m-b-5 add_more_room" style="">Add one more Room</button><button type="button" class="btn btn-danger m-b-5 remove_room" id="'+no_of_room+'" style="margin-left:10px;">Remove Room</button></div></div></div></div><div class="row"><div class="col-md-12"><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Stay Date From <span class="error">*</span> </label><div class="col-sm-6"><input type="text" name="stay_date_from[]" id="stay_date_from_'+no_of_room+'" value="" readonly="readonly" placeholder="Stay Date" class="form-control stay_date_from" ></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Stay Date To <span class="error">*</span> </label><div class="col-sm-6"><input type="text" name="stay_date_to[]" id="stay_date_to_'+no_of_room+'" value="" readonly="readonly" placeholder="Stay Date" class="form-control stay_date_to" ></div></div></div><div class="col-md-3"><div class="form-group"><label for="inputEmail3" class="col-sm-6 control-label">Selected Room Number</label><div class="col-sm-6" id="display_room_no_'+no_of_room+'"></div></div></div></div></div></div>');

	        	var opt = $('#room_id_1').html();
	 			$('#room_id_'+no_of_room).html(opt);
				$("#no_of_room").val(no_of_room);
        	} else {
           		swal("", "Please enter all mandatory fields values and fill it first.", "error");           		
           		return false;        		
        	}
 		});

		$(document).on('click', '.remove_room', function(){ 
        	var id = $(this).attr('id');
        	var no_of_room = $("#no_of_room").val();
        	// now we need to check a condition that a user can't delete any div element from middle...so it must be from last div only...
        	if(no_of_room == id){
	        	$("#"+id).remove();	
				--no_of_room;
				$("#no_of_room").val(no_of_room);
	//			alert("no_of_room = "+no_of_room);
				calculate_total_room_tarrif(1);// calculate total room price after change of price..        		
        	} else{
        		// it means its deleting from middle of the record....so we need to stop them to delete it from midle..
        		swal("", "Please remove from last records only. You can't remove from middle.", "error");		
           		return false; 
        	}
        });
		
		
		// write a code to get the guest details on the basis of mobile no or boooking id and fiil up that values in respected fileds..
		
		$("#fill_guest_details").click(function(){
			var mobile_no = $("#mobile").val();
			
			if(mobile_no == "" ){
           		swal("", "Please enter mobile no.", "error");
				return false;
			} else {
			// it means both or either one field is filed up..
			// so on the basis of filled filed's value we need to decide whether we are going to fetch the value by using mobile no or by booking id..
			// if both fileds are fillled up then it must be valid mobile no and booking id..
			// now we need to test whether both fileds are filled up or not 
				// it means mobile fileds have values...
				var dataString = 'mobile_no='+mobile_no;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/get_guest_detail_mobile/",
		 			success:function(return_data){
		 				if(return_data == "0"){
           					swal("", "Please check your mobile number.", "error");		 					
		 					$("#mobile").val('');
		 					$("#mobile").focus();
		 					return false;
		 				} else {
			 				var return_data = $.parseJSON(return_data);
							$('#guest_name').val(return_data[0]);
							$('#phone').val(return_data[1]);
							$('#address').val(return_data[2]);
							$('#email').val(return_data[3]);
							$('#guest_id').val(return_data[4]);
							$("#existing-customer-modal").modal('toggle');		 					
		 				}
		 			}
				});
			}
		});

		// By default meal plan of hotel will be hidden..
		// it will be enabled or shown after selecting a hotel from drop down list....
		// meal plan will be displayed on the basis of hotel and avialibility of meal plan for that hotel..
		$("#meal_plan_type").hide();

		$('#hotel_id').change(function() {
			//alert("change on select..")
			if($('#hotel_id').val() != ""){
				/// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
				var hotel_id = $('#hotel_id').val();
				
				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_meal_plan/"+hotel_id,
		 			success:function(return_data){		 				
		 				if(return_data != ""){
		 					// it means meal plan is added for the hotel....
							$("#meal_plan_type").show();
							$('#meal_plan').html('');
							$('#meal_plan').html(return_data);
		 				} else {
		 					// it means hotel meal plan is not added...
				            swal({
				                title: "Please add a meal plan for hotel",
				                text: "You will be redirected to different page to add a meal plan for this hotel.",
				                type: "warning",
				                showCancelButton: false,
				                confirmButtonColor: "#DD6B55",
				                confirmButtonText: "Yes, move to add meal plan page",
				                closeOnConfirm: false
				            }, function (isConfirm) {
				                if (!isConfirm) return;
				                $(location).attr("href", base_url+"hotel");
				            });
		 				}
		 			}
				});

				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_hotel_room_type/"+hotel_id,
		 			success:function(return_data){
//						alert(return_data);						
		 				if(return_data != ""){
		 					// it means meal plan is added for the hotel....
							$('#room_type').html('');
							$('#room_type').html(return_data);
		 				} else {
		 					// it means hotel meal plan is not added...
				            swal({
				                title: "Please add a room type for hotel",
				                text: "You will be redirected to different page to add a room for this hotel.",
				                type: "warning",
				                showCancelButton: true,
				                confirmButtonColor: "#DD6B55",
				                confirmButtonText: "Yes, move to add room for this hotel",
				                closeOnConfirm: false
				            }, function (isConfirm) {
				                if (!isConfirm) return;
				                $(location).attr("href", base_url+"room");
				            });
		 				}
		 			}
				});

				$.ajax({
		 			type:"POST",
		 			data:hotel_id,
		 			url:base_url+"reservation/get_hotel_info/"+hotel_id,
		 			success:function(return_data){
						var return_data = $.parseJSON(return_data);		 				
//						alert(return_data);						
						$('#service_tax').val(return_data[0]);
						$('#service_charge').val(return_data[1]);
		 			}
				});

			} else {
				$("#meal_plan_type").hide();
			}
		    
		});

	// write a code to register a travel agent by clicking the button..after registering an agent it must be selected in the drop down list..
		
		$("#add_travel_agent").click(function(){
//			alert("clkc on submit btn...");
			var travel_agent_name = $("#travel_agent_name").val().trim();
			var travel_agent_address = $("#travel_agent_address").val().trim();
			var travel_agent_phonenumber1 = $("#travel_agent_phonenumber1").val().trim();
			var travel_agent_phonenumber2 = $("#travel_agent_phonenumber2").val().trim();
			var travel_agent_contact_person = $("#travel_agent_contact_person").val().trim();
			var travel_agent_email_address = $("#travel_agent_email_address").val().trim();
			var travel_agent_website = $("#travel_agent_website").val().trim();
			
			
			if(travel_agent_name == "" || travel_agent_address == "" || travel_agent_phonenumber1 == "" || travel_agent_contact_person == ""){
				swal("", "Please fill out all required fields.", "error");		 					
				return false;				
			} else {
			// it means all required fields are filled up ..
			// now we need to test for valid email and website address...			
				if(travel_agent_email_address != "" ){
					var email_filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
					if (!email_filter.test(travel_agent_email_address)) {
						swal("", "Please enter a valid email address.", "error");	
						$("#travel_agent_email_address").val('');
						$("#travel_agent_email_address").focus();
						return false;
					}
				}					
				if(travel_agent_website != ""){
					var url_filter = /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
					if (!url_filter.test(travel_agent_website)) {
						swal("", "Please enter a valid website url. Your website url must start with http://www. or https://www.", "error");							
						$("#travel_agent_website").val('');
						$("#travel_agent_website").focus();
						return false;
					}
				}
				// now we need to test the unique travel agent name	from db table..
				// if the same travel agent name is already registered in table thn we need to display an error message..

				$.ajax({
		 			type:"POST",
		 			data:travel_agent_name,
		 			url:base_url+"reservation/is_agent_name/"+travel_agent_name+"/"+account_id+"/1",
		 			success:function(return_data){
//						alert(return_data);
						if(return_data == 1){
							// it means the current travel agent name is already in db table..so we will avoid the current agent name..
							swal("", "This travel agent name is already in our record. Please use another travel agent name.", "error");							
							$("#travel_agent_name").val('');
							$("#travel_agent_name").focus();
							return false;
						} 				
		 			}
				});

				// it means all validations are done now we need to insert the records in DB table ...
				var dataString = 'account_id='+account_id+'&travel_agent_name='+travel_agent_name+'&travel_agent_address='+travel_agent_address+'&travel_agent_phonenumber1='+travel_agent_phonenumber1+'&travel_agent_phonenumber2='+travel_agent_phonenumber2+'&travel_agent_email_address='+travel_agent_email_address+'&travel_agent_website='+travel_agent_website+'&travel_agent_contact_person='+travel_agent_contact_person;
				$.ajax({
		 			type:"POST",
		 			data:dataString,
		 			url:base_url+"reservation/add_travel_agent/",
		 			success:function(return_data){
//						alert(return_data);
						// now we have inserted the record in table....
						// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
						var travel_agent_id = return_data;
						var dataString = 'travel_agent_id='+travel_agent_id;
						$.ajax({
				 			type:"POST",
				 			data:dataString,
				 			url:base_url+"reservation/get_all_travel_agent_list/",
				 			success:function(return_data){
//								alert(return_data);
								$("#travel_agent_list").html('');
								$("#travel_agent_list").html(return_data);
												
				 			}
						});
		 			}
				});
				// now we need to close the travel agent registration modal.
				$("#travel-agent-modal").modal('toggle');				
				
			}
		});

	 	// call the modal to display the no of available room for selected hotel, plan etc..		
		
		$(document).on('change', '.room_id', function(){
			// get id attribute values on the basis of class name click..
			var select_id = $(this).attr('id');
			var room_no = [];
			// we need to get the no of row for check in room...it will be id values like 1,2 etc....
			// it will be in form of room_id_1.....
			no = select_id.substring(8);
			if($('#'+select_id).val() != ""){
				var room_id = $('#'+select_id).val();
				var travel_agent_id = "";
				var corporate_agent_id = "";
				// now we need to write a condition where we will chck whether we have entered checked in date or not..
				// if checked in date and meal plan is not entered then we will alert an error message..
				var checkin_date = $('#checkin_date').val();
				var checkout_date = $('#checkout_date').val();
				var plan_type_id = $('#plan_type_id').val();
				var occupancy = $('input[name=occupancy\\['+no+'\\]\\[\\]]:checked').val();
				// we need to check a condition where we will check whether any agent is selected or not...if agent is selected....then whcich either travel or corporate...
				if($("input:radio[name='agent']").is(":checked")) {
					//it means agent radio button is checked....
					var agent_type = $("input[name='agent']:checked").val();
					if(agent_type == "travel")
						var travel_agent_id = $('#travel_agent_id').val();
					else if(agent_type == "corporate")
						var corporate_agent_id = $('#corporate_agent_id').val();
				}
				
//				var room_no = $('input[name=room_no\\[\\]]').val();
//				var selected_room_id = $('input[name=room_id\\[\\]]').val();
				$('input[name^="room_no"]').each(function() {
//				    alert($(this).val());
					if($(this).val() != "")
				    	room_no.push($(this).val());
				});				
				
				if(checkin_date != "" && plan_type_id != "" && checkout_date != "" ){
					var dataString = 'room_id='+room_id+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date+'&account_id='+account_id+'&room_no='+room_no;

					$.ajax({
			 			type:"POST",
			 			data:dataString,
			 			url:base_url+"reservation/get_hotel_room/",
			 			success:function(return_data){
	//						alert(return_data);
							//  now we need to check whether we have any vacant room or not....
							if(return_data == ""){// it means there is no any room vacant....so we will just display a message..
								$('#room-number-selection-modal').modal('hide');
								// remove all previous values if anything is there...
								$("#room_rate_"+no).val('');
								$("#net_rate_"+no).val('');
								$("#extra_bed_price_"+no).val('');
								$("#extra_person_charge_"+no).val('');
								$("#luxury_tax_"+no).val('');
								$("#luxury_tax_amount").val('');

								swal("", "Please select any other room type and date. All rooms are booked under this type and date.", "error");
								$("#room_id_"+no+" > option").removeAttr("selected");
								return false;
							}else {								
								// now we have inserted the record in table....
								// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
								$("#room_number").html('');
								$("#room_number").html(return_data);												

								// get rooom price on the basis of meal plan and room type and occupancy...					

								var dataString = 'room_id='+room_id+'&plan_type_id='+plan_type_id+'&occupancy='+occupancy+'&travel_agent_id='+travel_agent_id+'&corporate_agent_id='+corporate_agent_id+'&no='+no+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date;
//								alert(dataString);
								$.ajax({
						 			type:"POST",
						 			data:dataString,
						 			url:base_url+"reservation/get_room_plan_price/",
						 			success:function(return_data){
						 				var return_data = $.parseJSON(return_data);
//										alert(return_data);
										// now we have inserted the record in table....
										// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
										$("#room_rate_"+no).val('');
										$("#room_rate_"+no).val(return_data[0]);												
										$("#net_rate_"+no).val('');
										$("#net_rate_"+no).val(return_data[0]);												
										$("#extra_bed_price_"+no).val('');
										$("#extra_bed_price_"+no).val(return_data[1]);												
										$("#extra_person_charge_"+no).val('');
										$("#extra_person_charge_"+no).val(return_data[2]);	
										$("#luxury_tax_"+no).val('');
										$("#luxury_tax_"+no).val(return_data[3]);												
										$("#luxury_tax_amount").val('');
										$("#luxury_tax_amount").val(return_data[3]);	

						 			}
								});

							 	$('#room-number-selection-modal').modal();

							 	calculate_total_room_tarrif(no);

							}
			 			}
					});


				} else {
					$("#room_id_"+no+" > option").removeAttr("selected");
					swal("","Please enter your check in & out date and select meal plan type.","error")					
					return false;
				}
			}
		    
		});

		// on the basis of agent selection we need to display agent ..
		// by default travel agent will be selected..
		// corporate agent will be displayed if its selected..
		$('input[type=radio][name=agent]').change(function () {			
			var agent = $(this).val();
			if(agent != "" && agent == "travel" ){
				$(".travel").show();
				$(".corporate").hide();
			} else if(agent != "" && agent == "corporate" ){
				$(".travel").hide();
				$(".corporate").show();
			}

        });

		// on the basis of agent selection we need to display agent ..
		// by default travel agent will be selected..
		// corporate agent will be displayed if its selected..
		$('input[type=radio][name=plan_type_id]').change(function () {			
			var plan_type_id = $(this).val();
			if(plan_type_id != "" ){
				calculate_total_room_tarrif(no);
			}
        });

		// we need to write a jquery code to add and remove a css class by using toggle class..
		// this will be used to select and deselect a room number..
		$(document).on('click', '.room_number_bg', function(){
			// get unique room id or get the id for clicked fileds..
			var room_number = $(this).attr('id');
//			$("#"+room_number).removeClass("btn-default");
  			$("#"+room_number).toggleClass("btn-inverse");
		});

		// write a function which will calculate the total number of room selected on the basis on inverse class..

		

		$("#select_room").click(function(){
			var total_selected_room = 0;
			var room_id = [];
			$('.btn-inverse').each(function() { 
			  	var id = $(this).attr('id');
			  	if(id != undefined) {
			    	room_id.push(id);
			    	total_selected_room++;
			  	}
			});
//			alert(no);
			// now we need to insert total selected room values to the respective text box..
			$("#no_of_rooms_"+no).val(total_selected_room);
			$("#room-number-selection-modal").modal('toggle');
			$("#room_no_"+no).val(room_id);
			$("#display_room_no_"+no).text(room_id);

		 	// now we need to calculate the total room tarrif cost...
		 	// it will be based on net rate * total no of selected rooms * no of day/night stayed in hotel +
		 	// extra bed * extra bed price * no of day/night stayed in hotel +
		 	// extra person * extra person charge * no of day/night stayed in hotel	

		 	calculate_total_room_tarrif(no);

		});

		// for payment type we need by default hide all bank related html..
		// we will shoow and hide accordingly on the basis of payment type selected from drop down..

		$("#cheque_details_central_reservation").hide();
		$("#dd_details_central_reservation").hide();
		$("#chq_details_central_reservation").hide();	
		$("#cc_details_central_reservation").hide();
		$("#neft_details_central_reservation").hide();

		$('#payment_type').change(function() {
			//alert("change on select..")
			if($('#payment_type').val() != ""){
				/// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
				var payment_type = $('#payment_type').val();

				if(payment_type == "cash" || payment_type == "payment_by_cash" ){
					$("#cheque_details_central_reservation").hide();					
					$("#dd_details_central_reservation").hide();
					$("#chq_details_central_reservation").hide();
					$("#cc_details_central_reservation").hide();
					$("#neft_details_central_reservation").hide();	
					$(".bank_name_status").hide();						
				}
				
				if(payment_type == "cheque" || payment_type == "demand_draft" || payment_type == "credit_card" || payment_type == "debit_card" || payment_type == "neft" || payment_type == "rtgs"){
					//$("#bank_details_central_reservation").show();
					if(payment_type == "cheque" ){
						$("#cheque_details_central_reservation").show();
						$("#chq_details_central_reservation").show();
						$(".bank_name_status").show();
						$("#dd_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").hide();		
					}
					if(payment_type == "demand_draft" ){
						$("#cheque_details_central_reservation").show();
						$("#dd_details_central_reservation").show();
						$(".bank_name_status").show();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").hide();		
					}
					if(payment_type == "credit_card" || payment_type == "debit_card" ){
						$("#cheque_details_central_reservation").hide();
						$("#dd_details_central_reservation").hide();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").show();
						$("#neft_details_central_reservation").hide();		
					}
					if(payment_type == "neft" || payment_type == "rtgs" ){
						$("#cheque_details_central_reservation").hide();
						$("#dd_details_central_reservation").hide();
						$("#chq_details_central_reservation").hide();
						$("#cc_details_central_reservation").hide();
						$("#neft_details_central_reservation").show();
						$(".bank_name_status").show();		
					}
				}										
			} else {
			
				$("#cheque_details_central_reservation").hide();					
				$("#dd_details_central_reservation").hide();
				$("#chq_details_central_reservation").hide();
				$("#cc_details_central_reservation").hide();
				$("#neft_details_central_reservation").hide();
				$(".bank_name_status").hide();		
			}
		    
		});			
		
	});

	// we need to write a jquery code to add and remove a css class by using toggle class..
	// this will be used to select and deselect a room number..
	$(document).on('keyup', '.calculate_room_tarrif', function(){

		calculate_total_room_tarrif(no);// calculate total room price after change of price..

	});

	// write a code to calculate the total amount oon the basis of discount, service tax, charge , misc charges, advance etc...
	$(document).on('keyup', '.calculate_total_amount', function(){
		
		var discount_amount = 0;
		var tax_amount = 0;
		var total_amount = 0;
		var grand_total = 0;
		var due = 0;

		// readonly fileds values...
		var total_room_tariff = $("#total_room_tariff").val();
		var luxury_tax_amount = $("#luxury_tax_amount").val();
		var tax_amount = $("#tax_amount").val();
		var total_amount = $("#total_amount").val();
		var grand_total = $("#grand_total").val();
		var due = $("#due").val();
		// editable fields value....
		var discount_type = $('input[type=radio][name=discount_type]:checked').val();
		var discount_amount = $("#discount_amount").val();
		var service_tax = $("#service_tax").val();
		var service_charge = $("#service_charge").val();
		var misc_charge = $("#misc_charge").val();
		var advance = $("#advance").val();


		// before proceeding to the calculation of amount we need to make sure that total room tariff, total amount, grant total fields values are not empty...

		if($.isNumeric(total_room_tariff) && total_room_tariff != 0 && total_room_tariff != "" && $.isNumeric(total_amount) && total_amount != 0 && total_amount != "" && $.isNumeric(grand_total) && grand_total != 0 && grand_total != "" ) {

		 	if(discount_amount != "" && discount_amount != 0 && discount_type != ""){
		 		if(discount_type == "percentage")
			 		discount_amount = parseInt(((discount_amount * total_room_tariff) / 100));
		 		else if(discount_type == "cash"){
		 			// discount amount must not be more than total room tarrif....
		 			if(parseInt(discount_amount) <= parseInt(total_room_tariff))
		 				discount_amount = parseInt(discount_amount);
		 			else{
		 				swal("","Please check discount amount value. It must not be more than total room tarrif","error");		 		
				 		$("#discount_amount").val('');
				 		$("#discount_amount").focus();
		 			}
		 		}
		 	} 

			tax_amount = 0;
		 	if(luxury_tax_amount != "" && luxury_tax_amount != 0 ){
		 		tax_amount = parseInt(luxury_tax_amount);
		 	} 
		 	if(service_tax != "" && service_tax != 0 ){
		 		tax_amount = parseInt(tax_amount) + parseInt(((service_tax * total_room_tariff) / 100));
		 	} 
		 	if(service_charge != "" && service_charge != 0 ){
		 		tax_amount = parseInt(tax_amount) + parseInt(((service_charge * total_room_tariff) / 100));
		 	} 
		 	if(discount_amount != "" && discount_amount != 0 )
		 		total_amount = parseInt(tax_amount) + parseInt(total_room_tariff) - parseInt(discount_amount) ;
		 	else
		 		total_amount = parseInt(tax_amount) + parseInt(total_room_tariff);							 		
		 	grand_total = total_amount;
		 	
		 	if(misc_charge != "" && misc_charge != 0 ){
		 		grand_total = parseInt(total_amount) + parseInt(misc_charge);
//		 		alert(grand_total);				
		 	} 
		 	due = grand_total;
		 	
		 	if(advance != "" && advance != 0 && advance < grand_total){
		 		due = grand_total - advance;
//		 		alert(advance);
		 	} else if(advance > grand_total){
//		 		alert(advance);
//		 		due = grand_total;
				swal("","Please check advance amount value.","error")		 		
		 		$("#advance").val('');
		 		$("#advance").focus();
//		 		return false;
		 	}							 	
//alert(discount_amount);
			$("#tax_amount").val(tax_amount);
			$("#total_amount").val(total_amount);
			$("#grand_total").val(grand_total);
			$("#due").val(due);	 		

		}
	});



	// we will define a global function which will be used to calculate the date differences...
	// this calculation will be done on the basis of no of day, net rate, extra bed price etc....

	function calculate_total_room_tarrif(no){

	 	// now we need to calculate the total room tarrif cost...
	 	// it will be based on net rate * total no of selected rooms * no of day/night stayed in hotel +
	 	// extra bed * extra bed price * no of day/night stayed in hotel +
	 	// extra person * extra person charge * no of day/night stayed in hotel	
//	 	alert(" no = "+no);
		var checkin_date = $('#checkin_date').val();
		var checkout_date = $('#checkout_date').val();
		var stay_date_from = $('#stay_date_from_'+no).val();
		var stay_date_to = $('#stay_date_to_'+no).val();
	 	var net_rate = $("#net_rate_"+no).val();
	 	var extra_bed = $("#extra_bed_"+no).val();
	 	var extra_bed_price = $("#extra_bed_price_"+no).val();
	 	var extra_person = $("#extra_person_"+no).val();
	 	var extra_person_charge = $("#extra_person_charge_"+no).val();
	 	var luxury_tax = $("#luxury_tax_"+no).val();
	 	var no_of_rooms = $("#no_of_rooms_"+no).val();
	 	var luxury_tax_amount = $("#luxury_tax_amount").val();
	 	var service_tax = $("#service_tax").val();
	 	var service_charge = $("#service_charge").val();
	 	var discount_amount = $("#discount_amount").val();
	 	var discount_type = $('input[type=radio][name=discount_type]:checked').val();
	 	var misc_charge = $("#misc_charge").val();
	 	var advance = $("#advance").val();
	 	var no_of_room = $("#no_of_room").val();
	 	var total_room_tariff = 0;
	 	var luxury_tax_amount = 0;

	 	var base_url = $.cookie("base_url");

		if(checkin_date != "" && checkout_date != "" && stay_date_from != "" && stay_date_to != "" && typeof(net_rate) != "undefined" && net_rate != "" && no_of_room != "" && typeof(no_of_room) != "undefined"){			
			for (i = 1; i <= no_of_room; i++) { 
				// we need to write a jquery code to get the date differences...
				stay_date_from = $('#stay_date_from_'+i).datepicker("getDate");
				stay_date_to = $('#stay_date_to_'+i).datepicker("getDate");
				no_of_day = (stay_date_to - stay_date_from) / (1000 * 60 * 60 * 24);
			 	net_rate = $("#net_rate_"+i).val();
			 	no_of_rooms = $("#no_of_rooms_"+i).val();
			 	extra_bed = $("#extra_bed_"+i).val();
			 	extra_bed_price = $("#extra_bed_price_"+i).val();
			 	extra_person = $("#extra_person_"+i).val();
			 	extra_person_charge = $("#extra_person_charge_"+i).val();
			 	luxury_tax = $("#luxury_tax_"+i).val();
//alert("nor = "+i+" net_rate = "+net_rate+" no_of_room = "+no_of_rooms);
				if(typeof(net_rate) != "undefined" && net_rate != "" && no_of_rooms != "" && typeof(no_of_rooms) != "undefined") {
				 	total_room_tariff = total_room_tariff + (net_rate * no_of_rooms * no_of_day);
				 	
				 	if(extra_bed != "" && extra_bed != 0 && extra_bed_price != "" && extra_bed_price != 0 ){
				 		total_room_tariff = total_room_tariff + (extra_bed * extra_bed_price * no_of_day);
				 	} 
				 	if(extra_person != "" && extra_person != 0 && extra_person_charge != "" && extra_person_charge != 0 ){
				 		total_room_tariff = total_room_tariff + (extra_person * extra_person_charge * no_of_day);
				 	} 
				 	if(luxury_tax != "" && luxury_tax != 0 ){
				 		luxury_tax_amount = luxury_tax_amount + (((luxury_tax * net_rate) / 100) * no_of_rooms * no_of_day);
				 	} 
					tax_amount = 0;
				 	if($.isNumeric(total_room_tariff)){
				 		$("#total_room_tariff").val(total_room_tariff);

					 	if(luxury_tax_amount != "" && luxury_tax_amount != 0 ){
					 		tax_amount = parseInt(luxury_tax_amount);
					 	} 
					 	if(service_tax != "" && service_tax != 0 ){
					 		tax_amount = parseInt(tax_amount) + parseInt(((service_tax * total_room_tariff) / 100));
					 	} 
					 	if(service_charge != "" && service_charge != 0 ){
					 		tax_amount = parseInt(tax_amount) + parseInt(((service_charge * total_room_tariff) / 100));
					 	} 
					 	if(discount_amount != "" && discount_amount != 0 && discount_type != ""){
					 		if(discount_type == "percentage")
					 			discount_amount = parseInt(((discount_amount * total_room_tariff) / 100));
					 		else if(discount_type == "cash"){
					 			// discount amount must not be more than total room tarrif....
					 			if(parseInt(discount) <= parseInt(total_room_tariff))
					 				discount_amount = parseInt(discount_amount);
					 			else{
					 				swal("","Please check discount amount value. It must not be more than total room tarrif","error");		 		
							 		$("#discount_amount").val('');
							 		$("#discount_amount").focus();
					 			}
					 		}
					 	} 
					 	$("#luxury_tax_amount").val(luxury_tax_amount);
//							 	alert(" LT = "+luxury_tax+" TA = "+tax_amount);
					 	$("#tax_amount").val(tax_amount);
					 	if(discount_amount != "" && discount_amount != 0 )
					 		total_amount = parseInt(tax_amount) + parseInt(total_room_tariff) - parseInt(discount_amount) ;
					 	else
					 		total_amount = parseInt(tax_amount) + parseInt(total_room_tariff);							 		
						$("#total_amount").val(total_amount);
					 	if(misc_charge != "" && misc_charge != 0 )
					 		grand_total = parseInt(total_amount) + parseInt(misc_charge);
					 	else
					 		grand_total = total_amount;

						$("#grand_total").val(grand_total);
					 	if(advance != "" && advance != 0 && advance < grand_total){
					 		due_amount = parseInt(total_amount) - parseInt(advance);
					 		$("#due").val(due_amount);
					 	} else if(advance > grand_total){
					 		$("#due").val(grand_total);
					 		swal("","Please check advance amount value.","error")
					 		$("#advance").val('');
					 		$("#advance").focus();
//							 		return false;
					 	}else {
					 		$("#due").val(total_amount);				 		
					 	}	
					}
				}
			}			
		}
		// else {
		// 	swal("", "Please check check in & out date, stay date, no of room, room price .", "error");
		// 	return false;			
		// }	

	}

//		write a function which will work on chnage event of occupancy radio button...
// 		this code will work on change event on radio button...
//		if htel name, meal plan and room type is not selected or not entered.. it wont work....

	function occupancy(no) {

		var room_id = $('#room_id_'+no).val();
		var base_url = $.cookie("base_url");
		var travel_agent_id = "";
		var corporate_agent_id = "";
		var occupancy = $('input[name=occupancy\\['+no+'\\]\\[\\]]:checked').val();
		var plan_type_id = $('#plan_type_id').val();
		var checkin_date = $('#checkin_date').val();
		var checkout_date = $('#checkout_date').val();

		// we need to check a condition where we will check whether any agent is selected or not...if agent is selected....then whcich either travel or corporate...
		if($("input:radio[name='agent']").is(":checked")) {
			//it means agent radio button is checked....
			var agent_type = $("input[name='agent']:checked").val();
			if(agent_type == "travel")
				travel_agent_id = $('#travel_agent_id').val();
			else if(agent_type == "corporate")
				corporate_agent_id = $('#corporate_agent_id').val();
		}
		
//		alert(" room_id ="+room_id);
		if(room_id != "" && plan_type_id != "" ){

			var dataString = 'room_id='+room_id+'&plan_type_id='+plan_type_id+'&occupancy='+occupancy+'&travel_agent_id='+travel_agent_id+'&corporate_agent_id='+corporate_agent_id+'&checkin_date='+checkin_date+'&checkout_date='+checkout_date;
			$.ajax({
	 			type:"POST",
	 			data:dataString,
	 			url:base_url+"reservation/get_room_plan_price/",
	 			success:function(return_data){
	 				var return_data = $.parseJSON(return_data);
//						alert(return_data);
					// now we have inserted the record in table....
					// now we need to make sure that newly inserted records must be automatically selected in the drop down list..
					$("#room_rate_"+no).val('');
					$("#room_rate_"+no).val(return_data[0]);
					$("#net_rate_"+no).val('');
					$("#net_rate_"+no).val(return_data[0]);																	
					calculate_total_room_tarrif(no); // calculate total room price after change of price..
	 			}
			});
			
		}else {
			swal("", "Please check hotel name, meal plan and room type selected values and try again.", "error");
			return false;
		}			

	}