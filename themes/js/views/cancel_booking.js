$(document).ready(function(){
        $("#card_no").mask("9999-9999-9999-9999"); // 
        $("#expiry_date").mask("99/99"); //


        var base_url = $.cookie("base_url");

        $("#cheque_details").hide();
        $("#dd_details").hide();
        $("#chq_details").hide();   
        $("#cc_details").hide();
        $("#neft_details").hide();
        $("#bank_details").hide();

        $('#payment_type').change(function() {
            //alert("change on select..")
            if($('#payment_type').val() != ""){
                /// on the basis of hotel id we need to get its meal plan type and room type for that hotel...
                var payment_type = $('#payment_type').val();

                if(payment_type == "cash" || payment_type == "payment_by_cash" )
                    $("#cheque_details").hide();                    
                    $("#dd_details").hide();
                    $("#chq_details").hide();
                    $("#cc_details").hide();
                    $("#neft_details").hide();  
                    $("#bank_details").hide();    
                
                if(payment_type == "cheque" || payment_type == "demand_draft" || payment_type == "credit_card" || payment_type == "debit_card" || payment_type == "neft" || payment_type == "rtgs"){
                    //$("#bank_details_central_reservation").show();
                    if(payment_type == "cheque" ){
                        $("#cheque_details").show();
                        $("#chq_details").show();
                        $("#bank_details").show();
                        $("#dd_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").hide();      
                    }
                    if(payment_type == "demand_draft" ){
                        $("#cheque_details").show();
                        $("#dd_details").show();
                        $("#bank_details").show();
                        $("#chq_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").hide();
                             
                    }
                    if(payment_type == "credit_card" || payment_type == "debit_card" ){
                        $("#cheque_details").hide();
                        $("#dd_details").hide();
                        $("#chq_details").hide();
                        $("#cc_details").show();
                        $("#neft_details").hide(); 
                        $("#bank_details").hide();      
                    }
                    if(payment_type == "neft" || payment_type == "rtgs" ){
                        $("#cheque_details").hide();
                        $("#dd_details").hide();
                        $("#chq_details").hide();
                        $("#cc_details").hide();
                        $("#neft_details").show(); 
                        $("#bank_details").show();     
                    }
                }
                                        

            } else {
            
                $("#cheque_details").hide();                    
                $("#dd_details").hide();
                $("#chq_details").hide();
                $("#cc_details").hide();
                $("#neft_details").hide(); 
                $("#bank_details").hide();     
            }
            
        });
});


$('#cancelBooking').click(function() {
        var bookingID = $('#bookingID').val();
        
        var refundAmt = $('#refund_amount').val();
        var cancellationReason = $('#cancellation_reason').val();
        var collectionPt = $('#collectionPt').val();
        var paymentType = $('#payment_type').val();
        var bankName = $('#bank_name').val();
        var cardNo = $('#card_no').val();
        var chequeNo = $('#cheque_no').val();
        var ddNo = $('#dd_no').val();
        var bankAccNo = $('#bank_account_no').val();
        var ifsc = $('#ifsc_code').val();
        var expiryDate = $('#expiry_date').val();

        var base_url = $.cookie("base_url");
        
        var dataString = 'bookingID='+ bookingID
                        + '&refundAmt=' + refundAmt        
                        + '&cancellationReason=' + cancellationReason
                        + '&collectionPt=' + collectionPt
                        + '&paymentType=' + paymentType
                        + '&bankName=' + bankName
                        + '&cardNo=' + cardNo
                        + '&chequeNo=' + chequeNo
                        + '&ddNo=' + ddNo
                        + '&bankAccNo=' + bankAccNo
                        + '&ifsc=' + ifsc
                        + '&expiryDate=' + expiryDate

              
        $.ajax({
            type: 'POST',
            url:base_url+"sales_report/cancelBooking/",

            data: dataString,
            success:function(data)
            { 
                window.location = base_url+"sales_report";
                swal("Success", "Booking has been cancelled!");      
            }      
        });                  

}); 



$(document).on('keypress', '.only_digit', function(e){ 
            if (this.value.length == 0 && e.which == '48' ){
              return false;
            } else if (e.which < 48 || e.which > 57 ){
                return false;          
            }
        });