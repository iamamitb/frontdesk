$(document).ready(function(){ 
$('.category_button').click(function()
{
    var select_button_id=$(".btn.btn-inverse.btn-rounded.category_button").attr('id');
    $( "#"+select_button_id ).removeClass('btn btn-inverse btn-rounded category_button').addClass( "btn btn-danger btn-rounded category_button" ); 
    var id=$(this).attr('id');
    $( "#"+id ).removeClass('btn btn-danger btn-rounded category_button').addClass( "btn btn-inverse btn-rounded category_button" ); 
    var category_id=id.split('_');
    var allmenu_by_category_array= allmenu_by_category;
    
    var item_value_array_filter=getObjects(allmenu_by_category_array, 'category_id',category_id[1] ); 
    var allmenu_by_category_array=JSON.stringify(item_value_array_filter);
    $("#menu_item").html('');  
    $.each(item_value_array_filter, function (index, valueobj) {
        $("#menu_item").append('<button type="button" class="square" id="menu_'+valueobj['menu_item_id']+'">'+valueobj['menu_name']+'</button>');      
     }); 
});
$('.room_service').click(function()
{
    var select_button_id=$(".btn.btn-inverse.room_service").attr('id');
    $( "#"+select_button_id ).removeClass('btn btn-inverse  room_service').addClass( "btn btn-danger room_service" ); 
    var id=$(this).attr('id');
    $( "#"+id ).removeClass('btn btn-danger room_service').addClass( "btn btn-inverse  room_service" );
    if(id=="take_away")
    {
    $('#room_number').hide();
    $('#guest_name').show();
    $('#mobile').show();
    $('#steward').hide();
    $('#booking_id').hide();
    $('#table_no').hide();    
    }
    if(id=="room_service")
    {
    $('#room_number').show();
    $('#guest_name').show();
    $('#mobile').show();
    $('#steward').show();
    $('#booking_id').show();    
    $('#table_no').hide(); 
    }
    if(id=="dine_in")
    {
    $('#room_number').hide();
    $('#guest_name').show();
    $('#mobile').show();
    $('#steward').show();
    $('#booking_id').hide();
    $('#table_no').show();     
    }
});     
$(document).on('click', '.square', function(){
    var menu=$(this).attr('id');
    var idsplit=menu.split('_');
    //var category_id=idsplit[1];
    var menu_id=idsplit[1];
    get_menu_div(menu_id);
});


$(document).on('click', '#cross_button', function(){
      $(this).closest( 'tr').remove();
      var item_count=1;
      $('#item_add tbody tr').each(function() { 
      $(this).find("td:eq(0)").text(item_count);
      $(this).find("td:eq(4)").attr('id','sub_total_'+item_count);
      $(this).find("td:eq(1)").find('input').attr('id','no_of_quantity_'+item_count);
      $(this).find("td:eq(3)").find('input').attr('id','rate_per_quantity_'+item_count);
      item_count++;
  });
      total();
});

$(document).on('keyup', '.item_class', function(){
    var value=$(this).val();
    var intRegex = /^\d+$/;
    if(!intRegex.test(value)) {
    $(this).val(0)
    }
    total();
});
$("#auto_complete_menu").select2();
$(document).on('change', '#auto_complete_menu', function(){
    var menu=$(this).val();
    if(menu !="")
    {
    var idsplit=menu.split('_');
    var menu_id=idsplit[1]
    get_menu_div(menu_id);
    }
   
});
$( "#submit_order" ).click(function v() {
    
    var rowCount = $('#item_add tbody tr').length; 
    var room_number=$("#room_number_id").val();
    var guest_name=$("#guest_name_id").val();
    var mobile=$("#mobile_id").val();
    var steward=$("#steward_id").val();
    var steward_text=$("#steward_id option:selected").text();
    var booking_id=$("#bookingid").val();
    var table_no=$("#table_no_id").val();
    var table_no_text=$("#table_no_id option:selected").text();
    var room_service=$(".btn.btn-inverse.room_service").attr('id');

    if(room_service=="take_away")
    {
        if(guest_name=="" )
        {
        $("#guest_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#guest_error").html('');    
        flag=true
        }
 
        if( mobile=='')
        {
        $("#mobile_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#mobile_error").html('');
        flag=true
        }
        $("#kot_room_number").html('');
        $("#kot_room_number").html('N/A');
        $("#kot_stward").html('');
        $("#kot_stward").html('N/A');
        $("#kot_table_no").html('');
        $("#kot_table_no").html('N/A');


    } 

    if(room_service=="room_service")
    {
        if(room_number=="" )
        {
        $("#room_number_error").html('Please select valid data');
        flag=false;
        }
        else
        {
        $("#room_number_error").html('');    
        flag=true
        }
 
        if(guest_name=="" )
        {
        $("#guest_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#guest_error").html('');    
        flag=true
        }
    
        if( mobile=='')
        {
        $("#mobile_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#mobile_error").html('');
        flag=true
        }
    
        if(   steward=='')
        {
        $("#steward_error").html('Please select valid data');
        flag=false;
        }
        else
        {
        $("#steward_error").html('');
        flag=true
        }
        $("#kot_room_number").html('');
        $("#kot_room_number").html(room_number);
        $("#kot_stward").html('');
        $("#kot_stward").html(steward_text);
        $("#kot_table_no").html('');
        $("#kot_table_no").html('N/A');

    } 
    if(room_service=="dine_in")
    {
        if(guest_name=="" )
        {
        $("#guest_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#guest_error").html('');    
        flag=true
        }
    
        if( mobile=='')
        {
        $("#mobile_error").html('Please enter valid data');
        flag=false;
        }
        else
        {
        $("#mobile_error").html('');
        flag=true
        }
    
        if( steward=='')
        {
        $("#steward_error").html('Please select valid data');
        flag=false;
        }
        else
        {
        $("#steward_error").html('');
        flag=true
        }
       if(  table_no=='')
        {
        $("#table_no_error").html('Please select valid data');
        flag=false;
        }
        else
        {
        $("#table_no_error").html('');
        flag=true
        }

        $("#kot_room_number").html('');
        $("#kot_room_number").html('N/A');
        $("#kot_stward").html('');
        $("#kot_stward").html(steward_text);
        $("#kot_table_no").html('');
        $("#kot_table_no").html(table_no_text);
        // $("#kot_room_number").html('');
        // $("#kot_room_number").append('<th>Room Number</th><td>N/A</td>');
        // $("#kot_stward").html('');
        // $("#kot_stward").append('<th>Steward Name</th><td>'+steward+'</td>');
        // $("#kot_table").html('');
        // $("#kot_table").append('<th>Table No</th><td>'+table_no+'</td>');
    } 


 
    if(rowCount != 0  && flag==true)
    {
    $("#item_add_body").html('');
 
      $('#item_add tbody tr').each(function() {

      var newRowContent="";

      var no_of_quantity=$(this).find("td:eq(1)").find('input').val();
      var item_name=$(this).find("td:eq(2)").text();
      var item_id=$(this).find("td:eq(2)").attr('id');
      item_id=item_id.split('_');
      var rate_per_quantity=$(this).find("td:eq(3)").find('input').val();
      var sub_total=$(this).find("td:eq(4)").text();
    newRowContent='<tr><td>'+(item_name)+'</td><td>'+(no_of_quantity)+'</td></tr>';
      $("#item_add_body").append(newRowContent);  
  });

    // $('#totel_list_item').val('');
    // $('#totel_list_item').val(item_array);
    var special_instructions=$("#special_instructions").val();
    $("#special_instruction_comment").html(special_instructions);

    $("#show_kot").modal('show');
    }
 
});

$( "#add_special_instruction" ).click(function() {
    $("#add_special_instruction_modal").modal('show');
});

$(document).on('click', '#submit_kot', function(e){ 
    var item_array_new=[];
    var room_number=$("#room_number_id").val();
    var guest_name=$("#guest_name_id").val();
    var mobile=$("#mobile_id").val();
    var steward=$("#steward_id").val();
    var booking_id=$("#bookingid").val();
    var table_no=$("#table_no_id").val();
    var room_service=$(".btn.btn-inverse.room_service").attr('id');
    var special_instructions=$("#special_instructions").val();
    var not_chargeable_class=$('#not_chargeable').attr('class');
    if(not_chargeable_class =='btn btn-default')
    {  var not_chargeable=0; } else {var not_chargeable=1; }
    var item_total_price=$('#total_item_price_hidden').val();
    
    $('#item_add tbody tr').each(function() {
      var no_of_quantity=$(this).find("td:eq(1)").find('input').val();
      var item_name=$(this).find("td:eq(2)").text();
      var item_id=$(this).find("td:eq(2)").attr('id');
      item_id=item_id.split('_');
      var rate_per_quantity=$(this).find("td:eq(3)").find('input').val();
      var sub_total=$(this).find("td:eq(4)").text();
      var add_vat=$(this).find("td:eq(5)").find('input').val();
      var alcholoc_beverage=$(this).find("td:eq(0)").find('input').val();
      item_array_new.push({'no_of_quantity' :no_of_quantity,'item_id':item_id[3],'item_name' :item_name, 'rate_per_quantity' :rate_per_quantity,'sub_total' :sub_total,'add_vat':add_vat,'alcholoc_beverage':alcholoc_beverage,});
      //total_array_merge.push(total_array); 
  });
    var url = window.location;
    var base_url = url.protocol + "//" + url.host + "/" + url.pathname.split('/')[1];
         

    $.ajax({
        type: 'POST',
        url: base_url+"/Pos_pos/additem",
        data: { room_number:room_number,guest_name:guest_name,mobile:mobile,steward:steward,booking_id:booking_id,table_no:table_no, item_array:item_array_new,room_service:room_service,special_instruction:special_instructions,not_chargeable:not_chargeable,item_total_price:item_total_price},
    });
         // $.ajax({
         //     url:  base_url+"/Pos_pos/additem",
         //     type: "POST",
         //     data: { room_number:room_number,guest_name:guest_name,mobile:mobile,steward:steward,booking_id:booking_id,table_no:table_no, item_array:item_array_new,room_service:room_service,special_instruction:special_instructions,not_chargeable:not_chargeable,item_total_price:item_total_price},
         //     //dataType: 'json',
         //     success: function (data) { window.location.href=base_url+'/Pos_pos/kot_design/'+data; //alert(data);///window.location.reload(); //alert(data); $("html").html(data);  
         //     },error: function (xhr, ajaxOptions, thrownError) {alert("ERROR:" + xhr.responseText+" - "+thrownError);}
         //  });
   
});

$(document).on('keypress', '#mobile_id', function(e){  
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        $('#mobile_error').html("Digits Only").show().fadeOut("slow");
        return false;
    }    
});
      

$("#guest_name_id").keypress(function (e){
            var code =e.keyCode || e.which;
            if((code<65 || code>90)&&(code<97 || code>122)&&code!=32&&code!=46){
               // swal("", "Only alphabates are allowed.", "error");
               $('#guest_error').html("Only alphabates are allowed").show().fadeOut("slow");
                return false;
            }
        });     
$(document).on('keydown', '#guest_name_id', function(e){ 
            if (e.which === 32 &&  e.target.selectionStart === 0){
             $('#guest_error').html("First character cannot be blank!").show().fadeOut("slow");         
                return false;             
            } 
});

$(document).on('change', '#room_number_id', function(){
    var roomno=$(this).val();
    
    if(roomno !="")
    {
    var allmenu_by_category_array= today_occupancy;
    var item_value_array_filter=getObjects(allmenu_by_category_array, 'rooms',roomno); 
    var allmenu_by_category_array=JSON.stringify(item_value_array_filter);

    $.each(item_value_array_filter, function (index, valueobj) {
    $("#guest_name_id").val(valueobj['guest_name']);
    $("#mobile_id").val(valueobj['guest_phone']);
    $("#bookingid").val(valueobj['booking_id']);
    });  
    }
    else
    {
    $("#guest_name_id").val('');
    $("#mobile_id").val('');
    $("#bookingid").val('');    
    } 
});
$(document).on('click', '#not_chargeable', function(){
    var not_chargeable_class=$(this).attr('class');
    var item_total_price=$('#total_item_price_hidden').val();
    if(not_chargeable_class=='btn btn-default')
    { 
    $(this).removeClass('btn btn-default').addClass( "btn btn-danger btn-default" );
    $("#total_item_price").html('<input type="hidden" id="total_item_price_hidden" value="'+parseFloat(item_total_price)+'"><b>Amount:</b> '+parseFloat(0));
    }
    else
    {
    $(this).removeClass('btn btn-danger btn-default').addClass( "btn btn-default" );
    $("#total_item_price").html('<input type="hidden" id="total_item_price_hidden" value="'+parseFloat(item_total_price)+'"><b>Amount:</b> '+parseFloat(item_total_price));
    }
    var total_item_price=$("#total_item_price_hidden").val();
});
function total()
{
    var item_total=0;
    var item_total_price=0.0;
    var sub_total=0;
    var rowCount = $('#item_add tbody tr').length;
    if(rowCount  == 1  || rowCount > 0 )
    {
    $("#special_instruction_not_chargeable_div").show();
    $('#submit_order_div').show();
    }
    if( rowCount == 0)
    {
    $("#special_instruction_not_chargeable_div").hide();
    $('#submit_order_div').hide();
    }
    for( var i=1;i <= rowCount;i++ ){
    // var id=$(this).attr('id');
    // var idsplit=id.split('_');
    
    // var menu_id=idsplit[3];
    var no_of_quantity=$("#no_of_quantity_"+i).val();
    var rate_per_quantity=$("#rate_per_quantity_"+i).val();
    sub_total=(parseInt(no_of_quantity)*parseFloat(rate_per_quantity).toFixed(2));
    $("#sub_total_"+i).html(parseFloat(sub_total).toFixed(2));
    item_total=item_total+parseInt(no_of_quantity);
    item_total_price=parseFloat(item_total_price)+(parseFloat(sub_total));

     }
    var not_chargeable_class=$('#not_chargeable').attr('class');
    if(not_chargeable_class =='btn btn-default')
    {  var total=item_total_price; } else {var total=0; }

    $("#total_item").html('<b>Items:</b> '+parseInt(rowCount));
    $("#total_item_price").html('<input type="hidden" id="total_item_price_hidden" value="'+parseFloat(item_total_price)+'"><b>Amount:</b> '+parseFloat(total));
}
function get_menu_div(menu_id)
{

    if ( $('#menu_item_id_'+menu_id).length){
    var index_value=$("#menu_item_id_"+menu_id).parents("tr").find("td:first").text();
    var no_of_quantity=$("#no_of_quantity_"+index_value).val();
    $("#no_of_quantity_"+index_value).val(parseInt(no_of_quantity)+1); 
    window.setTimeout(function(){$('#no_of_quantity_'+index_value).select();}, 0);
    } 
    else {
    var allmenu_by_category_array= allmenu_by_category;
    var item_value_array_filter=getObjects(allmenu_by_category_array, 'menu_item_id',menu_id); 
    var allmenu_by_category_array=JSON.stringify(item_value_array_filter);
    var rowCount = $('#item_add tbody tr').length;
    var rowvalue=parseInt(rowCount)+1;
    $.each(item_value_array_filter, function (index, valueobj) {
    var newRowContent='<tr ><td><input type="hidden" id="alcholoc_beverage_'+rowvalue+'" value="'+valueobj['alcholoc_beverage']+'">'+(rowvalue)+'</td><td><input type="text" class="form-control item_class" id="no_of_quantity_'+rowvalue+'" value="1"></td><td id="menu_item_id_'+menu_id+'">'+valueobj['menu_name']+'</td><td><input type="text" class="form-control item_class" id="rate_per_quantity_'+rowvalue+'" value="'+valueobj['selling_price']+'"></td><td id="sub_total_'+rowvalue+'">'+valueobj['selling_price']+'</td><td><input type="hidden" id="add_vat_'+rowvalue+'" value="'+valueobj['add_vat']+'"><button class="btn btn-icon btn-danger m-b-5" id="cross_button"> <i class="fa fa-remove"></i> </button></td></tr>';
    $("#item_add tbody").append(newRowContent);
    });  window.setTimeout(function(){$('#no_of_quantity_'+rowvalue).select();}, 0); }
    total();
}

});
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}