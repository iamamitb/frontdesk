$(document).ready(function(){
		//var days_in_month=$('#days_in_month').val();
		var count=1;
		$("#total_daily_availibility td").each(function(){
			$(this).text($("td.initcell[day='"+count+"']").length);
			count ++;
		});
		
		$(".room_name").each(function(){
			var room_name=$(this).attr('id');
			var count1=1;
			$("#"+room_name+" td").each(function(){
				$(this).text($("."+room_name+" td.initcell[day='"+count1+"']").length);
				count1 ++;
			});
		});
		/*var count2=1;
			$("#non-ac td").each(function(){
			$(this).text($(".non-ac td.initcell[day='"+count2+"']").length);
			count2 ++;
		});*/
	//alert($("td.initcell[day='22']").length);
	
	$("#hotel_id option:first").text("Select Hotel Name");
	
	$('.info').css('cursor', 'pointer');
	$( ".info" ).draggable({axis: 'x', distance: 50});	
	$("#hotel_id").change(function() {		
		$( "#my_from" ).submit();
	});
	$(document).on('click', '.cell', function() {
		var base_url = $.cookie("base_url");
		var testStr=$(this).attr('class');
		var booking_id=$(this).attr('data-title');
		//alert(booking_id);
			if(testStr.contains("maintenance")){
				swal({
			 title: "Information",
			 text: "This room under maintenance you cant make any reservation",
			 timer: 3000,
			 showConfirmButton: false,
			 type: "info",
           }); 				
				}
			else if(typeof booking_id != "undefined") {
				$('#customer-info-modal').modal('show');
				//var booking_id=$(this).attr('id');
				$.ajax({
				url: base_url+"booking_calendar/getGuestInfo",
				type: 'POST',
				dataType: "json",
				data: {'booking_id': booking_id},  
		
			success: function(msg) {
			$('#customer-info-modal').html(msg.item);
			//$('#customer-info-modal').modal('hide');
			}
			});
		}
    });
	
	
		$(document).on('click', '.sa-alert', function() {
			 swal("Error !", "Please select a room first", "error");
		});
	});
/* Below code will allow to poup a message in case no hotels found   */
var hotel_count = parseInt($("#no_of_hotels_hidden").val());

if (hotel_count==0 || hotel_count=='NaN' || hotel_count=="")
{
swal({title: "Information",
        text: "No hotels added yet. Please ensure hotels are added. Select the hotel first and then add rooms.",
        type: "info",
        closeOnConfirm: false,
        },
        function(){
        window.location = "hotel";
                  });    
    
}

var room_count = parseInt($("#no_of_room").val());
var name =$("[name='hotel_id'] option:selected").text();
if (room_count==0 || room_count=='NaN' || room_count=="")
{
swal({title: "Information",
        text: "No Room added yet. Please ensure Room are added in "+name+" hotel.",
        type: "info",
        closeOnConfirm: false,
        },
        function(){
        window.location = "room";
                  });    
       
    
    
}