$(document).ready(function(){ 
$('.bill_payment_details').click(function()
{
    var id=$(this).attr('id');
    var pos_bill_id=id.split('_');
    $('#hid_pos_bill').val(0);
    $('#hid_pos_bill').val(pos_bill_id[1]);
    var allpos_master_order= allpos_master_order_new;
    var item_value_array_filter=getObjects(allpos_master_order, 'pos_bill',pos_bill_id[1]);
    var allmenu_by_category_array=JSON.stringify(item_value_array_filter);
    $.each(item_value_array_filter, function (index, valueobj) {
    var total_tax=valueobj['pos_grand_total_vat']+valueobj['pos_grand_total_service_tax']+valueobj['pos_grand_total_service_charge'];
    total_tax=parseFloat(total_tax).toFixed(2);
    $("#bill_payment_title").html('Bill No '+valueobj['pos_bill']+' - Enter Payment Details');
    $('#bill_guest_details').html('<b>Guest name:</b> '+valueobj['pos_guest_name']);
    $('#total_payable').html('<b>Total Amount:</b> Rs. '+valueobj['pos_grand_total']+'  <b>Total Tax:</b> Rs. '+parseFloat(total_tax));
    $('#pending_amount').html('<b>Pending Amount:</b> Rs. '+(valueobj['pos_grand_total']+parseFloat(total_tax)));
    $('#pending_amount_id').val((valueobj['pos_grand_total']+parseFloat(total_tax)));

   });  
    //item_add alltax_new
    // var allpos_order= allpos_order_new;
    // var alltax=alltax_new;
    // var allpos_order_filter=getObjects(allpos_order, 'pos_bill',pos_bill_id[1]);
    // var allpos_order_filter_array=JSON.stringify(allpos_order_filter);
    // var rowvalue=1;
    // var grand_total=0;
    // $("#item_add tbody").html('');
    // $.each(allpos_order_filter, function (index, valueobj) {
    // var vat_amount=0;
    // var service_tax=0;
    // var service_charge=0;
    // var sub_total=0.0;
    // var total_tax=0;
    // // if(valueobj['add_vat']=='Yes')
    // // {
    // //    if( valueobj['alcholoc_beverage']=='1')
    // //   {
    // //     vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
    // //   }
    // //   else
    // //   {
    // //    vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
    // //   }
    // // }
    // vat_amount=(valueobj['pos_total_vat']); 
    // service_tax=(valueobj['pos_total_service_tax']);
    // service_charge=(valueobj['pos_total_service_charge']);
    // var total_tax=(parseFloat(vat_amount)+parseFloat(service_tax)+parseFloat(service_charge));
    // sub_total=parseInt(valueobj['pos_order_total_price'])+(parseFloat(total_tax));
    // grand_total=grand_total+parseInt(sub_total);
    // var newRowContent='<tr ><td>'+(rowvalue)+'</tparseFloat(vat_amount).toFixed(2)+parseFloat(vat_amount).toFixed(2)+parseFloat(vat_amount).toFixed(2);d><td>'+valueobj['menu_name']+'</td><td>'+valueobj['pos_order_total_price']+'</td><td>'+parseFloat(vat_amount).toFixed(2)+'</td><td>'+parseFloat(service_tax).toFixed(2)+'</td><td>'+parseFloat(service_charge).toFixed(2)+'</td><td>'+parseFloat(sub_total).toFixed(2)+'</td></tr>';
    // $("#item_add tbody").append(newRowContent); rowvalue++;
    // });
    // $("#total_item_price").html('<input type="hidden" id="total_item_price_hidden" value="'+parseFloat(grand_total)+'"><b>Amount:</b> '+parseFloat(grand_total));
    // //alert(alltax_new[0]['pos_bill_alcholic_beverages_vat']);  
    if(pos_bill_id[0]==2 || pos_bill_id[0]==3) { $('#spot_settle_show').show();   $('.row.all_amount').show();
    $('#settle_bill_button').show();  $('#spot_settle').prop('checked', true); 
    $('#room_number_show').hide();
    $('#guest_details_show').hide();
    $('#post_to_room_button').hide(); 
    }else{
    $('#spot_settle_show').hide(); 
    $('#room_number_show').hide();
    $('#guest_details_show').hide();
    $('#post_to_room_button').hide();
     $('.row.all_amount').show();
    $('#settle_bill_button').show();   
    }
    $('#amount_0').val('');
    $('#add_payment_div').html('');
    $('#amount_error_0').html('');
    $('#num_div').val('1');
    $('#payment_details_modal').modal('show');
});

$(document).on('click', '#add_payment', function(){
var num_div=$('#num_div').val();
$('#add_payment_div').append('<div class="row" id="main_'+parseInt(num_div)+'"><div class="col-md-12"><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Amount</label><input type="text" class="form-control advance_amount" id="amount_'+parseInt(num_div)+'" placeholder=""><p id="amount_error_'+parseInt(num_div)+'" style="color:red;"></p></div></div><div class="col-md-4"><div class="form-group"><label for="exampleInputEmail1">Payment Type</label><select class="form-control" id="payment_type_'+parseInt(num_div)+'"><option>Cash</option><option>Debit Card</option><option>Credit Card</option><option>Netbanking</option></select></div></div><div class="col-md-4"><br><button type="button" class="btn btn-success" id="add_payment">+</button><button type="button" class="btn btn-danger" id="minus_payment">-</button></div></div></div>');
num_div++;
$('#num_div').val(parseInt(num_div));
});

$(document).on('keyup', '.advance_amount', function(){
var intRegex = /^\d+$/;
var num_div=$('#num_div').val();
var grand_total=0;
var due_total=0;
var pending_amount=$('#pending_amount_id').val();
for (var i=0;i < parseInt(num_div); i++)
{
    var amount=$('#amount_'+i).val();
    if(!intRegex.test(amount)) {
    $('#amount_'+i).val(0);
    }
    if(amount==""){amount=0;}
    grand_total=grand_total+parseFloat(amount);
    if(grand_total > pending_amount){ $('#amount_'+i).val('0'); grand_total=grand_total-amount;}

}

due_total=pending_amount-grand_total;
$('#pending_amount').html('<b>Pending Amount:</b> Rs. '+(parseFloat(due_total).toFixed(2)));
});

$(document).on('click', '#minus_payment', function(){ 
var num_div=$('#num_div').val();
$('#main_'+(parseInt(num_div)-1)).remove();
num_div--;
$('#num_div').val(parseInt(num_div));
});

$(document).on('change', '#room_number_id', function(){
    var roomno=$(this).val();

    if(roomno !="")
    {
    var allmenu_by_category_array= today_occupancy;
    var item_value_array_filter=getObjects(allmenu_by_category_array, 'rooms',roomno); 
    var allmenu_by_category_array=JSON.stringify(item_value_array_filter);

    $.each(item_value_array_filter, function (index, valueobj) {
    $("#guest_name_id").html(valueobj['guest_name']);
    $("#room_number").html(roomno);
    $("#bookingdate").html(valueobj['booking_date']);
    });  
    }
    else
    {
    $("#guest_name_id").html('');
    $("#room_number").html('');
    $("#bookingdate").html('');   
    } 
});
$(".spot_settle_post_to_room").click(function(){
//$(document).on('click', '.spot_settle_post_to_room', function(){
    var id=$(this).attr('id');
    //alert(id);
    if(id=='spot_settle')
    {
    $('.row.all_amount').show();
    $('#settle_bill_button').show();
    $('#room_number_show').hide();
    $('#guest_details_show').hide();
    $('#post_to_room_button').hide();
    }
    else
    {
    $('.row.all_amount').hide();
    $('#settle_bill_button').hide();
    $('#room_number_show').show();
    $('#guest_details_show').show();
    $('#post_to_room_button').show(); 
    }

});
$(document).on('click', '.btn.btn-success.submit_payment', function(){
    var id=$(this).attr('id');
    var payment_array = [];
    var total_advance=0;
    var pending_amount=$('#pending_amount_id').val();
    var hid_pos_bill=$('#hid_pos_bill').val();
    var status=true;
    if(id=='settle_bill_button')
    {
     var num_div=$('#num_div').val();
     for(var i=0; i < parseInt(num_div); i++)
        {
        var amount=$('#amount_'+i).val();
        if(amount ==""  || amount == 0) { $('#amount_error_'+i).html('Invalid Input'); status=false;} else{$('#amount_error_'+i).html(''); status=true;}
        var payment_type=$('#payment_type_'+i).val(); 
        payment_array.push({'amount' :amount,'payment_type':payment_type});
        total_advance=total_advance+parseFloat(amount);
    }
     
    if(status == true){ 
    var url = window.location;
    var base_url = url.protocol + "//" + url.host + "/" + url.pathname.split('/')[1];
         $.ajax({
             url:  base_url+"/Pos_bill/addtransaction",
             type: "POST",
             data: { payment_array:payment_array,total_advance:total_advance,hid_pos_bill:hid_pos_bill,payment_mode:id },
             //dataType: 'json',
             success: function (data) {  window.location.reload();//salert(data);///window.location.reload(); //alert(data); $("html").html(data);  
             },error: function (xhr, ajaxOptions, thrownError) {alert("ERROR:" + xhr.responseText+" - "+thrownError);}
          });
        } 
    }
    else
    {
     var roomno=$('#room_number_id').val();
     alert(roomno);
    }

});
$(document).on('change', '#table_no_id', function(){
    var pos_table_id=$(this).val();
    var grand_total=0;
    var vat_total_amount=0;
    var sub_total=0;
    var allpos_order= allpos_order_new;
    var alltax=alltax_new;
    var allpos_order_filter=getObjects(allpos_order, 'pos_table_id',pos_table_id);
    var allpos_order_filter1=getObjects(allpos_order_filter,'pos_payment_status','pending');
    var allpos_order_filter_array=JSON.stringify(allpos_order_filter1);
    if( pos_table_id !=""  && allpos_order_filter1 !="")
    {
    $("#table_dine_in tbody").html('');  

    
    $.each(allpos_order_filter1, function (index, valueobj) {
    $("#dine_in_guest_name").val('');
    $("#dine_in_guest_name").val(valueobj['pos_guest_name']);
    $("#dine_in_mobile_no").val('');
    $("#dine_in_mobile_no").val(valueobj['pos_mobile']);
    $("#dine_in_bill_no").html('');
    $("#dine_in_bill_no").html(valueobj['pos_bill']);
    $("#dine_in_service_tax").html('');
    $("#dine_in_service_tax").html(alltax_new[0]['pos_bill_service_tax_value']);
    $("#dine_in_service_charge").html('');
    $("#dine_in_service_charge").html(alltax_new[0]['pos_bill_service_charge_value']);

    

    var vat_percentage=0;

    var vat_amount=0;
    if(valueobj['add_vat']=='Yes')
    {
       if( valueobj['alcholoc_beverage']=='1')
      {
        vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
        vat_percentage=alltax_new[0]['pos_bill_alcholic_beverages_vat'];
      }
      else
      {
       vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
       vat_percentage=alltax_new[0]['pos_bill_vat_number_value'];
      }
    }
    vat_total_amount=parseFloat(vat_total_amount)+parseFloat(vat_amount);  
    sub_total=parseFloat(sub_total)+parseFloat(valueobj['pos_order_total_price']);
    var rowvalue=1;
    var newRowContent='<tr ><td>'+(rowvalue)+'</td><td>'+valueobj['pos_id']+'</td><td>'+valueobj['menu_name']+'</td><td>'+valueobj['pos_order_rate']+'</td><td>'+valueobj['pos_order_quantity']+'</td><td>'+vat_percentage+'</td><td>'+parseFloat(vat_amount).toFixed(2)+'</td><td>'+parseFloat(valueobj['pos_order_total_price']).toFixed(2)+'</td></tr>';
    $("#table_dine_in tbody").append(newRowContent); rowvalue++
    });
    var total_sevice_tax=(sub_total*alltax_new[0]['pos_bill_service_tax_value'])/100;
    var total_sevice_charge=(sub_total*alltax_new[0]['pos_bill_service_charge_value'])/100;
    var total_tax=(parseFloat(vat_total_amount)+parseFloat(total_sevice_tax)+parseFloat(total_sevice_charge));
    var grand_total=parseInt(sub_total)+parseFloat(total_tax);
    $('#dine_in_total_payable').html(grand_total);
    }
    else
    {
    $("#table_dine_in tbody").html(''); 
    $("#dine_in_guest_name").val('');
    $("#dine_in_mobile_no").val('');
    $("#dine_in_bill_no").html('');
    $("#dine_in_service_tax").html('');
    $("#dine_in_service_charge").html('');
    $('#dine_in_total_payable').html('');
    }// // if(valueobj['add_vat']=='Yes')
    // // {
    // //    if( valueobj['alcholoc_beverage']=='1')
    // //   {
    // //     vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
    // //   }
    // //   else
    // //   {
    // //    vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
    // //   }
    // // }
    // vat_amount=(valueobj['pos_total_vat']);
                                  
    //alert(allpos_order_filter_array); table_dine_in
    // var rowvalue=1;
    // var grand_total=0;
    // $("#item_add tbody").html('');
    // $.each(allpos_order_filter, function (index, valueobj) {
    // var vat_amount=0;
    // var service_tax=0;
    // var service_charge=0;
    // var sub_total=0.0;
    // var total_tax=0;
    // // if(valueobj['add_vat']=='Yes')
    // // {
    // //    if( valueobj['alcholoc_beverage']=='1')
    // //   {
    // //     vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
    // //   }
    // //   else
    // //   {
    // //    vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
    // //   }
    // // }
    // vat_amount=(valueobj['pos_total_vat']); 
    // service_tax=(valueobj['pos_total_service_tax']);
    // service_charge=(valueobj['pos_total_service_charge']);
    // var total_tax=(parseFloat(vat_amount)+parseFloat(service_tax)+parseFloat(service_charge));
    // sub_total=parseInt(valueobj['pos_order_total_price'])+(parseFloat(total_tax));
    // grand_total=grand_total+parseInt(sub_total);
    // var newRowContent='<tr ><td>'+(rowvalue)+'</tparseFloat(vat_amount).toFixed(2)+parseFloat(vat_amount).toFixed(2)+parseFloat(vat_amount).toFixed(2);d><td>'+valueobj['menu_name']+'</td><td>'+valueobj['pos_order_total_price']+'</td><td>'+parseFloat(vat_amount).toFixed(2)+'</td><td>'+parseFloat(service_tax).toFixed(2)+'</td><td>'+parseFloat(service_charge).toFixed(2)+'</td><td>'+parseFloat(sub_total).toFixed(2)+'</td></tr>';
    // $("#item_add tbody").append(newRowContent); rowvalue++;
    // });
    // $("#total_item_price").html('<input type="hidden" id="total_item_price_hidden" value="'+parseFloat(grand_total)+'"><b>Amount:</b> '+parseFloat(grand_total));
    // //alert(alltax_new[0]['pos_bill_alcholic_beverages_vat']);  
}); 

$(document).on('keyup', '#get_bill_by_token_number', function(){
    var token_number=$(this).val();
    get_bill_information_by_token(token_number);
    
});
$(document).on('click', '#get_bill_by_token_number_span', function(){
    var token_number=$(this).html();
    get_bill_information_by_token(token_number);
    
});

$(document).on('change', '#room_service_room_number_id', function(){
    var pos_room_number=$(this).val();
    var grand_total=0;
    var vat_total_amount=0;
    var sub_total=0;
    var allpos_order= allpos_order_new;
    var alltax=alltax_new;
    var allpos_order_filter=getObjects(allpos_order, 'pos_room_no',pos_room_number);
    var allpos_order_filter1=getObjects(allpos_order_filter,'pos_payment_status','pending');
    var allpos_order_filter_array=JSON.stringify(allpos_order_filter1);
    if( pos_room_number !=""  && allpos_order_filter1 !="")
    {
    $("#table_room_service tbody").html('');  
    $.each(allpos_order_filter1, function (index, valueobj) {
    $("#room_service_guest_name").val('');
    $("#room_service_guest_name").val(valueobj['pos_guest_name']);
    $("#room_service_mobile_no").val('');
    $("#room_service_mobile_no").val(valueobj['pos_mobile']);
    $("#room_service_bill_no").html('');
    $("#room_service_bill_no").html(valueobj['pos_bill']);
    $("#dine_in_service_tax").html('');
    $("#room_service_service_tax").html(alltax_new[0]['pos_bill_service_tax_value']);
    $("#room_service_service_charge").html('');
    $("#room_service_service_charge").html(alltax_new[0]['pos_bill_service_charge_value']);

    

    var vat_percentage=0;

    var vat_amount=0;
    if(valueobj['add_vat']=='Yes')
    {
       if( valueobj['alcholoc_beverage']=='1')
      {
        vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
        vat_percentage=alltax_new[0]['pos_bill_alcholic_beverages_vat'];
      }
      else
      {
       vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
       vat_percentage=alltax_new[0]['pos_bill_vat_number_value'];
      }
    }
    vat_total_amount=parseFloat(vat_total_amount)+parseFloat(vat_amount);  
    sub_total=parseFloat(sub_total)+parseFloat(valueobj['pos_order_total_price']);
    var rowvalue=1;
    var newRowContent='<tr ><td>'+(rowvalue)+'</td><td>'+valueobj['pos_id']+'</td><td>'+valueobj['menu_name']+'</td><td>'+valueobj['pos_order_rate']+'</td><td>'+valueobj['pos_order_quantity']+'</td><td>'+vat_percentage+'</td><td>'+parseFloat(vat_amount).toFixed(2)+'</td><td>'+parseFloat(valueobj['pos_order_total_price']).toFixed(2)+'</td></tr>';
    $("#table_room_service tbody").append(newRowContent); rowvalue++
    });
    var total_sevice_tax=(sub_total*alltax_new[0]['pos_bill_service_tax_value'])/100;
    var total_sevice_charge=(sub_total*alltax_new[0]['pos_bill_service_charge_value'])/100;
    var total_tax=(parseFloat(vat_total_amount)+parseFloat(total_sevice_tax)+parseFloat(total_sevice_charge));
    var grand_total=parseInt(sub_total)+parseFloat(total_tax);
    $('#room_service_total_payable').html(grand_total);
    }
    else
    {
    $("#table_room_service tbody").html(''); 
    $("#room_service_guest_name").val('');
    $("#room_service_mobile_no").val('');
    $("#room_service_bill_no").html('');
    $("#room_service_service_tax").html('');
    $("#room_service_service_charge").html('');
    $('#room_service_total_payable').html('');
    }
  
});

});
function  get_bill_information_by_token(token_number){
    var alltoken_number=alltoken_number_new;
    var allpos_order= allpos_order_new;
    var alltax=alltax_new;
    var allpos_token_filter=getObjects(alltoken_number, 'token_number',token_number);
    if(allpos_token_filter !="")
    {
    var allpos_token_no_filter=getObjects(allpos_order, 'token_number',token_number);
    var allpos_token_no_filter1=getObjects(allpos_token_no_filter,'pos_payment_status','pending');
    var allpos_order_filter_array=JSON.stringify(allpos_token_no_filter1);

    var grand_total=0;
    var vat_total_amount=0;
    var sub_total=0;
    
    $("#table_take_away tbody").html(''); 
    $.each(allpos_token_no_filter1, function (index, valueobj) {
    $("#take_away_bill_no").html('');
    $("#take_away_bill_no").html(valueobj['pos_bill']);
    $("#take_away_service_tax").html('');
    $("#take_away_service_tax").html(alltax_new[0]['pos_bill_service_tax_value']);
    $("#take_away_service_charge").html('');
    $("#take_away_service_charge").html(alltax_new[0]['pos_bill_service_charge_value']);
    var vat_percentage=0;
    var vat_amount=0;
    if(valueobj['add_vat']=='Yes')
    {
       if( valueobj['alcholoc_beverage']=='1')
      {
        vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_alcholic_beverages_vat'])/100;
        vat_percentage=alltax_new[0]['pos_bill_alcholic_beverages_vat'];
      }
      else
      {
       vat_amount=(valueobj['pos_order_total_price']*alltax_new[0]['pos_bill_vat_number_value'])/100; 
       vat_percentage=alltax_new[0]['pos_bill_vat_number_value'];
      }
    }
    vat_total_amount=parseFloat(vat_total_amount)+parseFloat(vat_amount);  
    sub_total=parseFloat(sub_total)+parseFloat(valueobj['pos_order_total_price']);
    var rowvalue=1;
    var newRowContent='<tr ><td>'+(rowvalue)+'</td><td>'+valueobj['pos_id']+'</td><td>'+valueobj['menu_name']+'</td><td>'+valueobj['pos_order_rate']+'</td><td>'+valueobj['pos_order_quantity']+'</td><td>'+vat_percentage+'</td><td>'+parseFloat(vat_amount).toFixed(2)+'</td><td>'+parseFloat(valueobj['pos_order_total_price']).toFixed(2)+'</td></tr>';
    $("#table_take_away tbody").append(newRowContent); rowvalue++
     });
    var total_sevice_tax=(sub_total*alltax_new[0]['pos_bill_service_tax_value'])/100;
    var total_sevice_charge=(sub_total*alltax_new[0]['pos_bill_service_charge_value'])/100;
    var total_tax=(parseFloat(vat_total_amount)+parseFloat(total_sevice_tax)+parseFloat(total_sevice_charge));
    var grand_total=parseInt(sub_total)+parseFloat(total_tax);
    $('#take_away_total_payable').html(grand_total);
    }
    else
    {
    $("#table_take_away tbody").html(''); 
    $("#take_away_bill_no").html('');
    $("#take_away_service_tax").html('');
    $("#take_away_service_charge").html('');
    $('#take_away_total_payable').html('');
    }
}
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}